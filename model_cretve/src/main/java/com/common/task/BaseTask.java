package com.common.task;

import android.app.Dialog;
import android.os.AsyncTask;

import com.common.net.NetResult;

import java.util.HashMap;

/**
 * loginTask
 * 
 * @author wangzy
 * 
 */
public class BaseTask extends  AsyncTask<HashMap<String, String>, Void, NetResult> {

	NetCallBack mCallBack;
	public Dialog dialog;

	public BaseTask(NetCallBack callBack) {
		this.mCallBack = callBack;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		if (null != mCallBack) {
			mCallBack.onPreCall();
			mCallBack.onPreCall(this);
		}
	}

	@Override
	protected NetResult doInBackground(HashMap<String, String>... params) {
		if (null != mCallBack) {
			if(null==params){
				return	mCallBack.onDoInBack(null,this);
			}else{
				HashMap<String,String> paramMap=(HashMap<String, String>)params[0];
				NetResult result = mCallBack.onDoInBack(paramMap,this);
				return	result;
			}
		}
		return null;
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
		if (null != mCallBack) {
			mCallBack.onCanCell();
			mCallBack.onCanCell(this);
		}
	}

	@Override
	protected void onPostExecute(NetResult result) {
		super.onPostExecute(result);
		if (null != mCallBack) {
			mCallBack.onFinish(result);
			mCallBack.onFinish(result,this);
		}
	}
}

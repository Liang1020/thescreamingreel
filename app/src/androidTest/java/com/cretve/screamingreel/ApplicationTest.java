package com.cretve.screamingreel;

import android.os.Environment;
import android.test.AndroidTestCase;

import com.common.util.AESTool;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.cretve.aesencript.SecurityUtil;
import com.yink.sardar.CompressListener;
import com.yink.sardar.Compressor;
import com.yink.sardar.InitListener;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends AndroidTestCase {



    public void testAesScriptTool(){

        String id="sWeRpvlFjH5hzkmJ62Y0K4iT-MdYXbMMI";
        String key="wlV99B1serRPIFyLoelHazSw";

        String enId=SecurityUtil.encode(id,2086496266);
        LogUtil.i(App.tag,enId);
        String enKey=SecurityUtil.encode(key,2086496266);
        LogUtil.i(App.tag,enKey);

    }


    public void testen() throws Exception {

        String src="welcome";
        String key = "1234567890abcDEF";

        String dec=AESTool.Decrypt(src,key);
        LogUtil.i(App.tag,"dec:"+dec);
        String en=AESTool.Encrypt(dec,key);

        LogUtil.i(App.tag,"dec:"+en);


    }

    public void testGetAddres() throws IOException {

//        LatLng sydney = new LatLng(-34, 151);
//        List<Address> adres = Tool.getLocationFromLatlng(getContext(), sydney, 1);
//        LogUtil.i(App.tag, "find " + adres.size());

//        Instrumentation instrumentation=new Instrumentation();


        Locale local = Locale.getDefault();

        LogUtil.i(App.tag,"local:"+local.getCountry()+" "+local.getDisplayCountry()+" "+local.getISO3Country());

    }


    public void testCompressVideo() {
//        String src, String dst, final CompressListener compressListener

//      cmd = "-y -i /mnt/sdcard/videokit/in1.mp4 -strict -2 -vcodec libx264 -preset ultrafast -crf 24 -acodec aac -ar 44100 -ac 2 -b:a 96k -s 640x352 -aspect 16:9 /mnt/sdcard/videokit/out8.mp4";
        //ffmpe ffmpeg命令


        String src = "/sdcard/test.mp4";
        String dst = "/sdcard/test.mp4compres.mp4";
        final String cmd = "-y -i " + src + " -strict -2 -vcodec h264 -preset ultrafast -crf 24 -acodec aac -ar 44100 -ac 2 -b:a 96k -s 480x360 -aspect 16:9 " + dst;
        final Compressor com = new Compressor(getContext());
        com.loadBinary(new InitListener() {
            @Override
            public void onLoadSuccess() {

                com.execCommand(cmd, new CompressListener() {
                    @Override
                    public void onExecSuccess(String message) {
                        LogUtil.i(App.tag, "compres success!");
                    }

                    @Override
                    public void onExecFail(String reason) {
                        LogUtil.i(App.tag, "error:" + reason);

                    }

                    @Override
                    public void onExecProgress(String message) {
                        LogUtil.i(App.tag, "error:" + message);
                    }
                });
            }

            @Override
            public void onLoadFail(String reason) {
                LogUtil.e(App.tag, "ffmpeg init error:" + reason);
            }
        });
    }

    public void testFormatTime() {

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("E");


    }

    public void testDownloadPath() {

        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
        String fname = Tool.getFileName("http://www.nt.gov.au/d/Fisheries/Content/File/recreational/know-your-limits-booklet.pdf");

        LogUtil.i(App.tag, "path:" + fname);
    }

    public void testGetSignature() {
//        String signature = Tool.getSignature(getContext().getPackageName(), getContext());
//        LogUtil.i(App.tag, "signature:" + signature);
//

//        String test="不和谐";
//
//        String reg="^(?!.*(test)).*$";//用到了前瞻
////        LogUtil.i(App.tag,"x "+"不管信不信,反正现在很不合谐".matches(reg));//false不通过
////        LogUtil.i(App.tag,"x "+"不管信不信,反正现在非常合谐".matches(reg));//true通过
////        LogUtil.i(App.tag,"x "+"不合谐在某国是普遍存在的".matches(reg));//false不通过
//
//
//        LogUtil.i(App.tag,"result:"+"不和谐社会".matches(reg));
//        LogUtil.i(App.tag,"result:"+"不谐社会".matches(reg));


        String str = "Hello World";  //待判断的字符串
        String reg = ".*操.*";  //判断字符串中是否含有ll

        LogUtil.i(App.tag, "result:" + "操x".matches(reg));
        LogUtil.i(App.tag, "result:" + "你大爷".matches(reg));

    }

//    public void testGetSignature() {
//        String pkgname = "com.thescreamingreel.androidapp";
//        try {
//            /** 通过包管理器获得指定包名包含签名的包信息 **/
//
//            PackageManager manager = getContext().getPackageManager();
//            PackageInfo packageInfo = manager.getPackageInfo(pkgname, PackageManager.GET_SIGNATURES);
//            /******* 通过返回的包信息获得签名数组 *******/
//            android.content.pm.Signature[] signatures = packageInfo.signatures;
//
//            StringBuffer sbf = new StringBuffer();
//            /******* 循环遍历签名数组拼接应用签名 *******/
//            for (android.content.pm.Signature signature : signatures) {
//                sbf.append(signature.toString());
//            }
//            /************** 得到应用签名 **************/
//
//        LogUtil.i(App.tag,sbf.toString());
//
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//    }

    public void testCountryList() {

        String[] countryiso = Locale.getISOCountries();
        for (String c : countryiso) {
            LogUtil.i(App.tag, c);
        }

        LogUtil.i(App.tag, "================");
        Locale[] avaliable = Locale.getAvailableLocales();
        LogUtil.i(App.tag, "size1:" + countryiso.length + " " + avaliable.length);
        for (Locale locale : avaliable) {
            String country = locale.getDisplayCountry();
//            if(!StringUtils.isEmpty(country) && !country.contains("(")){
//                LogUtil.i(App.tag,"country:"+country);
//            }
            LogUtil.i(App.tag, "" + locale.getISO3Language() + " country: " + locale.getCountry());
//            LogUtil.i(App.tag,  locale.getDisplayName()+" | "+country+"|"+locale.getCountry()+"|"+locale.getISO3Country());
        }
    }
}
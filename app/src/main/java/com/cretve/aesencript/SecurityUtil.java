package com.cretve.aesencript;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

public class SecurityUtil {
	private static byte[] keyValue;

	private static byte[] iv;

	private static SecretKey key;
	private static AlgorithmParameterSpec paramSpec;
	private static Cipher ecipher;
	private static long h;

	static {
		System.loadLibrary("sec");
	}

	static KeyGenerator kgen;
	private static void init(long h) {
		SecurityUtil.h=h;
		
		keyValue = getKeyValue(h);
		iv = getIv(h);
	
		if (null != keyValue && null != iv && null==kgen) {
			try {
				kgen = KeyGenerator.getInstance("AES");
				kgen.init(128, new SecureRandom(keyValue));
				key = kgen.generateKey();
				paramSpec = new IvParameterSpec(iv);
				ecipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			} catch (NoSuchAlgorithmException e) {
			} catch (NoSuchPaddingException e) {
			}
		}

	}

	public static native byte[] getKeyValue(long h);

	public static native byte[] getIv(long h);

	public static String encode(String msg,long h) {
		
		String str = "";
		try {
			init(h);
			// 用密钥和一组算法参数初始化此 cipher
			ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
			// 加密并转换成16进制字符串
			str = asHex(ecipher.doFinal(msg.getBytes()));
		} catch (Exception e) {

		}
		return str;
	}

	public static String decode(String value,long h) {
		try {
			init(h);
			ecipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
			return new String(ecipher.doFinal(asBin(value)));
		} catch (Exception e) {

		}
		return "";
	}

	private static String asHex(byte buf[]) {
		StringBuffer strbuf = new StringBuffer(buf.length * 2);
		int i;
		for (i = 0; i < buf.length; i++) {
			if (((int) buf[i] & 0xff) < 0x10)// 小于十前面补零
				strbuf.append("0");
			strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
		}
		return strbuf.toString();
	}

	private static byte[] asBin(String src) {
		if (src.length() < 1)
			return null;
		byte[] encrypted = new byte[src.length() / 2];
		for (int i = 0; i < src.length() / 2; i++) {
			int high = Integer.parseInt(src.substring(i * 2, i * 2 + 1), 16);// 取高位字节
			int low = Integer.parseInt(src.substring(i * 2 + 1, i * 2 + 2), 16);// 取低位字节
			encrypted[i] = (byte) (high * 16 + low);
		}
		return encrypted;
	}

}

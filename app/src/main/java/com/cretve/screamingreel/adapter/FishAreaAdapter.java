package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.thescreamingreel.androidapp.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 15/11/30.
 */
public class FishAreaAdapter extends BaseAdapter {

    private List<AVObject> aVObjects;

    private Context context;

    private LayoutInflater layoutInflater;

    public FishAreaAdapter(Context context, List<AVObject> aVObjects) {
        this.context = context;
        this.aVObjects = aVObjects;
        this.layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return aVObjects.size();
    }

    @Override
    public Object getItem(int position) {
        return aVObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_fish_area, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textViewFishAreaInfo.setText(aVObjects.get(position).getString("title"));


        return convertView;
    }


   static class ViewHolder {

        @InjectView(R.id.textViewFishAreaInfo)
        TextView textViewFishAreaInfo;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }


    }

}

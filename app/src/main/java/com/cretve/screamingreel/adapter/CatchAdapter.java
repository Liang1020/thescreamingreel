package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.LevelListDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.common.util.DateTool;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.view.NetImageView;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.activity.FishAreaDetailMapActivity;
import com.cretve.screamingreel.activity.FishAreaMapActivity;
import com.cretve.screamingreel.parse.ParseHelper;
import com.indicator.view.IndicatorView;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 15/11/11.
 */
public class CatchAdapter extends BaseAdapter {

    private List<AVObject> aVObjects;
    private Context context;
    private LayoutInflater layoutInflater;
    private Point point = null;
    private int marginContent;
    private int headerHeight;

    private onItemClickListener onItemClickListener;
    private OnLikeFeollowClickListener onLikeFeollowClickListener;
    private boolean showOwner = false;

    public CatchAdapter(Context context, List<AVObject> aVObjects, boolean showOwner) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.aVObjects = aVObjects;
        this.point = Tool.getDisplayMetrics(context);
        this.marginContent = context.getResources().getDimensionPixelOffset(R.dimen.margin_mid);
        this.headerHeight = context.getResources().getDimensionPixelOffset(R.dimen.header_height);
        this.showOwner = showOwner;
    }

    @Override
    public int getCount() {
        return aVObjects.size();
    }

    @Override
    public Object getItem(int i) {
        return aVObjects.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        int w = point.x;


        ViewHolder viewHolder = null;
        if (null == view) {
            view = layoutInflater.inflate(R.layout.item_catch_list, null);
            viewHolder = new ViewHolder(view);
            int rw = w - marginContent * 2;

//            android.widget.AbsListView.LayoutParams
//                    vlp = new android.widget.AbsListView.LayoutParams(rw, rw + headerHeight);

            android.widget.AbsListView.LayoutParams
                    vlp = new android.widget.AbsListView.LayoutParams(rw, rw);
            view.setLayoutParams(vlp);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.linearLayoutTopOwner.setVisibility(showOwner ? View.VISIBLE : View.INVISIBLE);

        final AVObject pb = aVObjects.get(i);

        int likeCount = pb.getInt("likes_count");

        final AVObject pu = pb.getAVObject("owner");

        if (null != pu) {

            viewHolder.textViewName.setText(String.valueOf("" + pu.getString("first_name") + " " + pu.getString("last_name")));
            AVUser puser = (AVUser) pu;
            AVFile headerFile = puser.getAVFile("avatar");

            if (null != headerFile) {
//                SimpleImgLoaderLru.from(context).displayImage(viewHolder.netImageView, headerFile.getUrl(), R.drawable.icon_header_default, 150, 150);

                Picasso.with(context).load(headerFile.getUrl()).resize(150, 150).placeholder(R.drawable.icon_header_default).config(Bitmap.Config.RGB_565).into(viewHolder.netImageView);

            } else {
                Picasso.with(context).load(R.drawable.icon_header_default).resize(150, 150).config(Bitmap.Config.RGB_565).into(viewHolder.netImageView);
            }

            String likeText = likeCount > 99 ? "99+" : String.valueOf(likeCount);


            if (null != AVUser.getCurrentUser().getList("follow_users")) {

                if (AVUser.getCurrentUser().getList("follow_users").contains(pb.getAVUser("owner"))) {
                    viewHolder.textViewFollow.setText("Followed");
                    viewHolder.textViewFollow.setTextColor(context.getResources().getColor(R.color.gray_text_color));
                } else {
                    viewHolder.textViewFollow.setText("Follow");
                    viewHolder.textViewFollow.setTextColor(context.getResources().getColor(R.color.text_color_ios_blue));
                }
            } else {
                viewHolder.textViewFollow.setText("Follow");
                viewHolder.textViewFollow.setTextColor(context.getResources().getColor(R.color.text_color_ios_blue));
            }


            if (null != AVUser.getCurrentUser().getList("like_catches")) {

                if (AVUser.getCurrentUser().getList("like_catches").contains(pb)) {
                    viewHolder.textViewLikeCount.setText("Liked " + likeText);
                    viewHolder.iconOfLikeInAdaptor.setImageResource(R.drawable.icon_like_2);
                } else {
                    viewHolder.textViewLikeCount.setText("Like " + likeCount);
                    viewHolder.iconOfLikeInAdaptor.setImageResource(R.drawable.icon_like);
                }

            } else {
                viewHolder.textViewLikeCount.setText("Like " + likeCount);
                viewHolder.iconOfLikeInAdaptor.setImageResource(R.drawable.icon_like);
            }


            try {

                if (null != AVUser.getCurrentUser().getList("like_catches") && AVUser.getCurrentUser().getList("like_catches").contains(pb)) {
                    viewHolder.textViewLikeCount.setText("Liked " + likeText);
                    viewHolder.iconOfLikeInAdaptor.setImageResource(R.drawable.icon_like_2);
                } else {
                    viewHolder.textViewLikeCount.setText("Like " + likeCount);
                    viewHolder.iconOfLikeInAdaptor.setImageResource(R.drawable.icon_like);
                }

            } catch (Exception e) {
                e.printStackTrace();
                viewHolder.textViewLikeCount.setText("Like " + likeCount);
                viewHolder.iconOfLikeInAdaptor.setImageResource(R.drawable.icon_like);
            }


            String commentCount = pb.getNumber("comments_count").intValue() > 99 ? "99+" : String.valueOf(pb.getNumber("comments_count").intValue());
            viewHolder.textViewCommentCount.setText("Comment " + commentCount);

        }

        Date date = pb.getCreatedAt();

        String dateText = DateTool.getLocalFullTime4(date);

        //============rest width========
        {


            int conWidth = viewHolder.viewInfoContainerLinearLayout.getWidth();
            int len = (int) viewHolder.textViewCreatedTime.getPaint().measureText(dateText);
//
//        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) viewHolder.textViewCreatedTime.getLayoutParams();
//        lp.width = (int) len;

            viewHolder.textViewCreatedTime.setText(dateText);
//        viewHolder.textViewCreatedTime.setLayoutParams(lp);
            viewHolder.textViewCreatedTime.setMinWidth((int) (len * 1.5f));
            viewHolder.textViewLocation.setMaxWidth(point.x - (int) (len * 1.5f));
        }

        Number floatNumber = pb.getNumber("weight");
        if (null != floatNumber) {
            viewHolder.textViewWeight.setVisibility(View.VISIBLE);
            String weightText = String.valueOf(floatNumber.floatValue());
            if (weightText.endsWith(".0")) {
                viewHolder.textViewWeight.setText(floatNumber.intValue() + " kg");
            } else {
                viewHolder.textViewWeight.setText(floatNumber.floatValue() + " kg");
            }

//            viewHolder.textViewWeight.setText(+ " kg");

        } else {
            viewHolder.textViewWeight.setVisibility(View.GONE);
        }

        final AVObject areaObj = pb.getAVObject("area");
        if (null != areaObj && !StringUtils.isEmpty(areaObj.getString("title"))) {
            viewHolder.textViewLocation.setText(areaObj.getString("title"));
        } else {
            viewHolder.textViewLocation.setText("");
        }

        viewHolder.textViewLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Tool.isInstallGMS(context)) {
                    App.getApp().putTemPObject("area", areaObj);
                    App.getApp().putTemPObject("catch", pb);
                    Tool.startActivity(context, FishAreaMapActivity.class);
                } else {
                    App.getApp().putTemPObject("area", areaObj);
                    Tool.startActivity(context, FishAreaDetailMapActivity.class);
                }
            }
        });

        String desc = pb.getString("desc");
        if (!StringUtils.isEmpty(desc)) {
            viewHolder.textViewInfoDesc.setVisibility(View.VISIBLE);
            viewHolder.textViewInfoDesc.setText(desc);
        } else {
            viewHolder.textViewInfoDesc.setVisibility(View.GONE);
        }

        int rw = w - marginContent * 2;

        List<AVFile> photos = pb.getList("photos");

        if (!ListUtiles.isEmpty(photos)) {

            if (ListUtiles.getListSize(photos) > 1) {
                viewHolder.indicatorView.setVisibility(View.VISIBLE);
                viewHolder.indicatorView.setIndicatorSize(photos.size());
                viewHolder.indicatorView.checkIndex(-1);
            } else {
                viewHolder.indicatorView.setVisibility(View.INVISIBLE);
            }

            try {
                AVFile fistFile = photos.get(0);

                if (null != fistFile) {

                    String dsturl=fistFile.getUrl();
//                    String url = fistFile.getUrl();

                    if (ParseHelper.isVideo(dsturl)) {
                        App.getApp().getPicasso().load("videoframe://" + dsturl)
                                .resize(rw / 2, rw / 2)
                                .config(Bitmap.Config.RGB_565)
                                .into(viewHolder.imageViewBg);

                    } else {
//                        String url = fistFile.getThumbnailUrl(false, rw / 2, rw / 2, 75, "jpg");
                        Picasso.with(context).load(dsturl).
                                resize(w / 2, w / 2).
                                config(Bitmap.Config.RGB_565).
                                into(viewHolder.imageViewBg);
                    }
                }
            } catch (Exception e) {
                LogUtil.e(App.tag, "error in catch adapter:" + e.getLocalizedMessage());
            }

        } else {
//            viewHolder.imageViewBg.setImageResource(R.drawable.image_placeholder);
//            viewHolder.imageViewBg.setImageResource(R.drawable.image_placeholder);

            viewHolder.imageViewBg.setImageBitmap(null);
            viewHolder.indicatorView.setVisibility(View.INVISIBLE);

        }


        viewHolder.imageViewBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onItemClickListener) {
                    onItemClickListener.onItemClick(pb, i);
                }
            }
        });


        if (AVUser.getCurrentUser().equals(pb.getAVUser("owner"))) {
            viewHolder.textViewFollow.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.textViewFollow.setVisibility(View.VISIBLE);
        }

        viewHolder.textViewFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != onLikeFeollowClickListener) {
                    onLikeFeollowClickListener.onClickFollow(pb, CatchAdapter.this);
                }

            }
        });

        viewHolder.viewLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != onLikeFeollowClickListener) {
                    onLikeFeollowClickListener.onClickLike(pb, CatchAdapter.this);
                }

            }
        });

        viewHolder.viewShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseScreamingReelActivity) context).onShareClick(pb);
            }
        });

        viewHolder.linearLayoutPersonInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (null != onLikeFeollowClickListener) {
                    onLikeFeollowClickListener.onClickOwner(pu);
                }

            }
        });


        return view;
    }

    class ViewHolder {

        @InjectView(R.id.netImageHeader)
        NetImageView netImageView;

        @InjectView(R.id.textViewName)
        TextView textViewName;

        @InjectView(R.id.textViewLocation)
        TextView textViewLocation;

        @InjectView(R.id.textViewFollow)
        TextView textViewFollow;

        @InjectView(R.id.imageViewBg)
        ImageView imageViewBg;

        @InjectView(R.id.viewLike)
        View viewLike;

        @InjectView(R.id.iconOfLikeInAdaptor)
        ImageButton iconOfLikeInAdaptor;

        @InjectView(R.id.textViewLikeCount)
        TextView textViewLikeCount;

        @InjectView(R.id.viewComment)
        View viewComment;

        @InjectView(R.id.textViewCommentCount)
        TextView textViewCommentCount;

        @InjectView(R.id.viewShare)
        View viewShare;

        @InjectView(R.id.textViewCreatedTime)
        TextView textViewCreatedTime;

        @InjectView(R.id.textViewWeight)
        TextView textViewWeight;

        @InjectView(R.id.textViewInfoDesc)
        TextView textViewInfoDesc;

        @InjectView(R.id.indicatorView)
        IndicatorView indicatorView;

        @InjectView(R.id.viewInfoContainerLinearLayout)
        LinearLayout viewInfoContainerLinearLayout;

        @InjectView(R.id.linearLayoutTop)
        LinearLayout linearLayoutTopOwner;

        @InjectView(R.id.linearLayoutPersonInfo)
        LinearLayout linearLayoutPersonInfo;


        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }


    public static interface onItemClickListener {

        public void onItemClick(AVObject aVObject, int postion);
    }

    public CatchAdapter.onItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public void setOnItemClickListener(CatchAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public static interface OnLikeFeollowClickListener {
        public void onClickLike(AVObject catchObject, BaseAdapter catchAdater);

        public void onClickFollow(AVObject followObject, BaseAdapter catchAdater);

        public void onClickOwner(AVObject aVUser);
    }

    public OnLikeFeollowClickListener getOnLikeFeollowClickListener() {
        return onLikeFeollowClickListener;
    }

    public void setOnLikeFeollowClickListener(OnLikeFeollowClickListener onLikeFeollowClickListener) {
        this.onLikeFeollowClickListener = onLikeFeollowClickListener;
    }
}

package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 15/12/4.
 */
public class CommentDetailListAdapter extends BaseAdapter {


    private List<AVObject> arrayListAVObjects;
    private Context context;
    private LayoutInflater layoutInflater;

    public CommentDetailListAdapter(Context context, List<AVObject> parseDatas) {

        this.context = context;
        this.arrayListAVObjects = parseDatas;
        this.layoutInflater = LayoutInflater.from(context);

    }


    @Override
    public int getCount() {
        return arrayListAVObjects.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListAVObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_comment_detail, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        AVObject comment = arrayListAVObjects.get(position);

        AVUser owner = comment.getAVUser("owner");
        if (null != owner.getAVFile("avatar")) {
            Picasso.with(context).load(owner.getAVFile("avatar").getUrl()).resize(36, 36).config(Bitmap.Config.RGB_565).placeholder(R.drawable.icon_header_default).into(viewHolder.imageViewMyHeader);
        } else {
            viewHolder.imageViewMyHeader.setBackgroundResource(R.drawable.icon_header_default);
        }

        viewHolder.textViewMyName.setText(owner.getString("first_name") + " " + owner.getString("last_name"));
        viewHolder.textViewCommentContent.setText(comment.getString("content"));
        viewHolder.textViewTimeLabel.setReferenceTime(comment.getCreatedAt().getTime());

        return convertView;
    }

    class ViewHolder {


        @InjectView(R.id.imageViewMyHeader)
        ImageView imageViewMyHeader;

        @InjectView(R.id.textViewMyName)
        TextView textViewMyName;


        @InjectView(R.id.textViewCommentContent)
        TextView textViewCommentContent;


        @InjectView(R.id.textViewTimeLabel)
        RelativeTimeTextView textViewTimeLabel;


        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}

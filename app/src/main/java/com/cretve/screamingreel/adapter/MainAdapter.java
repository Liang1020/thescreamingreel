package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.common.view.BasePage;
import com.cretve.screamingreel.activity.MainActivity;
import com.cretve.screamingreel.activity.page.PageMusic;
import com.cretve.screamingreel.activity.page.PageProfile;
import com.cretve.screamingreel.activity.page.PageSearch;
import com.cretve.screamingreel.activity.page.PagecatchList;

import java.util.ArrayList;

/**
 * Created by wangzy on 15/11/9.
 */
public class MainAdapter extends PagerAdapter {


    private ArrayList<BasePage> pages;

    private Context context;

    private ViewPager viewPager;


    public MainAdapter(final Context context, final ViewPager viewPager) {
        this.context = context;
        this.pages = new ArrayList<BasePage>();
        this.viewPager = viewPager;


        PageProfile pageProfile = new PageProfile((MainActivity) context);
        PagecatchList pagecatchList = new PagecatchList((MainActivity) context);
        PageSearch pageSearch = new PageSearch((MainActivity) context);
        PageMusic pageMusic = new PageMusic((MainActivity) context);

        pages.add(pageProfile);
        pages.add(pagecatchList);
        pages.add(pageSearch);
        pages.add(pageMusic);

    }


    @Override
    public int getCount() {
        return pages.size();
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View rootView = pages.get(position).getRootView();
        container.addView(rootView);
        return pages.get(position).getRootView();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(pages.get(position).getRootView());
    }


    public PageProfile getPageProfile() {
        return (PageProfile) pages.get(0);
    }

    public BasePage getPageAtIndex(int index) {
        return pages.get(index);
    }

}

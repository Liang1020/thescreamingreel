package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.indicator.view.IndicatorView;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by wangzy on 15/11/3.
 */
public class GuildAdapter extends PagerAdapter {


    private ArrayList<View> views;

    private Context context;

    private ViewPager viewPager;

    private ViewPager imageViewPager;

    private IndicatorView indicatorView;


    private ImageView imageViewHeader;
    private LinearLayout linearlayoutHeader;

    private View guildView1;
    private View guildView2;
    private View guildView3;


    private EditText editTextLocationInput;
    private View linearLayoutSearch;
    private ListView listViewLocation;

    View viewFirstName;
    View viewLastName;
    View viewGender;


    View viewFirstNameP3;
    View viewLastNameP3;
    View viewGenderP3;
    View viewCountryCityP3;

    TextView textViewFirstName;
    TextView textViewLastName;
    TextView textViewGender;


    TextView textViewFirstNameP3;
    TextView textViewLastNameP3;
    TextView textViewGenderP3;
    TextView textViewCuntryCityP3;
    TextView textViewMobile;

    View viewMobile;


    private Button buttonGo;


    public GuildAdapter(final Context context, final ViewPager viewPager) {
        this.context = context;
        this.views = new ArrayList<View>();
        this.viewPager = viewPager;

        guildView1 = View.inflate(context, R.layout.page_guid_1, null);

        imageViewHeader = (ImageView) guildView1.findViewById(R.id.imageViewHeader);
        linearlayoutHeader = (LinearLayout) guildView1.findViewById(R.id.linearlayoutHeader);

        viewFirstName = guildView1.findViewById(R.id.viewFirstName);
        viewLastName = guildView1.findViewById(R.id.viewLastName);
        viewGender = guildView1.findViewById(R.id.viewGender);


        textViewFirstName = (TextView) guildView1.findViewById(R.id.textViewFirstName);
        textViewLastName = (TextView) guildView1.findViewById(R.id.textViewLastName);
        textViewGender = (TextView) guildView1.findViewById(R.id.textViewGender);


        guildView2 = View.inflate(context, R.layout.page_guid_2, null);


        editTextLocationInput = (EditText) guildView2.findViewById(R.id.editTextLocationInput);
        listViewLocation = (ListView) guildView2.findViewById(R.id.listViewLocation);

        linearLayoutSearch = guildView2.findViewById(R.id.linearLayoutSearch);


        guildView3 = View.inflate(context, R.layout.page_guid_3, null);


        textViewFirstNameP3 = (TextView) guildView3.findViewById(R.id.textViewFirstNameP3);
        textViewLastNameP3 = (TextView) guildView3.findViewById(R.id.textViewLastNameP3);
        textViewGenderP3 = (TextView) guildView3.findViewById(R.id.textViewGenderP3);
        textViewCuntryCityP3 = (TextView) guildView3.findViewById(R.id.textViewCuntryCityP3);
        textViewMobile = (TextView) guildView3.findViewById(R.id.textViewMobile);


        viewMobile = guildView3.findViewById(R.id.viewMobile);


        viewFirstNameP3 = guildView3.findViewById(R.id.viewFirstNameP3);
        viewLastNameP3 = guildView3.findViewById(R.id.viewLastNameP3);
        viewGenderP3 = guildView3.findViewById(R.id.viewGenderP3);
        viewCountryCityP3 = guildView3.findViewById(R.id.viewCountryCityP3);


        buttonGo = (Button) guildView3.findViewById(R.id.buttonGo);


        views.add(guildView1);
        views.add(guildView2);
        views.add(guildView3);

    }


    public void addImageViewHeaderFile(String file) {

        linearlayoutHeader.setVisibility(View.VISIBLE);
        imageViewHeader.setVisibility(View.VISIBLE);
        Picasso.with(context).load(new File(file)).config(Bitmap.Config.RGB_565)
                .resize(150, 150)
                .placeholder(R.drawable.icon_header_default)
                .into(imageViewHeader);


    }


    public void showFirstName() {
        viewFirstName.setVisibility(View.VISIBLE);
    }

    public void showLastName() {
        viewLastName.setVisibility(View.VISIBLE);
    }

    public void showGender() {
        viewGender.setVisibility(View.VISIBLE);
    }

    @Override
    public int getCount() {
        return views.size();
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(views.get(position));
        return views.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(views.get(position));
    }


    public TextView getTextViewFirstName() {
        return textViewFirstName;
    }

    public View getViewFirstName() {
        return viewFirstName;
    }

    public TextView getTextViewLastName() {
        return textViewLastName;
    }

    public TextView getTextViewGender() {
        return textViewGender;
    }

    public EditText getEditTextLocationInput() {
        return editTextLocationInput;
    }
    public View getSearchView(){

        return  linearLayoutSearch;
    }

    public ListView getListViewLocation() {
        return listViewLocation;
    }


    public TextView getTextViewFirstNameP3() {
        return textViewFirstNameP3;
    }

    public TextView getTextViewLastNameP3() {
        return textViewLastNameP3;
    }

    public TextView getTextViewGenderP3() {
        return textViewGenderP3;
    }

    public TextView getTextViewCuntryCityP3() {
        return textViewCuntryCityP3;
    }

    public View getViewFirstNameP3() {
        return viewFirstNameP3;
    }

    public View getViewLastNameP3() {
        return viewLastNameP3;
    }

    public View getViewGenderP3() {
        return viewGenderP3;
    }

    public View getViewCountryCityP3() {
        return viewCountryCityP3;
    }

    public Button getButtonGo() {
        return buttonGo;
    }

    public View getViewMobile() {
        return viewMobile;
    }

    public TextView getTextViewMobile() {
        return textViewMobile;
    }

    public void setTextViewMobile(TextView textViewMobile) {
        this.textViewMobile = textViewMobile;
    }
}

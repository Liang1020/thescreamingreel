package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thescreamingreel.androidapp.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 15/11/3.
 */
public class LimitAdapter extends BaseAdapter {

    private Context context;
    private List<String> arrayListLocations;
    private LayoutInflater layoutInflater;

    public LimitAdapter(Context context, List<String> aVObjects) {
        this.context = context;
        this.arrayListLocations = aVObjects;
        this.layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return arrayListLocations.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayListLocations.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder = null;
        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_limits, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String text = arrayListLocations.get(i);
        viewHolder.textViewText.setText(text);

        return convertView;
    }


    class ViewHolder {

        @InjectView(R.id.textViewText)
        TextView textViewText;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }
}

package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVUser;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 15/11/3.
 */
public class UserAdapter extends BaseAdapter {

    private Context context;
    private List<AVUser> arrayListLocations;
    private LayoutInflater layoutInflater;

    public UserAdapter(Context context, List<AVUser> aVObjects) {
        this.context = context;
        this.arrayListLocations = aVObjects;
        this.layoutInflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return arrayListLocations.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayListLocations.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder = null;
        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_angler, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        AVUser aVUser = arrayListLocations.get(i);

        viewHolder.textViewAnglerName.setText(aVUser.getString("first_name") + " " + aVUser.getString("last_name"));

        if (null != aVUser.getAVFile("avatar")) {

            Picasso.with(context).load(aVUser.getAVFile("avatar").getUrl())
                    .resize(150, 150)
                    .centerCrop()
                    .config(Bitmap.Config.RGB_565)
                    .placeholder(R.drawable.icon_header_default)
                    .into(viewHolder.imageViewHeader);

        } else {

            viewHolder.imageViewHeader.setImageResource(R.drawable.icon_header_default);
        }

        return convertView;
    }


    class ViewHolder {

        @InjectView(R.id.textViewAnglerName)
        TextView textViewAnglerName;

        @InjectView(R.id.imageViewHeader)
        ImageView imageViewHeader;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }
}

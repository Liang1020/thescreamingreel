package com.cretve.screamingreel.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.media.ExifInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.activity.VideoPlayerActivity;
import com.cretve.screamingreel.parse.ParseHelper;
import com.learnncode.mediachooser.GalleryCache;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 15/11/30.
 */
public class ItemCatchPictureAdapter extends BaseAdapter {


    private List<String> arrayListFiles;
    private Context context;
    private LayoutInflater layoutInflater;
    private Point p;
    private GalleryCache gallcach;
    private String location;

    public ItemCatchPictureAdapter(Context context) {
        this.context = context;
        this.arrayListFiles = new ArrayList<String>();
        this.layoutInflater = LayoutInflater.from(context);
        this.p = Tool.getDisplayMetrics(context);
    }


    public List<String> getAllSelectFiles() {

        return arrayListFiles;
    }

    @Override
    public int getCount() {
        return arrayListFiles.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListFiles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public void addImageFile(String file) {
        arrayListFiles.add(file);
        notifyDataSetChanged();
    }

    public void addMulImageFile(List<String> urls) {
        arrayListFiles.addAll(urls);
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        int wh = p.x / 3;

        ViewHolder viewHolder = null;
        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_catch_picture, null);
            viewHolder = new ViewHolder(convertView);

            GridView.LayoutParams lpp = new GridView.LayoutParams(wh, wh);
            convertView.setLayoutParams(lpp);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ImageView imageView = viewHolder.imageViewCatch;

        final String path = arrayListFiles.get(position);

        LogUtil.i(App.tag, "path in catch detail:" + path);

        if (!StringUtils.isEmpty(path)) {

            if (path.startsWith("http")) {

                if (ParseHelper.isVideo(path)) {
                    viewHolder.imageViewPlay.setVisibility(View.VISIBLE);
                    viewHolder.imageViewCatch.setTag(path);

//                    GalleryCache.getInstance(context, p.x, p.x).loadBitmap(path, imageView, R.color.bg_color);


                    App.getApp().getPicasso().load("videoframe://"+path)
                            .resize(wh,wh)
                            .centerCrop()
                            .config(Bitmap.Config.RGB_565)
                            .into(imageView);

                } else {
                    viewHolder.imageViewPlay.setVisibility(View.GONE);
                    viewHolder.imageViewCatch.setTag(null);
                    Picasso.with(context).load(path).resize(wh, wh).config(Bitmap.Config.RGB_565).into(imageView);
                }
            } else {
                if (ParseHelper.isVideo(path)) {
                    viewHolder.imageViewCatch.setTag(path);
                    viewHolder.imageViewPlay.setVisibility(View.VISIBLE);
//                    GalleryCache.getInstance(context, p.x, p.x).loadBitmap(path, imageView, R.color.bg_color);


                    App.getApp().getPicasso().load("videoframe://"+path)
                            .resize(wh,wh)
                            .centerCrop()
                            .config(Bitmap.Config.RGB_565)
                            .into(imageView);

                } else {
                    viewHolder.imageViewPlay.setVisibility(View.GONE);
                    viewHolder.imageViewCatch.setTag(null);
                    Picasso.with(context).load(new File(path)).resize(wh, wh).config(Bitmap.Config.RGB_565).into(imageView);
                }

            }
        } else {
            Picasso.with(context).load(R.drawable.image_placeholder).resize(wh, wh).config(Bitmap.Config.RGB_565).into(imageView);
            viewHolder.imageViewPlay.setVisibility(View.GONE);
        }


        viewHolder.imageViewPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                LogUtil.i(App.tag, "path in add:" + path);
                if ((path.startsWith("http") && path.endsWith("movie")) || (path.endsWith("mp4") || path.endsWith("3gp"))) {
                    Intent intent = new Intent(context, VideoPlayerActivity.class);
                    intent.putExtra("path", path);
                    context.startActivity(intent);
                }
            }
        });


        viewHolder.imageButtonDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogUtils.showConfirmDialog(((Activity) context), "", "Confirm delete ?", "Confirm", "Cancel", new DialogCallBackListener() {

                    @Override
                    public void onDone(boolean yesOrNo) {
                        if (yesOrNo) {
                            arrayListFiles.remove(position);
                            notifyDataSetChanged();
                        }
                    }
                });
            }
        });

        return convertView;
    }


    class ViewHolder {

        @InjectView(R.id.imageViewCatch)
        ImageView imageViewCatch;

        @InjectView(R.id.imageButtonDel)
        ImageButton imageButtonDel;

        @InjectView(R.id.imageViewPlay)
        ImageView imageViewPlay;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }



}

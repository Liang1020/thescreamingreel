package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.common.util.Tool;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by hyz on 15/12/1.
 */
public class SelectImageAdapter extends BaseAdapter {

    private List<AVFile> imageList;
    private Context context;
    private LayoutInflater layoutInflater;

    private onItemClickListener onItemClickListener;
    private Point p;

    public SelectImageAdapter(Context context, List<AVFile> objects) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.imageList = objects;
        p = Tool.getDisplayMetrics(context);
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public Object getItem(int position) {
        return imageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_select_image, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final AVFile file = imageList.get(position);
        final ImageView imageView = viewHolder.itemImageView;
        Picasso.with(context).load(file.getUrl())
                .resize(p.x/3, p.x/3)
                .placeholder(R.drawable.image_placeholder)
                .config(Bitmap.Config.RGB_565)
                .into(imageView);

        return convertView;
    }

    class ViewHolder {

        @InjectView(R.id.ItemImage)
        ImageView itemImageView;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    public static interface onItemClickListener{

        public void onItemClick(AVObject aVObject, int postion);
    }

    public SelectImageAdapter.onItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public void setOnItemClickListener(SelectImageAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}


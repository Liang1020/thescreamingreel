package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.util.ListUtiles;
import com.cretve.screamingreel.bean.TideBean;
import com.cretve.screamingreel.bean.WeatherBean;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 15/12/25.
 */
public class WeatherAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<WeatherBean> weatherBeens;

    public WeatherAdapter(Context context, ArrayList<WeatherBean> weatherBeens) {
        this.context = context;
        this.weatherBeens = weatherBeens;
        this.layoutInflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return weatherBeens.size();
    }

    @Override
    public Object getItem(int position) {
        return weatherBeens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_weather, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        WeatherBean wb = weatherBeens.get(position);
        viewHolder.textViewDate.setText(wb.getDateFormat());

        ArrayList<WeatherBean.Hoursly> hoursly = wb.getAllHoursly();

        WeatherBean.Hoursly hightHours = null;
        WeatherBean.Hoursly lowHours = null;

        for (int i = 0, isize = hoursly.size(); i < isize; i++) {

            WeatherBean.Hoursly hourly = hoursly.get(i);
            if (null == hightHours || (null != hightHours && hourly.getTempCInt() > hightHours.getTempCInt())) {
                hightHours = hourly;
            }
            if (null == lowHours || (null != lowHours && hourly.getTempCInt() < lowHours.getTempCInt())) {
                lowHours = hourly;
            }
        }


        viewHolder.textViewHigh.setText(null != hightHours ? hightHours.getTempC() + "℃" : "");
        viewHolder.textViewLow.setText(null != lowHours ? lowHours.getTempC() + "℃" : "");


        int sunRise = Integer.parseInt(wb.getSunRise().split(":")[0]);
        int sunSet = Integer.parseInt(wb.getSunSet().split(":")[0]);


        for (int i = 0, isize = hoursly.size(); i < isize; i++) {
            WeatherBean.Hoursly hourly = hoursly.get(i);
            if (null == hightHours || (null != hightHours && hourly.getTempCInt() > hightHours.getTempCInt())) {
                hightHours = hourly;
            }
            if (null == lowHours || (null != lowHours && hourly.getTempCInt() < lowHours.getTempCInt())) {
                lowHours = hourly;
            }
        }

        float lowDayWind = 1000f;
        float highDayWind = -1000.0f;

        float lowNightWind = 1000.0f;
        float highNightWind = -1000.0f;


        for (int i = 0, isize = hoursly.size(); i < isize; i++) {
            WeatherBean.Hoursly hourly = hoursly.get(i);
            float wind = hourly.getWindowKm();

            if (i >= (sunRise - 1) && i <= (sunSet + 11)) {
                //day
                if (wind > highDayWind) {
                    highDayWind = wind;
                }
                if (wind < lowDayWind) {
                    lowDayWind = wind;
                }

            } else {
                //night
                if (wind > highNightWind) {
                    highNightWind = wind;
                }
                if (wind < lowNightWind) {
                    lowNightWind = wind;
                }
            }

        }


        viewHolder.textViewDayDesc.setText(
                hightHours.getWeatherDesc()
                        + "." + "High around " + hightHours.getTempC() + "℃"
                        + "." + "Wind " + hightHours.getWinddir16Point() + " at " + (int) lowDayWind + " to " + (int) highDayWind + "km/h"
        );

        viewHolder.textViewNightDesc.setText(

                lowHours.getWeatherDesc()
                        + "." + "Low " + lowHours.getTempC() + "℃"
                        + "." + "Winds " + lowHours.getWinddir16Point() + " at " + (int) lowNightWind + " to " + (int) highNightWind + " km/h"
        );


        ArrayList<TideBean> tidebeans = wb.getTideBeans();

        if (!ListUtiles.isEmpty(tidebeans)) {

            ArrayList<TideBean> lowTimes = new ArrayList<TideBean>();
            ArrayList<TideBean> highTimes = new ArrayList<TideBean>();
            for (TideBean tideBean : tidebeans) {

                if (tideBean.getTide_type().equals("LOW")) {
                    lowTimes.add(tideBean);
                } else {
                    highTimes.add(tideBean);
                }
            }


            try {
                viewHolder.textViewLowTime1.setText(lowTimes.get(0).getTideTime());
            } catch (Exception e) {
                viewHolder.textViewLowTime1.setText("_:_");
            }

            try {
                viewHolder.textViewLowTime2.setText(lowTimes.get(1).getTideTime());
            } catch (Exception e) {
                viewHolder.textViewLowTime2.setText("_:_");
            }


            try {
                viewHolder.textViewHighTime1.setText(highTimes.get(0).getTideTime());
            } catch (Exception e) {
                viewHolder.textViewHighTime1.setText("_:_");
            }


            try {
                viewHolder.textViewHighTime2.setText(highTimes.get(1).getTideTime());
            } catch (Exception e) {
                viewHolder.textViewHighTime2.setText("_:_");
            }


        } else {
            viewHolder.textViewLowTime1.setText("_:_");
            viewHolder.textViewHighTime1.setText("_:_");
            viewHolder.textViewLowTime2.setText("_:_");
            viewHolder.textViewHighTime2.setText("_:_");
            viewHolder.textViewSwellHigh.setText("--");
        }

        try {
            viewHolder.textViewSwellHigh.setText(null != wb.getSweelHight() ? wb.getSweelHight() + " m" : "-");
        } catch (Exception e) {
            viewHolder.textViewSwellHigh.setText("-");
        }


        String highIcon = hightHours.getWeatherCode();
//        Picasso.with(context).load("file:///android_asset/DvpvklR.png").into(imageView2);
        Picasso.with(context).
                load("file:///android_asset/day/" + highIcon + ".png").
                config(Bitmap.Config.RGB_565).
                into(viewHolder.imageViewDayIcon);

        String lowIcon = lowHours.getWeatherCode();
        Picasso.with(context).
                load("file:///android_asset/night/" + lowIcon + ".png").
                config(Bitmap.Config.RGB_565).
                into(viewHolder.imageViewNightIcon);


        return convertView;
    }


    class ViewHolder {


        @InjectView(R.id.textViewDate)
        TextView textViewDate;

        @InjectView(R.id.textViewLow)
        TextView textViewLow;

        @InjectView(R.id.textViewHigh)
        TextView textViewHigh;

        @InjectView(R.id.imageViewDayIcon)
        ImageView imageViewDayIcon;

        @InjectView(R.id.textViewDayDesc)
        TextView textViewDayDesc;

        @InjectView(R.id.textViewNightDesc)
        TextView textViewNightDesc;

        @InjectView(R.id.imageViewNightIcon)
        ImageView imageViewNightIcon;

        @InjectView(R.id.textViewLowTime1)
        TextView textViewLowTime1;

        @InjectView(R.id.textViewHighTime1)
        TextView textViewHighTime1;

        @InjectView(R.id.textViewLowTime2)
        TextView textViewLowTime2;

        @InjectView(R.id.textViewHighTime2)
        TextView textViewHighTime2;

        @InjectView(R.id.textViewSwellHigh)
        TextView textViewSwellHigh;


        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }
}

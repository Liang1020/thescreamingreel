package com.cretve.screamingreel.adapter;


import android.app.Fragment;
import android.app.FragmentManager;

import com.cretve.screamingreel.activity.page.PageProfile;
import com.cretve.screamingreel.activity.page.PagecatchList;
import com.cretve.screamingreel.fragment.CatchListFragment;
import com.cretve.screamingreel.fragment.MusicFragment;
import com.cretve.screamingreel.fragment.ProfileFragment;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;

/**
 * Created by wangzy on 15/12/10.
 */
public class MainFragmentAdapter extends MyFragmentPageAdapter {

    private Fragment[] fragment;

    public MainFragmentAdapter(FragmentManager fm) {
        super(fm);
        fragment = new Fragment[4];
        fragment[0] = new ProfileFragment();
        fragment[1] = new CatchListFragment();

        GoogleMapOptions options = new GoogleMapOptions();
        options.mapType(GoogleMap.MAP_TYPE_NORMAL);

        fragment[2] = MapFragment.newInstance(options);
        fragment[3] = new MusicFragment();
    }

    @Override
    public Fragment getItem(int position) {


        return fragment[position];
    }

    @Override
    public int getCount() {
        return 4;
    }


    public PageProfile getPageProfile() {

        return ((ProfileFragment) fragment[0]).getBasePage();

    }

    public PagecatchList getPageCatchList() {
        return (PagecatchList) ((CatchListFragment) fragment[1]).getBasePage();
    }

    public MapFragment getMapFragment() {

        return (MapFragment) fragment[2];

    }
}

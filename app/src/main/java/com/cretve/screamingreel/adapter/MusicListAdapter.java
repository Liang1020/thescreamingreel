package com.cretve.screamingreel.adapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.FileUtils;
import com.common.util.LogUtil;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.activity.MainActivity;
import com.thescreamingreel.androidapp.R;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by hyz on 15/12/4.
 */
public class MusicListAdapter extends BaseAdapter {

    private List<String> musicList;
    private Context context;
    private LayoutInflater layoutInflater;
    private HashMap<String, Integer> musicMap;
    private MediaPlayer musicPlayer;
    private int playIndex = -1;

    private onItemClickListener onItemClickListener;

    public MusicListAdapter(Context context, List<String> objects) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.musicList = objects;
        initMusicMap();
    }

    @Override
    public int getCount() {
        return musicList.size();
    }

    @Override
    public Object getItem(int position) {
        return musicList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_music, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textViewName.setText(musicList.get(position));
        final ImageButton playButton = viewHolder.playImageButton;
        //playButton.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        final ViewHolder tempHolder = viewHolder;

        if (this.playIndex == position && musicPlayer.isPlaying()) {
            playButton.setImageResource(R.drawable.icon_pause);
            playButton.setTag("playing");

        } else {
            playButton.setTag("pause");
            playButton.setImageResource(R.drawable.icon_play);
        }

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playButton.getTag().equals("playing")) {
                    pauseMusic(position, tempHolder);
                } else {
                    playMusic(position, tempHolder);
                }
            }
        });

        viewHolder.textViewShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setMyRingtone(position);
            }
        });

        return convertView;
    }

    public void setMyRingtone(final int position) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (!Settings.System.canWrite(context)) {
                ((MainActivity) context).requestSettingsPermisson();
                return;
            }


            DialogUtils.showConfirmDialog((Activity) context, "Set as ringtone", "Do you want to set this sound effect as your ringtone?", "Confirm", "Cancel", new DialogCallBackListener() {

                @Override
                public void onDone(boolean yesOrNo) {
                    if (yesOrNo) {
                        Uri path = Uri.parse("android.resource://com.thescreamingreel.androidapp/raw/" + musicMap.get(musicList.get(position)));
                        RingtoneManager.setActualDefaultRingtoneUri(context, RingtoneManager.TYPE_RINGTONE, path);
                        ((MainActivity) context).showToastWithTime("Set ringtone successfully.", 3);
                    }

                }
            });
        } else
            DialogUtils.showConfirmDialog((Activity) context, "Set as ringtone", "Do you want to set this sound effect as your ringtone?", "Confirm", "Cancel", new DialogCallBackListener() {

                @Override
                public void onDone(boolean yesOrNo) {
                    if (yesOrNo) {

//                      String textPath="android.resource://com.thescreamingreel.androidapp/raw/" + musicMap.get(musicList.get(position));

                        try {
                            Integer ringRes = musicMap.get(musicList.get(position));
                            String dst = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC) + "scream_ring.mp3";

                            File dstFile = new File(dst);
                            if (dstFile.exists()) {
                                dstFile.delete();
                            }


                            LogUtil.i(App.tag, "copy to:" + dst);
                            FileUtils.copyRawtoSdcard(ringRes, context.getResources(), dst);
                            setMyRingtone(new File(dst));
                            ((MainActivity) context).showToastWithTime("Set ringtone successfully.", 3);
                        } catch (IOException e) {
                            LogUtil.e(App.tag, "copy error:" + e.getLocalizedMessage());
                            e.printStackTrace();
                            ((MainActivity) context).showToastWithTime("Set ringtone fail,sdcard error!", 3);
                        }


                    }

                }
            });


//        ContentValues values = new ContentValues();
//        values.put(MediaStore.MediaColumns.DATA, path.getPath());
//        values.put(MediaStore.MediaColumns.TITLE, "test");
//        values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/*");
//        values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
//        values.put(MediaStore.Audio.Media.IS_NOTIFICATION, false);
//        values.put(MediaStore.Audio.Media.IS_ALARM, false);
//        values.put(MediaStore.Audio.Media.IS_MUSIC, false);
//        Uri uri = MediaStore.Audio.Media.getContentUriForPath(sdfile.getAbsolutePath());
//        Uri newUri = context.getContentResolver().insert(path, values);

    }


    public void setMyRingtone(File file) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.MediaColumns.DATA, file.getAbsolutePath());
//    values.put(MediaStore.MediaColumns.TITLE, file.getName());
//    values.put(MediaStore.MediaColumns.SIZE, file.length());
        values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
//    values.put(MediaStore.Audio.Media.ARTIST, "Madonna");
//    values.put(MediaStore.Audio.Media.DURATION, 230);
        values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
        values.put(MediaStore.Audio.Media.IS_NOTIFICATION, false);
        values.put(MediaStore.Audio.Media.IS_ALARM, false);
        values.put(MediaStore.Audio.Media.IS_MUSIC, false);
        Uri uri = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
        Uri newUri = context.getContentResolver().insert(uri, values);
        RingtoneManager.setActualDefaultRingtoneUri(context, RingtoneManager.TYPE_RINGTONE, newUri);
    }


    public void setMyRingtone(String path) {
        File sdfile = new File(path);
        ContentValues values = new ContentValues();
        values.put(MediaStore.MediaColumns.DATA, sdfile.getAbsolutePath());
        values.put(MediaStore.MediaColumns.TITLE, sdfile.getName());
        values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/*");
        values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
        values.put(MediaStore.Audio.Media.IS_NOTIFICATION, false);
        values.put(MediaStore.Audio.Media.IS_ALARM, false);
        values.put(MediaStore.Audio.Media.IS_MUSIC, false);

        Uri uri = MediaStore.Audio.Media.getContentUriForPath(sdfile.getAbsolutePath());
        Uri newUri = context.getContentResolver().insert(uri, values);
        RingtoneManager.setActualDefaultRingtoneUri(context, RingtoneManager.TYPE_RINGTONE, newUri);
        ((MainActivity) context).showToastWithTime("Set ringtone successfully.", 3);
    }


    private void playMusic(int position, ViewHolder viewHolder) {
        this.playIndex = position;
        if (musicPlayer != null && musicPlayer.isPlaying()) {
            musicPlayer.stop();
            this.notifyDataSetChanged();
        }

        musicPlayer = MediaPlayer.create(context, musicMap.get(musicList.get(position)));
        musicPlayer.start();
        final ImageButton playButton = viewHolder.playImageButton;

        try {
            musicPlayer.setDataSource("");
        } catch (Exception e) {
            e.printStackTrace();
        }
        musicPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playButton.setImageResource(R.drawable.icon_play);
                playButton.setTag("pause");
            }
        });

        playButton.setImageResource(R.drawable.icon_pause);
        playButton.setTag("playing");
    }

    public void stopPlay(){
        if (musicPlayer != null && musicPlayer.isPlaying()) {
            musicPlayer.stop();
            playIndex=-1;
            this.notifyDataSetChanged();
        }
    }

    private void pauseMusic(int position, ViewHolder viewHolder) {
        if (musicPlayer != null && musicPlayer.isPlaying()) {
            musicPlayer.pause();
        }
        final ImageButton playButton = viewHolder.playImageButton;
        playButton.setImageResource(R.drawable.icon_play);
        playButton.setTag("pause");
    }







    private void initMusicMap() {
        musicMap = new HashMap<String, Integer>();
        musicMap.put("Fish Slap", R.raw.fish_slap);
        musicMap.put("Freshwater Screaming Reel", R.raw.freshwater_screaming_reel);
        musicMap.put("Saltwater Screaming Reel", R.raw.saltwater_screaming_reel);
        musicMap.put("Screaming Winding Reel", R.raw.screaming_winding_reel);
        musicMap.put("Smooth Diesel Boat Engine", R.raw.smooth_diesel_boat_engine);
        musicMap.put("Steady Screaming Reel", R.raw.steady_screaming_reel);
    }

    class ViewHolder {

        @InjectView(R.id.playImageButton)
        ImageButton playImageButton;

        @InjectView(R.id.textViewName)
        TextView textViewName;

        @InjectView(R.id.textViewShare)
        TextView textViewShare;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    public static interface onItemClickListener {
        public void onItemClick(int postion);
    }

    public MusicListAdapter.onItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public void setOnItemClickListener(MusicListAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
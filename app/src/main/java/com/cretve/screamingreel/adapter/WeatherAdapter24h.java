package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.common.util.ListUtiles;
import com.common.util.StringUtils;
import com.cretve.screamingreel.bean.TideBean;
import com.cretve.screamingreel.bean.WeatherBean;
import com.cretve.screamingreel.view.DirectionView;
import com.cretve.screamingreel.view.WeatherPercentView;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import java.util.ArrayList;

/**
 * Created by wangzy on 15/12/25.
 */
public class WeatherAdapter24h extends BaseAdapter {

    private Context context;
    private TextView textViewWindKmp;

    private TextView textViewTempratureHigh;
    private TextView textViewTempratureTonight;

    private LayoutInflater layoutInflater;
    private View contentView;
    private WeatherBean weatherBean;
    private ImageView imageViewTodayImage;
    private ImageView imageViewNightImage;
    private WeatherPercentView weatherPercentView;
    private TextView textViewHumidityPercent;
    private TextView textViwSwellHeight;

    private TextView textViewSunRiseTime;
    private TextView textViewSunSetTime;


    private TextView textViewLowTime1;
    private TextView textViewLowTime1Label;

    private TextView textViewLowTime2;
    private TextView textViewLowTime2Label;

    private TextView textViewLowTide1;
    private TextView textViewLowTide2;

    private TextView textViewHighTime1;
    private TextView textViewHighTime1Label;

    private TextView textViewHighTime2;
    private TextView textViewHighTime2Label;


    private TextView textViewHighTide1;
    private TextView textViewHighTide2;
    private TextView textViewWinddir16Point;

    private DirectionView imageViewTodayWind;


    public WeatherAdapter24h(Context context, WeatherBean weatherBean) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.weatherBean = weatherBean;
        this.contentView = layoutInflater.inflate(R.layout.list_weather_24h, null);
        this.weatherPercentView = (WeatherPercentView) contentView.findViewById(R.id.weatherPercentView);
        this.textViewHumidityPercent = (TextView) contentView.findViewById(R.id.textViewHumidityPercent);
        this.textViewSunRiseTime = (TextView) contentView.findViewById(R.id.textViewSunRiseTime);
        this.textViewSunSetTime = (TextView) contentView.findViewById(R.id.textViewSunSetTime);
        this.textViewWinddir16Point = (TextView) contentView.findViewById(R.id.textViewWinddir16Point);


        this.imageViewTodayWind = (DirectionView) contentView.findViewById(R.id.imageViewTodayWind);


        textViewLowTime1 = (TextView) contentView.findViewById(R.id.textViewLowTime1);
        textViewLowTime1Label = (TextView) contentView.findViewById(R.id.textViewLowTime1Label);

        textViewLowTime2 = (TextView) contentView.findViewById(R.id.textViewLowTime2);
        textViewLowTime2Label = (TextView) contentView.findViewById(R.id.textViewLowTime2Label);

        textViewLowTide1 = (TextView) contentView.findViewById(R.id.textViewLowTide1);
        textViewLowTide2 = (TextView) contentView.findViewById(R.id.textViewLowTide2);


        textViewHighTime1 = (TextView) contentView.findViewById(R.id.textViewHighTime1);
        textViewHighTime1Label = (TextView) contentView.findViewById(R.id.textViewHighTime1Label);

        textViewHighTime2 = (TextView) contentView.findViewById(R.id.textViewHighTime2);
        textViewHighTime2Label = (TextView) contentView.findViewById(R.id.textViewHighTime2Label);

        textViewHighTide1 = (TextView) contentView.findViewById(R.id.textViewHighTide1);
        textViewHighTide2 = (TextView) contentView.findViewById(R.id.textViewHighTide2);

        imageViewTodayImage = (ImageView) contentView.findViewById(R.id.imageViewTodayImage);
        imageViewNightImage = (ImageView) contentView.findViewById(R.id.imageViewNightImage);


        textViewTempratureHigh = (TextView) contentView.findViewById(R.id.textViewTempratureHigh);
        textViewTempratureTonight = (TextView) contentView.findViewById(R.id.textViewTempratureTonight);


        WeatherBean.Hoursly hightHours = null;
        WeatherBean.Hoursly lowHours = null;


        ArrayList<WeatherBean.Hoursly> hoursly = weatherBean.getAllHoursly();
        for (int i = 0, isize = hoursly.size(); i < isize; i++) {

            WeatherBean.Hoursly hourly = hoursly.get(i);
            if (null == hightHours || (null != hightHours && hourly.getTempCInt() > hightHours.getTempCInt())) {
                hightHours = hourly;
            }
            if (null == lowHours || (null != lowHours && hourly.getTempCInt() < lowHours.getTempCInt())) {
                lowHours = hourly;
            }
        }


        textViewSunRiseTime.setText(weatherBean.getSunRise().replace("AM", ""));
        textViewSunSetTime.setText(weatherBean.getSunSet().replace("PM", ""));

        textViewTempratureHigh.setText(null != hightHours ? hightHours.getTempC() : "");
        textViewTempratureTonight.setText(null != lowHours ? lowHours.getTempC() : "");


        this.textViwSwellHeight = (TextView) contentView.findViewById(R.id.textViwSwellHeight);
        this.textViwSwellHeight.setText(StringUtils.isEmpty(weatherBean.getSweelHight()) ? "-" : weatherBean.getSweelHight());

        try {
            weatherPercentView.setPercent(Integer.parseInt(weatherBean.getHumidity()));
            textViewHumidityPercent.setText(String.valueOf(weatherBean.getHumidity()));
        } catch (Exception e) {
        }

        if (!ListUtiles.isEmpty(weatherBean.getTideBeans())) {

            ArrayList<TideBean> tides = weatherBean.getTideBeans();


            ArrayList<TideBean> lowTimes = new ArrayList<TideBean>();
            ArrayList<TideBean> highTimes = new ArrayList<TideBean>();
            for (TideBean tideBean : tides) {

                if (tideBean.getTide_type().equals("LOW")) {
                    lowTimes.add(tideBean);
                } else {
                    highTimes.add(tideBean);
                }
            }


            try {

                textViewLowTime1.setText(lowTimes.get(0).getTideTime().split(" ")[0]);
                textViewLowTime1Label.setText(lowTimes.get(0).getTideTime().split(" ")[1].toLowerCase());

                textViewLowTide1.setText(lowTimes.get(0).getTideHeight_mt());
            } catch (Exception e) {
                textViewLowTime1.setText("_:_");
                textViewLowTide1.setText("0");
            }

            try {
                textViewHighTime1.setText(highTimes.get(0).getTideTime().split(" ")[0]);
                textViewHighTime1Label.setText(highTimes.get(0).getTideTime().split(" ")[1].toLowerCase());

                textViewHighTide1.setText(highTimes.get(0).getTideHeight_mt());
            } catch (Exception e) {
                textViewHighTime1.setText("_:_");
                textViewHighTide1.setText("0");
            }

            try {
                textViewLowTime2.setText(lowTimes.get(1).getTideTime().split(" ")[0]);
                textViewLowTime2Label.setText(lowTimes.get(1).getTideTime().split(" ")[1].toLowerCase());

                textViewLowTide2.setText(lowTimes.get(1).getTideHeight_mt());
            } catch (Exception e) {
                textViewLowTime2.setText("_:_");
                textViewLowTide2.setText("0");
            }

            try {
                textViewHighTime2.setText(highTimes.get(1).getTideTime().split(" ")[0]);
                textViewHighTime2Label.setText(highTimes.get(1).getTideTime().split(" ")[1].toLowerCase());

                textViewHighTide2.setText(highTimes.get(1).getTideHeight_mt());
            } catch (Exception e) {
                textViewHighTime2.setText("_:_");
                textViewHighTide2.setText("0");
            }

        } else {

            textViewLowTime1.setText("_:_");
            textViewLowTide1.setText("0");

            textViewHighTime1.setText("_:_");
            textViewHighTide1.setText("0");

            textViewLowTime2.setText("_:_");
            textViewLowTide2.setText("0");

            textViewHighTime2.setText("_:_");
            textViewHighTide2.setText("0");

        }


        this.textViewWinddir16Point.setText(weatherBean.getWinddir16Point());

        this.textViewWindKmp = (TextView) contentView.findViewById(R.id.textViewWindKmp);
        this.textViewWindKmp.setText(weatherBean.getWindspeedKmph());

        this.imageViewTodayWind.setDegree(Integer.parseInt(weatherBean.getWinddirDegree()));


        String highIcon = hightHours.getWeatherCode();
//        Picasso.with(context).load("file:///android_asset/DvpvklR.png").into(imageView2);
        Picasso.with(context).
                load("file:///android_asset/day/" + highIcon + ".png").
                resize(200, 200).
                config(Bitmap.Config.RGB_565).
                into(imageViewTodayImage);

        String lowIcon = lowHours.getWeatherCode();
        Picasso.with(context).
                load("file:///android_asset/night/" + lowIcon + ".png").
                resize(200, 200).
                config(Bitmap.Config.RGB_565).
                into(imageViewNightImage);

    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        return contentView;
    }
}

package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 15/11/27.
 */
public class FishAdapter extends BaseAdapter {

    private List<AVObject> fishes;
    private Context context;
    private LayoutInflater inflater;

    public FishAdapter(Context context, List<AVObject> fishes) {

        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.fishes = fishes;

    }

    @Override
    public int getCount() {
        return fishes.size();
    }

    @Override
    public Object getItem(int position) {
        return fishes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = inflater.inflate(R.layout.item_fish, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        AVObject pb = fishes.get(position);

        if(pb.has("name") && null!=pb.getString("name")){
            viewHolder.textViewFishName.setText(pb.getString("name"));
        }

        if(pb.has("image") && null!=pb.getAVFile("image")){
            String url=pb.getAVFile("image").getUrl();
            Picasso.with(context).load(url).
                    placeholder(R.drawable.icon_default_fish).
                    resize(200,90).config(Bitmap.Config.RGB_565).
                    into(viewHolder.imageViewFishImg);
        }else{

            Picasso.with(context).load(R.drawable.icon_default_fish).
                    resize(200,90).config(Bitmap.Config.RGB_565).
                    into(viewHolder.imageViewFishImg);
        }

        return convertView;
    }

    class ViewHolder {

        @InjectView(R.id.imageViewFishImg)
        ImageView imageViewFishImg;

        @InjectView(R.id.textViewFishName)
        TextView textViewFishName;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}

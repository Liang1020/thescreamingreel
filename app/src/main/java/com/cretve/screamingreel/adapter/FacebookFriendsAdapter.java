package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVACL;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVPush;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SendCallback;
import com.cretve.screamingreel.App;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import org.json.JSONObject;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by hyz on 15/12/1.
 */
public class FacebookFriendsAdapter extends BaseAdapter {

    private List<AVUser> friendsList;
    private Context context;
    private LayoutInflater layoutInflater;

    private onItemClickListener onItemClickListener;

    public FacebookFriendsAdapter(Context context, List<AVUser> objects) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.friendsList = objects;
    }

    @Override
    public int getCount() {
        return friendsList.size();
    }

    @Override
    public Object getItem(int position) {
        return friendsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_facebook_friends, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final AVUser user = friendsList.get(position);
        viewHolder.textViewName.setText((String)user.get("name"));
        final ImageView imageView = viewHolder.imageViewUser;

        final TextView textViewFollow = viewHolder.textViewFollow;


        List<Object> listFollowuser = AVUser.getCurrentUser().getList("follow_users");

        if (null!=listFollowuser && listFollowuser.contains(user) == false) {
            textViewFollow.setText("Follow");
            textViewFollow.setTextColor(context.getResources().getColor(R.color.text_color_ios_blue));

        } else {
            textViewFollow.setText("Followed");
            textViewFollow.setTextColor(context.getResources().getColor(R.color.gray_text_color));
        }


        textViewFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(null!=onItemClickListener){
                    onItemClickListener.onFollowClick(user,position,FacebookFriendsAdapter.this);
                }

            }
        });

//        textViewFollow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                List<Object> followList = AVUser.getCurrentUser().getList("follow_users");
//
//                if (null!=followList && followList.contains(user)) {
//                    textViewFollow.setText("Follow");
//                    textViewFollow.setTextColor(context.getResources().getColor(R.color.text_color_ios_blue));
//                    AVUser.getCurrentUser().getList("follow_users").remove(user);
//
//                } else {
//                    textViewFollow.setText("Followed");
//                    textViewFollow.setTextColor(context.getResources().getColor(R.color.gray_text_color));
//                    AVUser.getCurrentUser().addUnique("follow_users", user);
//                    sendFellowPush(user);
//                }
//
////                AVUser.getCurrentUser().saveInBackground();
//
//                ((BaseScreamingReelActivity) context).saveAVObject(AVUser.getCurrentUser(),true,true,null);
//            }
//        });

        Picasso.with(context).load((String)user.get("url"))
                .placeholder(R.drawable.icon_header_default)
                .config(Bitmap.Config.RGB_565)
                .resize(150,150)
                .centerCrop()
                .into(imageView);
        return convertView;
    }

    public void sendFellowPush(AVUser user) {

        try {
            AVObject notification = AVObject.create("Notifications");
            notification.put("sender", AVUser.getCurrentUser());
            notification.put("type", "Follow");
            notification.put("receiver", user);
            notification.put("read", false);



            AVACL acl=new AVACL();

            acl.setPublicReadAccess(true);
            acl.setPublicWriteAccess(true);
            notification.setACL(acl);


            notification.saveEventually();

            AVQuery query = AVInstallation.getQuery();
            query.whereEqualTo("user", user);
            JSONObject jsobObject = new JSONObject();
            String alert = AVUser.getCurrentUser().get("first_name") + " " + AVUser.getCurrentUser().get("last_name") + " followed your. ";
            jsobObject.put("alert", alert);
            jsobObject.put("key", App.K_USERFOLLOWED_NOTIFICATIONKEY);
            AVPush.sendDataInBackground(jsobObject, query, new SendCallback() {
                @Override
                public void done(AVException e) {

                }
            });
        } catch (Exception e2) {

        }
    }

    class ViewHolder {

        @InjectView(R.id.imageViewUser)
        ImageView imageViewUser;

        @InjectView(R.id.textViewName)
        TextView textViewName;

        @InjectView(R.id.textViewFollow)
        TextView textViewFollow;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

    public static interface onItemClickListener{
        public void onItemClick(AVObject aVObject,int postion);
        public void onFollowClick(AVObject aVObject,int postion,FacebookFriendsAdapter baseAdapter);
    }

    public FacebookFriendsAdapter.onItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public void setOnItemClickListener(FacebookFriendsAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}


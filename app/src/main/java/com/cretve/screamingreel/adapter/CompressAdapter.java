package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.common.util.Tool;
import com.thescreamingreel.androidapp.R;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 15/11/27.
 */
public class CompressAdapter extends BaseAdapter {

    private ArrayList<String> files;
    private Context context;
    private LayoutInflater inflater;

    public CompressAdapter(Context context, ArrayList<String> files) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.files = files;

    }

    @Override
    public int getCount() {
        return files.size();
    }

    @Override
    public Object getItem(int position) {
        return files.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = inflater.inflate(R.layout.item_compress, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String file = files.get(position);
        viewHolder.textViewFileName.setText(Tool.getFileName(file));

        return convertView;
    }

    class ViewHolder {


        @InjectView(R.id.textViewFileName)
        TextView textViewFileName;

        @InjectView(R.id.progressBar)
        ProgressBar progressBar;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}

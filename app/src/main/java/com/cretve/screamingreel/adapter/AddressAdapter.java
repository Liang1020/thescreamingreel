package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.location.Address;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thescreamingreel.androidapp.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 15/12/10.
 */
public class AddressAdapter extends BaseAdapter {


    private List<Address> addressList;
    private Context context;
    private LayoutInflater layoutInflater;

    public AddressAdapter(Context context, List<Address> list) {

        this.context = context;
        this.addressList = list;
        this.layoutInflater = LayoutInflater.from(context);


    }

    @Override
    public int getCount() {
        return addressList.size();
    }

    @Override
    public Object getItem(int position) {
        return addressList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_location, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Address address = addressList.get(position);
        String location = "";
        try {
            location = String.valueOf(address.getAdminArea() + " " + address.getFeatureName());
        } catch (Exception e) {
        }
        viewHolder.textViewLocation.setText(location.replace("null","").replace("Null","").trim());

//        viewHolder.textViewLocation.setText(location);

        return convertView;
    }


    class ViewHolder {

        @InjectView(R.id.textViewLocation)
        TextView textViewLocation;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }
}

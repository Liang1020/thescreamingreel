package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by wangzy on 15/12/15.
 */
public class GroupListAdapter extends BaseAdapter {


    private HashMap<String, List<AVObject>> hashMap;
    private Context context;
    private ArrayList<AVObject> aVObjects;
    private LayoutInflater layoutInflater;
    private String currentSpecies;

    public GroupListAdapter(Context context, HashMap<String, List<AVObject>> hashMapData) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.aVObjects = new ArrayList<AVObject>();
        this.hashMap = hashMapData;
        rebuildData(hashMapData);
    }


    class MapKeyComparator implements Comparator<String> {

        @Override
        public int compare(String str1, String str2) {

            return str1.compareTo(str2);
        }
    }

    public Map<String, List<AVObject>> sortMapByKey(Map<String, List<AVObject>> map) {

        if (map == null || map.isEmpty()) {
            return null;
        }
        Map<String, List<AVObject>> sortMap = new TreeMap<String, List<AVObject>>(new MapKeyComparator());

        sortMap.putAll(map);

        return sortMap;
    }

    private void rebuildData(HashMap<String, List<AVObject>> maps) {
        this.aVObjects = new ArrayList<AVObject>();

        if (null == maps || maps.isEmpty()) {
            return;
        }

        Map<String, List<AVObject>> sortMap = new TreeMap<>(maps);

        sortMap = sortMapByKey(sortMap);


        for (Map.Entry<String, List<AVObject>> entry : sortMap.entrySet()) {
            List<AVObject> aVObjects = entry.getValue();
            String fishName = aVObjects.get(0).getAVObject("fish").getString("name");

            AVObject objSpecies = AVObject.create("Catch");
            objSpecies.put("indicator", fishName);
            objSpecies.put("fishName", fishName);
            this.aVObjects.add(objSpecies);

            if (fishName.equals(currentSpecies)) {
                this.aVObjects.addAll(aVObjects);
            } else {
                if (aVObjects.size() > 3) {
                    this.aVObjects.addAll(aVObjects.subList(0, 3));
                    AVObject objShowMore = AVObject.create("Catch");
                    objShowMore.put("indicator", "Show more catches");
                    objShowMore.put("fishName", fishName);
                    this.aVObjects.add(objShowMore);
                } else {
                    this.aVObjects.addAll(aVObjects);
                }
            }

        }
    }

    @Override
    public int getCount() {
        return aVObjects.size();
    }

    @Override
    public Object getItem(int position) {
        return aVObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);

        ViewHolderNormal viewHolderNormal = null;
        ViewHolderShowMore showMore = null;
        ViewHolderSpecies viewHolderSpecies = null;

        if (null == convertView) {
            switch (type) {
                case TYPE_NORMAL:
                    convertView = layoutInflater.inflate(R.layout.item_catch_in_area, null);
                    viewHolderNormal = new ViewHolderNormal(convertView);
                    convertView.setTag(viewHolderNormal);
                    break;
                case TYPE_SHOWMORE:
                    convertView = layoutInflater.inflate(R.layout.item_catch_show_more, null);
                    showMore = new ViewHolderShowMore(convertView);
                    convertView.setTag(showMore);
                    break;
                case TYPE_SPCIES:
                    convertView = layoutInflater.inflate(R.layout.item_catch_split, null);
                    viewHolderSpecies = new ViewHolderSpecies(convertView);
                    convertView.setTag(viewHolderSpecies);
                    break;

            }
        } else {

            switch (type) {
                case TYPE_NORMAL:
                    viewHolderNormal = (ViewHolderNormal) convertView.getTag();
                    break;
                case TYPE_SHOWMORE:
                    showMore = (ViewHolderShowMore) convertView.getTag();
                    break;
                case TYPE_SPCIES:
                    viewHolderSpecies = (ViewHolderSpecies) convertView.getTag();
                    break;
            }

        }

        switch (type) {
            case TYPE_NORMAL:
                AVObject catchObject = aVObjects.get(position);
                AVUser owner = catchObject.getAVUser("owner");
                if (null != owner.getAVFile("avatar")) {
                    Picasso.with(context)
                            .load(owner.getAVFile("avatar").getUrl())
                            .config(Bitmap.Config.RGB_565)
                            .resize(150, 150)
                            .placeholder(R.drawable.icon_header_default)
                            .into(viewHolderNormal.imageViewOwnerHeader);
                } else {
                    viewHolderNormal.imageViewOwnerHeader.setImageResource(R.drawable.icon_header_default);
                }

                String ownerName = owner.getString("first_name") + " " + owner.getString("last_name");
//                LogUtil.i(App.tag, "ownerName:" + position + " " + ownerName);
                viewHolderNormal.textViewFishName.setText(ownerName);
                Number weight = catchObject.getNumber("weight");
                if (null != weight) {
                    String weightText = String.valueOf(weight.floatValue());
                    if (weightText.endsWith(".0")) {
                        viewHolderNormal.textViewWeight.setText(weight.intValue() + " kg");
                    } else {
                        viewHolderNormal.textViewWeight.setText(weight.floatValue() + " kg");
                    }
                    viewHolderNormal.textViewWeight.setVisibility(View.VISIBLE);
                } else {
                    viewHolderNormal.textViewWeight.setVisibility(View.INVISIBLE);
                }
                break;
            case TYPE_SHOWMORE:

                showMore.viewShowMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        currentSpecies = aVObjects.get(position - 1).getAVObject("fish").getString("name");
                        rebuildData(hashMap);
                        notifyDataSetChanged();
                    }
                });
                break;
            case TYPE_SPCIES:
                viewHolderSpecies.textViewSpices.setText(aVObjects.get(position).getString("fishName"));
                break;
        }


        return convertView;
    }


    class ViewHolderNormal {

        ImageView imageViewOwnerHeader;

        TextView textViewFishName;

        TextView textViewWeight;

        public ViewHolderNormal(View view) {
            imageViewOwnerHeader = (ImageView) view.findViewById(R.id.imageViewOwnerHeader);
            textViewFishName = (TextView) view.findViewById(R.id.textViewFishName);
            textViewWeight = (TextView) view.findViewById(R.id.textViewWeight);
        }
    }

    class ViewHolderSpecies {

        TextView textViewSpices;

        public ViewHolderSpecies(View view) {
            textViewSpices = (TextView) view.findViewById(R.id.textViewSpeciesName);
        }

    }

    class ViewHolderShowMore {

        View viewShowMore;

        public ViewHolderShowMore(View view) {
            viewShowMore = view;
        }
    }


    @Override
    public int getViewTypeCount() {
        return 4;
    }

    public final static int TYPE_NORMAL = 0;
    public final static int TYPE_SPCIES = 1;
    public final static int TYPE_SHOWMORE = 2;

    @Override
    public int getItemViewType(int position) {

        AVObject catchObject = aVObjects.get(position);
        if (catchObject.has("owner")) {
            return TYPE_NORMAL;
        } else {
            String indicator = catchObject.getString("indicator");
            if (indicator.startsWith("Show more")) {
                return TYPE_SHOWMORE;
            } else {

                return TYPE_SPCIES;
            }
        }
    }
}

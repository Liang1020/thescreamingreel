package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.thescreamingreel.androidapp.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 15/11/6.
 */
public class MenuAdapter extends BaseAdapter {

    private String[] menuItem = {"All catches", "Nearby catches", "My follows' catches"};

    private int index = 0;
    private LayoutInflater layoutInflater;
    private Context context;

    public MenuAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }


    public void select(int index) {
        this.index = index;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return menuItem.length;
    }

    @Override
    public Object getItem(int i) {
        return menuItem[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder = null;

        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_menu, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textViewMenucontent.setText(menuItem[i]);
        if (index == i) {
            viewHolder.imageViewCheck.setVisibility(View.VISIBLE);
            viewHolder.textViewMenucontent.setTextColor(context.getResources().getColor(R.color.text_color_ios_blue));
        } else {
            viewHolder.imageViewCheck.setVisibility(View.INVISIBLE);
            viewHolder.textViewMenucontent.setTextColor(Color.BLACK);
        }
        return convertView;
    }


    class ViewHolder {

        @InjectView(R.id.imageViewCheck)
        ImageView imageViewCheck;

        @InjectView(R.id.textViewMenucontent)
        TextView textViewMenucontent;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

}

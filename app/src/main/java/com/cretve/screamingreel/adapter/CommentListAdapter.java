package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.cretve.screamingreel.App;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 15/12/4.
 */
public class CommentListAdapter extends BaseAdapter {


    private List<AVObject> arrayListAVObjects;
    private Context context;
    private LayoutInflater layoutInflater;

    public CommentListAdapter(Context context, List<AVObject> parseDatas) {

        this.context = context;
        this.arrayListAVObjects = parseDatas;
        this.layoutInflater = LayoutInflater.from(context);

    }


    public void addCommentList(List<AVObject> objects) {
        this.arrayListAVObjects.addAll(objects);
        notifyDataSetChanged();

        LogUtil.i(App.tag, "total comment size:" + this.arrayListAVObjects.size());
    }


    @Override
    public int getCount() {
        return arrayListAVObjects.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListAVObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_comment, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        AVObject notification = arrayListAVObjects.get(position);

        AVUser aVUser = notification.getAVUser("sender");


        try {
            viewHolder.textViewMyName.setText(aVUser.getString("first_name") + " " + aVUser.getString("last_name"));
        } catch (Exception e) {
        }


        AVUser sender = notification.getAVUser("sender");

        if (null != sender && sender.has("avatar") && null != sender.getAVFile("avatar")) {
            AVFile headerFile = sender.getAVFile("avatar");
            Picasso.with(context).load(headerFile.getUrl()).resize(150, 150).placeholder(R.drawable.icon_header_default).config(Bitmap.Config.RGB_565).into(viewHolder.imageViewMyHeader);
        } else {
            viewHolder.imageViewMyHeader.setImageResource(R.drawable.icon_header_default);
        }

        if ("Comment".equals(notification.get("type"))) {
            AVObject comment = notification.getAVObject("comment");
            viewHolder.textViewCommentContent.setText(comment.getString("content"));

        } else if ("Like".equals(notification.getString("type"))) {

            viewHolder.textViewCommentContent.setText("liked the catch");
        }

        AVObject pcatch = notification.getAVObject("catch");

        if (null != pcatch && !ListUtiles.isEmpty(pcatch.getList("photos"))) {

            List<AVFile> pfs = pcatch.getList("photos");
            Picasso.with(context).load(pfs.get(0).getUrl())
                    .resize(150, 150)
                    .centerCrop()
                    .config(Bitmap.Config.RGB_565)
                    .placeholder(R.drawable.image_placeholder)
                    .into(viewHolder.imageViewCommenter);
        } else {
            Picasso.with(context)
                    .load(R.drawable.image_placeholder)
                    .resize(150, 150).
                    centerCrop().
                    into(viewHolder.imageViewCommenter);
        }
        return convertView;
    }

    class ViewHolder {


        @InjectView(R.id.imageViewMyHeader)
        ImageView imageViewMyHeader;

        @InjectView(R.id.imageViewCommenter)
        ImageView imageViewCommenter;

        @InjectView(R.id.textViewMyName)
        TextView textViewMyName;

        @InjectView(R.id.textViewCommentContent)
        TextView textViewCommentContent;


        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}

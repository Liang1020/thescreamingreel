package com.cretve.screamingreel.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.LogInCallback;
import com.avos.avoscloud.RequestPasswordResetCallback;
import com.avos.avoscloud.SaveCallback;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.FileUtils;
import com.common.util.MyAnimationUtils;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.indicator.view.IndicatorView;
import com.thescreamingreel.androidapp.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by wangzy on 15/10/29.
 */
public class WelcomeAdapter extends PagerAdapter {


    private ArrayList<View> views;

    private Context context;

    private ViewPager viewPager;

    private ViewPager imageViewPager;

    private IndicatorView indicatorView;

    private EditText editTextEmail;
    private EditText editTextPwd;

    private EditText editTextEmailRegister;
    private EditText editTextPwdRegister;
    private EditText editTextPwdRepeat;

    private OnLoginClickInBlockListener onLoginClickInBlockListener;


    private View loginBlock;
    private View registerBlock;
    public View viewCover;
    private View guild4;
    private View viewLabel;


    @Override
    public int getCount() {
        return views.size();
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(views.get(position));
        return views.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(views.get(position));
    }


    private void getFacebookUserDetails(final AVUser user, final LogInCallback callback) {

        Bundle bundle = new Bundle();
        bundle.putString("fields", "first_name, last_name, email, gender, locale, picture, hometown, location");
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me",
                bundle,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        // Application code
                        Log.e("myApp", "get facebook user details");

                        JSONObject object = response.getJSONObject();
                        if (object == null) {
                            callback.done(user, null);
                        }

                        try {

                            AVUser.getCurrentUser().put("fromFb", true);

                            StringBuilder fullName = new StringBuilder();


                            if (object.get("first_name") != null) {
                                fullName.append(object.getString("first_name"));
                                user.put("first_name", object.get("first_name"));
                            }

                            if (object.get("last_name") != null) {
                                fullName.append(" " + object.getString("last_name"));
                                user.put("last_name", object.get("last_name"));
                            }

                            if (!StringUtils.isEmpty(fullName.toString())) {
                                user.put("full_name", fullName.toString());
                            }

                            if (object.get("locale") != null) {
                                String str = (String) object.get("locale");
                                String newStr = str.substring(str.indexOf("_") + 1);
                                user.put("country", newStr);
                            }

                            user.put("facebookId", object.get("id"));


//                            if (object.get("location") != null) {
//
//                                try {
//                                    String city = object.getJSONObject("location").getString("name").split(",")[0];
////                                    LogUtil.e(App.tag, "fb location city:" + city);
//                                    attchCityFromFb(city);
//                                } catch (Exception e) {
//
//                                }
//                            }

                            if (object.get("gender") != null) {
                                String sex = (String) object.get("gender");
                                if (sex.equalsIgnoreCase(context.getString(R.string.regist_gender_female))) {
                                    sex = context.getString(R.string.regist_gender_female);
                                } else {
                                    sex = context.getString(R.string.regist_gender_male);
                                }
                                user.put("gender", sex);
                            }

                            if (object.get("email") != null)
                                user.put("contact_email", object.get("email"));

                            if (object.get("id") != null && user.get("avatar") == null) {

                                final String url = "https://graph.facebook.com/" + object.get("id") + "/picture?type=large&return_ssl_resources=1";

                                new BaseTask(new NetCallBack() {

                                    @Override
                                    public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                                        try {
                                            byte[] data = FileUtils.readFile2ByteUrl(url);
                                            AVUser aVUser = AVUser.getCurrentUser();
                                            final AVFile headerFile = new AVFile(aVUser.getObjectId(), data);
//                                            headerFile.save();
//                                            AVUser.getCurrentUser().put("avatar", headerFile);
//                                            user.saveEventually();
//
                                            headerFile.saveInBackground(new SaveCallback() {
                                                @Override
                                                public void done(AVException e) {
                                                    if (null == e) {
                                                        //save fail
                                                        AVUser.getCurrentUser().put("avatar", headerFile);

                                                    } else {

                                                    }


                                                    ((BaseScreamingReelActivity) context).saveAVObject(user, false, false, null);

//                                                    AVUser.getCurrentUser().saveInBackground();

//                                                    user.saveEventually();
                                                    callback.done(user, null);
                                                }
                                            });

                                            return new NetResult();
                                        } catch (Exception e) {
                                            //save file error
//                                            user.saveInBackground();
//                                            callback.done(user, null);
                                        }
                                        return null;
                                    }

                                    @Override
                                    public void onFinish(NetResult result, BaseTask baseTask) {
                                        super.onFinish(result, baseTask);
                                        callback.done(user, null);

                                    }
                                }).execute(new HashMap<String, String>());


                            } else {
                                callback.done(user, null);

//                                user.saveEventually();

                                ((BaseScreamingReelActivity) context).saveAVObject(user, false, false, null);

                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                            callback.done(null, new AVException(-1, "login error"));
                        }

                    }
                }
        ).executeAsync();
    }

    public void loginWithFacebook(final LogInCallback callback) {
        Collection<String> permissions = Arrays.asList("public_profile", "email", "user_friends", "user_hometown", "user_location");


//      ((LoginButton) guild4.findViewById(R.id.login_button)).setLoginBehavior(LoginBehavior.WEB_ONLY);

//        ParseFacebookUtils.logInWithReadPermissionsInBackground((Activity) context, permissions, new LogInCallback() {
//            @Override
//            public void done(AVUser user, AVException err) {
//                if (user == null) {
//                    //loginWithFacebook(callback);
//                    ((BaseActivity) context).hideProgressDialog();
//                    return;
//                }
//                getFacebookUserDetails(AVUser.getCurrentUser(), callback);
//            }
//        });

    }

    public WelcomeAdapter(final Context context, final ViewPager viewPager) {
        this.context = context;
        this.views = new ArrayList<View>();
        this.viewPager = viewPager;

        final View guild1 = View.inflate(context, R.layout.page_welcome_1, null);
        final View guild2 = View.inflate(context, R.layout.page_welcome_2, null);
        final View guild3 = View.inflate(context, R.layout.page_welcome_3, null);
        guild4 = View.inflate(context, R.layout.page_welcome_4, null);

        viewCover = (View) guild4.findViewById(R.id.viewCover);

        views.add(guild1);
        views.add(guild2);
        views.add(guild3);
        views.add(guild4);


        TextView textViewLoginLabel = (TextView) guild4.findViewById(R.id.textViewLoginEmailLabel);
        textViewLoginLabel.setText(Html.fromHtml("Login with <b>Email</b>"));

        TextView textViewRegisterEmailLabel = (TextView) guild4.findViewById(R.id.textViewRegisterEmailLabel);
        textViewRegisterEmailLabel.setText(Html.fromHtml("Register with <b>Email</b>"));


        TextView textViewLoginFb = (TextView) guild4.findViewById(R.id.textViewLoginFb);
        textViewLoginFb.setText(Html.fromHtml("Login with <b>Facebook</b>"));

        TextView textViewLoginEmail = (TextView) guild4.findViewById(R.id.textViewLoginEmail);
        textViewLoginEmail.setText(Html.fromHtml("Login with <b>Email</b>"));


        TextView textViewTap2Register = (TextView) guild4.findViewById(R.id.textViewTap2Register);
        textViewTap2Register.setText(Html.fromHtml("Don't have an account? Tap to <b>Register</b>"));

        viewLabel = guild4.findViewById(R.id.linearLayoutLabelContent);
        registerBlock = guild4.findViewById(R.id.linearlayout_block_register);
        loginBlock = guild4.findViewById(R.id.linearlayout_block_login);
        final View buttonCancelInBlock = guild4.findViewById(R.id.buttonCancelInBlock);

        editTextEmail = (EditText) guild4.findViewById(R.id.editTextEmail);
        editTextPwd = (EditText) guild4.findViewById(R.id.editTextPwd);

        editTextEmailRegister = (EditText) registerBlock.findViewById(R.id.editTextEmailRegister);
        editTextPwdRegister = (EditText) registerBlock.findViewById(R.id.editTextPwdRegister);
        editTextPwdRepeat = (EditText) registerBlock.findViewById(R.id.editTextPwdRepeatRegister);

        registerBlock.findViewById(R.id.buttonRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewCover.setVisibility(View.VISIBLE);

                if (null != onLoginClickInBlockListener) {
                    onLoginClickInBlockListener.onRegisterClickInBlock(editTextEmailRegister, editTextPwdRegister, editTextPwdRepeat);
                }

            }
        });


        guild4.findViewById(R.id.textViewForgetPassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginBlock.setVisibility(View.GONE);
                viewCover.setVisibility(View.GONE);


                DialogUtils.showInputDialog2WithKeyord((Activity) context, "Enter Email", "Confirm", "Cancel", "", "Please enter the email address for your account.", new DialogCallBackListener() {

                    @Override
                    public void onCallBack(boolean yesNo, String text, DialogInterface dialog) {

                        viewCover.setVisibility(View.GONE);

                        MyAnimationUtils.animationShowView(viewLabel, AnimationUtils.loadAnimation(context, android.R.anim.fade_in));

                        if(yesNo){

                            AVUser.requestPasswordResetInBackground(text, new RequestPasswordResetCallback() {
                                @Override
                                public void done(AVException e) {
                                    if (null != e) {
                                        ((BaseScreamingReelActivity) context).showLeanCloudError(e);
                                    }else{
                                        String msg="A reset password email has been sent to your email address. Please process it in 48 hours.";
                                        Tool.showMessageDialog(msg,(Activity) context);
                                    }
                                }
                            });
                        }

                    }
                });

            }
        });


        textViewTap2Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewCover.setVisibility(View.VISIBLE);
                //tap register.
                if (loginBlock.getVisibility() == View.VISIBLE) {
                    MyAnimationUtils.animationHideview(loginBlock, AnimationUtils.loadAnimation(context, android.R.anim.fade_out));
                }
                MyAnimationUtils.animationShowView(registerBlock, AnimationUtils.loadAnimation(context, android.R.anim.fade_in));
                MyAnimationUtils.animationHideview(viewLabel, AnimationUtils.loadAnimation(context, android.R.anim.fade_out));
            }
        });

        registerBlock.findViewById(R.id.buttonCancelInBlockRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewCover.setVisibility(View.GONE);
                MyAnimationUtils.animationShowView(viewLabel, AnimationUtils.loadAnimation(context, android.R.anim.fade_in));
                MyAnimationUtils.animationHideview(registerBlock, AnimationUtils.loadAnimation(context, android.R.anim.fade_out));
            }
        });


        buttonCancelInBlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewCover.setVisibility(View.GONE);
                MyAnimationUtils.animationShowView(viewLabel, AnimationUtils.loadAnimation(context, android.R.anim.fade_in));
                MyAnimationUtils.animationHideview(loginBlock, AnimationUtils.loadAnimation(context, android.R.anim.fade_out));
            }
        });


        guild4.findViewById(R.id.viewLoginEmail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewCover.setVisibility(View.VISIBLE);

                if (registerBlock.getVisibility() == View.VISIBLE) {
                    MyAnimationUtils.animationHideview(registerBlock, AnimationUtils.loadAnimation(context, android.R.anim.fade_out));
                }

                MyAnimationUtils.animationShowView(loginBlock, AnimationUtils.loadAnimation(context, android.R.anim.fade_in));
                MyAnimationUtils.animationHideview(viewLabel, AnimationUtils.loadAnimation(context, android.R.anim.fade_out));
            }
        });


        guild4.findViewById(R.id.buttonLoginInBlock).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != onLoginClickInBlockListener) {
                    onLoginClickInBlockListener.onLoginClickInBlock(editTextEmail, editTextPwd);
                }
            }
        });


        guild4.findViewById(R.id.viewLoginFb).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                ((BaseActivity) context).showProgressDialog();

                if(null!=onLoginClickInBlockListener){

                    onLoginClickInBlockListener.onLoginFBClick();
                }

//                loginWithFacebook(new LogInCallback() {
//                    @Override
//                    public void done(AVUser user, AVException err) {
//                        ((BaseActivity) context).hideProgressDialog();
//
//                        if (null == err) {
//                            if (user == null) {
//                                ((BaseActivity) context).showNotifyTextIn5Seconds(R.string.login_hint_facebook_cancelled);
//                            } else {
//                                Log.d("MyApp", "User logged in through Facebook!");
//                                if (null != onLoginClickInBlockListener) {
//                                    onLoginClickInBlockListener.onLoginFb(true);
//                                }
//                            }
//                        }
//                    }
//                });



            }
        });

    }


    public static interface OnLoginClickInBlockListener {

        public void onLoginClickInBlock(EditText editTextLogin, EditText editTextPwd);

        public void onRegisterClickInBlock(EditText editTextRegister, EditText editPwd, EditText editPwdRepeat);

        public void onLoginFb(boolean isNewFbUser);

        public void onLoginFBClick();
    }

    public OnLoginClickInBlockListener getOnLoginClickInBlock() {
        return onLoginClickInBlockListener;
    }

    public void setOnLoginClickInBlock(OnLoginClickInBlockListener onLoginClickInBlock) {
        this.onLoginClickInBlockListener = onLoginClickInBlock;
    }

    public void clearInputText(){


        if(null!=loginBlock){

            ((TextView)loginBlock.findViewById(R.id.editTextEmail)).setText("");
            ((TextView)loginBlock.findViewById(R.id.editTextPwd)).setText("");

        }


        if(null!=registerBlock){
            ((TextView)registerBlock.findViewById(R.id.editTextEmailRegister)).setText("");
            ((TextView)registerBlock.findViewById(R.id.editTextPwdRegister)).setText("");
            ((TextView)registerBlock.findViewById(R.id.editTextPwdRepeatRegister)).setText("");
        }


    }

    public EditText getEditTextPwd() {
        return editTextPwd;
    }

    public void setEditTextPwd(EditText editTextPwd) {
        this.editTextPwd = editTextPwd;
    }
}


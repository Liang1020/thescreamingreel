package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.avos.avoscloud.AVFile;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.parse.ParseHelper;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wangzy on 15/11/3.
 */
public class CatchDetailImageAdapter extends PagerAdapter {


    private ArrayList<View> views;

    private Context context;

    private ViewPager viewPager;

    private int w;
    private List<AVFile> aVFiles;

//    private GalleryCache gallcach;

    private LayoutInflater layoutInflater;
    private OnItemClickListener OnItemClickListener;
    private Point point;


    public CatchDetailImageAdapter(final Context context, final ViewPager viewPager, int w, List<AVFile> aVFiles) {
        this.context = context;
        this.views = new ArrayList<View>();
        this.viewPager = viewPager;
        this.w = w;
        this.aVFiles = aVFiles;
        this.layoutInflater = LayoutInflater.from(context);

         point = Tool.getDisplayMetrics(context);

//        final int memClass = ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE))
//                .getMemoryClass();
//        final int size = 1024 * 1024 * memClass / 8;
//        this.gallcach = new GalleryCache(size, point.x, point.x);

        for (int i = 0, isize = aVFiles.size(); i < isize; i++) {
            View view = layoutInflater.inflate(R.layout.item_catch_detail_pic, null);
            views.add(view);
        }
    }


    @Override
    public int getCount() {
        return aVFiles.size();
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(w, w);

        View view = views.get(position);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageViewSrc);
        ImageView imageviewPlay = (ImageView) view.findViewById(R.id.imageviewPlay);

        AVFile pf = aVFiles.get(position);

        if(null!=pf){
            final String url = aVFiles.get(position).getUrl();
            LogUtil.i(App.tag, "fileUrl:" + url);
            if (ParseHelper.isVideo(url)) {
//            imageView.setTag(url);
//            GalleryCache.getInstance(context, point.x,point.x).loadBitmap(url, imageView, R.color.bg_color);
                imageviewPlay.setVisibility(View.VISIBLE);

                App.getApp().getPicasso().load("videoframe://"+url)
                        .resize(w,w)
                        .config(Bitmap.Config.RGB_565)
                        .into(imageView);


            } else {
                Picasso.with(context).load(url).resize(w, w).placeholder(R.drawable.image_placeholder).into(imageView);
                imageviewPlay.setVisibility(View.INVISIBLE);
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(null!=OnItemClickListener){
                        OnItemClickListener.onItemClick(position,url);
                    }
                }
            });
        }else{
            Picasso.with(context).load(R.drawable.image_placeholder).resize(w, w).into(imageView);
            imageviewPlay.setVisibility(View.INVISIBLE);
        }

        container.addView(view, layoutParams);
        return views.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(views.get(position));
    }


    public static interface OnItemClickListener {
        public void onItemClick(int pos, String path);
    }

    public CatchDetailImageAdapter.OnItemClickListener getOnItemClickListener() {
        return OnItemClickListener;
    }

    public void setOnItemClickListener(CatchDetailImageAdapter.OnItemClickListener onItemClickListener) {
        OnItemClickListener = onItemClickListener;
    }
}

package com.cretve.screamingreel.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.common.util.StringUtils;
import com.thescreamingreel.androidapp.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 15/11/3.
 */
public class LocationAdapter extends BaseAdapter {

    private Context context;
    private List<AVObject> arrayListLocations;
    private LayoutInflater layoutInflater;
    private int margin = 0;

    public LocationAdapter(Context context, List<AVObject> aVObjects) {
        this.context = context;
        this.arrayListLocations = aVObjects;
        this.layoutInflater = LayoutInflater.from(context);
        this.margin = context.getResources().getDimensionPixelSize(R.dimen.margin_content);
    }


    @Override
    public int getCount() {
        return arrayListLocations.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayListLocations.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        ViewHolder viewHolder = null;
        if (null == convertView) {
            convertView = layoutInflater.inflate(R.layout.item_location, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        AVObject parseObj = arrayListLocations.get(i);

        if (parseObj.get("name").equals("City") || parseObj.get("name").equals("Country")) {
            viewHolder.textViewLocation.setBackgroundColor(Color.parseColor("#EFEFF4"));
            viewHolder.textViewLocation.setText(String.valueOf(parseObj.get("name")));
            viewHolder.textViewLocation.setPadding((int) (margin * 1.5), 0, 0, 0);

        } else {
            viewHolder.textViewLocation.setBackgroundResource(R.drawable.shape_block_white);
            viewHolder.textViewLocation.setPadding(margin, 0, 0, 0);

            if(StringUtils.isEmpty(parseObj.getString("geoId"))){
                viewHolder.textViewLocation.setText(String.valueOf(parseObj.get("name")));
            }else{
                viewHolder.textViewLocation.setText(String.valueOf(parseObj.get("name") + " " + parseObj.get("country")));
            }

        }


        return convertView;
    }


    static class ViewHolder {

        @InjectView(R.id.textViewLocation)
        TextView textViewLocation;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }
}

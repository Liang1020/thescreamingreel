/**
 * Copyright (C) 2014-2015 Imtoy Technologies. All rights reserved.
 *
 * @charset UTF-8
 * @author xiong_it
 */
package com.cretve.screamingreel.service;

import android.Manifest;
import android.app.IntentService;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.avos.avoscloud.AVGeoPoint;
import com.common.util.LogUtil;
import com.cretve.screamingreel.App;

/**
 * @author xiong_it
 * @description
 * @charset UTF-8
 * @date 2015-7-20上午10:31:39
 */
public class LocationService extends IntentService implements LocationListener {

    private static final String SERVICE_NAME = "LocationService";
    private static final long MIN_TIME = 1000l;
    private static final float MIN_DISTANCE = 1000f;

    private LocationManager locationManager;

    public LocationService() {
        super(SERVICE_NAME);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        LogUtil.i(App.tag, "onHandleIntent");
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);


        if (locationManager.getProvider(LocationManager.GPS_PROVIDER) != null) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME, MIN_DISTANCE, this);

        } else if (locationManager.getProvider(LocationManager.NETWORK_PROVIDER) != null) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);

        } else {
            Toast.makeText(this, "Can not location,please turn on location service", Toast.LENGTH_SHORT).show();
            Intent i = new Intent();
            i.setAction(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(i);
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        if (null != location) {
            App.getApp().setAvGeoPoint(new AVGeoPoint(location.getLatitude(), location.getLongitude()));
            App.getApp().setLocation(location);

            Toast.makeText(this, "Location success!", Toast.LENGTH_SHORT).show();
            LogUtil.i(App.tag, "定位成功，停止定位服务。");
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.removeUpdates(this);
            stopSelf();
        } else {
            LogUtil.i(App.tag, "定位不成功。");
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        LogUtil.i(App.tag, "enable onStatusChanged:" + provider);
    }

    @Override
    public void onProviderEnabled(String provider) {

        LogUtil.i(App.tag, "enable onProviderEnabled:" + provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        LogUtil.i(App.tag, "enable onProviderDisabled:" + provider);
    }


}

package com.cretve.screamingreel;

import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;
import com.squareup.picasso.RequestHandler;

import java.io.IOException;
import java.util.HashMap;

public class PicassoVideoFrameRequestHandler extends RequestHandler {
    public static final String SCHEME = "videoframe";

    @Override
    public boolean canHandleRequest(Request data) {
        return SCHEME.equals(data.uri.getScheme());
    }

    @Override
    public Result load(Request data, int networkPolicy) throws IOException {


        MediaMetadataRetriever retriever = new MediaMetadataRetriever();

        String uriText=data.uri.toString();

        String url = uriText.replace(SCHEME+"://","");

        if (url.startsWith("http")) {
            retriever.setDataSource(url, new HashMap<String, String>());
        } else {
            retriever.setDataSource(url);
        }

        Bitmap bitmap = retriever.getFrameAtTime();


        return new Result(bitmap, Picasso.LoadedFrom.DISK);
    }

}
package com.cretve.screamingreel.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.bigkoo.pickerview.view.BasePickerView;
import com.thescreamingreel.androidapp.R;

public class AddPhotoDialogInCatchList extends BasePickerView {

    public OnAddCatchListener onAddCatchListener;
    private TextView textViewTitle;

    public AddPhotoDialogInCatchList(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.dialog_main_add_catch, contentContainer);

        textViewTitle=(TextView) findViewById(R.id.textViewTitle);

        findViewById(R.id.buttonCancelOnAddCatch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        findViewById(R.id.buttonSkipPhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if(null!=onAddCatchListener){
                    onAddCatchListener.onSkipClick();
                }
            }
        });

        findViewById(R.id.buttonChosePhotoOnAddCatch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if(null!=onAddCatchListener){
                    onAddCatchListener.onChoosePhotoClick();
                }
            }
        });

        findViewById(R.id.buttonTakePhotoOnAddCatch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();

                if(null!=onAddCatchListener){
                    onAddCatchListener.onTakePhotoClick();
                }
            }
        });
    }


    public void hideCancelButton(){
        findViewById(R.id.buttonCancelOnAddCatch).setVisibility(View.GONE);
    }


    public void setdTitle(String title){
        textViewTitle.setText(title);
    }


    public static interface OnAddCatchListener{

        public void onTakePhotoClick();
        public void onChoosePhotoClick();
        public void onSkipClick();
    }

    public OnAddCatchListener getOnAddCatchListener() {
        return onAddCatchListener;
    }

    public void setOnAddCatchListener(OnAddCatchListener onAddCatchListener) {
        this.onAddCatchListener = onAddCatchListener;
    }

    @Override
    public void dismiss() {

        rootView.setVisibility(View.VISIBLE);
        rootView.clearAnimation();

        super.dismiss();



    }
}
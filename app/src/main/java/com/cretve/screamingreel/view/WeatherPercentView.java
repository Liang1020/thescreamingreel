package com.cretve.screamingreel.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by wangzy on 15/12/25.
 */
public class WeatherPercentView extends View {

    private int w;
    private int h;

    private int progressColor = 0xff1188ff;
    private int defaultColor = 0xffe2e2e2;
    private Paint paint;
    private float percent = 0.5f;


    public WeatherPercentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        paint.setAntiAlias(true);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        postInvalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        w = getWidth();
        h = getHeight();
        canvas.drawColor(Color.WHITE);

        int dotx = w / 2;
        int doty = h;

//        int r = Math.min(w, h);
        int r=w/2;
        paint.setColor(defaultColor);
        canvas.drawCircle(dotx, doty, r, paint);
        //======

        paint.setColor(progressColor);
        RectF rect = new RectF();
        rect.left = w / 2 - r;
        rect.top = h - r;
        rect.bottom = h + r;
        rect.right = w / 2 + r;
        paint.setColor(progressColor);
        if (percent > 1) {
            percent = percent * 1.0f / 100;
        }

        float gree = 180 * percent;
        if (gree > 180) {
            gree = 180;
        }
        canvas.drawArc(rect, 180.0f, gree, true, paint);
        //======
        paint.setColor(Color.WHITE);
        canvas.drawCircle(dotx, doty, r - 20, paint);

//        paint.setColor(Color.RED);
//        canvas.drawCircle(dotx, doty, w/2, paint);

    }

    public float getPercent() {
        return percent;
    }

    public void setPercent(float percent) {
        this.percent = percent;
        invalidate();
    }
}

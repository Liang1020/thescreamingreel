package com.cretve.screamingreel.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.thescreamingreel.androidapp.R;


/**
 * Created by wangzy on 15/12/25.
 */
public class DirectionView extends View {


    private Bitmap bitmap;
    private int bw;
    private int bh;
    private float degree = 20;
    private Paint paint;

    public DirectionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_direct);
        bw = bitmap.getWidth();
        bh = bitmap.getHeight();
        paint = new Paint();
        paint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int w = getWidth();
        int h = getHeight();

        int cw = w / 2;
        int ch = h / 2;

        canvas.drawColor(Color.WHITE);

//        Matrix matrix=new Matrix();
//        canvas.drawBitmap();

        int left = cw - bw / 2;
        int top = ch - bh / 2;

        Matrix matrix = new Matrix();
        matrix.postRotate(degree, bw / 2, bh / 2);
        matrix.postScale(0.8f, 0.8f, bw / 2, bh / 2);


        Bitmap bm = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                bitmap.getHeight(), matrix, true);


        canvas.drawBitmap(bm, left, top, paint);
        bm.recycle();

    }


    public float getDegree() {
        return degree;
    }

    public void setDegree(float degree) {
        this.degree = degree;
        postInvalidate();
    }
}

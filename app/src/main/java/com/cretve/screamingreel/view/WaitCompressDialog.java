package com.cretve.screamingreel.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.bigkoo.pickerview.view.BasePickerView;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.adapter.CompressAdapter;
import com.thescreamingreel.androidapp.R;
import com.yink.sardar.CompressCompeleteListener;
import com.yink.sardar.CompressListener;
import com.yink.sardar.Compressor;
import com.yink.sardar.InitListener;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by wangzy on 15/12/30.
 */
public class WaitCompressDialog extends BasePickerView {

    private Button buttonCancel;
    private Button buttonPost;
    private ArrayList<String> arrayListFiles;
    private OnCompressDialogListener onCompressDialogListener;
    private CompressAdapter compressAdapter;
    private ListView listView;

    public WaitCompressDialog(Context context,ArrayList<String> fileList) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_compress_list, contentContainer);
        listView=(ListView) findViewById(R.id.listView);

        this.arrayListFiles=fileList;
        compressAdapter=new CompressAdapter(context,this.arrayListFiles);
        listView.setAdapter(compressAdapter);

        buttonCancel = (Button) findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        buttonPost = (Button) findViewById(R.id.buttonPostCompress);
        buttonPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public OnCompressDialogListener getOnCompressDialogListener() {
        return onCompressDialogListener;
    }

    public void setOnCompressDialogListener(OnCompressDialogListener onCompressDialogListener) {
        this.onCompressDialogListener = onCompressDialogListener;
    }

    public static  interface OnCompressDialogListener{

        public void onConfirm();
        public void onCancel();
        public void onComplete();
        public void onDelete(String file);

    }




    private void compressVideo(final ArrayList<String> files, final CompressCompeleteListener compressCompeleteListener) {

        if (ListUtiles.isEmpty(files)) {
            if (null != compressCompeleteListener) {
                compressCompeleteListener.onExecSuccess(files, "done");
            }
            return;
        }

        final ArrayList<String> compressedFiels = new ArrayList<String>();

        for (final String file : files) {


            LogUtil.i(App.tag, "before video compress:" + (new File(file)).length());

            compressVideo(file, file + "compressed.mp4", new CompressListener() {
                @Override
                public void onExecSuccess(String message) {

                    compressedFiels.add(file + "compressed.mp4");

                    LogUtil.i(App.tag, "after video compress:" + (new File(file + "compressed.mp4")).length());

                    if (compressedFiels.size() == files.size()) {
                        compressCompeleteListener.onExecSuccess(compressedFiels, message);
                    }
                }

                @Override
                public void onExecFail(String reason) {
                    if (null != compressCompeleteListener) {
                        compressCompeleteListener.onExecProgress(file, reason);
                    }
                }

                @Override
                public void onExecProgress(String message) {
                    LogUtil.i(App.tag,"compress message:"+message);

                    if (null != compressCompeleteListener) {
                        compressCompeleteListener.onExecProgress(file, message);
                    }

                }
            });
        }
    }




    public Compressor com;
    String cmd;

    public void compressVideo(String src, String dst, final CompressListener compressListener) {

//      cmd = "-y -i /mnt/sdcard/videokit/in1.mp4 -strict -2 -vcodec libx264 -preset ultrafast -crf 24 -acodec aac -ar 44100 -ac 2 -b:a 96k -s 640x352 -aspect 16:9 /mnt/sdcard/videokit/out8.mp4";
        //ffmpe ffmpeg命令
        cmd = "-y -i " + src + " -strict -2 -vcodec h264 -preset ultrafast -crf 24 -acodec aac -ar 44100 -ac 2 -b:a 96k -s 480x360 -aspect 16:9 " + dst;

        if(null==com){
            com = new Compressor(context);
            try {
                com.loadBinary(new InitListener() {
                    @Override
                    public void onLoadSuccess() {
                        com.execCommand(cmd, compressListener);
                    }

                    @Override
                    public void onLoadFail(String reason) {
                        LogUtil.e(App.tag, "ffmpeg init error:" + reason);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            com.execCommand(cmd, compressListener);
        }
    }
}

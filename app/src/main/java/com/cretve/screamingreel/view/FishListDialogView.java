package com.cretve.screamingreel.view;

import android.content.Context;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.common.adapter.TextWatcherAdapter;
import com.common.util.ListUtiles;
import com.common.util.StringUtils;
import com.cretve.screamingreel.activity.MainActivity;
import com.cretve.screamingreel.adapter.FishAdapter;
import com.thescreamingreel.androidapp.R;

import java.util.List;

/**
 * Created by wangzy on 15/12/9.
 */
public class FishListDialogView extends MyBasePicker {

    private EditText editTextInput;
    private ListView listViewFishes;
    private MainActivity mainActivity;
    private AVQuery<AVObject> fishQuery;
    private FishAdapter fishAdapter;
    private OnFishItemClickListener onFishItemClickListener;


    public FishListDialogView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_fish_collection, contentContainer);
        this.mainActivity = (MainActivity) context;

        findViewById(R.id.viewClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onFishItemClickListener) {
                    onFishItemClickListener.onCloseFishDialog();
                }
            }
        });


        editTextInput = (EditText) findViewById(R.id.editFinshInput);
        editTextInput.addTextChangedListener(new TextWatcherAdapter() {
            @Override
            public void afterTextChanged(Editable s) {
                onInputChanged(mainActivity.getInput(editTextInput));
            }
        });

        listViewFishes = (ListView) findViewById(R.id.listViewFishes);

        editTextInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    listViewFishes.setVisibility(View.VISIBLE);
                }
            }
        });

        loadFish("");
    }


    @Override
    public void show() {
        super.show();
        editTextInput.setHint("Search fish");
        listViewFishes.setVisibility(View.VISIBLE);
    }

    public void onInputChanged(String text) {
        listViewFishes.setVisibility(View.VISIBLE);

        loadFish(text);
    }


    public void loadFish(String inputText) {

        fishQuery = AVQuery.getQuery("Fish");

        if (!StringUtils.isEmpty(inputText)) {
            fishQuery.whereMatches("name", ".*" + inputText + ".*", "i");
        }

        fishQuery.orderByAscending("name");
        fishQuery.setLimit(1000);
        fishQuery.findInBackground(new FindCallback<AVObject>() {

            @Override
            public void done(List<AVObject> objects, AVException e) {
                if (!ListUtiles.isEmpty(objects)) {
                    buildAdapter(objects);
                }
            }
        });

    }


    private void buildAdapter(final List<AVObject> fishes) {
        editTextInput.setHint("Search other fish");


        fishAdapter = new FishAdapter(mainActivity, fishes);
        listViewFishes.setAdapter(fishAdapter);
        listViewFishes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                listViewFishes.setVisibility(View.GONE);

                if (null != onFishItemClickListener) {
                    onFishItemClickListener.onFishSelect((AVObject) parent.getItemAtPosition(position), position);
                }
            }
        });

    }

    public static interface OnFishItemClickListener {
        public void onFishSelect(AVObject fishObj, int position);

        public void onCloseFishDialog();
    }

    public OnFishItemClickListener getOnFishItemClickListener() {
        return onFishItemClickListener;
    }

    public void setOnFishItemClickListener(OnFishItemClickListener onFishItemClickListener) {
        this.onFishItemClickListener = onFishItemClickListener;
    }

    public ListView getListViewFishes() {
        return listViewFishes;
    }

    public void setListViewFishes(ListView listViewFishes) {
        this.listViewFishes = listViewFishes;
    }
}

package com.cretve.screamingreel.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.bigkoo.pickerview.view.BasePickerView;
import com.thescreamingreel.androidapp.R;

public class DialogInviteFBView extends BasePickerView {

    public OnFBFollowListener onFBFollowListener;

    public DialogInviteFBView(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.dialog_photo_select, contentContainer);


        Button btn = (Button) findViewById(R.id.buttonTakePhoto);
        btn.setText(R.string.invite_method_facebook);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onFBFollowListener) {
                    onFBFollowListener.onFaceBookClick();
                }
            }
        });


        btn = (Button) findViewById(R.id.buttonChosePhoto);
        btn.setText(R.string.invite_method_email);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onFBFollowListener) {
                    onFBFollowListener.onEmailClick();
                }
            }
        });


        findViewById(R.id.buttonDismiss).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


    }


    public static interface OnFBFollowListener {

        public void onFaceBookClick();

        public void onEmailClick();

    }


    public void setOnFBFollowListener(OnFBFollowListener onAddCatchListener) {
        this.onFBFollowListener = onAddCatchListener;
    }
}
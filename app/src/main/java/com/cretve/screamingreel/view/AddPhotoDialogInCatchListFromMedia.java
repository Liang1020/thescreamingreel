package com.cretve.screamingreel.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.bigkoo.pickerview.view.BasePickerView;
import com.thescreamingreel.androidapp.R;

public class AddPhotoDialogInCatchListFromMedia extends BasePickerView {

    public OnAddMediaListener onAddMediaListener;
    private TextView textViewTitle;

    public AddPhotoDialogInCatchListFromMedia(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.dialog_main_add_catch_from_media, contentContainer);

        textViewTitle = (TextView) findViewById(R.id.textViewTitle);

        findViewById(R.id.buttonAddMedia).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != onAddMediaListener) {
                    onAddMediaListener.onAddMediaClick();
                }
                dismiss();
            }
        });
        findViewById(R.id.buttonCancelOnAddCatch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        findViewById(R.id.buttonSkipPhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onAddMediaListener) {
                    onAddMediaListener.onSkipClick();
                }
            }
        });

    }

    public void hideSkipButton(){

        findViewById(R.id.buttonSkipPhoto).setVisibility(View.GONE);
    }


    public void setdTitle(String title) {
        textViewTitle.setText(title);
    }


    public static interface OnAddMediaListener {

        public void onAddMediaClick();

        public void onSkipClick();
    }

    public OnAddMediaListener getOnAddMediaListener() {
        return onAddMediaListener;
    }

    public void setOnAddMediaListener(OnAddMediaListener onAddMediaListener) {
        this.onAddMediaListener = onAddMediaListener;
    }
}
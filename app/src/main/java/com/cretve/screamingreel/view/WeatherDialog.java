package com.cretve.screamingreel.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.os.AsyncTask;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVGeoPoint;
import com.common.BaseActivity;
import com.common.adapter.TextWatcherAdapter;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.adapter.WeatherAdapter;
import com.cretve.screamingreel.adapter.WeatherAdapter24h;
import com.cretve.screamingreel.bean.TideBean;
import com.cretve.screamingreel.bean.WeatherBean;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.thescreamingreel.androidapp.R;
import com.xlist.pull.refresh.XListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by wangzy on 15/12/25.
 */
public class WeatherDialog extends MyBasePicker implements XListView.IXListViewListener {


    private TextView textView24Hour;
    private TextView textViewWeek;
    private EditText editTextInputLocation;
    private final String key = "c7010afcd8381da2eee05cc3e56ee";
    private XListView listViewWeeks;
    private XListView listView24H;
    private WeatherAdapter weatherAdapter;
    private View viewLoading;
    private RelativeLayout mapFragmentContainer;
    private MapFragment mapFragmentWeather;
    private GoogleMap googleMap;
    private View contentWeatherList;
    private ArrayList<WeatherBean> wbs;
    private ArrayList<Marker> markers;
    private View linearLayoutSearchWeather;
    private LinearLayout linearLayoutStaticRefreshView;
    private boolean isLongClickMap=false;

    public WeatherDialog(final Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_weather, contentContainer);

        this.linearLayoutStaticRefreshView = (LinearLayout) findViewById(R.id.linearLayoutStaticRefreshView);

        contentWeatherList = findViewById(R.id.contentWeatherList);
        linearLayoutSearchWeather = findViewById(R.id.linearLayoutSearchWeather);
        linearLayoutSearchWeather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getAllWeather(editTextInputLocation.getText().toString(), null, true);

                mapFragmentContainer.setVisibility(View.GONE);
                contentWeatherList.setVisibility(View.VISIBLE);

            }
        });

        mapFragmentWeather = (MapFragment) (((Activity) context).getFragmentManager().findFragmentById(R.id.mapFragmentWeather));
        mapFragmentWeather.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                markers = new ArrayList<Marker>();
                WeatherDialog.this.googleMap = googleMap;

                googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override
                    public void onMapLongClick(LatLng latLng) {

                        editTextInputLocation.setText(null);

                        try {
                            List<Address> addres = Tool.getLocationFromLatlng(context, latLng, 1);
                            if (!ListUtiles.isEmpty(addres)) {

                                String fname = addres.get(0).getFeatureName();
                                WeatherDialog.this.googleMap.clear();
                                for (Marker marker : markers) {
                                    marker.remove();
                                }
                                markers.add(WeatherDialog.this.googleMap.addMarker(new MarkerOptions().position(latLng).title(fname)));
//                                editTextInputLocation.setText(fname);

                                mapFragmentContainer.setVisibility(View.GONE);
                                contentWeatherList.setVisibility(View.VISIBLE);

                                getAllWeather(fname, null, true);

                                isLongClickMap=true;

                            } else {
                                Tool.showMessageDialog("Can't find location info", (Activity) context);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        });

        mapFragmentContainer = (RelativeLayout) findViewById(R.id.mapFragmentContainer);


        listViewWeeks = (XListView) findViewById(R.id.listViewWeeks);
        listViewWeeks.setPullLoadEnable(false);
        listViewWeeks.setPullRefreshEnable(true);
        listViewWeeks.setXListViewListener(this);


        listView24H = (XListView) findViewById(R.id.listView24H);
        listView24H.setPullLoadEnable(false);
        listView24H.setPullRefreshEnable(true);
        listView24H.setXListViewListener(this);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.equals(textView24Hour)) {

                    textView24Hour.setTextColor(context.getResources().getColor(R.color.text_color_ios_blue));
                    textView24Hour.setBackgroundColor(Color.WHITE);

                    textViewWeek.setTextColor(Color.BLACK);
                    textViewWeek.setBackgroundColor(Color.parseColor("#ffe5e5e5"));

                    listViewWeeks.setVisibility(View.GONE);
                    listView24H.setVisibility(View.VISIBLE);

                } else {

                    textViewWeek.setTextColor(context.getResources().getColor(R.color.text_color_ios_blue));
                    textViewWeek.setBackgroundColor(Color.WHITE);

                    textView24Hour.setTextColor(Color.BLACK);
                    textView24Hour.setBackgroundColor(Color.parseColor("#ffe5e5e5"));

                    listViewWeeks.setVisibility(View.VISIBLE);
                    listView24H.setVisibility(View.GONE);
                }

            }
        };


        textView24Hour = (TextView) findViewById(R.id.textView24Hour);
        textView24Hour.setOnClickListener(onClickListener);
        textViewWeek = (TextView) findViewById(R.id.textViewWeek);
        textViewWeek.setOnClickListener(onClickListener);
        editTextInputLocation = (EditText) findViewById(R.id.editTextInputLocation);

        viewLoading = findViewById(R.id.viewLoading);

        editTextInputLocation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (null != googleMap) {
                        mapFragmentContainer.setVisibility(View.VISIBLE);
                        contentWeatherList.setVisibility(View.GONE);
                    }
                }
            }
        });

        editTextInputLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != googleMap) {
                    mapFragmentContainer.setVisibility(View.VISIBLE);
                    contentWeatherList.setVisibility(View.GONE);
                }
            }
        });


        editTextInputLocation.addTextChangedListener(new TextWatcherAdapter() {
            @Override
            public void afterTextChanged(Editable s) {

//                mapFragmentContainer.setVisibility(View.VISIBLE);
//                contentWeatherList.setVisibility(View.GONE);

//                getAllWeather(editTextInputLocation.getText().toString(), null);

            }
        });

        findViewById(R.id.linearLayoutClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }


    @Override
    public void dismiss() {

        InputMethodManager inputmanger = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputmanger.hideSoftInputFromWindow(editTextInputLocation.getWindowToken(), 0);

        super.dismiss();

    }

    @Override
    public void show() {
        mapFragmentContainer.setVisibility(View.GONE);
        contentWeatherList.setVisibility(View.VISIBLE);
        super.show();

        if (null == weatherAdapter || StringUtils.isEmpty(editTextInputLocation.getText().toString())) {

            String location = ((BaseActivity) context).getInput(editTextInputLocation);
            if (!StringUtils.isEmpty(location)) {
                getAllWeather(location, null, true);
            } else if (null != App.getApp().getAvGeoPoint()) {
//                getAddressByLocation(App.getApp().getParseGeoPoint());
                getAllWeather(null, App.getApp().getAvGeoPoint(), true);
            } else {
                LogUtil.i(App.tag,"some thing wrong on request weather");
            }
        }else{
            LogUtil.i(App.tag,"do not need request auto");
        }

    }


    private void getAddressByLocation(AVGeoPoint parseGeoPoint) {

        BaseTask baseTask = new BaseTask(new NetCallBack() {


            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                try {
                    LatLng latLng = new LatLng(App.getApp().getAvGeoPoint().getLatitude(), App.getApp().getAvGeoPoint().getLongitude());

                    List<Address> adres = Tool.getLocationFromLatlng(context, latLng, 1);

                    NetResult netResult = new NetResult();
                    netResult.setTag(adres);
                    return netResult;
                } catch (Exception e) {
                }

                return null;

            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (null != result) {
                    List<Address> list = (List<Address>) result.getTag();
                    if (!ListUtiles.isEmpty(list) && StringUtils.isEmpty(editTextInputLocation.getText().toString())) {
                        Address ad = list.get(0);
                        String addres = ad.getLocality();
                        editTextInputLocation.setText(addres);
                    }
                }
            }
        });

        baseTask.execute(new HashMap<String, String>());
    }


    private BaseTask baseTaskAllWeather;

    private void getAllWeather(String area, AVGeoPoint geoPoint, final boolean showStaticRefresh) {

        String url = "";
        if (!StringUtils.isEmpty(area)) {

            try {
                area = URLEncoder.encode(area, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            url = "https://api.worldweatheronline.com/premium/v1/weather.ashx?q=" + area + "&format=json&num_of_days=7&mca=no&includelocation=yes&tp=1&key=" + key;
        } else if (null != geoPoint) {
            String location = geoPoint.getLatitude() + "," + geoPoint.getLongitude();
            url = "https://api.worldweatheronline.com/premium/v1/weather.ashx?q=" + location + "&format=json&num_of_days=7&mca=no&includelocation=yes&tp=1&key=" + key;
        } else if (null != App.getApp().getAvGeoPoint()) {
            String location = App.getApp().getAvGeoPoint().getLatitude() + "," + App.getApp().getAvGeoPoint().getLongitude();
            url = "https://api.worldweatheronline.com/premium/v1/weather.ashx?q=" + location + "&format=json&num_of_days=7&mca=no&includelocation=yes&tp=1&key=" + key;
        }

        final String resutUrl = url;

//        LogUtil.i(App.tag, "url:" + resutUrl);

        if (!StringUtils.isEmpty(resutUrl)) {

            if (null != baseTaskAllWeather && baseTaskAllWeather.getStatus() == AsyncTask.Status.RUNNING) {
                baseTaskAllWeather.cancel(true);
            }

            if (null != baseTaskSea && baseTaskSea.getStatus() == AsyncTask.Status.RUNNING) {
                baseTaskSea.cancel(true);
            }


            baseTaskAllWeather = new BaseTask(new NetCallBack() {

                @Override
                public void onPreCall() {

                    listViewWeeks.setAdapter(null);
                    listView24H.setAdapter(null);
                    if (showStaticRefresh) {
                        linearLayoutStaticRefreshView.setVisibility(View.VISIBLE);
                    }

                }

                @Override
                public void onCanCell() {

                    linearLayoutStaticRefreshView.setVisibility(View.GONE);
                }

                @Override
                public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                    NetResult netResult = new NetResult();
                    try {
                        URL urlc = new URL(resutUrl);
                        StringBuffer sbf = new StringBuffer();
                        BufferedInputStream bfi = new BufferedInputStream(urlc.openStream());
                        byte buffer[] = new byte[512];
                        int ret = -1;
                        while ((ret = bfi.read(buffer)) != -1) {
                            sbf.append(new String(buffer, 0, ret));
                        }
                        bfi.close();
                        netResult.setTag(sbf.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    return netResult;
                }

                @Override
                public void onFinish(NetResult result, BaseTask baseTask) {
                    if (null != result && null != result.getTag()) {
//                        LogUtil.i(App.tag, "reslut" + result.getTag());
                        if (buildAdapter((String) result.getTag())) {
//                            linearLayoutStaticRefreshView.setVisibility(View.VISIBLE);
                        } else {
                            hideRefreshView();
                        }
                    }

                    hideRefreshView();
                }
            });

            baseTaskAllWeather.execute(new HashMap<String, String>());
        }

    }

    private void showRefreshView() {
//        linearLayoutStaticRefreshView.setVisibility(View.VISIBLE);
    }

    private void hideRefreshView() {

        linearLayoutStaticRefreshView.setVisibility(View.GONE);
        listViewWeeks.stopRefresh();
        listView24H.stopRefresh();
    }

    private boolean buildAdapter(String json) {
        boolean showrefresh = parseWeahter(json);

        if (!ListUtiles.isEmpty(wbs)) {
            mapFragmentContainer.setVisibility(View.GONE);
            contentWeatherList.setVisibility(View.VISIBLE);
            weatherAdapter = new WeatherAdapter(context, wbs);
            listViewWeeks.setAdapter(weatherAdapter);
            build24hAdapter();
//            listView24H.setAdapter(new WeatherAdapter24h(context, wbs.get(0)));
        }
        return showrefresh;
    }

    private void build24hAdapter() {
        if (!ListUtiles.isEmpty(wbs)) {
            listView24H.setAdapter(new WeatherAdapter24h(context, wbs.get(0)));
        }
    }

    private boolean parseWeahter(String json) {
        wbs = new ArrayList<WeatherBean>();

        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray weatherWeek = jsonObject.getJSONObject("data").getJSONArray("weather");

            JSONObject current_conditionObj = jsonObject.getJSONObject("data").getJSONArray("current_condition").getJSONObject(0);

            String humidity = current_conditionObj.getString("humidity");
            String winddir16Point = current_conditionObj.getString("winddir16Point");
            String winddirDegree = current_conditionObj.getString("winddirDegree");
            String windspeedKmph = current_conditionObj.getString("windspeedKmph");


            for (int i = 0, isize = weatherWeek.length(); i < isize; i++) {

                WeatherBean weahterBean = new WeatherBean();
                weahterBean.setHumidity(humidity);
                weahterBean.setWinddir16Point(winddir16Point);
                weahterBean.setWinddirDegree(winddirDegree);
                weahterBean.setWindspeedKmph(windspeedKmph);


                JSONObject weahterObj = weatherWeek.getJSONObject(i);

                JSONObject sunObj = weahterObj.getJSONArray("astronomy").getJSONObject(0);

                weahterBean.setSunRise(sunObj.getString("sunrise"));
                weahterBean.setSunSet(sunObj.getString("sunset"));
                weahterBean.setDate(weahterObj.getString("date"));


                JSONArray hoursly = weahterObj.getJSONArray("hourly");

                for (int j = 0, jsize = hoursly.length(); j < jsize; j++) {

                    JSONObject hourslyObj = hoursly.getJSONObject(j);

                    WeatherBean.Hoursly hourslyBean = new WeatherBean.Hoursly();

                    hourslyBean.setHumidity(hourslyObj.getString("humidity"));
                    hourslyBean.setTempC(hourslyObj.getString("tempC"));
                    hourslyBean.setTempF(hourslyObj.getString("tempF"));
                    hourslyBean.setWeatherDesc(hourslyObj.getJSONArray("weatherDesc").getJSONObject(0).getString("value"));
                    hourslyBean.setWinddir16Point(hourslyObj.getString("winddir16Point"));
                    hourslyBean.setWinddirDegree(hourslyObj.getString("winddirDegree"));
                    hourslyBean.setWindspeedKmph(hourslyObj.getString("windspeedKmph"));
                    hourslyBean.setWeatherCode(hourslyObj.getString("weatherCode"));
                    weahterBean.addHoursly(hourslyBean);
                }

                wbs.add(weahterBean);
            }


            JSONObject nearArea = jsonObject.getJSONObject("data").getJSONArray("nearest_area").getJSONObject(0);


            String areaName = nearArea.getJSONArray("areaName").getJSONObject(0).getString("value");
            String country = nearArea.getJSONArray("country").getJSONObject(0).getString("value");
            String region = nearArea.getJSONArray("region").getJSONObject(0).getString("value");


            String input = ((BaseActivity) context).getInput(editTextInputLocation);

            if (StringUtils.isEmpty(input) || isLongClickMap) {
                editTextInputLocation.setText(areaName+","+region+","+country);
            }

            isLongClickMap=false;

            String lat = nearArea.getString("latitude");
            String lng = nearArea.getString("longitude");
            if (!StringUtils.isEmpty(lat) && !StringUtils.isEmpty(lng)) {
                get24HoursWeatherForSea(lat, lng);
                return true;
            } else {
                return false;
            }


        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.e(App.tag, "get weather error:" + e.getLocalizedMessage());
        }

        return false;
    }


    private BaseTask baseTaskSea;

    private void get24HoursWeatherForSea(String lat, String lng) {

        String location = lat + "," + lng;
        final String url = "https://api.worldweatheronline.com/premium/v1/marine.ashx?q=" + location + "&format=json&tide=yes&tp=24&key=" + key;

        if (!StringUtils.isEmpty(url)) {
            final String resutUrl = url;

//            LogUtil.i(App.tag, "resultUrlForSea:" + resutUrl);
            if (null != baseTaskSea && baseTaskSea.getStatus() == AsyncTask.Status.RUNNING) {
                baseTaskSea.cancel(true);
            }

            baseTaskSea = new BaseTask(new NetCallBack() {

                @Override
                public void onPreCall() {
//                    linearLayoutStaticRefreshView.setVisibility(View.VISIBLE);

                    listView24H.setAdapter(null);
                    listViewWeeks.setAdapter(null);

                }

                @Override
                public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                    NetResult netResult = new NetResult();
                    try {
                        URL urlc = new URL(resutUrl);
                        StringBuffer sbf = new StringBuffer();
                        BufferedInputStream bfi = new BufferedInputStream(urlc.openStream());
                        byte buffer[] = new byte[512];
                        int ret = -1;
                        while ((ret = bfi.read(buffer)) != -1) {
                            sbf.append(new String(buffer, 0, ret));
                        }
                        bfi.close();
                        netResult.setTag(sbf.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    return netResult;
                }

                @Override
                public void onFinish(NetResult result, BaseTask baseTask) {
                    if (null != result && null != result.getTag()) {
                        parseSeaWeather((String) result.getTag(), wbs);
//                        LogUtil.i(App.tag, "reslut for sea:" + result.getTag());
                    }

                    hideRefreshView();
                }
            });

            baseTaskSea.execute(new HashMap<String, String>());

        }

    }


    private void parseSeaWeather(String json, final ArrayList<WeatherBean> weatherBeans) {

        try {
            JSONObject jsonObject = new JSONObject(json);

            JSONObject allWobject = jsonObject.getJSONObject("data");


            if (allWobject.has("weather")) {

                JSONArray weatherWeek = allWobject.getJSONArray("weather");


                for (int i = 0, isize = weatherWeek.length(); i < isize; i++) {

                    try {

                        JSONObject weahterObj = weatherWeek.getJSONObject(i);
                        String swellHeight_m = weahterObj.getJSONArray("hourly").getJSONObject(0).getString("swellHeight_m");

                        WeatherBean wb = weatherBeans.get(i);
                        wb.setSweelHight(swellHeight_m);

                        if (weahterObj.has("tides") && null != weahterObj.getJSONArray("tides")) {

                            ArrayList<TideBean> tbs = new ArrayList<>();

                            JSONArray tides = weahterObj.getJSONArray("tides");
                            if (null != tides.get(0)) {
                                JSONArray rtides = tides.getJSONObject(0).getJSONArray("tide_data");
                                for (int j = 0, jsize = rtides.length(); j < jsize; j++) {

                                    JSONObject jb = rtides.getJSONObject(j);
                                    TideBean tb = new TideBean();
                                    tb.setSwellHeight_m(swellHeight_m);
                                    tb.setTide_type(jb.getString("tide_type"));
                                    tb.setTideDateTime(jb.getString("tideDateTime"));
                                    tb.setTideHeight_mt(jb.getString("tideHeight_mt"));
                                    tb.setTideTime(jb.getString("tideTime"));
                                    tbs.add(tb);
                                }
                                wb.setTideBeans(tbs);
                            }
                        }
                    } catch (Exception e) {

                        LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
                    }


                }

                weatherAdapter.notifyDataSetChanged();

                build24hAdapter();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            LogUtil.e(App.tag, "get sea weather error:" + e.getLocalizedMessage());
        }

    }

    @Override
    public void onRefresh(XListView v) {
        String area = editTextInputLocation.getText().toString();
        AVGeoPoint laglng = App.getApp().getAvGeoPoint();
        if (!StringUtils.isEmpty(area)) {
            getAllWeather(area, null, false);
        } else if (null != laglng) {
            getAllWeather(null, laglng, false);
        } else {
            v.stopRefresh();
            Tool.showMessageDialog("Please input or select a location", (Activity) context);
        }

//        v.stopRefresh();
    }

    @Override
    public void onLoadMore(XListView v) {

    }

    @Override
    public void onTouch(boolean downOrUp) {

    }
}

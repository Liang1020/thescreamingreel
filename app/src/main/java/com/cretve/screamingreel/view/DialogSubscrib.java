package com.cretve.screamingreel.view;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.activity.SubscriptionTermActivity;
import com.thescreamingreel.androidapp.R;

/**
 * Created by wangzy on 16/7/19.
 */
public class DialogSubscrib extends MyBasePicker {


    TextView textViewMonth;

    TextView textViewYear;

    public DialogSubscrib(final Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.dialog_subscrible, contentContainer);

        textViewMonth = (TextView) findViewById(R.id.textViewMonth);
        textViewYear = (TextView) findViewById(R.id.textViewYear);

        textViewMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                textViewMonth.setBackgroundResource(R.drawable.bg_pur_unselect);
//                textViewMonth.setTextColor(context.getResources().getColor(R.color.text_color_ios_blue));
//
//                textViewYear.setBackgroundResource(R.drawable.bg_pur_select);
//                textViewYear.setTextColor(context.getResources().getColor(R.color.text_color_white));


                Intent intent = new Intent();
                intent.putExtra(App.SUBSCRIPTION_TYPE, "month");
                Tool.startActivity(context, SubscriptionTermActivity.class, intent);
                dismiss();

            }
        });

        textViewYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                textViewMonth.setBackgroundResource(R.drawable.bg_pur_select);
//                textViewMonth.setTextColor(context.getResources().getColor(R.color.text_color_white));
//
//                textViewYear.setBackgroundResource(R.drawable.bg_pur_unselect);
//                textViewYear.setTextColor(context.getResources().getColor(R.color.text_color_ios_blue));


                Intent intent = new Intent();
                intent.putExtra(App.SUBSCRIPTION_TYPE, "year");
                Tool.startActivity(context, SubscriptionTermActivity.class, intent);

                dismiss();

            }
        });


//        findViewById(R.id.textViewCanCelAnyTime).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dismiss();
//            }
//        });

        findViewById(R.id.imageButtonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


    }


}

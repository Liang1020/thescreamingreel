package com.cretve.screamingreel.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.common.view.UnderSegmeentView;
import com.thescreamingreel.androidapp.R;

import java.util.ArrayList;

/**
 * Created by wangzy on 15/11/19.
 */
public class HeaderTab extends RelativeLayout {

    private View rootView;
    private Context activity;
    private int checkIndex = 0;
    private ArrayList<ImageView> arrayListImageViews;
    private OnMenuCheckListener onMenuCheckListener;
    private UnderSegmeentView underSegmeentView;
    private int underViewHeight = 5;//read this dimen from integer

    public HeaderTab(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.activity = context;
        this.underViewHeight = context.getResources().getInteger(R.integer.under_line_height);

        underSegmeentView = new UnderSegmeentView(context);

        RelativeLayout.LayoutParams linearLayout2 = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, underViewHeight);
        linearLayout2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        addView(underSegmeentView, linearLayout2);


        RelativeLayout.LayoutParams linearLayout = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        linearLayout.addRule(RelativeLayout.ABOVE, underSegmeentView.getId());
        addView(init(context), linearLayout);

        checkIndexWithNoCallBack(1,false);
    }

    public View init(Context activity) {
        this.rootView = View.inflate(activity, R.layout.main_header, null);
        this.arrayListImageViews = new ArrayList<ImageView>();

        arrayListImageViews.add((ImageView) rootView.findViewWithTag("0"));

        arrayListImageViews.add((ImageView) rootView.findViewWithTag("1"));

        arrayListImageViews.add((ImageView) rootView.findViewWithTag("2"));

        arrayListImageViews.add((ImageView) rootView.findViewWithTag("3"));


        for (int i = 0; i < 4; i++) {

            String tag = String.valueOf("0" + i);

            rootView.findViewWithTag(tag).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    String tag = (String) view.getTag();
                    int index = Integer.parseInt(tag);
                    checkIndex = index;
                    checkIndex(index);
                    if (null != onMenuCheckListener) {
                        onMenuCheckListener.onMenuClick(index);
                    }
                }
            });


        }

        return rootView;
    }


    public void checkIndexWithNoCallBack(int index,boolean animation) {


        this.checkIndex = index;
        if(animation){
            underSegmeentView.setCurrentAnimation(index);
        }else{
            underSegmeentView.setCurrent(index);
        }
//        underSegmeentView.setCurrent(index);
//        underSegmeentView.setCurrentAnimation(index);

        for (int i = 0, isize = arrayListImageViews.size(); i < isize; i++) {
            ImageView img = arrayListImageViews.get(i);

            switch (i) {
                case 0:
                    if (index == i) {
                        img.setImageResource(R.drawable.icon_user_select);
                    } else {
                        img.setImageResource(R.drawable.icon_user);
                    }
                    break;
                case 1:
                    if (index == i) {
                        img.setImageResource(R.drawable.icon_feed_select);
                    } else {
                        img.setImageResource(R.drawable.icon_feed);
                    }
                    break;
                case 2:
                    if (index == i) {
                        img.setImageResource(R.drawable.icon_fish_select);
                    } else {
                        img.setImageResource(R.drawable.icon_fish);
                    }
                    break;

                case 3:
                    if (index == i) {
                        img.setImageResource(R.drawable.icon_music_select);
                    } else {
                        img.setImageResource(R.drawable.icon_music);
                    }
                    break;
            }
        }
    }

    public void checkIndex(int index) {
        if (null != onMenuCheckListener) {
            onMenuCheckListener.onMenuClick(index);
        }
    }


    public View getRootView() {
        return rootView;
    }

    public void showUnderLine(int i) {
        underSegmeentView.setCurrent(i);
    }


    public static interface OnMenuCheckListener {
        public void onMenuClick(int index);
    }

    public OnMenuCheckListener getOnMenuCheckListener() {
        return onMenuCheckListener;
    }

    public void setOnMenuCheckListener(OnMenuCheckListener onMenuCheckListener) {
        this.onMenuCheckListener = onMenuCheckListener;
    }
}

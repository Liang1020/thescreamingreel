package com.cretve.screamingreel.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.bigkoo.pickerview.view.BasePickerView;
import com.common.util.StringUtils;
import com.thescreamingreel.androidapp.R;

public class PhotoSelectDialogSimple extends BasePickerView {

    public OnPhotoSelectionActionListener onPhotoSelectionActionListener;


    private TextView textViewTitle;

    public PhotoSelectDialogSimple(Context context) {
        super(context);

        LayoutInflater.from(context).inflate(R.layout.dialog_photo_select_simple, contentContainer);

        textViewTitle = (TextView) findViewById(R.id.textViewTitle);

        findViewById(R.id.buttonDismiss).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onPhotoSelectionActionListener) {
                    onPhotoSelectionActionListener.onDismis();
                }
            }
        });

        findViewById(R.id.buttonAddMedia).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (null != onPhotoSelectionActionListener) {
                    onPhotoSelectionActionListener.onAddClick();
                }
            }
        });

    }

    public void setTextViewTitle(String title) {

        if (StringUtils.isEmpty(title)) {
            textViewTitle.setVisibility(View.GONE);
        } else {
            textViewTitle.setVisibility(View.VISIBLE);
        }
        textViewTitle.setText(title);

    }


    public static interface OnPhotoSelectionActionListener {


        public void onAddClick();


        public void onDismis();
    }

    public OnPhotoSelectionActionListener onPhotoSelectionAction() {
        return onPhotoSelectionActionListener;
    }

    public void setOnAddCatchListener(OnPhotoSelectionActionListener onAddCatchListener) {
        this.onPhotoSelectionActionListener = onAddCatchListener;
    }
}
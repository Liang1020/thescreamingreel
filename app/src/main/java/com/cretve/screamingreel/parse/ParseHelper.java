package com.cretve.screamingreel.parse;

import com.common.util.StringUtils;

import java.io.File;

/**
 * Created by wangzy on 15/12/30.
 */
public class ParseHelper {


    public static boolean isVideo(String input) {

//        String[] fileSets = new String[]{"mp4"};
//        List<String> list = Arrays.asList(fileSets);
//
//        return list.contains(input);

        if (StringUtils.isEmpty(input)) {
            return false;
        }


        if (input.endsWith("mp4")) {
            return true;
        }
        if (input.endsWith("movie")) {
            return true;
        }
        if (input.endsWith("3gp")) {
            return true;
        }

        return false;
    }




    public static boolean isVideo(File file) {

        return  isVideo(file.getAbsolutePath());

    }
}

package com.cretve.screamingreel.bean;

/**
 * Created by wangzy on 12/28/15.
 */
public class TideBean {


    private String tide_type;
    private String tideDateTime;
    private String tideHeight_mt;
    private String tideTime;

    private String swellHeight_m;


    public String getTide_type() {
        return tide_type;
    }

    public void setTide_type(String tide_type) {
        this.tide_type = tide_type;
    }

    public String getTideDateTime() {
        return tideDateTime;
    }

    public void setTideDateTime(String tideDateTime) {
        this.tideDateTime = tideDateTime;
    }

    public String getTideHeight_mt() {
        return tideHeight_mt;
    }

    public void setTideHeight_mt(String tideHeight_mt) {
        this.tideHeight_mt = tideHeight_mt;
    }

    public String getTideTime() {
        return tideTime;
    }

    public void setTideTime(String tideTime) {
        this.tideTime = tideTime;
    }

    public String getSwellHeight_m() {
        return swellHeight_m;
    }

    public void setSwellHeight_m(String swellHeight_m) {
        this.swellHeight_m = swellHeight_m;
    }
}

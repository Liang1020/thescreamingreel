package com.cretve.screamingreel.bean;

import com.common.util.LogUtil;
import com.cretve.screamingreel.App;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by wangzy on 15/12/28.
 */
public class WeatherBean {

    private String humidity;
    private String winddir16Point;
    private String winddirDegree;
    private String windspeedKmph;

    private String sunRise;
    private String sunSet;

    private String date;
    private String dateFormat;

    private String hightTemp;
    private String lowTemp;

    private String dayWeatherDesc;
    private String dayWeatherCode;


    private String nightWeatherDesc;
    private String nightWeatherCode;


    private String windDirection;
    private String windSpeed;

    private String tideLow1;
    private String tideHigh1;

    private String tideLow2;
    private String tideHigh2;

    private String sweelHight;

    private ArrayList<TideBean> tideBeans;

    private ArrayList<Hoursly> arrayListHours;

    public WeatherBean() {
        arrayListHours = new ArrayList<Hoursly>();
    }

    public void addHoursly(Hoursly hoursly) {
        arrayListHours.add(hoursly);
    }

    public ArrayList<Hoursly> getAllHoursly() {
        return arrayListHours;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getWindspeedKmph() {
        return windspeedKmph;
    }

    public void setWindspeedKmph(String windspeedKmph) {
        this.windspeedKmph = windspeedKmph;
    }

    private SimpleDateFormat sdf = new SimpleDateFormat("EEEE,MMMMMMMMM dd");
    private SimpleDateFormat sdfparse = new SimpleDateFormat("yyyy-MM-dd");
    Calendar calendar = Calendar.getInstance();

    public String getDateFormat() {

        try {
            String restlt = sdf.format(sdfparse.parse(date));
            return restlt;
        } catch (Exception e) {

        }
        return "";

    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getHightTemp() {
        return hightTemp;
    }

    public void setHightTemp(String hightTemp) {
        this.hightTemp = hightTemp;
    }

    public String getLowTemp() {
        return lowTemp;
    }

    public void setLowTemp(String lowTemp) {
        this.lowTemp = lowTemp;
    }

    public String getDayWeatherDesc() {
        return dayWeatherDesc;
    }

    public void setDayWeatherDesc(String dayWeatherDesc) {
        this.dayWeatherDesc = dayWeatherDesc;
    }

    public String getDayWeatherCode() {
        return dayWeatherCode;
    }

    public void setDayWeatherCode(String dayWeatherCode) {
        this.dayWeatherCode = dayWeatherCode;
    }

    public String getNightWeatherDesc() {
        return nightWeatherDesc;
    }

    public void setNightWeatherDesc(String nightWeatherDesc) {
        this.nightWeatherDesc = nightWeatherDesc;
    }

    public String getWinddir16Point() {
        return winddir16Point;
    }

    public void setWinddir16Point(String winddir16Point) {
        this.winddir16Point = winddir16Point;
    }

    public String getWinddirDegree() {
        return winddirDegree;
    }

    public void setWinddirDegree(String winddirDegree) {
        this.winddirDegree = winddirDegree;
    }

    public String getNightWeatherCode() {
        return nightWeatherCode;
    }

    public void setNightWeatherCode(String nightWeatherCode) {
        this.nightWeatherCode = nightWeatherCode;
    }

    public String getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(String windDirection) {
        this.windDirection = windDirection;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getTideLow1() {
        return tideLow1;
    }

    public void setTideLow1(String tideLow1) {
        this.tideLow1 = tideLow1;
    }

    public String getTideHigh1() {
        return tideHigh1;
    }

    public void setTideHigh1(String tideHigh1) {
        this.tideHigh1 = tideHigh1;
    }

    public String getTideLow2() {
        return tideLow2;
    }

    public void setTideLow2(String tideLow2) {
        this.tideLow2 = tideLow2;
    }

    public String getTideHigh2() {
        return tideHigh2;
    }

    public void setTideHigh2(String tideHigh2) {
        this.tideHigh2 = tideHigh2;
    }

    public String getSweelHight() {
        return sweelHight;
    }

    public void setSweelHight(String sweelHight) {
        this.sweelHight = sweelHight;
    }

    public String getSunRise() {
        return sunRise;
    }

    public void setSunRise(String sunRise) {
        this.sunRise = sunRise;
    }

    public String getSunSet() {
        return sunSet;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public void setSunSet(String sunSet) {
        this.sunSet = sunSet;
    }


    public static class Hoursly {

        private String humidity;
        private String weatherDesc;
        private String tempC;
        private String tempF;

        private String winddir16Point;//风向
        private String winddirDegree;//方向角度
        private String windspeedKmph;//风速


        private String tideLow1;
        private String tideHigh1;

        private String tideLow2;
        private String tieHigh2;

        private String weatherCode;


        public String getHumidity() {
            return humidity;
        }


        public void setHumidity(String humidity) {
            this.humidity = humidity;
        }

        public String getWeatherDesc() {
            return weatherDesc;
        }

        public void setWeatherDesc(String weatherDesc) {
            this.weatherDesc = weatherDesc;
        }

        public String getTempC() {
            return tempC;
        }

        public void setTempC(String tempC) {
            this.tempC = tempC;
        }

        public String getTempF() {
            return tempF;
        }

        public void setTempF(String tempF) {
            this.tempF = tempF;
        }

        public String getWinddir16Point() {
            return winddir16Point;
        }

        public void setWinddir16Point(String winddir16Point) {
            this.winddir16Point = winddir16Point;
        }

        public String getWinddirDegree() {
            return winddirDegree;
        }

        public void setWinddirDegree(String winddirDegree) {
            this.winddirDegree = winddirDegree;
        }

        public String getWindspeedKmph() {
            return windspeedKmph;
        }

        public void setWindspeedKmph(String windspeedKmph) {
            this.windspeedKmph = windspeedKmph;
        }

        public String getTideLow1() {
            return tideLow1;
        }

        public void setTideLow1(String tideLow1) {
            this.tideLow1 = tideLow1;
        }

        public String getTideHigh1() {
            return tideHigh1;
        }

        public void setTideHigh1(String tideHigh1) {
            this.tideHigh1 = tideHigh1;
        }

        public String getTideLow2() {
            return tideLow2;
        }

        public void setTideLow2(String tideLow2) {
            this.tideLow2 = tideLow2;
        }

        public String getTieHigh2() {
            return tieHigh2;
        }

        public void setTieHigh2(String tieHigh2) {
            this.tieHigh2 = tieHigh2;
        }

        public int getTempFInt() {

            try {

                return Integer.parseInt(tempF);
            } catch (Exception e) {
            }

            return 0;
        }

        public int getTempCInt() {
            try {

                return Integer.parseInt(tempC);
            } catch (Exception e) {
            }

            return 0;
        }


        public String getWeatherCode() {
            return weatherCode;
        }

        public void setWeatherCode(String weatherCode) {
            this.weatherCode = weatherCode;
        }

        public float getWindowKm(){

            try {
                return  Float.parseFloat(windspeedKmph);
            }catch (Exception e){

                LogUtil.e(App.tag,"wind speed error");
            }
            return 0f;
        }
    }


    public ArrayList<TideBean> getTideBeans() {
        return tideBeans;
    }

    public void setTideBeans(ArrayList<TideBean> tideBeans) {
        this.tideBeans = tideBeans;
    }
}

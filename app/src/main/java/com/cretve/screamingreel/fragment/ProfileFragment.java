package com.cretve.screamingreel.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.common.util.LogUtil;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.activity.MainActivity;
import com.cretve.screamingreel.activity.page.PageProfile;

/**
 * Created by wangzy on 15/12/10.
 */
public class ProfileFragment extends Fragment {


    private PageProfile basePage;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (null == basePage) {
            basePage = new PageProfile((MainActivity) getActivity());
        }

        return basePage.getRootView();
    }

    @Override
    public void onResume() {
        LogUtil.i(App.tag, "onresumeFragment:" + toString());
        basePage.onResume();
        super.onResume();
    }

    public PageProfile getBasePage() {

        return basePage;
    }
}

package com.cretve.screamingreel.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cretve.screamingreel.activity.MainActivity;
import com.cretve.screamingreel.activity.page.PagecatchList;

/**
 * Created by wangzy on 15/12/10.
 */
public class CatchListFragment extends Fragment {

    private PagecatchList basePage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (null == basePage) {
            basePage = new PagecatchList((MainActivity) getActivity());
        }
        return basePage.getRootView();
    }

    public PagecatchList getBasePage() {
        return basePage;
    }

    @Override
    public void onResume() {
        super.onResume();
//        basePage.refresh(false,false,true);

        basePage.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        basePage.onActivityResult(requestCode, resultCode, data);
    }
}

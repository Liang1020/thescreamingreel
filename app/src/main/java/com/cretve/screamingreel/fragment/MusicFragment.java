package com.cretve.screamingreel.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.common.view.BasePage;
import com.cretve.screamingreel.activity.MainActivity;
import com.cretve.screamingreel.activity.page.PageMusic;

/**
 * Created by wangzy on 15/12/10.
 */
public class MusicFragment extends Fragment {


    private BasePage basePage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (null == basePage) {
            basePage = new PageMusic((MainActivity) getActivity());
        }

        return basePage.getRootView();

    }

    @Override
    public void onPause() {
        super.onPause();

        stopPlay();

    }

    public void stopPlay() {

        try {
            ((PageMusic) basePage).stopPlay();
        } catch (Exception e) {
        }

    }

    public void show() {

        try {
            ((PageMusic) basePage).show();
        } catch (Exception e) {
        }

    }



    public BasePage getBasePage() {
        return basePage;
    }
}

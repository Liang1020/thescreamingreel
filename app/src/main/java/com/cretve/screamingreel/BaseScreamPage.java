package com.cretve.screamingreel;

/**
 * Created by wangzy on 15/11/9.
 */

import com.common.view.BasePage;
import com.cretve.screamingreel.activity.MainActivity;

/**
 * Created by wangzy on 15/9/16.
 */
public abstract  class BaseScreamPage extends BasePage<MainActivity> {

    public BaseScreamPage(MainActivity activity) {
        super(activity);
        initView();
    }
}
package com.cretve.screamingreel;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;

import com.avos.avoscloud.AVACL;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVPush;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.DeleteCallback;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.SendCallback;
import com.baidu.location.BDLocation;
import com.common.BaseActivity;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.cretve.screamingreel.activity.SelectImageActivity;
import com.cretve.screamingreel.photo.lib.SharedpreferencesUtil;
import com.cretve.screamingreel.util.GPSConverter;
import com.cretve.screamingreel.view.DialogInviteFBView;
import com.cretve.screamingreel.view.PhotoSelectDialog;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.learnncode.mediachooser.MediaChooser;
import com.learnncode.mediachooser.activity.HomeFragmentActivity;
import com.thescreamingreel.androidapp.R;
import com.xlist.pull.refresh.XListView;
import com.yink.sardar.CompressListener;
import com.yink.sardar.Compressor;
import com.yink.sardar.InitListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by wangzy on 15/10/29.
 */
public class BaseScreamingReelActivity extends BaseActivity {


    private View pageViewLocationSelection;
    private EditText editTextLocationInput;
    private ListView listViewLocation;
    protected Animation animationShow, animationHide;
    protected View photoView;
    public HashMap<Integer, AsyncTask> taskMap;


    public int pageSize = 10;
    public int currentPage = 0;
    public int totallCount = 0;


    //=====new photo lib ====
    public short SELECT_PIC_KITKAT = 801;
    public short SELECT_PIC = 802;
    public short TAKE_BIG_PICTURE = 803;
    public short CROP_BIG_PICTURE = 804;
    public int outputX = 150;
    public int outputY = 150;

    //=================
    public final short SHORT_REQUEST_ADDCATCH = 1000;
    public final short SHORT_REQUEST_SELECTIMAGE = 1001;
    public final short SHORT_ACCESS_LOCATION = 1003;
    public final short SHORT_REQUEST_READ_EXTEAL = 1004;
    public final short SHORT_REQUEST_READ_EXTEAL_CATCH = 1005;
    public final short SHORT_REQUEST_MEDIAS = 1006;
    public final short SHORT_LOG_OUT = 1007;
    public final short SHORT_LOG_OUT_PWD = 1008;


    public static final int REQUEST_RESOLVE_ERROR = 888;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        taskMap = new HashMap<Integer, AsyncTask>();

        animationHide = AnimationUtils.loadAnimation(this, R.anim.slide_bottom_out);
        animationShow = AnimationUtils.loadAnimation(this, R.anim.slide_bottom_in);

        sharedpreferencesUtil = new SharedpreferencesUtil(this);


        IntentFilter videoIntentFilter = new IntentFilter(MediaChooser.VIDEO_SELECTED_ACTION_FROM_MEDIA_CHOOSER);
        registerReceiver(videoBroadcastReceiver, videoIntentFilter);

        IntentFilter imageIntentFilter = new IntentFilter(MediaChooser.IMAGE_SELECTED_ACTION_FROM_MEDIA_CHOOSER);
        registerReceiver(imageBroadcastReceiver, imageIntentFilter);
    }


    public void sendLikePush(AVObject aVObjectCatch) {
        try {

            AVObject notification = AVObject.create("Notifications");

            notification.put("sender", AVUser.getCurrentUser());
            notification.put("type", "Like");
            notification.put("receiver", aVObjectCatch.getAVUser("owner"));
            notification.put("catch", aVObjectCatch);
            notification.put("read", false);


            AVACL acl = new AVACL();

            acl.setPublicReadAccess(true);
            acl.setPublicWriteAccess(true);
            notification.setACL(acl);


            notification.saveEventually();


            AVQuery query = AVInstallation.getQuery();
            query.whereEqualTo("user", aVObjectCatch.get("owner"));
            JSONObject jsobObject = new JSONObject();
            String alert = AVUser.getCurrentUser().get("first_name") + " " + AVUser.getCurrentUser().get("last_name") + " liked your catch ";
            if (null != aVObjectCatch.get("fish")) {
                alert += " with " + aVObjectCatch.getAVObject("fish").getString("name");
            }
            jsobObject.put("alert", alert);
            jsobObject.put("key", App.K_CATCHLIKED_NOTIFICATIONKEY);
            jsobObject.put("objectId", aVObjectCatch.getObjectId());
            jsobObject.put("action", "com.avos.UPDATE_STATUS");

            AVPush.sendDataInBackground(jsobObject, query, new SendCallback() {
                @Override
                public void done(AVException e) {

                }
            });


        } catch (Exception e2) {
            LogUtil.e(App.tag, "error:" + e2.getLocalizedMessage());
            LogUtil.e(App.tag, "exception in sendLikePush ");
        }

    }

    public void sendFellowPush(AVObject aVObjectCatch) {

        try {

            AVObject notification = AVObject.create("Notifications");

            notification.put("sender", AVUser.getCurrentUser());
            notification.put("type", "Follow");
            notification.put("receiver", aVObjectCatch.getAVObject("owner"));
            notification.put("catch", aVObjectCatch);
            notification.put("read", false);


            AVACL acl = new AVACL();

            acl.setPublicReadAccess(true);
            acl.setPublicWriteAccess(true);
            notification.setACL(acl);


            notification.saveEventually();


            AVQuery query = AVInstallation.getQuery();
            query.whereEqualTo("user", aVObjectCatch.get("owner"));
            JSONObject jsobObject = new JSONObject();
            String alert = AVUser.getCurrentUser().get("first_name") + " " + AVUser.getCurrentUser().get("last_name") + " followed your. ";
            jsobObject.put("alert", alert);
            jsobObject.put("key", App.K_USERFOLLOWED_NOTIFICATIONKEY);
            jsobObject.put("objectId", aVObjectCatch.getObjectId());
            jsobObject.put("action", "com.avos.UPDATE_STATUS");

            AVPush.sendDataInBackground(jsobObject, query, new SendCallback() {
                @Override
                public void done(AVException e) {
                    if (null != e) {
                        LogUtil.i(App.tag, "send follow push error:" + e.getLocalizedMessage());
                    }

                }
            });

        } catch (Exception e2) {
            LogUtil.e(App.tag, "exception in sendFellowPush ");
        }

    }

    /**
     * go to custome media store
     */
    public void startSelectMediaFromChose(Activity activity) {
        Intent intent = new Intent(activity, HomeFragmentActivity.class);
        startActivityForResult(intent, SHORT_REQUEST_MEDIAS);
    }

    public void onShareClick(AVObject catchItem) {
        if (null != catchItem) {

            if (ListUtiles.isEmpty(catchItem.getList("photos"))) {
                DialogUtils.showMessageDialog("", "No photo here to share", "confirm", this, new DialogCallBackListener() {
                    @Override
                    public void onDone(boolean yesOrNo) {
                        super.onDone(yesOrNo);
                    }
                });
                return;
            }
            App.getApp().putTemPObject("catch", catchItem);
            Tool.startActivityForResult(this, SelectImageActivity.class, SHORT_REQUEST_SELECTIMAGE);
        }
    }


    public void showLocationSelect(OnCitySelectListener onCitySelectListener) {
        if (null == pageViewLocationSelection) {
            pageViewLocationSelection = View.inflate(this, R.layout.activity_location_selection, null);
        }

    }

    public void showInviteView() {

        DialogInviteFBView dialogInviteFBView = new DialogInviteFBView(this);
        dialogInviteFBView.setOnFBFollowListener(new DialogInviteFBView.OnFBFollowListener() {
            @Override
            public void onFaceBookClick() {
                onFacebookInviteClick();
            }

            @Override
            public void onEmailClick() {
                onEmailClickInvite();
            }
        });
        dialogInviteFBView.show();
    }

    public void showPhotoView() {

        PhotoSelectDialog photoSelectDialog = new PhotoSelectDialog(this);
        photoSelectDialog.setOnAddCatchListener(new PhotoSelectDialog.OnPhotoSelectionActionListener() {
            @Override
            public void onTakePhotoClick() {
                startTakePicture(150, 150);
            }

            @Override
            public void onChoosePhotoClick() {
                startSelectPicture(150, 150);
            }

            @Override
            public void onDismis() {

            }
        });
        photoSelectDialog.show();


    }


    String previewImageUrl;

    public void onFacebookInviteClick() {
        final String appLinkUrl = "https://fb.me/761842413919923";

        previewImageUrl = "http://ac-ypd0jxo7.clouddn.com/35a16b1f1a970d8b.png";
//
//        try {
//            AVQuery querConfigImg = AVQuery.getQuery("Config");
//            querConfigImg.include("facebook_invite_img");
//
//            querConfigImg.getFirstInBackground(new GetCallback() {
//                @Override
//                public void done(AVObject avObject, AVException e) {
//                    if (null == e) {
//                        LogUtil.i(App.tag, "avobject:" + avObject.toString());
//                        if (AppInviteDialog.canShow()) {
//                            AppInviteContent content = new AppInviteContent.Builder()
//                                    .setApplinkUrl(appLinkUrl)
//                                    .setPreviewImageUrl(previewImageUrl)
//                                    .build();
//                            AppInviteDialog.show(BaseScreamingReelActivity.this, content);
//                        }
//                    } else {
//                        LogUtil.e(App.tag, "get fb inviate img error:" + e.getLocalizedMessage());
//                    }
//                }
//
//                @Override
//                protected void internalDone0(Object o, AVException e) {
//
//                }
//            });
//
//        } catch (Exception e) {
//            LogUtil.e(App.tag, "previewImageUrl error!");
//            previewImageUrl = "http://ac-ypd0jxo7.clouddn.com/35a16b1f1a970d8b.png";
//        }


        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
                    .setPreviewImageUrl(previewImageUrl)
                    .build();
            AppInviteDialog.show(BaseScreamingReelActivity.this, content);
        } else {
            LogUtil.i(App.tag, "can not show dialog now!");
        }

    }


    public void showLeanCloudError(AVException e) {
        if (null != e) {
            if (e.getCause() instanceof UnknownHostException) {
                showNotifyTextIn5Seconds(R.string.error_net);
                return;
            }


            String json = e.getLocalizedMessage();
            try {

                JSONObject jo = new JSONObject(json);

                String error = jo.getString("error");

                LogUtil.i(App.tag, "error:" + error);

                showNotifyTextIn5Seconds(error);


            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
    }


    public void onEmailClickInvite() {
        try {
            Intent email = new Intent(android.content.Intent.ACTION_SEND);
            email.setType("application/octet-stream");

            String emailTitle = "Invite to The Screaming Reel App";
            String emailContent = "Hey, check out this app called The Screaming Reel.\n\n" +
                    "It's a dedicated social platform for fishermen to share their catch, " +
                    "locate where the fish are biting, observe weather conditions before launching the boat, " +
                    "and most importantly, boast about the one that didn’t get away.\n\n" +
                    "It's free for a limited time. Check it out at www.thescreamingreel.com";

            email.putExtra(android.content.Intent.EXTRA_SUBJECT, emailTitle);
            email.putExtra(android.content.Intent.EXTRA_TEXT, emailContent);

            // 调用系统的邮件系统
            startActivity(Intent.createChooser(email, "Invite"));

//        String[] emailReciver = new String[] {  };
//

        } catch (Exception e) {
            LogUtil.d(App.tag, e.getMessage());
        }
    }


    public static interface OnCitySelectListener {
        public void onSelectCity(AVObject cityObj);
    }


    public void saveAVObject(final AVObject aVObject, final boolean showDialog, final boolean showNotify, final SaveCallback saveCallback) {


        final BaseTask saveTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask task) {
                if (showDialog) {
                    showNewDialogWithNewTask(task);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = new NetResult();
                try {
                    LogUtil.i(App.tag, "save in background:" + baseTask.hashCode());

                    if (aVObject instanceof AVUser) {
                        aVObject.put("full_name", aVObject.getString("first_name") + " " + aVObject.getString("last_name"));
                    }

                    aVObject.setFetchWhenSave(true);
                    aVObject.save();

                    AVUser.getCurrentUser().fetch();

                } catch (Exception e) {
                    LogUtil.e(App.tag, "save parse error:" + e.getLocalizedMessage());
                    netResult.setException(e);
                }
                return netResult;
            }


            @Override
            public void onCanCell(BaseTask baseTask) {
                LogUtil.i(App.tag, "cancell:" + baseTask.hashCode());
                if (null == baseTask) {
                    return;
                }
                taskMap.remove(baseTask);
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                LogUtil.i(App.tag, "finish:" + baseTask.hashCode());

                if (showDialog) {
                    hideNewDialogWithTask(baseTask);
                }

//                if (null == result) {
//                    showCustomException();
//                    return;
//                }

                if (null != result) {

                    Exception exception = result.getException();
                    if (null == exception && null != saveCallback) {
                        saveCallback.done(null);
                    } else {

                        if (exception instanceof AVException) {

                            if (showNotify) {
                                showLeanCloudError((AVException) exception);
                            }

                        } else {
                            if (null != exception) {
                                LogUtil.e(App.tag, "excetion:" + exception.getLocalizedMessage());
                            }
                        }

                    }
                }
//
//                if(null!=result.getException() && (result.getException() instanceof java.text.AVException)){
//                    if (null != saveCallback) {
//                        saveCallback.done(null == result.getException() ? null : (AVException) result.getException());
//                    }
//                }else if(null!=result.getException()){
//
//                    LogUtil.e(App.tag,"error:"+result.getException().getLocalizedMessage());
//                }
//
//
//                if (null != result.getException() && showNotify) {
//                    showNotifyTextIn5Seconds(((AVException) result.getException()).getMessage());
//                }
            }
        });
        saveTask.execute(new HashMap<String, String>());

        putTask(saveTask);
    }


//    public void showCustomException() {
//
//        String msg = "Due to growth of The Screaming Reel app, we are upgrading the server, " +
//                "which may impact some functions within the app for the next few days." +
//                " Please check The Screaming Reel website for more information.";
//
//
//        Tool.showMessageDialog(msg, this);
//
//
//    }


    public void saveAVObject(final AVObject aVObject, final AVObject secondObject, final boolean showDialog, final boolean showNotify, final SaveCallback saveCallback) {


        final BaseTask saveTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask task) {
                if (showDialog) {
                    showNewDialogWithNewTask(task);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {
                NetResult netResult = new NetResult();
                try {
                    LogUtil.i(App.tag, "save in background:" + baseTask.hashCode());


//                    Task<Void> t1 = aVObject.saveEventually();
//                    Task<Void> t2 = secondObject.saveEventually();
//                    secondObject.save();
//                    aVObject.save();

                    aVObject.setFetchWhenSave(true);
                    secondObject.setFetchWhenSave(true);


                    List<AVObject> aVObjects = new ArrayList<>();

                    aVObjects.add(aVObject);
                    aVObjects.add(secondObject);

                    AVObject.saveAll(aVObjects);


                } catch (Exception e) {
                    e.printStackTrace();
                    netResult.setException(e);
                    LogUtil.e(App.tag, "save parse object error:" + e.getLocalizedMessage());
                }
                return netResult;
            }


            @Override
            public void onCanCell(BaseTask baseTask) {
                LogUtil.i(App.tag, "cancell:" + baseTask.hashCode());
                if (null == baseTask) {
                    return;
                }
                taskMap.remove(baseTask);
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                LogUtil.i(App.tag, "finish:" + baseTask.hashCode());

                if (showDialog) {
                    hideNewDialogWithTask(baseTask);
                }
//
//                if (null == result) {
//                    showCustomException();
//                    return;
//                }

                if (null != result) {

                    Exception exception = result.getException();
                    if (null == exception && null != saveCallback) {
                        saveCallback.done(null);
                    } else {

                        if (exception instanceof AVException) {
                            if (showNotify) {
//                                showNotifyTextIn5Seconds(((AVException) result.getException()).getMessage());
                                showLeanCloudError((AVException) exception);
                            }
                        } else {
                            if (null != exception) {
                                LogUtil.e(App.tag, "excetion:" + exception.getLocalizedMessage());
                            }
                        }

                    }
                }
//
//                if(null!=result.getException() && (result.getException() instanceof java.text.AVException)){
//                    if (null != saveCallback) {
//                        saveCallback.done(null == result.getException() ? null : (AVException) result.getException());
//                    }
//                }else if(null!=result.getException()){
//
//                    LogUtil.e(App.tag,"error:"+result.getException().getLocalizedMessage());
//                }
//
//
//                if (null != result.getException() && showNotify) {
//                    showNotifyTextIn5Seconds(((AVException) result.getException()).getMessage());
//                }
            }
        });
        saveTask.execute(new HashMap<String, String>());

        putTask(saveTask);
    }


    private BaseTask deleteTask = null;

    public void deleteAllCatchObject(final AVObject aVObjectCatch, final boolean showDialog, final DeleteCallback deleteCallback) {

        if (null != deleteTask && deleteTask.getStatus() == AsyncTask.Status.RUNNING) {
            deleteTask.cancel(true);
        }

        deleteTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {

                if (showDialog) {
                    showNewDialogWithNewTask(baseTask);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {


                NetResult netResult = new NetResult();

                try {

                    AVQuery notifycationQuery = AVQuery.getQuery("Notifications");
                    notifycationQuery.whereEqualTo("catch", aVObjectCatch);
                    notifycationQuery.deleteAll();

                    AVQuery commentQuery = AVQuery.getQuery("Comment");
                    commentQuery.whereEqualTo("catch", aVObjectCatch);
                    commentQuery.deleteAll();

                    aVObjectCatch.deleteEventually();


                } catch (AVException e) {
                    netResult.setException(e);
                    LogUtil.i(App.tag, "delete catch error:" + e.getLocalizedMessage());
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    hideNewDialogWithTask(baseTask);
                }
                if (null != result.getException()) {
                    if (null != deleteCallback) {
                        deleteCallback.done((AVException) result.getException());
                    }

                } else {
                    LogUtil.e(App.tag, "delete success");

                    if (null != deleteCallback) {
                        deleteCallback.done(null);
                    }
                }


            }
        });

        deleteTask.execute(new HashMap<String, String>());
    }


    public void putTask(AsyncTask task) {
        LogUtil.e(App.tag, "putTask:" + task.hashCode());
        taskMap.put(task.hashCode(), task);
    }


    public void stopXlist(XListView xListViewMyTravel) {
        xListViewMyTravel.stopRefresh();
        xListViewMyTravel.stopLoadMore();
    }

    public void dealFooter(List arrayList, int totalNumber, XListView listViewNannies) {
        try {
            if (arrayList.size() >= totalNumber) {
                listViewNannies.getmFooterView().hide();
            } else {
                listViewNannies.getmFooterView().show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        LogUtil.i(App.tag, "data size:" + arrayList.size());
    }

    public void clearTaskMap() {

        Set<Map.Entry<Integer, AsyncTask>> set = taskMap.entrySet();

        for (Map.Entry<Integer, AsyncTask> taskEntry : set) {
            AsyncTask task = taskEntry.getValue();
            if (null != task && task.getStatus() == AsyncTask.Status.RUNNING) {
                task.cancel(true);
                LogUtil.i(App.tag, "cancell task:" + task.hashCode());
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public void startActivity(Intent intent) {

        try {
            super.startActivity(intent);
        } catch (Exception e) {
            onStartActivityException(e);
        }

    }

    /**
     * when call startActivity exception
     *
     * @param e
     */
    public void onStartActivityException(Exception e) {

    }

    @Override
    protected void onDestroy() {


        unregisterReceiver(imageBroadcastReceiver);
        unregisterReceiver(videoBroadcastReceiver);

        clearTaskMap();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    public SharedpreferencesUtil sharedpreferencesUtil;


    public byte TYPE_REQEST_PICTURE_TAKE = 0;
    public byte TYPE_REQEST_PICTURE_SELECT = 1;

    public byte type_request_picture = TYPE_REQEST_PICTURE_TAKE;

    public void startTakePicture() {
        type_request_picture = TYPE_REQEST_PICTURE_TAKE;
        sharedpreferencesUtil.delectImageTemp();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, sharedpreferencesUtil.getImageTempNameUri());
        startActivityForResult(intent, TAKE_BIG_PICTURE);
    }

    public void startSelectPicture() {
        type_request_picture = TYPE_REQEST_PICTURE_SELECT;
        sharedpreferencesUtil.delectImageTemp();
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/jpeg");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            startActivityForResult(intent, SELECT_PIC_KITKAT);
        } else {
            startActivityForResult(intent, SELECT_PIC);
        }
    }

    public void startTakePicture(int w, int h) {
        this.outputX = w;
        this.outputY = h;
        startTakePicture();
    }

    public void startSelectPicture(int w, int h) {
        this.outputX = w;
        this.outputY = h;
        startSelectPicture();
    }


    public void cropImageUri(Uri uriIn, Uri uriOut, int outputX, int outputY, int requestCode) {

        LogUtil.i(App.tag,"ImageUri: "+ uriIn.getPath());
        LogUtil.i(App.tag, "ImageLocation: "+ readExif(uriIn.getPath()));

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uriIn, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", outputX);
        intent.putExtra("outputY", outputY);
        intent.putExtra("scale", true);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriOut);
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true); // no face detection
//        intent.putExtra("scaleUpIfNeeded", true);//add
        startActivityForResult(intent, requestCode);

    }

    private String readExif(String file){
        String exif="Exif: " + file;
        try {
            ExifInterface exifInterface = new ExifInterface(file);

            exif += "\n TAG_GPS_LATITUDE: " + GPSConverter.convertLat(exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE), exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF));
            exif += "\n TAG_GPS_LONGITUDE: " + GPSConverter.convertLon(exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE), exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF));

        } catch (IOException e) {
            LogUtil.e(App.tag, e.getMessage());;
        }
        return exif;
    }
    public void onLocationReceive(BDLocation bdLocation, Location googleLocation) {


    }


    public String getNumberTextFromParseNumber(Number number) {

        if (null == number) {
            return "";
        }

        String text = String.valueOf(number.floatValue());
        if (StringUtils.isEmpty(text)) {
            return "";
        } else if (text.endsWith(".0")) {
            return String.valueOf(number.intValue());
        } else {
            return String.valueOf(number.floatValue());
        }

    }

    public Compressor com;
    String cmd;

    public void compressVideo(String src, String dst, final CompressListener compressListener)throws Exception {

//      cmd = "-y -i /mnt/sdcard/videokit/in1.mp4 -strict -2 -vcodec libx264 -preset ultrafast -crf 24 -acodec aac -ar 44100 -ac 2 -b:a 96k -s 640x352 -aspect 16:9 /mnt/sdcard/videokit/out8.mp4";
        //ffmpe ffmpeg命令
        cmd = "-y -i " + src + " -strict -2 -vcodec h264 -preset ultrafast -crf 24 -acodec aac -ar 44100 -ac 2 -b:a 96k -s 480x360 -aspect 16:9 " + dst;



        if (null == com) {
            com = new Compressor(this);

            com.loadBinary(new InitListener() {
                @Override
                public void onLoadSuccess() {
                    com.execCommand(cmd, compressListener);
                }

                @Override
                public void onLoadFail(String reason) {
                    LogUtil.e(App.tag, "ffmpeg init error:" + reason);
                }
            });


        } else {
            com.execCommand(cmd, compressListener);
        }




    }


    public void showSoftWareKeyBorad() {
        View view = getWindow().peekDecorView();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
    }

    public void closeSoftWareKeyBoradInBase() {

        /**隐藏软键盘**/
        View view = getWindow().peekDecorView();
        if (view != null) {
            InputMethodManager inputmanger = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputmanger.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

//        // Check if no view has focus:
//        View view = this.getCurrentFocus();
//        if (view != null) {
//            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//        }
    }


    public void closeSoftKeyBorad(InputMethodManager imm, View view) {

        if (null == imm) {
            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        }
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//        imm.hideSoftInputFromWindow(rootView.getWindowToken(),0);
    }

    public void hideSoftKeyborad(InputMethodManager imm, View view) {

        if (null == imm) {
            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public void onReceiveVideoSelect(Context context, Intent intent) {
//        Toast.makeText(BaseScreamingReelActivity.this, "yippiee Video ", Toast.LENGTH_SHORT).show();
//        Toast.makeText(BaseScreamingReelActivity.this, "Video SIZE :" + intent.getStringArrayListExtra("list").size(), Toast.LENGTH_SHORT).show();
    }

    public void onReceivePictureSelect(Context context, Intent intent) {

//        Toast.makeText(BaseScreamingReelActivity.this, "yippiee Image ", Toast.LENGTH_SHORT).show();
//        Toast.makeText(BaseScreamingReelActivity.this, "Image SIZE :" + intent.getStringArrayListExtra("list").size(), Toast.LENGTH_SHORT).show();

    }

    BroadcastReceiver videoBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            onReceiveVideoSelect(context, intent);
        }
    };


    BroadcastReceiver imageBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            onReceivePictureSelect(context, intent);
        }
    };


/*do not delete below code for photo select*/
//  deal picture
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        //===deal with pciture selected
//
//        String mFileName;
//        Bitmap mColorBitmap;
//
//        if (requestCode == SELECT_PIC) {
//            if (resultCode == RESULT_OK && data != null) {
//                mFileName = PhotoLibUtils.getDataColumn(getApplicationContext(), data.getData(), null, null);
//                PhotoLibUtils.copyFile(mFileName, sharedpreferencesUtil.getImageTempNameString2());
//                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
//                cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
//            }
//        }
//        if (requestCode == SELECT_PIC_KITKAT) {
//            if (resultCode == RESULT_OK && data != null) {
//                mFileName = PhotoLibUtils.getPath(getApplicationContext(), data.getData());
//                String newPath = sharedpreferencesUtil.getImageTempNameString2();
//                PhotoLibUtils.copyFile(mFileName, newPath);
//                Uri uri = sharedpreferencesUtil.getImageTempNameUri();
//                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
//                cropImageUri(uri, outoutUri, outputX, outputY, CROP_BIG_PICTURE);
//
//            }
//        }
//        if (requestCode == TAKE_BIG_PICTURE) {
//            Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
//            cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
//        }
//        if (requestCode == CROP_BIG_PICTURE && resultCode==Activity.RESULT_OK) {
//            if (sharedpreferencesUtil.getImageTempNameUri() != null) {
//
//                Uri result = sharedpreferencesUtil.getImageTempNameUriCroped();
//                App.firstRun();
//
//                if (!StringUtils.isEmpty(result.getPath())) {
//                    upLoadHeaderImg(result.getPath(), new OnUploadCallBack() {
//                        @Override
//                        public void onUploadEnd(NetResult netResult) {
//
//                            if (null != netResult) {
//                                if (netResult.isOk()) {
//
////                                    SimpleImgLoaderLru.from(MainActivity.this).clearCache();
//
//                                    getmFrag().initHeaderInfoUseCache(null);
//                                    pageMieStone.initHeaderInfoOnleyUser();
//
//                                    requestUserProfile(new OnRequestEndCallBack() {
//                                        @Override
//                                        public void onRequestEnd(NetResult netResult) {
//                                            if (null != netResult) {
//                                                User user = (User) netResult.getData()[0];
//                                                //========
//                                                App.getApp().setUser(user);
//                                                ParseInstallation.getCurrentInstallation().put("userId", user.getId());
//                                                ParseInstallation.getCurrentInstallation().saveInBackground();
//
//                                            } else {
//                                                showNotifyTextIn5Seconds(R.string.error_net);
//                                            }
//                                        }
//                                    }, true);
//                                } else {
//                                    showNotifyTextIn5Seconds(netResult.getMessage());
//                                }
//                            } else {
//                                showNotifyTextIn5Seconds(R.string.error_net);
//                            }
//                        }
//                    }, false);
//                }
//                LogUtil.i(App.tag, " file path croped:" + result.getPath());
//
//            }
//        }
//    }


    public int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
//        return whiteIcon ? R.drawable.icon_notify_small : R.drawable.icon_notify_big;

        return whiteIcon ? R.drawable.push_logo_ic_small : R.drawable.push_logo_ic;
//        return whiteIcon ? R.drawable.push_logo_small : R.drawable.push_logo;
//        return  R.drawable.push_logo;
    }
}


package com.cretve.screamingreel.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;

import com.avos.avoscloud.AVUser;
import com.common.util.LogUtil;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.activity.MainActivity;
import com.cretve.screamingreel.activity.SplashActivity;
import com.thescreamingreel.androidapp.R;

import org.json.JSONObject;

import java.util.Locale;

/**
 * Created by wangzy on 15/9/22.
 */
public class MyAVPushReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            LogUtil.e(App.tag, "receive msg:" + getPushData(intent).toString());
            notification(context, intent);
        } catch (Exception e) {
            LogUtil.e(App.tag, "error:" + e.getLocalizedMessage());
        }
    }


    private void notification(Context context, Intent intent) {


        if (null == AVUser.getCurrentUser()) {
            return;
        }
        try {

//            String action = intent.getAction();
//            String channel = intent.getExtras().getString("com.avos.avoscloud.Channel");
            //获取消息内容

            JSONObject pushData = new JSONObject(intent.getExtras().getString("com.avos.avoscloud.Data"));

//        JSONObject pushData = getPushData(intent);
            if (pushData != null && (pushData.has("alert") || pushData.has("title"))) {

                Intent i = null;
                if (App.getApp().isMainActivityCreate()) {

                    i = new Intent();
                    if (App.getApp().isMainActivityVisible()) {
                        i = new Intent();
                    } else {
                        i = new Intent(context, MainActivity.class);
                    }
                } else {
                    i = new Intent(context, SplashActivity.class);
                }

                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                String title = pushData.optString("title", context.getResources().getString(R.string.app_name));
                String alert = pushData.optString("alert", "Notification received.");
                String tickerText = String.format(Locale.getDefault(), "%s: %s", new Object[]{title, alert});


                PendingIntent pi = PendingIntent.getActivity(context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
                NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    // 下面需兼容Android 2.x版本是的处理方式
                    // Notification notify1 = new Notification(R.drawable.message,
                    // "TickerText:" + "您有新短消息，请注意查收！", System.currentTimeMillis());
//                Notification notify1 = new Notification();
//                notify1.icon = R.mipmap.ic_launcher;
//                notify1.when = System.currentTimeMillis();
//                notify1.setLatestEventInfo(context, title, alert, pi);
//                notify1.number = 1;
//                notify1.flags |= Notification.FLAG_AUTO_CANCEL; // FLAG_AUTO_CANCEL表明当通知被用户点击时，通知将被清除。
//                nm.notify(R.string.app_name, notify1);

                } else if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB && android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {

//                PendingIntent pendingIntent2 = PendingIntent.getActivity(context, 0,i, 0);
                    Notification notify2 = new Notification.Builder(context)
                            .setSmallIcon(R.drawable.push_logo_ic_small)
                            // 设置状态栏中的小图片，尺寸一般建议在24×24，这个图片同样也是在下拉状态栏中所显示
                            // ，如果在那里需要更换更大的图片，可以使用setLargeIcon(Bitmap
                            // icon)
                            .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.push_logo_ic))
                            .setTicker(alert)// 设置在status
                            .setContentTitle(title)// 设置在下拉status
                            // bar后Activity，本例子中的NotififyMessage的TextView中显示的标题
                            .setContentText(alert)// TextView中显示的详细内容
                            .setContentIntent(pi) // 关联PendingIntent
                            .setNumber(1) // 在TextView的右方显示的数字，可放大图片看，在最右侧。这个number同时也起到一个序列号的左右，如果多个触发多个通知（同一ID），可以指定显示哪一个。
                            .getNotification(); // 需要注意build()是在API level
                    // 16及之后增加的，在API11中可以使用getNotificatin()来代替
                    notify2.flags |= Notification.FLAG_AUTO_CANCEL;
                    nm.notify(R.string.app_name, notify2);

                } else if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {

//                PendingIntent pendingIntent3 = PendingIntent.getActivity(context, 0,i, 0);
                    // 通过Notification.Builder来创建通知，注意API Level
                    // API16之后才支持
                    Notification notify3 = null;

                    notify3 = new Notification.Builder(context)
                            .setSmallIcon(getNotificationIcon())

                            .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.push_logo_ic))
                            .setTicker(alert)
                            .setContentTitle(title)
                            .setContentText(alert)
                            .setContentIntent(pi).setNumber(1).build();
                    // 需要注意build()是在API
                    // level16及之后增加的，API11可以使用getNotificatin()来替代
                    notify3.flags |= Notification.FLAG_AUTO_CANCEL; // FLAG_AUTO_CANCEL表明当通知被用户点击时，通知将被清除。
                    nm.notify(R.string.app_name, notify3);// 步骤4：通过通知管理器来发起通知。如果id不同，则每click，在status哪里增加一个提示

                }

//            NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//            Notification n = new Notification(R.mipmap.ic_launcher, title, System.currentTimeMillis());
//            n.flags = Notification.FLAG_AUTO_CANCEL;
//            n.defaults = Notification.DEFAULT_ALL;
//            n.setLatestEventInfo(context, title, alert, pi);
//            nm.notify(R.string.app_name, n);


            }


        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.e(App.tag, "error receive exception!");
        }


    }

    private int getNotificationIcon() {
        boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
//        return whiteIcon ? R.drawable.icon_notify_small : R.drawable.icon_notify_big;

        return whiteIcon ? R.drawable.push_logo_ic_small : R.drawable.push_logo_ic;
//        return whiteIcon ? R.drawable.push_logo_small : R.drawable.push_logo;
//        return  R.drawable.push_logo;
    }

    private JSONObject getPushData(Intent intent) {
        try {

            String result = intent.getStringExtra("com.avos.avoscloud.Data");
            LogUtil.i(App.tag, "receive msg:" + result);

            return new JSONObject(result);
        } catch (Exception var3) {
            LogUtil.e("com.parse.ParsePushReceiver", "Unexpected JSONException when receiving push data: ", var3);
            return null;
        }
    }


}

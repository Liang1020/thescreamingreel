package com.cretve.screamingreel.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.common.util.ListUtiles;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.CommentDetailListAdapter;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class CommentDetailActivity extends BaseScreamingReelActivity {


    private AVObject parseCatch;

    private AVQuery<AVObject> aVQuery;

    private CommentDetailListAdapter commentDetailListAdapter;
    private List<AVObject> aVObjects;

    @InjectView(R.id.listView)
    ListView listView;

    @InjectView(R.id.imageViewFish)
    ImageView imageViewFish;

    @InjectView(R.id.textViewFishName)
    TextView textViewFishName;

    @InjectView(R.id.editTextPostContent)
    EditText editTextPostContent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_detail);
        ButterKnife.inject(this);
        parseCatch = (AVObject) App.getApp().getTempObject("catch");
        if (null != parseCatch) {
            loadComments();
            List<AVFile> photos = parseCatch.getList("photos");
            if (!ListUtiles.isEmpty(photos)) {
                Picasso.with(this).load(photos.get(0).getUrl()).config(Bitmap.Config.RGB_565).resize(36, 36).into(imageViewFish);
            }

            AVObject fishObj = parseCatch.getAVObject("fish");
            if (null != fishObj) {
                fishObj.fetchIfNeededInBackground(new GetCallback<AVObject>() {
                    @Override
                    public void done(AVObject object, AVException e) {
                        textViewFishName.setText(object.getString("name"));
                    }
                });
            }


        }
    }

    private void loadComments() {
        aVQuery = AVQuery.getQuery("Comment");
        aVQuery.include("owner");
        aVQuery.include("catch");
        aVQuery.whereEqualTo("catch", parseCatch);
        aVQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> objects, AVException e) {
                if (null == e) {
                    buildCommentsList(objects);
                } else {
                    showNotifyTextIn5Seconds(e.getLocalizedMessage());
                }
            }
        });
    }

    @Override
    public void onStartActivityException(Exception e) {
        super.onStartActivityException(e);
        showToastWithTime("Activity not found:" + e.getLocalizedMessage(), 3);
    }

    @OnClick(R.id.viewPost)
    public void onPostClick() {

        String conent = editTextPostContent.getText().toString();
        if (!StringUtils.isEmpty(conent)) {

            final AVObject parseComment = AVObject.create("Comment");

            parseComment.put("content", conent);
            parseComment.put("read", false);
            parseComment.put("owner", AVUser.getCurrentUser());
            parseComment.put("catch", parseCatch);

            saveAVObject(parseComment, true, true, new SaveCallback() {
                @Override
                public void done(AVException e) {
                    if (isFinishing()) {
                        return;
                    }

                    if (null == e) {
                        aVObjects.add(parseComment);
                        commentDetailListAdapter.notifyDataSetChanged();
                    }

                }
            });


        }
    }

    private void buildCommentsList(List<AVObject> aVObjects) {
        this.aVObjects = aVObjects;
        commentDetailListAdapter = new CommentDetailListAdapter(this, this.aVObjects);
        listView.setAdapter(commentDetailListAdapter);

    }


    @OnClick(R.id.viewFishSpecies)
    public void onClickFish() {
        App.getApp().putTemPObject("catch", parseCatch);
        Tool.startActivity(this, CatchDetailActivity.class);

    }


    @OnClick(R.id.viewBack)
    public void onBack() {
        finish();
    }
}

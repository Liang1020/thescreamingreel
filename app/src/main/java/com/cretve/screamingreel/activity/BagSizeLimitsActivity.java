package com.cretve.screamingreel.activity;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.LimitAdapter;
import com.thescreamingreel.androidapp.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class BagSizeLimitsActivity extends BaseScreamingReelActivity {


    @InjectView(R.id.listView)
    ListView listView;

    private String[] text = {

            "New South Wales (Saltwater)",
            "New South Wales  (Freshwater)",
            "Queensland (Tidal waters)",
            "Queensland (Freshwater)",
            "South Australia",
            "Tasmania",
            "Victoria",
            "Western Australia",
            "Northern Territory"
    };

    private String[] url = {

            "http://www.dpi.nsw.gov.au/fisheries/recreational/regulations/sw/sw-bag-and-size",
            "http://www.dpi.nsw.gov.au/fisheries/recreational/regulations/fw/fw-bag-and-size",
            "https://www.daf.qld.gov.au/fisheries/recreational/rules-regulations/size-possession-limits-tidal",
            "https://www.daf.qld.gov.au/fisheries/recreational/rules-regulations/size-possession-limits-fresh",
            "http://pir.sa.gov.au/fishing/fishing_limits",
            "http://dpipwe.tas.gov.au/sea-fishing-aquaculture/recreational-fishing/catch-limits",
            "http://agriculture.vic.gov.au/fisheries/recreational-fishing",
            "http://www.fish.wa.gov.au/fishing-and-aquaculture/recreational-fishing/recreational-fishing-rules/bag_and_size_limits/Pages/default.aspx",
            "http://www.nt.gov.au/d/Fisheries/Content/File/recreational/know-your-limits-booklet.pdf"
    };


    DownloadManager downloadManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bag_size_limits);
        ButterKnife.inject(this);
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        LimitAdapter limitAdapter = new LimitAdapter(this, Arrays.asList(text));
        listView.setAdapter(limitAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String html = url[position];
                if (!html.endsWith("pdf")) {
                    App.getApp().putTemPObject("title", text[position]);
                    App.getApp().putTemPObject("url", html);
                    Tool.startActivity(BagSizeLimitsActivity.this, WebViewActivity.class);
                } else {

                    try {
                        Tool.startUrl(BagSizeLimitsActivity.this, html);
                    } catch (Exception e) {
                        showNotifyTextIn5Seconds("Down load error!");
                    }
                }
            }
        });
    }


    private void startWithDownloadManager(String html) {

        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(html));
        //设置在什么网络情况下进行下载
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
        //设置通知栏标题
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
        request.setTitle("Downloading");

        request.setDescription(Tool.getFileName(html) + " is downloading.");
        request.setAllowedOverRoaming(true);
        //设置文件存放目录
        request.setDestinationInExternalFilesDir(BagSizeLimitsActivity.this, Environment.DIRECTORY_DOWNLOADS, Tool.getFileName(html));
        long reference = downloadManager.enqueue(request);


    }

    public Intent getPdfFileIntent(String path) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromFile(new File(path));
        i.setDataAndType(uri, "application/pdf");
        return i;
    }

    private class DownLoadCompleteReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
                long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

                Toast.makeText(BagSizeLimitsActivity.this, "编号：" + id + "的下载任务已经完成！", Toast.LENGTH_SHORT).show();

            } else if (intent.getAction().equals(DownloadManager.ACTION_NOTIFICATION_CLICKED)) {

                Toast.makeText(BagSizeLimitsActivity.this, "别瞎点！！！", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            int what = msg.what;
            switch (what) {


            }
        }
    };

    @OnClick(R.id.viewBack)
    public void onBackClick() {
        finish();
    }

    public static class DownLoadThread extends Thread {

        private WeakReference<BagSizeLimitsActivity> weakReference;
        private String urlText;

        public DownLoadThread(WeakReference<BagSizeLimitsActivity> activity, String urlText) {
            this.weakReference = activity;
            this.urlText = urlText;
            start();
        }

        @Override
        public void run() {

            try {
                URL url = new URL(urlText);
                InputStream ins = url.openStream();
                BufferedInputStream bfi = new BufferedInputStream(ins);
                byte buffer[] = new byte[512];
                int ret = -1;
                String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
//                /storage/emulated/0/Download
                String fileName = "temp.pdf";
                File pathFile = new File(path);
                File dstFile = null;
                if (pathFile.exists()) {
                    dstFile = new File(path + File.separator + fileName);
                    dstFile.createNewFile();
                } else {
                    //下载出错
                    return;
                }

                while (-1 != (ret = bfi.read(buffer)) && null != weakReference.get() && !weakReference.get().isFinishing()) {


                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}

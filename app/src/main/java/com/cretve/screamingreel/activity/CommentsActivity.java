package com.cretve.screamingreel.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.CountCallback;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.CommentListAdapter;
import com.thescreamingreel.androidapp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class CommentsActivity extends BaseScreamingReelActivity {


    @InjectView(R.id.textViewCountComments)
    TextView textViewCountComments;

    @InjectView(R.id.listViewComments)
    ListView listViewComments;


    List<AVObject> listComments;
    CommentListAdapter commentListAdapter;

    AVQuery countQuery;
    AVQuery<AVObject> contentQuery;

    private int totalCommentsCount=-1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        ButterKnife.inject(this);

        listComments = new ArrayList<>();

        commentListAdapter = new CommentListAdapter(this, listComments);
        listViewComments.setAdapter(commentListAdapter);

        listViewComments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AVObject notification = (AVObject) parent.getItemAtPosition(position);

                if (null != notification) {

                    if ("Comment".equals(notification.get("type"))) {

                        AVQuery<AVObject> query = AVQuery.getQuery("Catch");
                        query.include("owner");
                        query.include("area");
                        query.include("fish");
                        query.include("photos");
                        query.whereNotEqualTo("suspend", true);

                        showProgressDialog();

                        AVObject thatCatch = notification.getAVObject("catch");
                        if (null != thatCatch) {
                            query.getInBackground(thatCatch.getObjectId(), new GetCallback<AVObject>() {
                                @Override
                                public void done(AVObject object, AVException e) {
                                    hideProgressDialog();
                                    if (null == e) {
                                        App.getApp().putTemPObject("catch", object);
                                        Tool.startActivity(CommentsActivity.this, CatchDetailActivity.class);
//                                                } else if (null != e) {
                                    }
                                }
                            });
                        } else {
                            hideProgressDialog();
                            showNotifyTextIn5Seconds("The catch was deleted!");
                        }


//                        AVObject thatCatch = notification.getAVObject("catch");
//                        if (null != thatCatch) {
//                            //goto Comment detail
//                            App.getApp().putTemPObject("catch", notification.getAVObject("catch"));
//                            Tool.startActivity(CommentsActivity.this, CommentDetailActivity.class);
//                        } else {
//                            showToastWithTime("The catch was deleted!", 3);
//                        }
//


                    } else if ("Like".equals(notification.getString("type"))) {

                        // goto Catch detail
//                                    App.getApp().putTemPObject("catch",notification.getAVObject("catch"));
//                                    Tool.startActivity(CommentsActivity.this,CatchDetailActivity.class);

                        AVQuery<AVObject> query = AVQuery.getQuery("Catch");
                        query.include("owner");
                        query.include("area");
                        query.include("fish");
                        query.include("photos");
                        query.whereNotEqualTo("suspend", true);

                        showProgressDialog();

                        AVObject thatCatch = notification.getAVObject("catch");
                        if (null != thatCatch) {
                            query.getInBackground(thatCatch.getObjectId(), new GetCallback<AVObject>() {
                                @Override
                                public void done(AVObject object, AVException e) {
                                    hideProgressDialog();
                                    if (null == e) {
                                        App.getApp().putTemPObject("catch", object);
                                        Tool.startActivity(CommentsActivity.this, CatchDetailActivity.class);
//                                                } else if (null != e) {
                                    }
                                }
                            });
                        } else {
                            hideProgressDialog();
                            showNotifyTextIn5Seconds("The catch was deleted!");
                        }
                    }


                    //============================================================

                    notification.put("read",true);
                    notification.isFetchWhenSave();
                    notification.saveEventually(new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            commentListAdapter.notifyDataSetChanged();
                        }
                    });


                } else {
                    showToastWithTime("Unknown error", 3);
                }
            }
        });


        loadAllcomment();
    }


    @Override
    protected void onResume() {
        super.onResume();

        loadAllcomment();
    }

    private void loadAllcomment(){
        loadCommentCount();
        loadCommentsContent();
    }


    private void loadCommentCount() {


        countQuery = AVQuery.getQuery("Notifications");
        countQuery.whereEqualTo("receiver", AVUser.getCurrentUser());
        countQuery.whereNotEqualTo("sender",AVUser.getCurrentUser());


        String[] types = {"Like", "Comment"};
        countQuery.whereContainedIn("type", Arrays.asList(types));
        countQuery.whereEqualTo("read", false);
        //==================================================

        AVQuery countQueryMe = AVQuery.getQuery("Notifications");

        String[] typesMe = {"Like", "Comment"};
        countQueryMe.whereContainedIn("type", Arrays.asList(typesMe));
        countQueryMe.whereEqualTo("read", false);
        countQueryMe.whereEqualTo("sender", AVUser.getCurrentUser());
        countQueryMe.whereEqualTo("receiver", AVUser.getCurrentUser());
        //=====================================

//        List<AVQuery<AVObject>> queries = new ArrayList<AVQuery<AVObject>>();
//
//        queries.add(countQuery);
//        queries.add(countQueryMe);

        countQuery.countInBackground(new CountCallback() {
            @Override
            public void done(int i, AVException e) {
                if(null==e){
                    if(-1==totalCommentsCount){
                        totalCommentsCount=i;
                    }else{
                        totalCommentsCount+=i;
                        textViewCountComments.setText("There are " + String.valueOf(totalCommentsCount) + " new comments");
                    }
                }

            }
        });

        countQueryMe.countInBackground(new CountCallback() {
            @Override
            public void done(int i, AVException e) {
                if(null==e){
                    if(-1==totalCommentsCount){
                        totalCommentsCount=i;
                    }else{
                        totalCommentsCount+=i;
                        textViewCountComments.setText("There are " + String.valueOf(totalCommentsCount) + " new comments");
                    }
                }
            }
        });



//        AVQuery<AVObject> mainQuery = AVQuery.and(queries);
//
//        mainQuery.countInBackground(new CountCallback() {
//            @Override
//            public void done(int count, AVException e) {
//                if (null == e) {
//                    textViewCountComments.setText("There are " + String.valueOf(count) + " new comments");
//                }
//            }
//        });



    }


    private ArrayList<AVObject> tempList=new ArrayList<>();
    private boolean done1=false;
    private boolean done2=false;

    public void loadCommentsContent() {

        contentQuery = AVQuery.getQuery("Notifications");
        contentQuery.include("sender");
        contentQuery.include("receiver");
        contentQuery.include("catch");
        contentQuery.include("catch.photos");
        contentQuery.include("comment");
        contentQuery.whereEqualTo("receiver", AVUser.getCurrentUser());
        contentQuery.whereNotEqualTo("sender", AVUser.getCurrentUser());


        final String[] container = {"Like", "Comment"};
        contentQuery.whereContainedIn("type", Arrays.asList(container));
        contentQuery.whereEqualTo("read", false);
        contentQuery.orderByDescending("createdAt");

        //==================================================

        AVQuery<AVObject> contentQueryMe = AVQuery.getQuery("Notifications");
        contentQueryMe.include("sender");
        contentQueryMe.include("receiver");
        contentQueryMe.include("catch");
        contentQueryMe.include("catch.photos");
        contentQueryMe.include("comment");
        contentQueryMe.whereEqualTo("sender", AVUser.getCurrentUser());
        contentQueryMe.whereEqualTo("receiver", AVUser.getCurrentUser());


        final String[] containerMe = {"Like", "Comment"};
        contentQueryMe.whereContainedIn("type", Arrays.asList(containerMe));
        contentQueryMe.whereEqualTo("read", false);
        contentQueryMe.orderByDescending("createdAt");


        tempList.clear();

        done1=false;
        done2=false;

        showProgressDialog();

        contentQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                done1=true;
                if(null==e){
                    tempList.addAll(list);
                    if(done1 & done2){
                        commentListAdapter.addCommentList(tempList);
                    }
                }

                if(done1 & done2){
                    hideProgressDialog();
                }

            }
        });

        contentQueryMe.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                done2=true;
                if(null==e){
                    tempList.addAll(list);
                    if(done1 & done2){
                        commentListAdapter.addCommentList(tempList);
                    }
                }

                if(done1 & done2){
                    hideProgressDialog();
                }
            }
        });


//        List<AVQuery<AVObject>> queries = new ArrayList<AVQuery<AVObject>>();
//
//        queries.add(contentQuery);
//        queries.add(contentQueryMe);
//
//
//        AVQuery<AVObject> mainQuery = AVQuery.and(queries);
//
//
//        mainQuery.findInBackground(new FindCallback<AVObject>() {
//            @Override
//            public void done(List<AVObject> list, AVException e) {
//                if(null==e){
//                    commentListAdapter.addCommentList(list);
//                }
//            }
//        });


//        contentQuery.findInBackground(new FindCallback<AVObject>() {
//            @Override
//            public void done(List<AVObject> objects, AVException e) {
//                if (null == e) {
//                    commentListAdapter.addCommentList(objects);
//                } else {
////                    showNotifyTextIn5Seconds(e.getLocalizedMessage());
//                }
//
//            }
//        });

    }


    public void onBack() {
        finish();
    }

    @Override
    public void onBackPressed() {
        onBack();
    }

    @OnClick(R.id.viewBack)
    public void onBcakClick() {
        onBack();
    }

}

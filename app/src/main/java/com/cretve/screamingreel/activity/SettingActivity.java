package com.cretve.screamingreel.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.CountCallback;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.view.NetImageView;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.DataBaseConstants;
import com.thescreamingreel.androidapp.R;

import org.json.JSONArray;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class SettingActivity extends BaseScreamingReelActivity {

    @InjectView(R.id.textViewVersion)
    TextView textViewVersion;

    @InjectView(R.id.textViewUserName)
    TextView textViewUserName;

    @InjectView(R.id.imageViewHeader)
    NetImageView netImageViewHeader;

    @InjectView(R.id.textViewCatches)
    TextView textViewCatches;

    @InjectView(R.id.textViewFlollowing)
    TextView textViewFlollowing;

    @InjectView(R.id.textViewFollowers)
    TextView textViewFollowers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.inject(this);
        initView();
    }

    @Override
    public void initView() {
        textViewVersion.setText(Tool.getVersionName(this) + "(" + Tool.getVersionCode(this) + ")");
        AVUser currentUser = AVUser.getCurrentUser();
        if (null != currentUser) {
            textViewUserName.setText(currentUser.getString("firstName") + " " + currentUser.getString("lastName"));

            JSONArray array = currentUser.getJSONArray("follow_users");
            if (null != array) {
                textViewFlollowing.setText(String.valueOf(array.length()));
            } else {
                textViewFlollowing.setText(String.valueOf(0));
            }

            loadCatch();
        }
    }

    private void loadCatch() {
        AVQuery<AVObject> parseCatch = AVQuery.getQuery(DataBaseConstants.tab_catchs);
        parseCatch.whereEqualTo("owner", AVUser.getCurrentUser());

        parseCatch.countInBackground(new CountCallback() {
            @Override
            public void done(int count, AVException e) {
                if (e == null) {
                    textViewCatches.setText(String.valueOf(count));
                }
            }
        });
    }


    @OnClick(R.id.imageButtonFeed)
    public void onClickFeed() {
        back2CatchList(false);
    }


    private void back2CatchList(boolean isLogout) {
        Intent intent = new Intent();
        intent.putExtra("isLogout", isLogout);
        Tool.startActivity(this, CatchListActivity.class, intent);
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        finish();
    }


    @OnClick(R.id.textViewCangePwd)
    public void onClickChangePwd() {

        String yes = getResources().getString(R.string.confirm);
        String no = getResources().getString(R.string.cancel);

        View dialogView = View.inflate(this, R.layout.dialog_pwd_chagne, null);
        final EditText editTextPwd = (EditText) dialogView.findViewById(R.id.editTextNewPwd);
        final EditText editTextConfirm = (EditText) dialogView.findViewById(R.id.editTextConfirmPwd);

        AlertDialog.Builder builder = new AlertDialog.Builder(SettingActivity.this);
        builder.setView(dialogView);
        builder.setMessage(R.string.setting_input_pwd);
        builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setPositiveButton(yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                String pwd = getInput(editTextPwd);
                if (StringUtils.isEmpty(pwd)) {
                    showNotifyTextIn5Seconds(R.string.login_hint_enter_pwd_notify);
                    return;
                }

                String rpwd = getInput(editTextConfirm);
                if (!pwd.equals(rpwd)) {
                    showNotifyTextIn5Seconds(R.string.register_pwd_repeat_error);
                    return;
                }

                changePwd(pwd);
            }
        });
        builder.create().show();

    }

    private void changePwd(final String pwd) {

        reSetTask();
        baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall() {
                showProgressDialogWithTask(baseTask);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap,BaseTask bTask) {

                NetResult netResult = new NetResult();
                try {
                    AVUser aVUser = AVUser.getCurrentUser();
                    if (null != aVUser) {
                        aVUser.setPassword(pwd);
                    }
                    aVUser.save();
                } catch (AVException e) {
                    netResult.setException(e);
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result) {
                if (null == result.getException()) {
                    showNotifyTextIn5Seconds(R.string.setting_pwd_success);
                } else {
                    showNotifyTextIn5Seconds(((AVException) result.getException()).getMessage());
                }

            }
        });

        baseTask.execute(new HashMap<String, String>());

    }


    @OnClick(R.id.textViewLogOut)
    public void onLogutClick() {
        String msg = "LogOut?";
        DialogCallBackListener dlg = new DialogCallBackListener() {
            @Override
            public void onDone(boolean yesOrNo) {
                if (yesOrNo) {
                    logout();
                }
            }
        };

        DialogUtils.showConfirmDialog(this, "", msg, getResources().getString(R.string.confirm), getResources().getString(R.string.cancel), dlg);

    }


    private void logout() {
        reSetTask();

        baseTask = new BaseTask(new NetCallBack() {
            @Override
            public void onPreCall() {
                showProgressDialogWithTask(baseTask);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap,BaseTask btask) {

                NetResult netResult = new NetResult();
                try {
                    AVUser.logOut();
                } catch (Exception e) {
                    netResult.setException(e);

                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result) {
                hideProgressDialogWithTask();
                if (null == result.getException()) {
                    back2CatchList(true);
                } else {
                    showNotifyTextIn5Seconds(((AVException) result.getException()).getMessage());
                }
            }
        });

        baseTask.execute(new HashMap<String, String>());

    }
}

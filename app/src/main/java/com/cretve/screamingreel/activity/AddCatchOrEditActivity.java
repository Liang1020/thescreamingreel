package com.cretve.screamingreel.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.avos.avoscloud.AVACL;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.DeleteCallback;
import com.avos.avoscloud.SaveCallback;
import com.bigkoo.pickerview.OptionsPickerView;
import com.bigkoo.pickerview.bean.ProvinceBean;
import com.common.adapter.AnimationListenerAdapter;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.NetTool;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.view.MyGridView;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.ItemCatchPictureAdapter;
import com.cretve.screamingreel.parse.ParseHelper;
import com.cretve.screamingreel.photo.lib.PhotoLibUtils;
import com.cretve.screamingreel.util.GPSConverter;
import com.cretve.screamingreel.view.PhotoSelectDialogSimple;
import com.datepicker.DateTimePickerDialog;
import com.google.android.gms.maps.model.LatLng;
import com.learnncode.mediachooser.MediaChooserConstants;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.thescreamingreel.androidapp.R;
import com.video.MediaController;
import com.yink.sardar.CompressListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class AddCatchOrEditActivity extends BaseScreamingReelActivity {


    @InjectView(R.id.linearLayoutDetail)
    LinearLayout linearLayoutDetail;

    @InjectView(R.id.scrollViewContent)
    ScrollView scrollViewContent;

    @InjectView(R.id.gridViewPicture)
    MyGridView gridView;

    @InjectView(R.id.buttonShowMoreOrLess)
    Button buttonShowMoreOrLess;

    @InjectView(R.id.textViewLocationInfo)
    TextView textViewLocationInfo;

    @InjectView(R.id.textViewSpeciesInfo)
    TextView textViewSpeciesInfo;

    @InjectView(R.id.textViewBaitInfo)
    TextView textViewBaitInfo;

    @InjectView(R.id.textViewWeightInfo)
    TextView textViewWeightInfo;

    @InjectView(R.id.textViewLengthInfo)
    TextView textViewLengthInfo;


    @InjectView(R.id.textViewFishMethodInfo)
    TextView textViewFishMethodInfo;

    @InjectView(R.id.textViewWaterVisibilityInfo)
    TextView textViewWaterVisibilityInfo;


    @InjectView(R.id.textViewCatchDepthInfo)
    TextView textViewCatchDepthInfo;

    @InjectView(R.id.textViewBottomDepthInfo)
    TextView textViewBottomDepthInfo;

    @InjectView(R.id.textViewTimeInfo)
    TextView textViewTimeInfo;

    @InjectView(R.id.editDesc)
    EditText editDesc;

    @InjectView(R.id.viewDelete)
    View viewDelete;

    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    @InjectView(R.id.buttonPost)
    Button buttonPost;

    private ArrayList<String> listVideo;


    private Animation animationScaleIn, animationScaleOut;

    private short SHORT_PIK_FISH = 100;
    private short SHORT_PIK_LOCATION = 101;
    private short SHORT_PIK_SPECIES = 102;


    private ItemCatchPictureAdapter itemCatchPictureAdapter;
    private AVObject aVObjectLocation;
    private InputMethodManager imm;
    private boolean isHideLocation;
    private LatLng latLng;
    private int fileCountSaved = 0;

    private AVObject aVObjectCatch;
    private AVObject aVObjectFish;
    private AVObject aVObjectArea;

    @InjectView(R.id.relativeLayoutCompressDialog)
    RelativeLayout relativeLayoutCompressDialog;

    private boolean isCompress = false;

    @InjectView(R.id.relativeLayoutRoot)
    View rootView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_catch);
        ButterKnife.inject(this);

        if (App.getApp().hasTempKey("MyCatch")) {
            aVObjectCatch = (AVObject) App.getApp().getTempObject("MyCatch");
            isHideLocation = aVObjectCatch.getBoolean("hide_location");
        }

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        this.animationScaleIn = AnimationUtils.loadAnimation(this, R.anim.scale_in);
        this.animationScaleOut = AnimationUtils.loadAnimation(this, R.anim.scale_out);
        this.animationScaleIn.setAnimationListener(new AnimationListenerAdapter() {

            @Override
            public void onAnimationEnd(Animation animation) {

                handler.sendEmptyMessageAtTime(0, 500);
            }
        });

        this.animationScaleOut.setAnimationListener(new AnimationListenerAdapter() {
            @Override
            public void onAnimationEnd(Animation animation) {
                handler.sendEmptyMessageAtTime(0, 500);
            }
        });

        itemCatchPictureAdapter = new ItemCatchPictureAdapter(this);
        gridView.setAdapter(itemCatchPictureAdapter);

        if (null != getIntent() && getIntent().hasExtra("fileUrl")) {
            ArrayList<String> fileUrl = getIntent().getStringArrayListExtra("fileUrl");
            //max video file is 1,max picture file is 1,this code is ok
            for (String tfile : fileUrl) {
                if (ParseHelper.isVideo(tfile)) {
                    compressVideo(tfile);
                } else {
                    itemCatchPictureAdapter.addImageFile(tfile);
                }
            }

        }
        initView();
    }

    private void showCompressDialog() {
        relativeLayoutCompressDialog.setVisibility(View.VISIBLE);
    }

    private void hideCompressDialog() {
        relativeLayoutCompressDialog.setVisibility(View.GONE);
    }

    private void compressVideo(final String src) {

        if (!StringUtils.isEmpty(src) && (new File(src)).exists()) {

            final String dst = src + "compres.mp4";
            LogUtil.i(App.tag, "befor compressed:" + (new File(src)).length());

            showCompressDialog();

            isCompress = true;


            try {
                compressVideo(src, dst, new CompressListener() {
                    @Override
                    public void onExecSuccess(String message) {
                        listVideo = null;
                        isCompress = false;
                        hideCompressDialog();
                        File dstFile = new File(dst);
                        if (!isFinishing()) {
                            if (MediaChooserConstants.ChekcMediaFileSize(dstFile, true) < 10) {
                                itemCatchPictureAdapter.addImageFile(dst);
                            } else {
                                showNotifyTextIn5Seconds("The file is too large! ");
                            }
                        }
                        LogUtil.i(App.tag, "after compressed:" + (new File(dst)).length());

                    }

                    @Override
                    public void onExecFail(String reason) {
                        hideCompressDialog();
                        isCompress = false;
                        listVideo = null;
                        showNotifyTextIn5Seconds("Compress fail please reselect! ");
                    }

                    @Override
                    public void onExecProgress(String message) {
                        LogUtil.i(App.tag, "progress:" + message);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();

                LogUtil.i(App.tag, "compress with java api..");
                compresByJavaApiApiLevel18(src, dst, new MyCompressListener() {
                    @Override
                    public void onCompressStart() {
                        showCompressDialog();
                    }

                    @Override
                    public void onCompressEnd(boolean success) {
                        hideCompressDialog();
                        if (true == success) {
                            listVideo = null;
                            isCompress = false;
                            hideCompressDialog();
                            File dstFile = new File(dst);
                            if (!isFinishing()) {
                                if (MediaChooserConstants.ChekcMediaFileSize(dstFile, true) < 10) {
                                    itemCatchPictureAdapter.addImageFile(dst);
                                } else {
                                    showNotifyTextIn5Seconds("The file is too large! ");
                                }
                            }
                            LogUtil.i(App.tag, "after compressed:" + (new File(dst)).length());

                        } else {
                            isCompress = false;
                            listVideo = null;
                            showNotifyTextIn5Seconds("Compress fail please reselect! ");
                        }

                    }
                });
            }
        }

    }


    public void compresByJavaApiApiLevel18(final String src, final String dst, final MyCompressListener compresListener) {


        BaseTask compressTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall() {
                showCompressDialog();

                if (null != compresListener) {
                    compresListener.onCompressStart();
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                boolean ret = MediaController.getInstance().convertVideo(src, dst);
                NetResult result = new NetResult();
                result.setTag(ret);
                return result;
            }

            @Override
            public void onFinish(NetResult result) {
                hideCompressDialog();
                if (null != compresListener) {
                    compresListener.onCompressEnd((Boolean) result.getTag());
                }
            }
        });

        compressTask.execute(new HashMap<String, String>());

    }


    public static interface MyCompressListener {

        public void onCompressStart();

        public void onCompressEnd(boolean result);
    }


    @Override
    public void initView() {

        if (null != aVObjectCatch) {
            viewDelete.setVisibility(View.VISIBLE);
            textViewTitle.setText("Edit Catch");
            editDesc.setText(aVObjectCatch.getString("desc"));

            if (null != aVObjectCatch.getAVObject("area")) {
                this.aVObjectArea = aVObjectCatch.getAVObject("area");
                textViewLocationInfo.setText(this.aVObjectArea.getString("title"));
                textViewLocationInfo.setTag(this.aVObjectArea);
            }

            if (null != aVObjectCatch.getAVObject("fish")) {
                this.aVObjectFish = aVObjectCatch.getAVObject("fish");
                textViewSpeciesInfo.setText(this.aVObjectFish.getString("name"));
                textViewSpeciesInfo.setTag(this.aVObjectFish);
            }

            if (null != aVObjectCatch.getList("photos")) {
                List<AVFile> aVFiles = aVObjectCatch.getList("photos");
                ArrayList<String> files = new ArrayList<String>();
                for (AVFile pf : aVFiles) {
                    if (null != pf) {
                        files.add(pf.getUrl());
                    }
                }
                itemCatchPictureAdapter.addMulImageFile(files);
            }

            textViewBaitInfo.setText(aVObjectCatch.getString("bait"));

            if (null != aVObjectCatch.getNumber("weight")) {

                textViewWeightInfo.setText(getNumberTextFromParseNumber(aVObjectCatch.getNumber("weight")) + " kg");

            }

            if (null != aVObjectCatch.getNumber("length")) {
                textViewLengthInfo.setText(getNumberTextFromParseNumber(aVObjectCatch.getNumber("length")) + " cm");
            }

            textViewFishMethodInfo.setText(aVObjectCatch.getString("method"));

            if (null != aVObjectCatch.getDate("date")) {
                Date date = aVObjectCatch.getDate("date");
                Calendar ca = Calendar.getInstance();
                ca.setTime(date);
                String timeLabel = simpleDateFormat.format(date);
                textViewTimeInfo.setText(timeLabel + " " + (ca.get(Calendar.AM_PM) == 0 ? "AM" : "PM"));
                textViewTimeInfo.setTag(date);
            }

            if (null != aVObjectCatch.getNumber("bottom_depth")) {
                textViewBottomDepthInfo.setText(getNumberTextFromParseNumber(aVObjectCatch.getNumber("bottom_depth")) + " m");
            }

            if (null != aVObjectCatch.getNumber("catch_depth")) {
                textViewCatchDepthInfo.setText(getNumberTextFromParseNumber(aVObjectCatch.getNumber("catch_depth")) + " m");
            }

            if (null != aVObjectCatch.getString("water_visibility")) {

                textViewWaterVisibilityInfo.setText((aVObjectCatch.getString("water_visibility")));
            }

            buttonPost.setText("Save");
        } else {
            viewDelete.setVisibility(View.INVISIBLE);
            textViewTitle.setText("Add a Catch");
            buttonPost.setText("Post");
        }

    }


    @OnClick(R.id.viewFishMethod)
    public void onFishMethodClick() {


        OptionsPickerView pvOptions;
        pvOptions = new OptionsPickerView(this);
        ArrayList<ProvinceBean> options1Items = new ArrayList<ProvinceBean>();

        final String[] fishMethod = {
                "Bottom fishing",
                "Casting",
                "Fly fishing",
                "Free line",
                "Ice fishing",
                "Jerk fishing",
                "Jig fishing",
                "Pole fishing",
                "Sea fishing",
                "Surfcasting",
                "Trolling",
                "Vertical fishing"
        };

        for (int i = 0, isize = fishMethod.length; i < isize; i++) {
            options1Items.add(new ProvinceBean(i, fishMethod[i]));
        }

        pvOptions.setPicker(options1Items);
        pvOptions.setCyclic(false);
        pvOptions.setSelectOptions(0);

        pvOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                LogUtil.i(App.tag, "select:" + options1);

                textViewFishMethodInfo.setText(fishMethod[options1]);

            }
        });


        pvOptions.show();
    }


    @OnClick(R.id.viewDelete)
    public void onDeletCatch() {

        DialogUtils.showConfirmDialog(this, "", "Delete this Catch?", "Confirm", "Cancel", new DialogCallBackListener() {
            @Override
            public void onDone(boolean yesOrNo) {
                if (yesOrNo) {

                    if (null != aVObjectCatch) {

                        deleteAllCatchObject(aVObjectCatch, true, new DeleteCallback() {
                            @Override
                            public void done(AVException e) {
                                if (null == e) {
                                    setResult(RESULT_OK);
                                    finish();
                                } else {
                                    showNotifyTextIn5Seconds(e.getLocalizedMessage());
                                }

                            }
                        });
//
//
//                        showProgressDialog();
//
//                        AVQuery commentQuery = AVQuery.getQuery("Comment");
//                        commentQuery.whereEqualTo("catch", aVObjectCatch);
//                        commentQuery.deleteAllInBackground(new DeleteCallback() {
//                            @Override
//                            public void done(AVException e) {
//                                if (null == e) {
//
//                                }
//                            }
//                        });
//
//                        AVQuery notifycationQuery = AVQuery.getQuery("Notifications");
//                        notifycationQuery.whereEqualTo("catch", aVObjectCatch);
//
//                        notifycationQuery.deleteAllInBackground(new DeleteCallback() {
//                            @Override
//                            public void done(AVException e) {
//                                if (null == e) {
//
//                                }
//                            }
//                        });
//
//
//                        aVObjectCatch.deleteInBackground(new DeleteCallback() {
//                            @Override
//                            public void done(AVException e) {
//                                hideProgressDialog();
//                                if (null == e) {
//
//                                    setResult(RESULT_OK);
//                                    finish();
//                                } else {
//                                    showNotifyTextIn5Seconds(e.getLocalizedMessage());
//                                }
//                            }
//                        });
                    }
                }
            }
        });
    }

    @OnClick(R.id.viewTime)
    public void onTiemClick() {

        showDialogTime(textViewTimeInfo);
    }

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd,yyyy,hh:mm:ss");

    public void showDialogTime(final TextView textViewTimeInfo) {

        if (null != textViewTimeInfo.getTag()) {
            int ampm = getInput(textViewTimeInfo).endsWith("AM") ? 0 : 1;
            Calendar calendar = Calendar.getInstance();
            calendar.setTime((Date) textViewTimeInfo.getTag());

            DateTimePickerDialog.mDate = calendar;
            DateTimePickerDialog.ampm = ampm;
//            dialog.setmDate(calendar,ampm);
        }

        DateTimePickerDialog dialog = new DateTimePickerDialog(this, System.currentTimeMillis());


        dialog.setOnDateTimeSetListener(new DateTimePickerDialog.OnDateTimeSetListener() {
            public void OnDateTimeSet(AlertDialog dialog, long date, int ampm) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(date);
                Date d = calendar.getTime();
                textViewTimeInfo.setText(simpleDateFormat.format(d) + " " + String.valueOf((ampm == 0) ? "AM" : "PM"));
                textViewTimeInfo.setTag(d);

            }
        });
        dialog.show();
    }


    @OnClick(R.id.viewBottomDepth)
    public void onClickBottomDepth() {
        showEditDialog("Input Bottom Depth (m)", textViewBottomDepthInfo, getInput(textViewBottomDepthInfo), InputType.TYPE_CLASS_NUMBER, "m");
    }

    @OnClick(R.id.viewWalter)
    public void onWalterVisibility() {

//        showEditDialog("Input Bottom Depth (m)", textViewWaterVisibilityInfo, getInput(textViewWaterVisibilityInfo), InputType.TYPE_CLASS_NUMBER);


        OptionsPickerView pvOptions;
        pvOptions = new OptionsPickerView(this);
        ArrayList<ProvinceBean> options1Items = new ArrayList<ProvinceBean>();

        final String[] fishMethod = {
                "Excellent",
                "Very good",
                "Good",
                "Fair",
                "Poor",
                "Very poor",
        };


        for (int i = 0, isize = fishMethod.length; i < isize; i++) {
            options1Items.add(new ProvinceBean(i, fishMethod[i]));
        }

        pvOptions.setPicker(options1Items);
        pvOptions.setCyclic(false);
        pvOptions.setSelectOptions(0);

        pvOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                textViewWaterVisibilityInfo.setText(fishMethod[options1]);
            }
        });

        pvOptions.show();

    }


    @OnClick(R.id.viewCatchDepth)
    public void onCatchDepthClick() {

        showEditDialog("Input Catch Depth (m)", textViewCatchDepthInfo, getInput(textViewCatchDepthInfo), InputType.TYPE_CLASS_NUMBER, "m");

    }


    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            scrollViewContent.fullScroll(View.FOCUS_DOWN);

//            scrollViewContent.smoothScrollTo(0,scrollViewContent.getBottom());
        }
    };


    @OnClick(R.id.buttonShowMoreOrLess)
    public void onShowMoreOrLess() {


        if (linearLayoutDetail.getVisibility() == View.VISIBLE) {
            linearLayoutDetail.setVisibility(View.GONE);
//            MyAnimationUtils.animationHideview(linearLayoutDetail, animationScaleOut);
            buttonShowMoreOrLess.setText("Show more details");


        } else {
            linearLayoutDetail.setVisibility(View.VISIBLE);
//            MyAnimationUtils.animationShowView(linearLayoutDetail, animationScaleIn);
            buttonShowMoreOrLess.setText("Show less details");
        }
        handler.sendEmptyMessageDelayed(0, 200);
    }

    PhotoSelectDialogSimple photoSelectDialog = null;

    @OnClick(R.id.viewAddPhoto)
    public void onAddPictureClick() {

//        closeSoftKeyBorad(null, editDesc);

        hideSoftKeyborad(null, editDesc);

        if (null == photoSelectDialog) {
            photoSelectDialog = new PhotoSelectDialogSimple(this);
        }

        photoSelectDialog.setOnAddCatchListener(new PhotoSelectDialogSimple.OnPhotoSelectionActionListener() {
            @Override
            public void onAddClick() {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(AddCatchOrEditActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        startSelectMediaFromChose(AddCatchOrEditActivity.this);
                    } else {
                        ActivityCompat.requestPermissions(AddCatchOrEditActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, SHORT_REQUEST_READ_EXTEAL);
                    }
                } else {
                    startSelectMediaFromChose(AddCatchOrEditActivity.this);
                }
            }

            @Override
            public void onDismis() {
            }
        });
        photoSelectDialog.show();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == SHORT_REQUEST_READ_EXTEAL) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //yes allow take picture or
                if (type_request_picture == TYPE_REQEST_PICTURE_SELECT) {
                    startSelectMediaFromChose(AddCatchOrEditActivity.this);
                } else {
                    startSelectMediaFromChose(AddCatchOrEditActivity.this);
                }
            } else {
                showNotifyTextIn5Seconds("Read external storage access denied!");
            }
        }


    }

    @OnClick(R.id.viewSpecies)
    public void onSpeciesClick() {
        Tool.startActivityForResult(this, FishSpeciesActivity.class, SHORT_PIK_SPECIES);
    }

    @OnClick(R.id.relativeLayoutHeader)
    public void onBackClick() {
        onBack();
    }


    @OnClick(R.id.viewLocation)
    public void onLocationClick() {
        showEditDialog("Input Location", textViewLocationInfo, getInput(textViewLocationInfo), InputType.TYPE_CLASS_TEXT, "");
        // Tool.startActivityForResult(this, FishAreaSelectActivity.class, SHORT_PIK_LOCATION);
    }

    @OnClick(R.id.viewBait)
    public void onViewBaitClick() {
        showEditDialog("Input Bait", textViewBaitInfo, getInput(textViewBaitInfo), InputType.TYPE_CLASS_TEXT, "");
    }

    @OnClick(R.id.viewWeight)
    public void onWeightClick() {

        showEditDialog("Input Weight (kg)", textViewWeightInfo, getInput(textViewWeightInfo), InputType.TYPE_CLASS_NUMBER, "kg");

    }

    @OnClick(R.id.viewLength)
    public void onLengthClick() {
        showEditDialog("Input Length (cm)", textViewLengthInfo, getInput(textViewLengthInfo), InputType.TYPE_CLASS_NUMBER, "cm");
    }


    private void showEditDialog(final String title, final TextView textView, final String oldContent, final int inputType, final String suffix) {

        View dialogView = View.inflate(this, R.layout.dialog_input, null);
        final TextView textViewLabel = (TextView) dialogView.findViewById(R.id.textViewLabel);
        final EditText editContent = (EditText) dialogView.findViewById(R.id.editContent);

        if (!StringUtils.isEmpty(oldContent)) {

            if (oldContent.endsWith(suffix)) {
                editContent.setText(oldContent.trim().replace(suffix, ""));
            } else {
                editContent.setText(oldContent.trim());
            }
        }

        editContent.setInputType(inputType);
        if (inputType == InputType.TYPE_CLASS_NUMBER) {
            editContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5)});
        } else {
            editContent.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
        }


        editContent.setSelection(getInput(editContent).trim().length());
        textViewLabel.setText(title);

        AlertDialog.Builder builder = new AlertDialog.Builder(AddCatchOrEditActivity.this);
        builder.setView(dialogView);
        final AlertDialog dlg = builder.create();

        dlg.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        dlg.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);


        Button buttonReset = (Button) dialogView.findViewById(R.id.buttonReset);
        Button buttonConfirm = (Button) dialogView.findViewById(R.id.buttonConfirm);

        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlg.dismiss();
//                hideSoftKeyborad(imm, dlg.getCurrentFocus());
//                dlg.getWindow().setSoftInputMode(
//                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
//                );

            }
        });

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlg.dismiss();
//                hideSoftKeyborad(imm, dlg.getCurrentFocus());

//                dlg.getWindow().setSoftInputMode(
//                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
//                );


                String content = getInput(editContent);
                if (!StringUtils.isEmpty(content)) {
//                    textView.setText(content);

                    if (content.endsWith(suffix)) {
                        textView.setText(content.trim());
                    } else {
                        textView.setText(content.trim() + " " + suffix);
                    }
                } else {
                    editContent.startAnimation(AnimationUtils.loadAnimation(AddCatchOrEditActivity.this, R.anim.shake));
                }
            }
        });

        dlg.show();
    }


    public void closeSoftKeyBorad(InputMethodManager imm, View view) {

        if (null == imm) {
            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        }
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//        imm.hideSoftInputFromWindow(rootView.getWindowToken(),0);
    }


    public void hideSoftKeyborad(InputMethodManager imm, View view) {

        if (null == imm) {
            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        }
        imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
    }

    public void hideSoftKeyBoradAgain(InputMethodManager imm, View view) {

        if (null == imm) {
            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }

    private String readExif(String file) {
        String exif = "";
        try {
            ExifInterface exifInterface = new ExifInterface(file);

            exif += GPSConverter.convertLat(exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE), exifInterface.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF));
            exif += "," + GPSConverter.convertLon(exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE), exifInterface.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF));

        } catch (IOException e) {
            LogUtil.e(App.tag, e.getMessage());
            ;
        }
        return exif;
    }

    boolean isUploadAllPicSuccess = true;

    @OnClick(R.id.buttonPost)
    public void onPostClick() {

        if (!NetTool.isNetworkAvailable(this)) {

            showNotifyTextIn5Seconds(R.string.error_net);

            return;
        }

        isUploadAllPicSuccess = true;

        if (checkValue()) {

            if (null == aVObjectCatch) {
                aVObjectCatch = AVObject.create("Catch");
                aVObjectCatch.put("comments_count", 0);
                aVObjectCatch.put("likes_count", 0);
                aVObjectCatch.put("owner", AVUser.getCurrentUser());

                AVACL pacl = new AVACL();
                pacl.setPublicReadAccess(true);
                pacl.setPublicWriteAccess(true);
                aVObjectCatch.setACL(pacl);

//                if (null != latLng) {
//                    aVObjectCatch.put("location", new AVGeoPoint(latLng.latitude, latLng.longitude));
//                } else if (null != App.getApp().getAvGeoPoint()) {
//                    aVObjectCatch.put("location", App.getApp().getAvGeoPoint());
//                }

            }
//            else {
//
//                if (null != App.getApp().getAvGeoPoint()) {
//                    aVObjectCatch.put("location", App.getApp().getAvGeoPoint());
//                } else if (null != latLng) {
//                    aVObjectCatch.put("location", new AVGeoPoint(latLng.latitude, latLng.longitude));
//                }
//            }

            if (!StringUtils.isEmpty(getInput(editDesc))) {
                aVObjectCatch.put("desc", getInput(editDesc));
            }


            if (null != textViewLocationInfo.getTag()) {
                aVObjectCatch.put("area", getInput(textViewLocationInfo));
            }

//            aVObjectCatch.put("hide_location", isHideLocation);

            if (null != textViewSpeciesInfo.getTag()) {
                aVObjectCatch.put("fish", textViewSpeciesInfo.getTag());
            }


            if (!StringUtils.isEmpty(getInput(textViewBaitInfo))) {
                aVObjectCatch.put("bait", getInput(textViewBaitInfo));
            }

            if (!StringUtils.isEmpty(getInput(textViewWeightInfo))) {
                aVObjectCatch.put("weight", Tool.getInputNumber(textViewWeightInfo));
            }

            if (!StringUtils.isEmpty(getInput(textViewLengthInfo))) {
                aVObjectCatch.put("length", Tool.getInputNumber(textViewLengthInfo));
            }

            if (!StringUtils.isEmpty(getInput(textViewFishMethodInfo))) {
                aVObjectCatch.put("method", getInput(textViewFishMethodInfo));
            }

            if (!StringUtils.isEmpty(getInput(textViewTimeInfo))) {
                aVObjectCatch.put("date", textViewTimeInfo.getTag());
            }

            if (!StringUtils.isEmpty(getInput(textViewBottomDepthInfo))) {
                aVObjectCatch.put("bottom_depth", Tool.getInputNumber(textViewBottomDepthInfo));
            }

            if (!StringUtils.isEmpty(getInput(textViewCatchDepthInfo))) {
                aVObjectCatch.put("catch_depth", Tool.getInputNumber(textViewCatchDepthInfo));
            }

            if (!StringUtils.isEmpty(getInput(textViewWaterVisibilityInfo))) {
                aVObjectCatch.put("water_visibility", (getInput(textViewWaterVisibilityInfo)));
            }

            //deal those files

            List<String> allFilesSected = itemCatchPictureAdapter.getAllSelectFiles();
            final List<AVFile> aVFilesNewNeedSave = new ArrayList<AVFile>();


            final ArrayList<AVFile> parseOldNeedResaveFile = new ArrayList<AVFile>();
            final ArrayList<String> videoFiles = new ArrayList<String>();


            if (!ListUtiles.isEmpty(allFilesSected)) {

                for (String file : allFilesSected) {

                    if (file.startsWith("http")) {
                        List<AVFile> oldFileList = aVObjectCatch.getList("photos");
                        if (null != oldFileList && !ListUtiles.isEmpty(oldFileList)) {
                            for (AVFile pfold : oldFileList) {
                                if (pfold.getUrl().equals(file)) {
                                    parseOldNeedResaveFile.add(pfold);
                                    break;
                                }
                            }
                        }
                    } else {
                        //处理文件上传
                        File picFile = new File(file);

                        if (picFile.exists()) {
                            try {
//                                AVFile aVFile = AVFile.withFile(picFile.getName(), picFile);
                                AVFile aVFile = AVFile.withAbsoluteLocalPath(picFile.getName(), picFile.getAbsolutePath());

                                if (ParseHelper.isVideo(picFile)) {
                                    aVFile.addMetaData("type", "video/mp4");
                                } else {
                                    aVFile.addMetaData("type", "image/jpeg");
                                }
                                aVFilesNewNeedSave.add(aVFile);
                            } catch (Exception e) {
                                LogUtil.e(App.tag, "add exception throw!");
                            }
                        }

                    }
                }

            }

            fileCountSaved = 0;
            if (!ListUtiles.isEmpty(aVFilesNewNeedSave)) {
                showProgressDialog(false);

                for (int i = 0, isize = ListUtiles.getListSize(aVFilesNewNeedSave); i < isize; i++) {
                    if (isUploadAllPicSuccess == false) {
                        hideProgressDialog();
                        break;
                    }

                    aVFilesNewNeedSave.get(i).saveInBackground(new SaveCallback() {
                        @Override
                        public void done(AVException e) {

                            if (e == null) {
                                fileCountSaved++;
                                LogUtil.i(App.tag, "save file:" + fileCountSaved + "/" + aVFilesNewNeedSave.size());
                                if (fileCountSaved == ListUtiles.getListSize(aVFilesNewNeedSave)) {
                                    hideProgressDialog();

                                    if (!ListUtiles.isEmpty(aVFilesNewNeedSave)) {
                                        aVObjectCatch.put("photos", aVFilesNewNeedSave);
                                    }

                                    if (!ListUtiles.isEmpty(parseOldNeedResaveFile)) {
                                        aVObjectCatch.addAll("photos", parseOldNeedResaveFile);
                                    }

                                    saveAVObject(aVObjectCatch, true, true, new SaveCallback() {
                                        @Override
                                        public void done(AVException e) {
                                            if (null == e) {
                                                setResult(RESULT_OK);
                                                App.getApp().putTemPObject("NewCatch", aVObjectCatch);
                                                Intent intent = new Intent();
                                                intent.putExtra(App.IMAGE_LOCATION, readExif(sharedpreferencesUtil.getImageTempNameUri().getPath()));
                                                intent.putExtra(App.IMAGE_AREA, textViewLocationInfo.getText());
                                                Tool.startActivity(AddCatchOrEditActivity.this, FishLocationConfirmationActivity.class, intent);
                                                finish();
                                            } else {
                                                hideProgressDialog();
                                                showLeanCloudError(e);
                                                isUploadAllPicSuccess = false;
                                                return;
                                            }
                                        }
                                    });
                                }
                            } else {
                                isUploadAllPicSuccess = false;
                                showLeanCloudError(e);
                                return;
                            }
                        }
                    });
                }

                if (isUploadAllPicSuccess == false) {
                    return;
                }

            } else {

                if (ListUtiles.isEmpty(allFilesSected)) {
                    aVObjectCatch.remove("photos");
                } else {
                    aVObjectCatch.put("photos", parseOldNeedResaveFile);
                }

                saveAVObject(aVObjectCatch, true, true, new SaveCallback() {
                    @Override
                    public void done(AVException e) {
                        if (null == e) {
                            setResult(RESULT_OK);
                            App.getApp().putTemPObject("NewCatch", aVObjectCatch);
                            Intent intent = new Intent();
                            intent.putExtra(App.IMAGE_LOCATION, readExif(sharedpreferencesUtil.getImageTempNameUri().getPath()));
                            intent.putExtra(App.IMAGE_AREA, textViewLocationInfo.getText());
                            Tool.startActivity(AddCatchOrEditActivity.this, FishLocationConfirmationActivity.class, intent);
                            finish();
                        } else {
                            showLeanCloudError(e);
                        }
                    }
                });
            }
        }

    }


    public boolean checkValue() {

//        if (null == textViewLocationInfo.getTag()) {
//            showNotifyTextIn5Seconds("Location is required.");
//            return false;
//        }
//        if (null == textViewSpeciesInfo.getTag()) {
//            showNotifyTextIn5Seconds("Species is required.");
//            return false;
//        }


        return true;
    }


    @Override
    public void onBackPressed() {
        onBack();
    }

    public void onBack() {
        if (isCompress) {
            return;
        }
        finish();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //====pik fish spicies
        if (requestCode == SHORT_PIK_SPECIES && resultCode == RESULT_OK) {

            if (App.getApp().hasTempKey("fish")) {
                aVObjectFish = (AVObject) App.getApp().getTempObject("fish");
                textViewSpeciesInfo.setText(aVObjectFish.getString("name"));
                textViewSpeciesInfo.setTag(aVObjectFish);
            }

        }

        //======pik location
//        if (requestCode == SHORT_PIK_LOCATION) {
//
//            LatLng tlatLng = (LatLng) App.getApp().getTempObject("latlng");
//
//            if (App.getApp().hasTempKey("area") && App.getApp().hasTempKey("area")) {
//                aVObjectArea = (AVObject) App.getApp().getTempObject("area");
//                textViewLocationInfo.setText(aVObjectArea.getString("title"));
//                textViewLocationInfo.setTag(aVObjectArea);
//            } else if (null != data) {
//
//                aVObjectArea = AVObject.create("Area");
//                aVObjectArea.put("title", data.getStringExtra("location_name"));
//                aVObjectArea.put("coordinate", new AVGeoPoint(tlatLng.latitude, tlatLng.longitude));
//                aVObjectArea.put("pending", true);
//                textViewLocationInfo.setText(data.getStringExtra("location_name"));
//                textViewLocationInfo.setTag(aVObjectArea);
//
//            }
//
//            if (resultCode == LocationMapActivity.USE_PIN) {//使用选顶地点
//                isHideLocation = false;
//                latLng = tlatLng;
//            }
//            if (resultCode == LocationMapActivity.HIDE_LOCATION) {
//                isHideLocation = true;
//                latLng = tlatLng;
//            }
//        }

        //=======deal pick pciture from camera or location store
        String mFileName;
        if (requestCode == SELECT_PIC && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getDataColumn(getApplicationContext(), data.getData(), null, null);
                PhotoLibUtils.copyFile(mFileName, sharedpreferencesUtil.getImageTempNameString2());
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
                cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
            }
        }
        if (requestCode == SELECT_PIC_KITKAT && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getPath(getApplicationContext(), data.getData());
                String newPath = sharedpreferencesUtil.getImageTempNameString2();
                PhotoLibUtils.copyFile(mFileName, newPath);
                Uri uri = sharedpreferencesUtil.getImageTempNameUri();
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
                cropImageUri(uri, outoutUri, outputX, outputY, CROP_BIG_PICTURE);

            }
        }
        if (requestCode == TAKE_BIG_PICTURE && resultCode == RESULT_OK) {
            Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
            cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
        }
        if (requestCode == CROP_BIG_PICTURE && resultCode == Activity.RESULT_OK) {

            final ArrayList<String> allFiles = new ArrayList<String>();

            if (sharedpreferencesUtil.getImageTempNameUri() != null) {
                Uri result = sharedpreferencesUtil.getImageTempNameUriCroped();
                App.firstRun();

                if (!StringUtils.isEmpty(result.getPath())) {

                    final String picPath = result.getPath();
                    final String picPathCompred = picPath + System.currentTimeMillis() + ".compred.jpg";

                    LogUtil.i(App.tag, "src path:" + picPath);
                    LogUtil.i(App.tag, "compred path:" + picPathCompred);

                    final File selectFile = new File(picPath);
                    if (null != selectFile && selectFile.exists()) {


                        Target target = new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmapSource, Picasso.LoadedFrom from) {
                                try {
                                    bitmapSource.compress(Bitmap.CompressFormat.JPEG, 75, new FileOutputStream(picPathCompred));
                                    LogUtil.i(App.tag, "file length after compress:" + picPathCompred.length());
                                    allFiles.add(picPathCompred);

                                    itemCatchPictureAdapter.addMulImageFile(allFiles);

                                } catch (Exception e) {
                                    LogUtil.e(App.tag, "compress picture fail!");
                                }


                                hideProgressDialog();
                                textViewTitle.setTag(null);
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                                textViewTitle.setTag(null);
                                hideProgressDialog();
                                LogUtil.e(App.tag, "load bitmap error...");
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                                showProgressDialog(false);
                            }
                        };

                        textViewTitle.setTag(target);
                        Picasso.with(App.getApp()).invalidate(selectFile);
                        Picasso.with(App.getApp()).load(selectFile).into(target);

                    } else {
                        LogUtil.i(App.tag, "file not exist!");
                    }
                }
            }

            //compress when select video ok
            if (!ListUtiles.isEmpty(listVideo)) {
                compressVideo(listVideo.get(0));
            }
        }


        if (requestCode == SHORT_REQUEST_MEDIAS && resultCode == RESULT_OK) {
            List<String> picture = data.getStringArrayListExtra("listPicture");
            listVideo = data.getStringArrayListExtra("listVideo");
            //if just select video ,compres at once ,else when crop picture finish,compress
            if (!ListUtiles.isEmpty(picture)) {
                if (1 == ListUtiles.getListSize(picture)) {
                    mFileName = picture.get(0);
                    sharedpreferencesUtil.setNewImageTempName();
                    PhotoLibUtils.copyFile(mFileName, sharedpreferencesUtil.getImageTempNameString2());
                    Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();

                    outputX = 512;
                    outputY = 512;

                    cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
                }
            } else if (!ListUtiles.isEmpty(listVideo)) {
                compressVideo(listVideo.get(0));
            }

        }
    }


    @Override
    protected void onDestroy() {

        if (null != com) {
            com.killPro();
            com = null;
        }
        System.gc();
        aVObjectCatch = null;
        super.onDestroy();
    }

}

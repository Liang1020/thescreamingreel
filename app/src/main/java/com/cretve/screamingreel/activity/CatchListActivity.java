package com.cretve.screamingreel.activity;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.common.util.LogUtil;
import com.common.util.MyAnimationUtils;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.MenuAdapter;
import com.thescreamingreel.androidapp.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by wangzy on 15/11/2.
 */
public class CatchListActivity extends BaseScreamingReelActivity {


    @InjectView(R.id.listViewMenu)
    ListView listViewMenu;

    @InjectView(R.id.linearLayoutMenuContent)
    LinearLayout linearLayoutMenuContent;

    @InjectView(R.id.imageViewIconDown)
    ImageView imageViewIconDown;

    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        LogUtil.i(App.tag, "oncreate catch list");
    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_catch_list);
        ButterKnife.inject(this);
        final MenuAdapter menuAdapter = new MenuAdapter(this);
        listViewMenu.setAdapter(menuAdapter);
        listViewMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                menuAdapter.select(i);
            }
        });
    }


    @OnClick(R.id.linearLayoutTitleMenu)
    public void onMenuClick() {

        linearLayoutMenuContent.clearAnimation();

        if (linearLayoutMenuContent.getVisibility() == View.VISIBLE) {

            MyAnimationUtils.animationHideview(linearLayoutMenuContent, AnimationUtils.loadAnimation(this, R.anim.slide_out_to_top));

            textViewTitle.setTextColor(getResources().getColor(R.color.text_color_ios_blue));
            imageViewIconDown.setVisibility(View.VISIBLE);

        } else {
            MyAnimationUtils.animationShowView(linearLayoutMenuContent, AnimationUtils.loadAnimation(this, R.anim.slide_in_from_top));

            textViewTitle.setTextColor(getResources().getColor(R.color.gray_text_color));
            imageViewIconDown.setVisibility(View.GONE);
        }
    }

    private void showMenu() {
        int height = linearLayoutMenuContent.getHeight();




    }

    public void hideMenu() {
        int height = linearLayoutMenuContent.getHeight();

        ValueAnimator valueAnimator = ValueAnimator.ofInt(0,-height);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int v = (int) valueAnimator.getAnimatedValue();
                RelativeLayout.LayoutParams lpp = (RelativeLayout.LayoutParams) linearLayoutMenuContent.getLayoutParams();
                lpp.topMargin = v;
                linearLayoutMenuContent.setLayoutParams(lpp);
            }
        });
        valueAnimator.setDuration(300);
        valueAnimator.start();



    }


    @OnClick(R.id.imageButtonSetting)
    public void onClickSetting() {
        Tool.startActivity(this, SettingActivity.class);
        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (null != intent && intent.getBooleanExtra("isLogout", false)) {
            finish();//this is logout intent
        }
    }
}

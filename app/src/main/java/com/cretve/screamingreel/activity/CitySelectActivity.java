package com.cretve.screamingreel.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.AssertTool;
import com.common.util.ListUtiles;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.LocationAdapter;
import com.thescreamingreel.androidapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

/**
 * Created by wangzy on 15/10/29.
 */
public class CitySelectActivity extends BaseScreamingReelActivity {


    @InjectView(R.id.editTextLocationInput)
    EditText editTextLocationInput;

    @InjectView(R.id.listViewLocation)
    ListView listViewLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_selection);
        ButterKnife.inject(this);
    }


    @OnClick(R.id.viewBack)
    public void onBackClick() {
        finish();
    }

    private String lastSearch = "";


    @Override
    protected void onPause() {
        super.onPause();
        hideSoftKeyborad(null, editTextLocationInput);

    }

    @OnTextChanged(R.id.editTextLocationInput)
    public void onInputTextChange() {

//        String input = getInput(editTextLocationInput);
//        if (null != input && input.endsWith(" ") && input.trim().equals(lastSearch)) {
//            editTextLocationInput.setText(input.replace(" ", ""));
//            editTextLocationInput.setSelection(input.trim().length());
//            return;
//        }

        String searcTarget = getInput(editTextLocationInput).trim();
        if (!lastSearch.equals(searcTarget)) {
            getLocationList(lastSearch, false);
            lastSearch = searcTarget;
        }
    }

    @OnClick(R.id.linearLayoutSearch)
    public void onSearchClick() {
        getLocationList(getInput(editTextLocationInput), true);
    }

    private void getLocationList(final String input, final boolean showDialog) {
        reSetTask();
        baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    showNewDialogWithNewTask(baseTask);
                }
//                showNewDialogWithNewTask(baseTask);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask bTask) {

                NetResult netResult = new NetResult();

                try {

                    AVQuery<AVObject> queryCountry = AVQuery.getQuery("City");
                    queryCountry.whereMatches("country", ".*" + input + ".*", "i");

                    AVQuery<AVObject> queryCity = AVQuery.getQuery("City");
//                    queryCity.whereContains("name", input);

//                    ".*" + inputText + ".*",
                    queryCity.whereMatches("name", ".*" + input + ".*", "i");


                    List<AVQuery<AVObject>> queries = new ArrayList<AVQuery<AVObject>>();
                    queries.add(queryCountry);
                    queries.add(queryCity);

                    AVQuery<AVObject> mainQuery = AVQuery.or(queries);

                    List<AVObject> list = mainQuery.find();
                    netResult.setTag(list);

                } catch (AVException e) {
                    netResult.setException(e);
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    hideNewDialogWithTask(baseTask);
                }

//                hideNewDialogWithTask(baseTask);
                if (null == result.getException()) {
                    List<AVObject> aVObjectCitys = (List<AVObject>) result.getTag();
                    buildCountryAdapter(input, aVObjectCitys);
                } else {
                    showNotifyTextIn5Seconds(((AVException) result.getException()).getMessage());
                }
            }
        });


        baseTask.execute(new HashMap<String, String>());
    }


    private void buildCountryAdapter(String input, List<AVObject> aVObjectCitys) {

        ArrayList<AVObject> totalObjects = new ArrayList<AVObject>();

        if (ListUtiles.isEmpty(aVObjectCitys)) {
            showToastWithTime("No result!", 2);
        } else {
            AVObject aVObjectCity = AVObject.create("City");
            aVObjectCity.put("name", "City");
            totalObjects.add(aVObjectCity);
        }
        //===========================
        totalObjects.addAll(aVObjectCitys);
        //===========================

        ArrayList<String> countrys = AssertTool.readLinesFromAssertsFiles(this, "country");
        List<AVObject> countryObjs = new ArrayList<AVObject>();

        for (int i = 0, isize = countrys.size(); i < isize; i++) {
            String[] country = countrys.get(i).split(",");
            String code = country[0].trim();
            String name = country[1].trim();
            if (name.contains(input)) {
                AVObject aVObject = AVObject.create("City");
                aVObject.put("country", code);
                aVObject.put("name", name);
                countryObjs.add(aVObject);
            }
        }

        if (!ListUtiles.isEmpty(countryObjs)) {
            AVObject countryObjDivder = AVObject.create("City");
            countryObjDivder.put("name", "Country");
            totalObjects.add(countryObjDivder);
            //===========
            totalObjects.addAll(countryObjs);
        }

//        LogUtil.i(App.tag, "list size:" + aVObjectCitys.size());
        LocationAdapter locationAdapter = new LocationAdapter(CitySelectActivity.this, totalObjects);
        listViewLocation.setAdapter(locationAdapter);
        listViewLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AVObject plocation = (AVObject) adapterView.getItemAtPosition(i);
                if (null != plocation && (!"City".equals(plocation.get("name")) && (!"Country".equals(plocation.get("name"))))) {
                    putDataBack(plocation);
                }
            }
        });
    }

    public void putDataBack(AVObject aVObject) {

        App.getApp().putTemPObject("city", aVObject);
        setResult(Activity.RESULT_OK);
        finish();
    }


    public static interface OnCitySelectListener {
        public void onSelectCity(AVObject cityObj);
    }
}


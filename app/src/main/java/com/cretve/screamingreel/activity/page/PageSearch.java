package com.cretve.screamingreel.activity.page;

import android.app.FragmentTransaction;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.common.util.Tool;
import com.cretve.screamingreel.BaseScreamPage;
import com.cretve.screamingreel.activity.MainActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.thescreamingreel.androidapp.R;

/**
 * Created by wangzy on 15/11/9.
 */
public class PageSearch extends BaseScreamPage implements OnMapReadyCallback{

    private View coverView;
    private EditText editTextLocationInput;
    private LinearLayout linearLayoutMapContainer;
    private static final String MAP_FRAGMENT_TAG = "map";
    public PageSearch(MainActivity activity) {
        super(activity);
    }

    @Override
    public void initView() {
        this.rootView = View.inflate(activity, R.layout.page_search, null);
        this.coverView = findViewById(R.id.viewCover);
        this.coverView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coverView.setVisibility(View.GONE);
            }
        });
        this.editTextLocationInput = findEditTextById(R.id.editTextLocationInput);
        this.editTextLocationInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    coverView.setVisibility(View.VISIBLE);
                } else {
                    coverView.setVisibility(View.GONE);
                }
            }
        });
        this.editTextLocationInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coverView.setVisibility(View.VISIBLE);
                activity.fishListDialogView.getListViewFishes().setVisibility(View.VISIBLE);
            }
        });
        this.linearLayoutMapContainer = findLinearLayout(R.id.linearLayoutMapContainer);

        setupMap();
    }

    private void setupMap() {

        if (!Tool.isInstallGMS(activity)) {

            return;
        }



//        SupportMapFragment mapFragment = (SupportMapFragment)activity.getSupportFragmentManager().findFragmentByTag(MAP_FRAGMENT_TAG);
//
//        // We only create a fragment if it doesn't already exist.
//        if (mapFragment == null) {
//            // To programmatically add the map, we first create a SupportMapFragment.
//            mapFragment = SupportMapFragment.newInstance();
//
//            // Then we add it using a FragmentTransaction.
//            FragmentTransaction fragmentTransaction =getSupportFragmentManager().beginTransaction();
//            fragmentTransaction.add(android.R.id.content, mapFragment, MAP_FRAGMENT_TAG);
//            fragmentTransaction.commit();
//        }
//        mapFragment.getMapAsync(this);

//        MapFragment.newInstance();


        MapFragment mapFragment=new MapFragment();

        FragmentTransaction transaction =activity.getFragmentManager().beginTransaction();
        transaction.add(R.id.linearLayoutMapContainer,mapFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

    }
}

package com.cretve.screamingreel.activity;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.baidu.location.BDLocation;
import com.common.adapter.TextWatcherAdapter;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.MyAnimationUtils;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.activity.page.PageProfile;
import com.cretve.screamingreel.adapter.AddressAdapter;
import com.cretve.screamingreel.adapter.MainFragmentAdapter;
import com.cretve.screamingreel.fragment.MusicFragment;
import com.cretve.screamingreel.photo.lib.PhotoLibUtils;
import com.cretve.screamingreel.util.IabBroadcastReceiver;
import com.cretve.screamingreel.util.IabHelper;
import com.cretve.screamingreel.util.IabResult;
import com.cretve.screamingreel.util.Inventory;
import com.cretve.screamingreel.util.Purchase;
import com.cretve.screamingreel.view.AddPhotoDialogInCatchListFromMedia;
import com.cretve.screamingreel.view.DialogSubscrib;
import com.cretve.screamingreel.view.FishListDialogView;
import com.cretve.screamingreel.view.HeaderTab;
import com.cretve.screamingreel.view.PhotoSelectDialog;
import com.cretve.screamingreel.view.WeatherDialog;
import com.facebook.FacebookSdk;
import com.facebook.share.widget.LikeView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.learnncode.mediachooser.MediaChooser;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.thescreamingreel.androidapp.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by wangzy on 15/11/9.
 */
public class MainActivity extends BaseScreamingReelActivity implements IabBroadcastReceiver.IabBroadcastListener {

    IabHelper mHelper;
    IabBroadcastReceiver mBroadcastReceiver;

    boolean mSubscribed = false;

    static final String ITEM = "ITEM_MAP_RINGTONE";
    static final String SKU_PRODUCT_MONTHLY = "com.thescreamingreel.androidapp.inappproduct";
    static final String SKU_PRODUCT_YEARLY = "com.thescreamingreel.androidapp.year";

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            if (!intent.getBooleanExtra("isCompleted", false)) return;
            LogUtil.i("IAB", "ui refresh begin");
            mSubscribed = true;
            initView();
        }
    };


    @InjectView(R.id.viewPager)
    ViewPager viewPager;

    @InjectView(R.id.headerTab)
    HeaderTab headerTab;

    //    MainAdapter mainAdapter;
    MainFragmentAdapter mainAdapter;

    @InjectView(R.id.linearLayoutBottomOperateBar)
    LinearLayout linearLayoutBottomOperateBar;

    @InjectView(R.id.imageButtonFishData)
    ImageButton imageButtonFishData;

    @InjectView(R.id.imageButtonWeather)
    ImageButton imageButtonWeather;

    @InjectView(R.id.imageButtonAddCatch)
    ImageButton imageButtonAddCatch;

    @InjectView(R.id.linearlayout_block_facebookLike)
    View facebookLikeBlock;

    @InjectView(R.id.linearLayoutBottomFish)
    LinearLayout linearLayoutBottomFish;


    @InjectView(R.id.viewCoverMap)
    View viewCoverMap;

    @InjectView(R.id.viewSearhMap)
    View viewSearhMap;

    @InjectView(R.id.editTextLocationInput)
    EditText editTextLocationInput;

    @InjectView(R.id.listViewMaps)
    ListView listViewMaps;

    @InjectView(R.id.imageViewCloseInput)
    ImageView imageViewCloseInput;

    @InjectView(R.id.log)
    TextView textLog;


    private List<AVObject> listCurrentCatces;
    private List<AVObject> listCurrentAreas;


    private final byte REQUESTPICTURE_NONE = -1;
    private final byte REQUESTPICTURE_HEADER = 0;
    private final byte REQUESTPICTURE_CATCH = 1;


    private byte requestPicture = REQUESTPICTURE_HEADER;
    private TextView skipTextView;
//    private TextView facebookLikeTextView;

    private GoogleMap googleMap;
    private BaseTask requestLocationTask;
    private ArrayList<String> listVideo;


    private final int INT_MAX_RESULT_COUNT = 20;
    private double FLOAT_NEAR_KILOMITER = 1000;

    private String base64EncodedPublicKey;
    private ArrayList<String> skuList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LogUtil.i("OnCreate", "----------------------------------");

        IntentFilter filter = new IntentFilter(SubscriptionTermActivity.action);
        registerReceiver(broadcastReceiver, filter);

        base64EncodedPublicKey = getResources().getString(R.string.base64_encoded_public_key);

        ButterKnife.inject(this);
        App.getApp().setMainActivityCreate(true);

        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.enableDebugLogging(true);

        mSubscribed = checkIsExpiredFromLeancloud();

        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {

                if (!result.isSuccess()) {
                    LogUtil.i("IAB", "Problem setting up in-app billing: " + result);
                    return;
                }

                if (mHelper == null) return;

                mBroadcastReceiver = new IabBroadcastReceiver(MainActivity.this);
                IntentFilter broadcastFilter = new IntentFilter(IabBroadcastReceiver.ACTION);
                registerReceiver(mBroadcastReceiver, broadcastFilter);

                skuList.add(SKU_PRODUCT_MONTHLY);
                skuList.add(SKU_PRODUCT_YEARLY);

                try {
                    mHelper.queryInventoryAsync(true, null, skuList, mGotInventoryListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    LogUtil.e("IAB", "Error querying inventory. Another async operation in progress.");
                }

            }
        });

        initView();
        FacebookSdk.sdkInitialize(App.getApp());

    }



    @OnClick(R.id.imageViewCloseInput)
    public void onDeleteInput() {
        editTextLocationInput.setText("");
        listViewMaps.setVisibility(View.GONE);
        hideSoftKeyborad(null, getCurrentFocus());
    }


//    private void requestAccessLocationWithNoPermissionCheck() {
//        if(null!=App.getApp().getParseGeoPoint()){
//            return;
//        }
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                startLocationService();
//            }
//        } else {
//            startLocationService();
//        }
//
//    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitBy2Click(); //调用双击退出函数
        }
        return false;
    }

    /**
     * 双击退出函数
     */
    private static Boolean isExit = false;

    private void exitBy2Click() {
        Timer tExit = null;
        if (isExit == false) {
            isExit = true; // 准备退出
            Toast.makeText(this, "Click again to exit app!", Toast.LENGTH_SHORT).show();
            tExit = new Timer();
            tExit.schedule(new TimerTask() {
                @Override
                public void run() {
                    isExit = false; // 取消退出
                }
            }, 2000); // 如果2秒钟内没有按下返回键，则启动定时器取消掉刚才执行的任务

        } else {
            Intent intent = new Intent(this, WelcomeActivity.class);
            intent.putExtra("isExit", true);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        App.getApp().setMainActivityCreate(false);

        unregisterReceiver(broadcastReceiver);
        if (mBroadcastReceiver != null) {
            unregisterReceiver(mBroadcastReceiver);
        }

        if (mHelper != null) {
            mHelper.disposeWhenFinished();
            mHelper = null;
        }
    }

    private void showSearchContent() {
        viewCoverMap.setVisibility(View.VISIBLE);
        viewSearhMap.setVisibility(View.VISIBLE);

        if (StringUtils.isEmpty(getInput(editTextLocationInput))) {
            listViewMaps.setVisibility(View.GONE);
        } else {
            listViewMaps.setVisibility(View.VISIBLE);
        }
    }

    public void hideSearchContent() {
        viewCoverMap.setVisibility(View.GONE);
        viewSearhMap.setVisibility(View.GONE);
        listViewMaps.setVisibility(View.GONE);
    }


    WeatherDialog weatherDialog = null;

    @OnClick(R.id.imageButtonWeather)
    public void onWeatherClick() {

        if (null == weatherDialog) {
            weatherDialog = new WeatherDialog(this);
        }
        weatherDialog.show();
    }

    private boolean isMarkerClick = false;

    private void initMapDragListener(final GoogleMap googleMap) {

        if (null != googleMap) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                requestAccessLocation();
            } else {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                } else {
                    googleMap.setMyLocationEnabled(true);
                }
            }

            googleMap.getUiSettings().setMapToolbarEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            googleMap.getUiSettings().setCompassEnabled(true);

//            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//                @Override
//                public void onMapClick(LatLng latLng) {
//                    LogUtil.i(App.tag,"laglng:("+latLng.latitude+","+latLng.longitude+")");
//                }
//            });

            googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {

                    if (isMarkerClick) {
                        isMarkerClick = false;
                        return;
                    }

                    if (mode_search != MODE_FISH_ONLEY) {
                        requestAreaAndCatchFromMapCenter(null);
                    }
                }
            });


            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    isMarkerClick = true;
                    return false;
                }
            });

            googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {
                    if (null != location) {
                        App.getApp().setAvGeoPoint(new AVGeoPoint(location.getLatitude(), location.getLongitude()));
                    }
                }
            });

            googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                @Override
                public View getInfoWindow(Marker marker) {
                    View v = null;

                    String contents[] = marker.getSnippet().split(",");

                    if (contents[2].equals("0")) {//catch
                        v = getLayoutInflater().inflate(R.layout.map_infor_window, null);

                        String title = marker.getTitle();
                        TextView textViewTitle = (TextView) v.findViewById(R.id.textViewName);
                        textViewTitle.setText(title);
                        ImageView imageViewHeader = (ImageView) v.findViewById(R.id.imageViewHeader);

                        String imgUrl = marker.getSnippet().split(",")[1];
                        if (!StringUtils.isEmpty(imgUrl)) {
                            Picasso.with(MainActivity.this)
                                    .load(imgUrl)
                                    .resize(70, 70)
                                    .config(Bitmap.Config.RGB_565)
                                    .placeholder(R.drawable.icon_header_default)
                                    .into(imageViewHeader);
                        }
                    } else if (contents[2].equals("1")) {//area
                        v = getLayoutInflater().inflate(R.layout.map_infor_window_location, null);
                        String title = marker.getTitle();
                        TextView textViewTitle = (TextView) v.findViewById(R.id.textViewName);
                        textViewTitle.setText(title);
                    }

                    return v;
                }

                @Override
                public View getInfoContents(Marker marker) {

                    return null;
                }
            });


            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    try {

                        String content[] = marker.getSnippet().split(",");
                        if (content[2].equals("0") && !ListUtiles.isEmpty(listCurrentCatces)) {
                            String objectId = content[0];
                            for (AVObject catchObj : listCurrentCatces) {

                                if (catchObj.getObjectId().equals(objectId)) {

                                    App.getApp().putTemPObject("catch", catchObj);
                                    Tool.startActivity(MainActivity.this, CatchDetailActivity.class);
                                    return;
                                }
                            }
                        } else {
                            //goto fish Area
                            if (!ListUtiles.isEmpty(listCurrentAreas)) {
                                String objectId = content[0];
                                for (AVObject aVObject : listCurrentAreas) {
                                    if (objectId.equals(aVObject.getObjectId())) {
                                        App.getApp().putTemPObject("area", aVObject);
                                        Tool.startActivity(MainActivity.this, FishAreaDetailMapActivity.class);
                                        return;
                                    }
                                }

                            }
                        }
                    } catch (Exception e) {

                        LogUtil.e(App.tag, "infow window error:" + e.getLocalizedMessage());
                    }
                }
            });

            int scale = (int) (googleMap.getMaxZoomLevel() * 5.0f / 10.0f);
            if (googleMap.isMyLocationEnabled() && null != googleMap.getMyLocation()) {
                Location mylocation = googleMap.getMyLocation();
                LatLng latLng = new LatLng(mylocation.getLatitude(), mylocation.getLongitude());
                App.getApp().setAvGeoPoint(new AVGeoPoint(latLng.latitude, latLng.longitude));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, scale));

            } else if (null != App.getApp().getAvGeoPoint()) {
                AVGeoPoint parseGeoPoint = App.getApp().getAvGeoPoint();
                LatLng latLng = new LatLng(parseGeoPoint.getLatitude(), parseGeoPoint.getLongitude());
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, scale));
            } else {
                LatLng sydney = new LatLng(-33.87043364562841, 151.21310759335756);
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, scale));
            }

//            if (null != mylocation) {
//                LatLng latLng = new LatLng(mylocation.getLatitude(), mylocation.getLongitude());
//                App.getApp().setParseGeoPoint(new ParseGeoPoint(latLng.latitude, latLng.longitude));
//                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, scale));
//            } else {
//                LatLng sydney = new LatLng(-34, 151);
//                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney,scale));
//            }
}
}


    private void closeInfowWindow() {

        for (Marker marker : arrayListCatchMarkes) {
            marker.hideInfoWindow();
        }
    }


    private boolean checkIsExpiredFromLeancloud() {

        boolean subscribed = false;

        AVUser user = AVUser.getCurrentUser();
        if (null != user) {

            Object expiredObj = user.get(App.expiredKey);
            if (null == expiredObj) {//free for old user
                subscribed = true;
            } else {
                subscribed = !user.getBoolean(App.expiredKey);
            }
            LogUtil.i(App.tag, "check if subscribed from leancloud:" + mSubscribed);
        }

        return subscribed;
    }

    @Override
    public void initView() {

        if (mainAdapter == null) {
            mainAdapter = new MainFragmentAdapter(getFragmentManager());
        }
        viewPager.setAdapter(mainAdapter);
        mainAdapter.getMapFragment().getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                MainActivity.this.googleMap = googleMap;
                initMapDragListener(googleMap);
                LogUtil.i(App.tag, "request gooogle map success");
            }
        });


        headerTab.setOnMenuCheckListener(new HeaderTab.OnMenuCheckListener() {
            @Override
            public void onMenuClick(int index) {
                viewPager.setCurrentItem(index, true);

                if (2 == index && !mSubscribed) {
                    showBottomOperate(2);
                } else {
                    showBottomOperate(index);
                }

            }
        });


        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                LogUtil.i(App.tag, "onPageSelected:" + position);
                headerTab.checkIndexWithNoCallBack(position, true);
                showBottomOperate(position);

                viewCoverMap.setVisibility(View.GONE);
                listViewMaps.setVisibility(View.GONE);

                if (2 == position) {
                    showSearchInput();
                } else {
                    hideSearchInput();
                }

                if ((2 == position && !mSubscribed) || (3 == position && !mSubscribed)) {
//                    showBottomOperate(position);

                    DialogSubscrib dialogSubscrib = new DialogSubscrib(MainActivity.this);
                    dialogSubscrib.show();

//                  Tool.startActivity(MainActivity.this, PurchaseActivity.class);

                }


                if (mSubscribed) {
                    ((MusicFragment) mainAdapter.getItem(3)).show();
                    imageButtonFishData.setClickable(true);
                } else {
                    hideSearchInput();
                    imageButtonFishData.setClickable(false);
//                    if (googleMap != null) googleMap.clear();
//                    hideSearchContent();
//                    showBottomOperate(2);

                }

                ((MusicFragment) mainAdapter.getItem(3)).stopPlay();

            }
        });

        viewPager.setCurrentItem(1);

//        headerTab.checkIndex(1);
//        headerTab.showUnderLine(1);

//        headerTab.checkIndexWithNoCallBack(1);


        //================
        skipTextView = (TextView) facebookLikeBlock.findViewById(R.id.skipTextView);
//        facebookLikeTextView = (TextView) facebookLikeBlock.findViewById(R.id.facebookLikeTextView);

        skipTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyAnimationUtils.animationHideview(facebookLikeBlock, AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out));
            }
        });

//        facebookLikeTextView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                MyAnimationUtils.animationHideview(facebookLikeBlock, AnimationUtils.loadAnimation(getApplicationContext(), android.R.anim.fade_out));
//                LikeView likeView = (LikeView) facebookLikeBlock.findViewById(R.id.likeView);
//
//                try {
//                    Method tt = LikeView.class.getDeclaredMethod("toggleLike");
//                    tt.setAccessible(true);
//                    tt.invoke(likeView);
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });


        LikeView likeView = (LikeView) facebookLikeBlock.findViewById(R.id.likeView);
        likeView.setLikeViewStyle(LikeView.Style.BUTTON);
        likeView.setObjectIdAndType(
                "https://www.facebook.com/thescreamingreel",
                LikeView.ObjectType.PAGE);

        SharedPreferences sharedata = getSharedPreferences("data", 0);
        Boolean showLike = sharedata.getBoolean("showLike", false);
        if (showLike == false) {
            showFacebookLike();
            // save show param
            SharedPreferences.Editor editor = getSharedPreferences("data", 0).edit();
            editor.putBoolean("showLike", true);
            editor.commit();
        }

//        PagecatchList catchePage = (PagecatchList) mainAdapter.getPageAtIndex(1);

        fishListDialogView = new FishListDialogView(this);
        editTextLocationInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    showSearchContent();
                } else {
                    hideSearchContent();
                    hideSoftKeyborad(null, editTextLocationInput);
                }


                String input = getInput(editTextLocationInput);
                if (!StringUtils.isEmpty(input)) {
                    imageViewCloseInput.setVisibility(View.VISIBLE);
                } else {
                    imageViewCloseInput.setVisibility(View.INVISIBLE);
                    listViewMaps.setVisibility(View.GONE);
                }

            }
        });

        viewCoverMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewCoverMap.setVisibility(View.GONE);

                hideSoftKeyborad(null, editTextLocationInput);
            }
        });

        editTextLocationInput.addTextChangedListener(new TextWatcherAdapter() {
            @Override
            public void afterTextChanged(Editable s) {

                String input = getInput(editTextLocationInput);
                if (!StringUtils.isEmpty(input)) {
                    imageViewCloseInput.setVisibility(View.VISIBLE);
                    requestPlaceList(getInput(editTextLocationInput));
                } else {
                    imageViewCloseInput.setVisibility(View.INVISIBLE);
                    listViewMaps.setVisibility(View.GONE);
                    hideSoftKeyborad(null, editTextLocationInput);
                }

            }
        });

        editTextLocationInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewCoverMap.setVisibility(View.VISIBLE);

                if (StringUtils.isEmpty(getInput(editTextLocationInput))) {
                    listViewMaps.setVisibility(View.GONE);
                } else {
                    listViewMaps.setVisibility(View.VISIBLE);
                }

            }
        });


    }

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            LogUtil.i("IAB", "Inventory: "+ inventory);
            if (mHelper == null) return;

            if (result.isFailure()) {
                return;
            }

//            LogUtil.i("IAB", "SKU: " +inventory.getSkuDetails(SKU_PRODUCT_MONTHLY)+ " \\ " + inventory.getSkuDetails(SKU_PRODUCT_YEARLY) );
//            LogUtil.i("IAB", "STATE: " +inventory.getPurchase(SKU_PRODUCT_MONTHLY).getPurchaseState());

            Purchase productMonthly = inventory.getPurchase(SKU_PRODUCT_MONTHLY);
            Purchase productYearly = inventory.getPurchase(SKU_PRODUCT_YEARLY);

            LogUtil.i("IAB", "Purchase:"+ productMonthly+" || "+ productYearly);

            boolean  month = verifyDeveloperPayload(productMonthly);
            boolean  year = verifyDeveloperPayload(productYearly);

            mSubscribed = (productMonthly != null && verifyDeveloperPayload(productMonthly)) || (productYearly != null && verifyDeveloperPayload(productYearly));
            LogUtil.d("IAB", "Subscription Status: " + mSubscribed);

            Object expiredObj = AVUser.getCurrentUser().get(App.expiredKey);
            if (!mSubscribed) {
                if (null == expiredObj) {//if current user is old user expiredObj is null
                    mSubscribed = true;
                }
            }

            boolean expired = !mSubscribed;

            if (null != expiredObj) {// /if current user is old user expiredObj is null
                AVUser.getCurrentUser().put(App.expiredKey, expired);
                AVUser.getCurrentUser().setFetchWhenSave(true);
                AVUser.getCurrentUser().saveEventually();
            }

            LogUtil.d("IAB", "User " + (mSubscribed ? "HAS" : "DOES NOT HAVE")
                    + " subscription.");

            Process process = null;
            try {
                process = Runtime.getRuntime().exec("logcat -d");
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                StringBuilder log = new StringBuilder();
                String line ="";
                while((line = bufferedReader.readLine()) != null){
                    log.append(line);
                }
                textLog.setText(log.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    boolean verifyDeveloperPayload(Purchase p) {

        if(p == null || p.getPurchaseState() == 3){
            String computedPayload = AVUser.getCurrentUser().getUsername() + ITEM;
            LogUtil.i("IAB", "Payload: "+ computedPayload);
            return false;
        }

        String responsePayload = p.getDeveloperPayload();
        String computedPayload = AVUser.getCurrentUser().getUsername() + ITEM;

        LogUtil.i("IAB", "Payload:"+ responsePayload + " || "+computedPayload);
        return responsePayload != null && responsePayload.equals(computedPayload);

    }


    BaseTask mixTask = null;
    AVQuery areaQuery = null;

    public void requestAreaAndCatchFromMapCenter(final AVGeoPoint selectionPoint) {
        if (null == googleMap) {
            return;
        }

        if (null != mixTask && mixTask.getStatus() == AsyncTask.Status.RUNNING) {
            mixTask.cancel(true);
        }

        AVGeoPoint parseGeoPoint = null;
        if (null == selectionPoint) {
            LatLng mapCenter = Tool.getCenterLocation(googleMap);
            parseGeoPoint = new AVGeoPoint(mapCenter.latitude, mapCenter.longitude);
        } else {
            parseGeoPoint = selectionPoint;
        }
        final AVGeoPoint restLocation = parseGeoPoint;


        mixTask = new BaseTask(new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = new NetResult();
                try {

                    netResult = new NetResult();

//                    if (null != queryCatch) {
//                        queryCatch.cancel();
//                    }

                    queryCatch = AVQuery.getQuery("Catch");
                    queryCatch.setLimit(INT_MAX_RESULT_COUNT);
                    queryCatch.include("owner");
                    queryCatch.include("fish");
                    queryCatch.include("area");
                    queryCatch.include("photos");
                    queryCatch.whereNotEqualTo("suspend", true);
                    queryCatch.whereEqualTo("hide_location", false);

//                    queryCatch.whereNear("location", restLocation);
                    queryCatch.whereWithinKilometers("location", restLocation, FLOAT_NEAR_KILOMITER);


                    List<AVObject> catchList = queryCatch.find();
                    List<AVObject> areaList = null;

                    //========
                    if (null != areaQuery) {
                        areaQuery.cancel();
                    }

                    areaQuery = AVQuery.getQuery("Area");
                    areaQuery.setLimit(INT_MAX_RESULT_COUNT);
                    areaQuery.whereNotEqualTo("pending", true);
                    areaQuery.whereWithinKilometers("coordinate", restLocation, FLOAT_NEAR_KILOMITER);
                    areaList = areaQuery.find();

                    Object[] data = {areaList, catchList};
                    netResult.setData(data);

                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.tag, "error xxx:" + e.getLocalizedMessage());
                    netResult.setException(e);
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (isFinishing()) return;
                if (null == result.getException()) {
                    Object[] datas = result.getData();

                    if (mSubscribed) {
                        addAreaAndCatch2Map((List<AVObject>) datas[0], (List<AVObject>) datas[1]);
                    } else {
                        addAreaAndCatch2Map(new ArrayList(), (List<AVObject>) datas[1]);
                    }


                } else {

                }
            }
        });
        mixTask.execute(new HashMap<String, String>());

        LogUtil.i(App.tag, "request area and catch.....");
    }


    private final static int MODE_FISH_ONLEY = 0;
    private final static int MODE_MIX = 1;

    private int mode_search = MODE_MIX;


    public void requestCatchByFish(final AVObject fishObject) {
        mode_search = MODE_FISH_ONLEY;
        if (null == googleMap) {
            return;
        }

        if (null != mixTask && mixTask.getStatus() == AsyncTask.Status.RUNNING) {
            mixTask.cancel(true);
        }


        mixTask = new BaseTask(new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                NetResult netResult = new NetResult();
                try {

                    netResult = new NetResult();
//                    if (null != queryCatch) {
//                        queryCatch.cancel();
//                    }
                    queryCatch = AVQuery.getQuery("Catch");
                    queryCatch.setLimit(INT_MAX_RESULT_COUNT);
                    queryCatch.include("owner");
                    queryCatch.include("fish");
                    queryCatch.include("area");
                    queryCatch.include("photos");
                    queryCatch.whereEqualTo("fish", fishObject);
                    queryCatch.whereNotEqualTo("suspend", true);
                    queryCatch.whereEqualTo("hide_location", false);

                    List<AVObject> catchList = queryCatch.find();

                    Object[] data = {catchList};
                    netResult.setData(data);

                } catch (Exception e) {
                    e.printStackTrace();
                    LogUtil.e(App.tag, "error xxx:" + e.getLocalizedMessage());
                    netResult.setException(e);
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (isFinishing()) return;
                if (null == result.getException()) {
                    Object[] datas = result.getData();
                    listCurrentCatces = (List<AVObject>) datas[0];
                    addMarkerCatch2Map((List<AVObject>) datas[0], true);
                } else {

                }
            }
        });
        mixTask.execute(new HashMap<String, String>());

        LogUtil.i(App.tag, "request area and catch.....");
    }

    AVQuery<AVObject> queryCatch = null;

    private void requestCatchByLocation(LatLng location, AVObject fishObject) {

//        if (null != queryCatch) {
//            queryCatch.cancel();
//        }
        queryCatch = AVQuery.getQuery("Catch");
        queryCatch.setLimit(INT_MAX_RESULT_COUNT);
        queryCatch.include("owner");
        queryCatch.include("fish");
        queryCatch.include("area");
        queryCatch.whereNotEqualTo("suspend", true);
        queryCatch.whereEqualTo("hide_location", false);

        if (null != location) {
//            queryCatch.whereNear("location", new ParseGeoPoint(location.latitude, location.longitude));

            queryCatch.whereWithinKilometers("location", new AVGeoPoint(location.latitude, location.longitude), FLOAT_NEAR_KILOMITER);
        }

        if (null != fishObject) {
            queryCatch.whereEqualTo("fish", fishObject);
        }
        queryCatch.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> objects, AVException e) {
                if (!isFinishing()) {
                    if (null == e) {
                        if (!ListUtiles.isEmpty(objects)) {
                            listCurrentCatces = objects;
                            addMarkerCatch2Map(objects, true);
                        } else {
//                            showNotifyTextIn5Seconds("No catch found!");
                        }
                    } else {
                        showNotifyTextIn5Seconds(null == e ? getString(R.string.error_net) : e.getLocalizedMessage());
                    }
                }
            }
        });

    }


    private ArrayList<Marker> arrayListAreaMarkers = new ArrayList<Marker>();

    private void addAreaAndCatch2Map(List<AVObject> areas, List<AVObject> catches) {
        LogUtil.i(App.tag, "found area:" + ListUtiles.getListSize(areas) + " catch:" + ListUtiles.getListSize(catches));

        if (null != googleMap) {
            listCurrentAreas = areas;
            for (Marker marker : arrayListAreaMarkers) {
                marker.remove();
            }
            arrayListAreaMarkers.clear();
            if (!ListUtiles.isEmpty(areas)) {

                for (AVObject areaObject : areas) {
                    AVGeoPoint areaGeo = areaObject.getAVGeoPoint("coordinate");
                    if (null != areaGeo) {
                        LatLng latLng1 = new LatLng(areaGeo.getLatitude(), areaGeo.getLongitude());
                        MarkerOptions location = new MarkerOptions().
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_fishing_area))
                                .position(latLng1)
                                .title(areaObject.getString("title"))
                                .draggable(true);

                        Marker marker = googleMap.addMarker(location);
                        marker.setTitle(areaObject.getString("title"));
                        marker.setSnippet(areaObject.getObjectId() + "," + areaObject.getString("title") + "," + "1");

                        arrayListAreaMarkers.add(marker);

                    }
                }
            }
            if (!ListUtiles.isEmpty(catches)) {
                LatLng firstlatLng = null;
                for (Marker marker : arrayListCatchMarkes) {
                    marker.remove();
                }
                arrayListCatchMarkes.clear();
                listCurrentCatces = catches;

                for (int i = 0, isize = catches.size(); i < isize; i++) {
                    AVObject catchObj = catches.get(i);
                    if (null != catchObj.getAVGeoPoint("location")) {

                        AVGeoPoint pgo = catchObj.getAVGeoPoint("location");
                        LatLng latLng1 = new LatLng(pgo.getLatitude(), pgo.getLongitude());
                        if (null == firstlatLng) {
                            firstlatLng = latLng1;
                        }
                        AVUser owner = catchObj.getAVUser("owner");
                        String title = owner.getString("first_name") + " " + owner.getString("last_name");
                        MarkerOptions location = new MarkerOptions().
                                icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_fish_location))
                                .position(latLng1)
                                .title(title)
                                .draggable(true);

                        Marker marker = googleMap.addMarker(location);

                        if (null != owner.getAVFile("avatar")) {
                            String url = owner.getAVFile("avatar").getUrl();
                            marker.setSnippet(catchObj.getObjectId() + "," + url + "," + "0");
                        } else {
                            marker.setSnippet(catchObj.getObjectId() + "," + " " + "," + "0");
                        }
                        arrayListCatchMarkes.add(marker);
                    }
                }
            }

//            List<Marker> listAllMarkers = new ArrayList<>();
//            listAllMarkers.addAll(arrayListCatchMarkes);
//            listAllMarkers.addAll(arrayListAreaMarkers);
//
//            CameraUpdate cameraUpdate = Tool.getMarkerCenter(listAllMarkers, 20);
//            if (null != cameraUpdate) {
//                googleMap.animateCamera(cameraUpdate);
//            }

        }
    }

    private ArrayList<Marker> arrayListCatchMarkes = new ArrayList<Marker>();

    private void addMarkerCatch2Map(List<AVObject> catches, boolean clearCatch) {

        if (null != googleMap) {
            LatLng firstlatLng = null;


            for (Marker marker : arrayListCatchMarkes) {
                marker.remove();
            }

            if (clearCatch) {
                for (Marker marker : arrayListCatchMarkes) {
                    marker.remove();
                }
                arrayListCatchMarkes.clear();
            }

            googleMap.clear();

            arrayListCatchMarkes.clear();

            for (int i = 0, isize = catches.size(); i < isize; i++) {
                AVObject catchObj = catches.get(i);
                if (null != catchObj.getAVGeoPoint("location")) {

                    AVGeoPoint pgo = catchObj.getAVGeoPoint("location");
                    LatLng latLng1 = new LatLng(pgo.getLatitude(), pgo.getLongitude());
                    if (null == firstlatLng) {
                        firstlatLng = latLng1;
                    }

                    AVUser owner = catchObj.getAVUser("owner");

                    String title = owner.getString("first_name") + " " + owner.getString("last_name");

                    MarkerOptions location = new MarkerOptions().
                            icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_fish_location))
                            .position(latLng1)
                            .title(title)
                            .draggable(true);

                    Marker marker = googleMap.addMarker(location);

                    if (null != owner.getAVFile("avatar")) {
                        String url = owner.getAVFile("avatar").getUrl();
                        marker.setSnippet(catchObj.getObjectId() + "," + url + "," + "0");
                    }

                    arrayListCatchMarkes.add(marker);
                }
            }

            if (null != firstlatLng) {

//                int scale = (int) (googleMap.getMaxZoomLevel() * 8.0f / 10.0f);
//                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(firstlatLng, (scale)));

                googleMap.animateCamera(Tool.getMarkerCenter(arrayListCatchMarkes, Tool.getDisplayMetrics(App.getApp()).x / 5));


            }
        }
    }


    private void requestPlaceList(final String adddress) {

        if (!StringUtils.isEmpty(adddress)) {

            if (null != requestLocationTask && requestLocationTask.getStatus() == AsyncTask.Status.RUNNING) {
                requestLocationTask.cancel(true);
            }

            requestLocationTask = new BaseTask(new NetCallBack() {

                @Override
                public void onPreCall() {

                    LogUtil.i(App.tag, "request:" + adddress);
                }

                @Override
                public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                    NetResult netResult = null;
                    try {
                        netResult = new NetResult();
                        List<Address> list = Tool.getLocationFromAddress(MainActivity.this, adddress, 10);
                        netResult.setTag(list);
                    } catch (Exception e) {
                        LogUtil.e(App.tag, "error in search fish area");
                    }

                    return netResult;
                }

                @Override
                public void onFinish(NetResult result, BaseTask baseTask) {
//                    LogUtil.i(App.tag, "request:end:" + ((List<Address>) result.getTag()).size());
                    if (!isFinishing()) {
                        if (null != result) {
                            List<Address> list = (List<Address>) result.getTag();
                            buildLocationListAdapter(list);
                        } else {
                            showNotifyTextIn5Seconds(R.string.error_net);
                        }
                    }
                }
            });
            requestLocationTask.execute(new HashMap<String, String>());
        }
    }

    private void buildLocationListAdapter(List<Address> list) {
        if (!ListUtiles.isEmpty(list) && !StringUtils.isEmpty(getInput(editTextLocationInput))) {


            listViewMaps.setVisibility(View.VISIBLE);

            AddressAdapter addressAdapter = new AddressAdapter(MainActivity.this, list);
            listViewMaps.setAdapter(addressAdapter);

            listViewMaps.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Address addres = (Address) parent.getItemAtPosition(position);
                    if (null != addres) {
                        listViewMaps.setVisibility(View.GONE);
                        viewCoverMap.setVisibility(View.GONE);
                        if (null != googleMap) {
                            LatLng target = new LatLng(addres.getLatitude(), addres.getLongitude());

                            requestAreaAndCatchFromMapCenter(new AVGeoPoint(target.latitude, target.longitude));

                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(target, googleMap.getMaxZoomLevel() * 8 / 10));
                        } else {
                            showNotifyTextIn5Seconds("Google map is not ready yet!");
                        }
                    }

                    hideSoftKeyborad(null, editTextLocationInput);
                }
            });
        }
    }


    public void requestSettingsPermisson() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 10086);
            }
        }
    }

    private void showBottomOperate(int index) {
        showBottomBar(index);
        if (index != 2 && linearLayoutBottomFish.getVisibility() == View.VISIBLE) {
            linearLayoutBottomFish.setVisibility(View.GONE);
        }
    }


    AddPhotoDialogInCatchListFromMedia addPhotoDialogInCatchList = null;

    @OnClick(R.id.imageButtonAddCatch)
    public void onAddCatchClick() {
        if (null == addPhotoDialogInCatchList) {
            addPhotoDialogInCatchList = new AddPhotoDialogInCatchListFromMedia(this);
        }

        addPhotoDialogInCatchList.setOnAddMediaListener(new AddPhotoDialogInCatchListFromMedia.OnAddMediaListener() {
            @Override
            public void onAddMediaClick() {

                requestAddPhotoOrVideo();
            }

            @Override
            public void onSkipClick() {
                gotoAddCatchActivity(null);
            }
        });
        addPhotoDialogInCatchList.show();
    }


    @Override
    public void onReceivePictureSelect(Context context, Intent intent) {
        List<String> files = intent.getStringArrayListExtra("list");
        LogUtil.i(App.tag, "find " + files.size() + " picture");
    }

    @Override
    public void onReceiveVideoSelect(Context context, Intent intent) {
        List<String> files = intent.getStringArrayListExtra("list");
        LogUtil.i(App.tag, "find " + files.size() + " video");
    }

    private void requestAddPhotoOrVideo() {

        MediaChooser.setMaxMediaLimit(1, 1);
        MediaChooser.setVideoSize(10);
        MediaChooser.setImageSize(10);
        MediaChooser.setMaxVideoDuration(60);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                requestPicture = REQUESTPICTURE_CATCH;
                startSelectMediaFromChose(MainActivity.this);
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, SHORT_REQUEST_READ_EXTEAL_CATCH);
            }
        } else {
            requestPicture = REQUESTPICTURE_CATCH;
            startSelectMediaFromChose(MainActivity.this);
        }

    }

    @InjectView(R.id.imageButtonMapMode)
    ImageButton imageButtonMapMode;


    @OnClick(R.id.imageButtonMapMode)
    public void onButtonMapClick() {
        if (null != googleMap) {
            int mapType = googleMap.getMapType();
            if (mapType == GoogleMap.MAP_TYPE_NORMAL) {
                googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                imageButtonMapMode.setBackgroundResource(R.drawable.icon_map_map);
            }
            if (mapType == GoogleMap.MAP_TYPE_SATELLITE) {
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                imageButtonMapMode.setBackgroundResource(R.drawable.icon_map_satellite);
            }
        } else {
            showNotifyTextIn5Seconds("Map not ready yet!");
        }
    }

    @OnClick(R.id.viewCloseBottom)
    public void onCloseBotttom() {

        mode_search = MODE_MIX;
        linearLayoutBottomFish.setVisibility(View.GONE);
        fishListDialogView.dismiss();
        if (null != googleMap) {
            googleMap.clear();

            LatLng latlng = Tool.getCenterLocation(googleMap);
            if (null != latlng) {
                requestAreaAndCatchFromMapCenter(new AVGeoPoint(latlng.latitude, latlng.longitude));
            }
        }
    }

    @InjectView(R.id.imageViewFishPic)
    ImageView imageViewFishPic;

    @InjectView(R.id.textViewFishName)
    TextView textViewFishName;

    private void showBottomFish(AVObject pb) {

        if (pb.has("name") && null != pb.getString("name")) {
            textViewFishName.setText(pb.getString("name"));
        }
        imageViewFishPic.setScaleType(ImageView.ScaleType.FIT_START);
        if (pb.has("image") && null != pb.getAVFile("image")) {

            Picasso.with(this).load(pb.getAVFile("image").getUrl()).placeholder(R.drawable.icon_default_fish).resize(300, 150).config(Bitmap.Config.RGB_565).into(imageViewFishPic);
        } else {
            imageViewFishPic.setImageResource(R.drawable.icon_default_fish);
        }

        linearLayoutBottomFish.setTag(pb);
        linearLayoutBottomFish.setVisibility(View.VISIBLE);
        requestCatchByFish(pb);
    }


    @OnClick(R.id.viewClickArea)
    public void onClickArea() {

        AVObject fishObj = (AVObject) linearLayoutBottomFish.getTag();
        if (null != fishObj) {
            App.getApp().putTemPObject("fish", fishObj);
            Tool.startActivity(this, FishDetailActivity.class);
        }
    }

    private void gotoAddCatchActivity(ArrayList<String> fileList) {
        if (ListUtiles.isEmpty(fileList)) {
            Tool.startActivityForResult(MainActivity.this, AddCatchOrEditActivity.class, SHORT_REQUEST_ADDCATCH);
        } else {
            Intent intent = new Intent(MainActivity.this, AddCatchOrEditActivity.class);
            intent.putStringArrayListExtra("fileUrl", fileList);
            startActivityForResult(intent, SHORT_REQUEST_ADDCATCH);
        }

    }


    private void showSearchInput() {
        viewSearhMap.setVisibility(View.VISIBLE);


        if (StringUtils.isEmpty(getInput(editTextLocationInput))) {

            listViewMaps.setVisibility(View.GONE);
        } else {
            listViewMaps.setVisibility(View.VISIBLE);
        }

    }

    private void hideSearchInput() {
        viewSearhMap.setVisibility(View.GONE);
        listViewMaps.setVisibility(View.GONE);
    }

    public void showBottomBar(int index) {

        if (index == 0) {
            linearLayoutBottomOperateBar.setVisibility(View.GONE);
        }
        if (index == 1) {
            linearLayoutBottomOperateBar.setVisibility(View.VISIBLE);
            imageButtonFishData.setVisibility(View.GONE);
            imageButtonWeather.setVisibility(View.GONE);
            imageButtonMapMode.setVisibility(View.GONE);
            imageButtonAddCatch.setVisibility(View.VISIBLE);

        }
        if (index == 2) {
            linearLayoutBottomOperateBar.setVisibility(View.VISIBLE);
            imageButtonFishData.setVisibility(View.VISIBLE);
            imageButtonWeather.setVisibility(View.VISIBLE);
            imageButtonMapMode.setVisibility(View.VISIBLE);
            imageButtonAddCatch.setVisibility(View.GONE);
        }

        if (index == 3) {
            linearLayoutBottomOperateBar.setVisibility(View.GONE);
            imageButtonFishData.setVisibility(View.VISIBLE);
            imageButtonWeather.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onStartActivityException(Exception e) {
        LogUtil.e(App.tag, "error:" + e.toString());
        showNotifyTextIn5Seconds(e.getLocalizedMessage());
    }

    int resumCount = 0;

    @Override
    protected void onResume() {
        super.onResume();
        LogUtil.i("OnResume", "----------------------------------");
        resumCount++;
        if (1 == resumCount) {
            headerTab.showUnderLine(1);
        }

//        int currentItem = viewPager.getCurrentItem();
//        mainAdapter.getPageAtIndex(currentItem).onResume();

        if (null != googleMap && googleMap.isMyLocationEnabled() && null != googleMap.getMyLocation()) {
            App.getApp().setLocation(googleMap.getMyLocation());
//            App.getApp().setParseGeoPoint(new ParseGeoPoint(location.getLatitude(), location.getLongitude()));
        }

//        if (null == App.getApp().getParseGeoPoint()) {
//            requestAccessLocationWithNoPermissionCheck();
//        }

        if (null != facebookLikeBlock && facebookLikeBlock.getVisibility() == View.VISIBLE) {
            MyAnimationUtils.animationHideview(facebookLikeBlock, AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
        }

        if(mHelper != null && !skuList.isEmpty()){
            try {
                mHelper.queryInventoryAsync(true, null, skuList, mGotInventoryListener);
            } catch (IabHelper.IabAsyncInProgressException e) {
                LogUtil.e("IAB", "Error querying inventory. Another async operation in progress.");
            }

            Object expiredObj = AVUser.getCurrentUser().get(App.expiredKey);
            if(expiredObj != null){
               if(checkIsExpiredFromLeancloud() != mSubscribed){
                   initView();
               }
            }

        }

        App.getApp().setMainActivityVisible(true);


    }


    public FishListDialogView fishListDialogView = null;


    @OnClick(R.id.imageButtonFishData)
    public void onButtonFishDataClick() {

        fishListDialogView.setOnFishItemClickListener(new FishListDialogView.OnFishItemClickListener() {
            @Override
            public void onFishSelect(AVObject fishObj, int position) {
                requestCatchByLocation(null, fishObj);
                viewCoverMap.setVisibility(View.GONE);
                showBottomFish(fishObj);
                hideSoftKeyborad(null, editTextLocationInput);
            }

            @Override
            public void onCloseFishDialog() {
                showSearchInput();

                hideSoftKeyborad(null, editTextLocationInput);

            }
        });
        fishListDialogView.show();

    }

    @Override
    public void onBackPressed() {
        if (null != fishListDialogView && fishListDialogView.isShowing()) {
            fishListDialogView.dismiss();
            return;
        }
        super.onBackPressed();
    }

    public void showPhotoViewForChangeHeaderPicture() {

        PhotoSelectDialog photoSelectDialog = new PhotoSelectDialog(this);
        photoSelectDialog.setOnAddCatchListener(new PhotoSelectDialog.OnPhotoSelectionActionListener() {
            @Override
            public void onTakePhotoClick() {
                onTakePhtoClick();
            }

            @Override
            public void onChoosePhotoClick() {

                onChosePhtoClick();
            }

            @Override
            public void onDismis() {

            }
        });
        photoSelectDialog.show();

    }


    public void takePictureFromCamera(int w, int h) {
        requestPicture = REQUESTPICTURE_HEADER;
        startTakePicture(w, h);
    }

    public void selectPictureFromAblum(int w, int h) {
        requestPicture = REQUESTPICTURE_HEADER;
        startSelectPicture(w, h);
    }


    public void onTakePhtoClick() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                takePictureFromCamera(150, 150);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, SHORT_REQUEST_READ_EXTEAL);
            }
        } else {
            takePictureFromCamera(150, 150);
        }
    }

    public void onChosePhtoClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                selectPictureFromAblum(150, 150);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, SHORT_REQUEST_READ_EXTEAL);
            }
        } else {
            selectPictureFromAblum(150, 150);
        }

    }

    public void showFacebookLike() {
        MyAnimationUtils.animationShowView(facebookLikeBlock, AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
    }


    public void requestWriteExternalStorage() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                //yes allow you access
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                }, SHORT_REQUEST_READ_EXTEAL);
            }
        }
    }


//    public void requestAccessLocation() {
//
//        if(null!=App.getApp().getParseGeoPoint()){
//            return;
//        }
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                if (null != googleMap) {
//                    googleMap.setMyLocationEnabled(true);
//                }
//                startLocationService();
//
//            } else {
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
//                }, SHORT_ACCESS_LOCATION);
//            }
//        } else {
//            startLocationService();
//        }
//    }

    public void startLocationService() {

//        Intent intent = new Intent(this, LocationService.class);
//        startService(intent);
        App.getApp().setWeakReferenceActivity(new WeakReference<BaseScreamingReelActivity>(this));
        App.getApp().startLocation();
    }

    @Override
    protected void onStop() {
        super.onStop();
        App.getApp().stopLocation();
        App.getApp().setMainActivityVisible(false);

    }

    @Override
    public void onLocationReceive(BDLocation bdLocation, Location googleLocation) {
        if (null != bdLocation) {

            App.getApp().setBdLocation(bdLocation);

            LogUtil.e(App.tag, "定位成功");
            if (null != googleMap) {
                int scale = (int) (googleMap.getMaxZoomLevel() * 5.0f / 10.0f);
                LatLng latLng = new LatLng(bdLocation.getLatitude(), bdLocation.getLongitude());
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, scale));
            }
            App.getApp().stopLocation();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (isFinishing()) {
            return;
        }

        if (requestCode == SHORT_ACCESS_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (null != googleMap) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    googleMap.setMyLocationEnabled(true);
                }
            } else {
                showNotifyTextIn5Seconds("Location access denied!");
            }
        }


        if (requestCode == SHORT_REQUEST_READ_EXTEAL_CATCH) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //yes allow take picture or
                if (type_request_picture == TYPE_REQEST_PICTURE_SELECT) {
                    requestPicture = REQUESTPICTURE_CATCH;
                    startSelectPicture(512, 512);
                } else {
                    requestPicture = REQUESTPICTURE_CATCH;
                    startTakePicture(512, 512);
                }
            } else {
                showNotifyTextIn5Seconds("Read external storage access denied!");
            }
        }

        if (requestCode == SHORT_REQUEST_READ_EXTEAL) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //yes allow take picture or
                if (type_request_picture == TYPE_REQEST_PICTURE_SELECT) {
                    requestPicture = REQUESTPICTURE_HEADER;
                    startSelectPicture(150, 150);
                } else {
                    requestPicture = REQUESTPICTURE_HEADER;
                    startTakePicture(150, 150);
                }
            } else {
                showNotifyTextIn5Seconds("Read external storage access denied!");
            }
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SHORT_REQUEST_ADDCATCH && resultCode == Activity.RESULT_OK) {
            if (null != mainAdapter) {

                mainAdapter.getPageCatchList().refresh(true, false, true);

            }
            return;
        }

        //==========

        String mFileName;
        if (requestCode == SELECT_PIC && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getDataColumn(getApplicationContext(), data.getData(), null, null);
                PhotoLibUtils.copyFile(mFileName, sharedpreferencesUtil.getImageTempNameString2());
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
                cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
            }
        }
        if (requestCode == SELECT_PIC_KITKAT && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getPath(getApplicationContext(), data.getData());
                String newPath = sharedpreferencesUtil.getImageTempNameString2();
                PhotoLibUtils.copyFile(mFileName, newPath);
                Uri uri = sharedpreferencesUtil.getImageTempNameUri();
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
                cropImageUri(uri, outoutUri, outputX, outputY, CROP_BIG_PICTURE);
            }
        }
        if (requestCode == TAKE_BIG_PICTURE && resultCode == RESULT_OK) {
            Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
            cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
        }
        if (requestCode == CROP_BIG_PICTURE && resultCode == Activity.RESULT_OK) {

            if (sharedpreferencesUtil.getImageTempNameUri() != null) {
                Uri result = sharedpreferencesUtil.getImageTempNameUriCroped();
                App.firstRun();
                if (!StringUtils.isEmpty(result.getPath())) {

                    if (requestPicture == REQUESTPICTURE_HEADER) {
                        if (null != mainAdapter) {
                            PageProfile pageProfile = mainAdapter.getPageProfile();
                            if (null == pageProfile) {
                                LogUtil.e(App.tag, "PageProfile is null");
                            }
                            pageProfile.onPhotoCropped(result);
                        }
                    }
                    if (requestPicture == REQUESTPICTURE_CATCH) {

                        final ArrayList<String> filesList = new ArrayList<String>();
                        if (null != listVideo) {
                            filesList.addAll(listVideo);
                        }

                        final String picPath = result.getPath();
                        final String picPathCompred = picPath + System.currentTimeMillis() + ".compred.jpg";

                        LogUtil.i(App.tag, "src path:" + picPath);
                        LogUtil.i(App.tag, "compred path:" + picPathCompred);

                        File selectFile = new File(picPath);
//                        showProgressDialog(false);

                        if (null != selectFile && selectFile.exists()) {
                            try {


                                Target target = new Target() {
                                    @Override
                                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                        hideProgressDialog();

                                        try {
                                            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, new FileOutputStream(picPathCompred));

                                            LogUtil.i(App.tag, "file length after compress:" + picPathCompred.length());
                                            filesList.add(picPathCompred);
                                            gotoAddCatchActivity(filesList);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            Tool.showMessageDialog("Picture compressed fail,please reselect!", MainActivity.this);
                                        }

                                        imageButtonAddCatch.setTag(null);

                                    }

                                    @Override
                                    public void onBitmapFailed(Drawable errorDrawable) {
                                        hideProgressDialog();

                                    }

                                    @Override
                                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                                        showProgressDialog(false);


                                    }
                                };

                                imageButtonAddCatch.setTag(target);
                                Picasso.with(App.getApp()).invalidate(new File(picPath));
                                Picasso.with(App.getApp()).load(new File(picPath)).into(target);

                            } catch (Exception e) {
                                LogUtil.e(App.tag, "compress picture fail!");
                            }
                        } else {
                            LogUtil.i(App.tag, "file not exist!");
                        }
//                        hideProgressDialog();
                    }
                }
                LogUtil.i(App.tag, " file path croped:" + result.getPath());
            }
            requestPicture = REQUESTPICTURE_NONE;
        }


        if (requestCode == SHORT_REQUEST_MEDIAS && resultCode == RESULT_OK) {

            listVideo = data.getStringArrayListExtra("listVideo");

            List<String> picture = data.getStringArrayListExtra("listPicture");

            if (1 == ListUtiles.getListSize(picture)) {
                mFileName = picture.get(0);
//                String newPath = sharedpreferencesUtil.getImageTempNameString2();
//                PhotoLibUtils.copyFile(mFileName, newPath);
                sharedpreferencesUtil.setNewImageTempName();
                PhotoLibUtils.copyFile(mFileName, sharedpreferencesUtil.getImageTempNameString2());
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();

                outputX = 512;
                outputY = 512;

                cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
            } else if (!ListUtiles.isEmpty(listVideo)) {
                gotoAddCatchActivity(listVideo);
            }
        }

        if (requestCode == 10086) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.System.canWrite(this)) {
                    showToastWithTime("WRITE_SETTINGS permission granted", 3);
                } else {
                    showToastWithTime("WRITE_SETTINGS permission not granted", 3);
                }
            }
        }
    }


    @Override
    public void receivedBroadcast() {
        LogUtil.i("IAB", "Received broadcast notification. Querying inventory.");
        try {
            mHelper.queryInventoryAsync(mGotInventoryListener);
        } catch (IabHelper.IabAsyncInProgressException e) {
            LogUtil.e("IAB", "Error querying inventory. Another async operation in progress.");
        }
    }

}

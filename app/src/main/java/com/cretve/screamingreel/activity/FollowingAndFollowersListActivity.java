package com.cretve.screamingreel.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.common.util.ListUtiles;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.UserAdapterFolloingFellower;
import com.thescreamingreel.androidapp.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class FollowingAndFollowersListActivity extends BaseScreamingReelActivity {


    @InjectView(R.id.listViewUsers)
    ListView listViewUsers;

    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    AVUser aVUser;
    private String type;


    private UserAdapterFolloingFellower userAdapterFolloingFellower;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following_and_followers_list);
        ButterKnife.inject(this);
        aVUser = (AVUser) App.getApp().getTempObject("user");
        type = (String) App.getApp().getTempObject("type");
        initView();
    }

    @OnClick(R.id.viewBack)
    public void onBackClick() {
        finish();
    }

    @Override
    public void initView() {
        textViewTitle.setText(type);
        if ("Following".equals(type)) {
            loadFollowing();
        } else {
            loadFollowers();
        }

    }


    private void buildAdapter(List<AVUser> objectsInput) {

        userAdapterFolloingFellower = new UserAdapterFolloingFellower(FollowingAndFollowersListActivity.this, objectsInput);
        listViewUsers.setAdapter(userAdapterFolloingFellower);
        listViewUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                App.getApp().putTemPObject("user", (AVUser) parent.getAdapter().getItem(position));
                Tool.startActivity(FollowingAndFollowersListActivity.this, ProfileActivity.class);
            }

        });

        AVUser.getCurrentUser().put("followers_count", ListUtiles.getListSize(objectsInput));
        saveAVObject(AVUser.getCurrentUser(), false, false, null);
    }


    private void loadFollowing() {

        final List<AVObject> list = aVUser.getList("follow_users");

        aVUser.fetchAllIfNeededInBackground(list, new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> list, AVException e) {
                if (null == e) {
                    List<AVUser> resultList = aVUser.getList("follow_users", AVUser.class);
                    if (null == e) {
                        buildAdapter(resultList);
                    } else {
                        showNotifyTextIn5Seconds(e.getLocalizedMessage());
                    }

                }
            }
        });


//        aVUser.fetchAllIfNeededInBackground(list, new FindCallback<AVObject>() {
//            @Override
//            public void done(List<AVObject> list, AVException e) {
//                if (!isFinishing()) {
//                    if (null == e) {
//                        buildAdapter((List<AVUser>) list);
//                    } else {
//                        showNotifyTextIn5Seconds(e.getLocalizedMessage());
//                    }
//                }
//
//            }
//        });


//        aVUser.fetchAllIfNeededInBackground(list, new FindCallback<AVUser>() {
//            @Override
//            public void done(List<AVUser> objects, AVException e) {
//                if (!isFinishing()) {
//                    if (null == e) {
//                        buildAdapter(objects);
//                    } else {
//                        showNotifyTextIn5Seconds(e.getLocalizedMessage());
//                    }
//                }
//
//            }
//        });


    }

    private void loadFollowers() {
        AVQuery<AVUser> followersQuery = AVUser.getQuery();
        followersQuery.whereEqualTo("follow_users", aVUser);

        followersQuery.findInBackground(new FindCallback<AVUser>() {
            @Override
            public void done(List<AVUser> objects, AVException e) {
                if (!isFinishing()) {
                    if (null == e) {
                        buildAdapter(objects);
                    } else {
                        showNotifyTextIn5Seconds(e.getLocalizedMessage());
                    }
                }
            }
        });
    }
}

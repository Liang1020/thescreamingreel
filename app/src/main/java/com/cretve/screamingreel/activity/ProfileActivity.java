package com.cretve.screamingreel.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.avos.avoscloud.AVACL;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVPush;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.CountCallback;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.SendCallback;
import com.common.util.ListUtiles;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.CatchAdapter;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ProfileActivity extends BaseScreamingReelActivity {


    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    @InjectView(R.id.textViewName)
    TextView textViewName;

    @InjectView(R.id.textViewCatchCount)
    TextView textViewCatchCount;

    @InjectView(R.id.textViewFellowCount)
    TextView textViewFellowCount;

    @InjectView(R.id.textViewFollowersCount)
    TextView textViewFollowersCount;

    @InjectView(R.id.textViewGender)
    TextView textViewGender;

    @InjectView(R.id.textViewCountry)
    TextView textViewCountry;

    @InjectView(R.id.textViewMobile)
    TextView textViewMobile;

    @InjectView(R.id.textViewEmail)
    TextView textViewEmail;

    @InjectView(R.id.viewFello)
    TextView textViewFello;


    @InjectView(R.id.imageViewHeader)
    ImageView imageViewHeader;

    AVUser aVUser;


    @InjectView(R.id.listViewCatches)
    ListView listViewCatches;

    private CatchAdapter catchAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me);
        ButterKnife.inject(this);
        aVUser = (AVUser) App.getApp().getTempObject("user");
        initView();
    }


    @OnClick(R.id.viewMyCatches)
    public void onCatchesClick() {
        App.getApp().putTemPObject("user", aVUser);
        Tool.startActivity(this, MyCatchesActivity.class);
    }


    @OnClick(R.id.viewBack)
    public void onBack() {
        finish();
    }

    @OnClick(R.id.viewFello)
    public void onClickFellow() {

        List<Object> fellowUsers = AVUser.getCurrentUser().getList("follow_users");
        if (null != fellowUsers) {
            if (fellowUsers.contains(aVUser)) {
                textViewFello.setText("Follow");
                textViewFello.setTextColor(getResources().getColor(R.color.text_color_ios_blue));


                ArrayList<AVUser> deltedUser = new ArrayList<>();
                deltedUser.add(aVUser);
                AVUser.getCurrentUser().removeAll("follow_users", deltedUser);//.getList("follow_users").remove(aVUser);

            } else {
                textViewFello.setText("Followed");
                textViewFello.setTextColor(getResources().getColor(R.color.gray_text_color));
                AVUser.getCurrentUser().addUnique("follow_users", aVUser);
                sendFellowAction();
            }
        } else {
            textViewFello.setText("Followed");
            textViewFello.setTextColor(getResources().getColor(R.color.text_color_ios_blue));
            AVUser.getCurrentUser().addUnique("follow_users", aVUser);
            sendFellowAction();
        }


        saveAVObject(AVUser.getCurrentUser(), true, true, new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (null == e) {
                    getCount();
                }
            }
        });

//        AVUser.getCurrentUser().saveEventually(new SaveCallback() {
//            @Override
//            public void done(AVException e) {
//                if (null == e && !isFinishing()) {
//                    getCount();
//                }
//            }
//        });
    }


    @OnClick(R.id.viewFolloing)
    public void onClickViewFolloing() {

        App.getApp().putTemPObject("user", aVUser);
        App.getApp().putTemPObject("type", "Following");
        Tool.startActivity(this, FollowingAndFollowersListActivity.class);

    }

    @OnClick(R.id.viewFolloers)
    public void onCLickFollowers() {
        App.getApp().putTemPObject("user", aVUser);
        App.getApp().putTemPObject("type", "Followers");
        Tool.startActivity(this, FollowingAndFollowersListActivity.class);
    }


    private void loadCatchs(final boolean showDalog) {

        AVQuery<AVObject> catchQuery = AVQuery.getQuery("Catch");
        catchQuery.include("owner");
        catchQuery.include("area");
        catchQuery.include("fish");
        catchQuery.include("photos");
        catchQuery.whereEqualTo("owner", aVUser);
        catchQuery.whereNotEqualTo("suspend", true);
        catchQuery.orderByDescending("createdAt");

        if (showDalog) {
            showProgressDialog(false);
        }


        catchQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> objects, AVException e) {
                if (showDalog) {
                    hideProgressDialog();
                }

                if (!isFinishing()) {
                    if (null == e) {
                        buildAdapter(objects);
                    } else {
                        showNotifyTextIn5Seconds(e.getLocalizedMessage());
                    }
                }
            }
        });

    }


    private void buildAdapter(List<AVObject> objects) {

        catchAdapter = new CatchAdapter(this, objects, false);
        listViewCatches.setAdapter(catchAdapter);
        Tool.setListViewHeightBasedOnChildren(listViewCatches);


        catchAdapter.setOnItemClickListener(new CatchAdapter.onItemClickListener() {
            @Override
            public void onItemClick(AVObject aVObject, int postion) {

                App.getApp().putTemPObject("catch", aVObject);
                Tool.startActivity(ProfileActivity.this, CatchDetailActivity.class);
            }
        });

        catchAdapter.setOnLikeFeollowClickListener(new CatchAdapter.OnLikeFeollowClickListener() {


            @Override
            public void onClickLike(AVObject pb, BaseAdapter adapter) {


                if (null != AVUser.getCurrentUser().getList("like_catches")) {

                    if (AVUser.getCurrentUser().getList("like_catches").contains(pb)) {
                        AVUser.getCurrentUser().getList("like_catches").remove(pb);
                        pb.increment("likes_count", -1);
                        unLikeAction(pb, adapter);
                    } else {
                        AVUser.getCurrentUser().addUnique("like_catches", pb);
                        try {
                            pb.increment("likes_count");
                        } catch (Exception e) {
                            pb.put("likes_count", Integer.parseInt("0"));
                        }
                        likeAction(pb, adapter);
                    }
                } else {
                    AVUser.getCurrentUser().addUnique("like_catches", pb);
                    pb.increment("likes_count");
                    likeAction(pb, adapter);
                }


            }

            @Override
            public void onClickFollow(AVObject followObject, BaseAdapter catchAdater) {

            }

            @Override
            public void onClickOwner(AVObject aVUser) {

            }
        });

    }

    public void likeAction(final AVObject aVObject, final BaseAdapter adapter) {

        saveAVObject(aVObject, AVUser.getCurrentUser(), true, true, new SaveCallback() {


            @Override
            public void done(AVException e) {
                if (null == e && !isFinishing()) {
                    adapter.notifyDataSetChanged();
                    sendLikePush(aVObject);
                }
            }
        });

    }

    public void unLikeAction(AVObject aVObject, final BaseAdapter adapter) {

        saveAVObject(aVObject, AVUser.getCurrentUser(), true, true, new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (null == e && !isFinishing()) {
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }


    private void sendFellowAction() {

        try {
            AVObject notification = AVObject.create("Notifications");
            notification.put("sender", AVUser.getCurrentUser());
            notification.put("type", "Follow");
            notification.put("receiver", aVUser);
            notification.put("read", false);



            AVACL acl=new AVACL();

            acl.setPublicReadAccess(true);
            acl.setPublicWriteAccess(true);
            notification.setACL(acl);


            notification.saveEventually();


            AVQuery query = AVInstallation.getQuery();
            query.whereEqualTo("user", aVUser);


            JSONObject json = new JSONObject();

            String alert = AVUser.getCurrentUser().getString("first_name") + " " + AVUser.getCurrentUser().getString("last_name") + " followed you.";
            json.put("alert", alert);
            json.put("key", App.K_USERFOLLOWED_NOTIFICATIONKEY);

            AVPush.sendDataInBackground(json, query, new SendCallback() {
                @Override
                public void done(AVException e) {

                }
            });

        } catch (Exception e) {
        }

    }

    private void getCount() {

        AVQuery<AVObject> query = AVQuery.getQuery("Catch");
        query.whereEqualTo("owner", aVUser);
        query.whereNotEqualTo("suspend", false);
        query.countInBackground(new CountCallback() {
            @Override
            public void done(int count, AVException e) {
                if (null == e && !isFinishing()) {
                    textViewCatchCount.setText(String.valueOf(count));
                }
            }
        });


        AVQuery<AVUser> query2 = AVUser.getQuery();
        query2.whereEqualTo("follow_users", aVUser);
        query2.countInBackground(new CountCallback() {
            @Override
            public void done(int count, AVException e) {
                if (null == e && !isFinishing()) {
                    textViewFollowersCount.setText(String.valueOf(count));
                }
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();

        initView();
    }

    @Override
    public void initView() {
        if (null == aVUser) {
            return;
        }

        if (AVUser.getCurrentUser().equals(aVUser)) {
            textViewFello.setVisibility(View.INVISIBLE);
        } else {
            textViewFello.setVisibility(View.VISIBLE);
        }

        textViewTitle.setText(aVUser.getString("first_name") + " " + aVUser.getString("last_name"));
        textViewName.setText(aVUser.getString("first_name") + " " + aVUser.getString("last_name"));


        if (!aVUser.equals(AVUser.getCurrentUser())) {

            List<Object> followUses = AVUser.getCurrentUser().getList("follow_users");
            if (null != followUses) {
                if (followUses.contains(aVUser)) {
                    textViewFello.setText("Followed");
                    textViewFello.setTextColor(getResources().getColor(R.color.gray_text_color));
                } else {
                    textViewFello.setText("Follow");
                    textViewFello.setTextColor(getResources().getColor(R.color.text_color_ios_blue));
                }
            }
        }


        AVFile headerFile = aVUser.getAVFile("avatar");
        if (null != headerFile) {
            Picasso.with(this).load(headerFile.getUrl()).resize(150, 150)
                    .config(Bitmap.Config.RGB_565)
                    .placeholder(R.drawable.icon_header_default)
                    .into(imageViewHeader);
        }

        textViewGender.setText(aVUser.getString("gender"));
        textViewEmail.setText(aVUser.getString("email"));
        textViewMobile.setText(aVUser.getString("mobile"));
        textViewFellowCount.setText(String.valueOf(ListUtiles.getListSize(aVUser.getList("follow_users"))));

        final AVObject cityObject = aVUser.getAVObject("city");
        if (null != cityObject) {

            if (cityObject.isDataAvailable()) {
                textViewCountry.setText(cityObject.getString("name") + ", " + cityObject.getString("country"));
            } else {
                textViewCountry.setText("");
                cityObject.fetchIfNeededInBackground(new GetCallback<AVObject>() {
                    @Override
                    public void done(AVObject object, AVException e) {
                        textViewCountry.setText(cityObject.getString("name") + ", " + cityObject.getString("country"));
                    }
                });
            }
        } else if (null != AVUser.getCurrentUser().getString("country")) {
            textViewCountry.setText(AVUser.getCurrentUser().getString("country"));
        }

        refreshData(true);

    }

    private void refreshData(boolean showDialog) {
        getCount();
        loadCatchs(showDialog);
    }
}

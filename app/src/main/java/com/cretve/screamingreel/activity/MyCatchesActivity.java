package com.cretve.screamingreel.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SaveCallback;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.CatchAdapter;
import com.thescreamingreel.androidapp.R;
import com.xlist.pull.refresh.XListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class MyCatchesActivity extends BaseScreamingReelActivity {


    @InjectView(R.id.xlistViewCatchList)
    XListView xlistViewCatchList;
    private ArrayList<AVObject> arrayListAllCatchs;
    private CatchAdapter catchAdapter;
    private View xlistViewStatisRefreshHeader;
    private short SHORT_EDIT = 199;


    private AVUser aVUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_catches);
        ButterKnife.inject(this);

        this.arrayListAllCatchs = new ArrayList<AVObject>();
        this.catchAdapter = new CatchAdapter(this, arrayListAllCatchs, false);
        this.xlistViewCatchList.setAdapter(catchAdapter);
        this.xlistViewStatisRefreshHeader = findViewById(R.id.xlistViewStatisRefreshHeader);


        this.xlistViewCatchList.setPullRefreshEnable(true);
        this.xlistViewCatchList.setPullLoadEnable(true);
        this.xlistViewCatchList.getmFooterView().hide();
        this.xlistViewCatchList.setAdapter(catchAdapter);

        this.catchAdapter.setOnItemClickListener(new CatchAdapter.onItemClickListener() {
            @Override
            public void onItemClick(AVObject aVObject, int postion) {
                App.getApp().putTemPObject("catch", aVObject);
                Tool.startActivity(MyCatchesActivity.this, CatchDetailActivity.class);
            }
        });


        this.xlistViewCatchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AVObject aVObjectCatch = (AVObject) parent.getItemAtPosition(position);
                App.getApp().putTemPObject("catch", aVObjectCatch);
                Tool.startActivity(MyCatchesActivity.this, CatchDetailActivity.class);
            }
        });
        this.xlistViewCatchList.setXListViewListener(new XListView.IXListViewListener() {
            @Override
            public void onRefresh(XListView v) {

                refresh(false, true, true);
            }

            @Override
            public void onLoadMore(XListView v) {
                refresh(false, true, false);

            }

            @Override
            public void onTouch(boolean downOrUp) {

            }
        });
        showStaticHeader();

        AVUser tempUser = (AVUser) App.getApp().getTempObject("user");
        if (null == tempUser) {
            aVUser = AVUser.getCurrentUser();
        } else {
            aVUser = tempUser;
        }

        refresh(false, true, true);
    }


    public void refresh(final boolean showDialog, final boolean showNotify, final boolean refreshOrLoadMore) {

        BaseTask baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    showNewDialogWithNewTask(baseTask);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask bTask) {
                NetResult netResult = new NetResult();
                try {
                    AVQuery<AVObject> aVQuery = AVQuery.getQuery("Catch");
                    aVQuery.include("owner");
                    aVQuery.include("area");
                    aVQuery.include("fish");
                    aVQuery.include("photos");

                    aVQuery.whereNotEqualTo("suspend", true);
                    aVQuery.orderByDescending("createdAt");
                    aVQuery.whereEqualTo("owner", aVUser);

                    int count = 0;
                    AVQuery<AVObject> countQuery = AVQuery.getQuery("Catch");
                    countQuery.whereEqualTo("owner", aVUser);
                    countQuery.setLimit(Integer.MAX_VALUE);
                    count = countQuery.count();

                    aVQuery.setLimit(pageSize);

                    if (refreshOrLoadMore) {//refresh
                        aVQuery.setSkip(0);
                    } else {//loadmore
                        aVQuery.setSkip(arrayListAllCatchs.size());
                    }
                    List<AVObject> objects = aVQuery.find();
                    Object[] data = {objects, count};
                    netResult.setData(data);
                } catch (Exception e) {
                    netResult.setException(e);
                    LogUtil.e(App.tag, "get catch list error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    hideNewDialogWithTask(baseTask);
                }
                if (null == result.getException()) {
                    List<AVObject> aVObjects = (List<AVObject>) result.getData()[0];
                    totallCount = (Integer) result.getData()[1];
                    buildAdapter(aVObjects, refreshOrLoadMore);
                } else {

                    if (showNotify) {
                        if (result.getException() instanceof AVException) {

                            showLeanCloudError((AVException) result.getException());
                        }
                    }
                }

                stopXlist(xlistViewCatchList);
                dealFooter(arrayListAllCatchs, totallCount, xlistViewCatchList);
                hideStaticHeader();
            }
        });

        baseTask.execute(new HashMap<String, String>(0));
        putTask(baseTask);
    }


    public void likeAction(final AVObject aVObject, final BaseAdapter adapter) {

        saveAVObject(aVObject, AVUser.getCurrentUser(), true, true, new SaveCallback() {


            @Override
            public void done(AVException e) {
                if (null == e && !isFinishing()) {
                    adapter.notifyDataSetChanged();
                    sendLikePush(aVObject);
                }
            }
        });

    }

    public void unLikeAction(AVObject aVObject, final BaseAdapter adapter) {

        saveAVObject(aVObject, AVUser.getCurrentUser(), true, true, new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (null == e && !isFinishing()) {
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }


    private void buildAdapter(List<AVObject> catches, boolean refreshOrLoadMore) {

        xlistViewCatchList.setEnabled(false);
        if (refreshOrLoadMore) {//refresh
            arrayListAllCatchs.clear();
            arrayListAllCatchs.addAll(catches);
        } else {
            arrayListAllCatchs.addAll(catches);
        }
        catchAdapter.notifyDataSetChanged();

        xlistViewCatchList.setEnabled(true);

        catchAdapter.setOnLikeFeollowClickListener(new CatchAdapter.OnLikeFeollowClickListener() {
            @Override
            public void onClickLike(AVObject pb, BaseAdapter adapter) {

                if (null != AVUser.getCurrentUser().getList("like_catches")) {

                    if (AVUser.getCurrentUser().getList("like_catches").contains(pb)) {


                        ArrayList<AVObject> deleteList = new ArrayList<AVObject>();

                        deleteList.add(pb);

                        AVUser.getCurrentUser().removeAll("like_catches",deleteList);//.getList("like_catches").remove(pb);


                        pb.increment("likes_count", -1);
                        unLikeAction(pb, adapter);
                    } else {
                        AVUser.getCurrentUser().addUnique("like_catches", pb);
                        try {
                            pb.increment("likes_count");
                        } catch (Exception e) {
                            pb.put("likes_count", Integer.parseInt("0"));
                        }
                        likeAction(pb, adapter);
                    }
                } else {
                    AVUser.getCurrentUser().addUnique("like_catches", pb);
                    pb.increment("likes_count");
                    likeAction(pb, adapter);
                }

            }

            @Override
            public void onClickFollow(AVObject followObject, BaseAdapter catchAdater) {

            }

            @Override
            public void onClickOwner(AVObject aVUser) {

                App.getApp().putTemPObject("user",aVUser);
                Tool.startActivity(MyCatchesActivity.this, ProfileActivity.class);

            }
        });

        catchAdapter.setOnItemClickListener(new CatchAdapter.onItemClickListener() {
            @Override
            public void onItemClick(final AVObject aVObject, int postion) {


                AVUser owner = aVObject.getAVUser("owner");
                if (null != owner) {

                    if (AVUser.getCurrentUser().equals(owner)) {
                        App.getApp().putTemPObject("MyCatch", aVObject);
                        Tool.startActivityForResult(MyCatchesActivity.this, AddCatchOrEditActivity.class, SHORT_EDIT);

                    } else {

                        App.getApp().putTemPObject("catch", aVObject);
                        Tool.startActivityForResult(MyCatchesActivity.this, CatchDetailActivity.class, SHORT_EDIT);
                    }

                }




//                aVObject.pinInBackground(new SaveCallback() {
//                    @Override
//                    public void done(com.parse.AVException e) {
//                        if (null == e) {
//                            App.getApp().putTemPObject("MyCatch", aVObject);
//                            Tool.startActivityForResult(MyCatchesActivity.this, AddCatchOrEditActivity.class, SHORT_EDIT);
//                        }
//                    }
//                });

            }
        });

        stopXlist(xlistViewCatchList);
    }


    public void showStaticHeader() {
        xlistViewStatisRefreshHeader.setVisibility(View.VISIBLE);
    }

    public void hideStaticHeader() {
        xlistViewStatisRefreshHeader.setVisibility(View.GONE);
    }

    public void onBack() {
        finish();
    }

    @OnClick(R.id.viewBack)
    public void onBackClick() {
        onBack();
    }

    @Override
    public void onBackPressed() {

        onBack();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SHORT_EDIT && resultCode == Activity.RESULT_OK) {

            refresh(true, true, true);
        }
    }
}

package com.cretve.screamingreel.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class FishAreaMapActivity extends FragmentActivity implements OnMapReadyCallback {


    private GoogleMap googleMap;
    private AVObject aVObjectArea;
    private AVObject aVObjectCatch;


    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fish_area_map);
        ButterKnife.inject(this);
        aVObjectArea = (AVObject) App.getApp().getTempObject("area");
        aVObjectCatch = (AVObject) App.getApp().getTempObject("catch");
        if (null != aVObjectArea) {
            textViewTitle.setText(aVObjectArea.getString("title"));
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @OnClick(R.id.viewBack)
    public void onViewBack() {
        finish();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setMapToolbarEnabled(false);
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        this.googleMap.getUiSettings().setCompassEnabled(true);

        this.googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                View v = null;

                String contents[] = marker.getSnippet().split(",");

                if (contents[2].equals("0")) {//catch
                    v = getLayoutInflater().inflate(R.layout.map_infor_window, null);

                    String title = marker.getTitle();
                    TextView textViewTitle = (TextView) v.findViewById(R.id.textViewName);
                    textViewTitle.setText(title);
                    ImageView imageViewHeader = (ImageView) v.findViewById(R.id.imageViewHeader);

                    String imgUrl = marker.getSnippet().split(",")[1];
                    if (!StringUtils.isEmpty(imgUrl)) {
                        Picasso.with(FishAreaMapActivity.this)
                                .load(imgUrl)
                                .resize(70, 70)
                                .config(Bitmap.Config.RGB_565)
                                .placeholder(R.drawable.icon_header_default)
                                .into(imageViewHeader);
                    }
                } else if (contents[2].equals("1")) {//area
                    v = getLayoutInflater().inflate(R.layout.map_infor_window_location, null);
                    String title = marker.getTitle();
                    TextView textViewTitle = (TextView) v.findViewById(R.id.textViewName);
                    textViewTitle.setText(title);
                }

                return v;
            }

            @Override
            public View getInfoContents(Marker marker) {

                return null;
            }
        });


        this.googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                try {

                    String content[] = marker.getSnippet().split(",");
                    if (content[2].equals("0")) {
                        App.getApp().putTemPObject("catch", aVObjectCatch);
                        Tool.startActivity(FishAreaMapActivity.this, CatchDetailActivity.class);
                    } else {
                        App.getApp().putTemPObject("area", aVObjectArea);
                        Tool.startActivity(FishAreaMapActivity.this, FishAreaDetailMapActivity.class);
                    }
                } catch (Exception e) {

                    LogUtil.e(App.tag, "infow window error:" + e.getLocalizedMessage());
                }
            }
        });

        if (null != googleMap) {
            AVGeoPoint areaGeo = aVObjectArea.getAVGeoPoint("coordinate");
            if (null != areaGeo) {
                LatLng latLng1 = new LatLng(areaGeo.getLatitude(), areaGeo.getLongitude());
                MarkerOptions location = new MarkerOptions().
                        icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_fishing_area))
                        .position(latLng1)
                        .title(aVObjectArea.getString("title"))
                        .draggable(true);

                Marker marker = googleMap.addMarker(location);
                marker.setTitle(aVObjectArea.getString("title"));
                marker.setSnippet(aVObjectArea.getObjectId() + "," + aVObjectArea.getString("title") + "," + "1");
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng1, googleMap.getMaxZoomLevel() / 2));
            }

            if (null!=aVObjectCatch && null != aVObjectCatch.getAVGeoPoint("location")) {

                AVGeoPoint pgo = aVObjectCatch.getAVGeoPoint("location");
                LatLng latLng1 = new LatLng(pgo.getLatitude(), pgo.getLongitude());
                AVUser owner = aVObjectCatch.getAVUser("owner");
                String title = owner.getString("first_name") + " " + owner.getString("last_name");
                MarkerOptions location = new MarkerOptions().
                        icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_fish_location))
                        .position(latLng1)
                        .title(title)
                        .draggable(true);

                Marker marker = googleMap.addMarker(location);

                if (null != owner.getAVFile("avatar")) {
                    String url = owner.getAVFile("avatar").getUrl();
                    marker.setSnippet(aVObjectCatch.getObjectId() + "," + url + "," + "0");
                } else {
                    marker.setSnippet(aVObjectCatch.getObjectId() + "," + " " + "," + "0");
                }
            }
        }


    }


}

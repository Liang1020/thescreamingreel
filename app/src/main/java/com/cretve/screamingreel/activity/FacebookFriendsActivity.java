package com.cretve.screamingreel.activity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SaveCallback;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.FacebookFriendsAdapter;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.thescreamingreel.androidapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class FacebookFriendsActivity extends BaseScreamingReelActivity {

    @InjectView(R.id.facebookFriendslistView)
    ListView facebookFriendslistView;

    @InjectView(R.id.textViewNumberOfFriends)
    TextView textViewNumberOfFriends;

    private FacebookFriendsAdapter friendsAdapter;
    private ArrayList<AVUser> friendsList = new ArrayList<AVUser>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_facebook_friends);
        ButterKnife.inject(this);

//        for(int i=0;i<info_Names.length;i++){
//            Map<String,Object> item = new HashMap<String,Object>();
//            item.put("img", R.drawable.icon_user);
//            item.put("name", info_Names[i]);
//            item.put("button", new Button(this));
//            mInfos.add(item);
//        }

//        SimpleAdapter adapter = new SimpleAdapter(this, mInfos, R.layout.item_facebook_friends,
//                new String[]{"img","name", "button"},
//                new int[]{R.id.info_img,R.id.info_name,R.id.buttonFollow});
//        facebookFriendslistView.setAdapter(adapter);

        initView();
    }

    @Override
    public void initView() {
        textViewNumberOfFriends.setText(String.valueOf(friendsList.size()) + " " + getString(R.string.facebook_friends_description));
        this.friendsAdapter = new FacebookFriendsAdapter(this, friendsList);
        facebookFriendslistView.setAdapter(friendsAdapter);
        friendsAdapter.setOnItemClickListener(new FacebookFriendsAdapter.onItemClickListener() {
            @Override
            public void onItemClick(AVObject aVObject, int postion) {

            }

            @Override
            public void onFollowClick(AVObject aVObject, int postion, final FacebookFriendsAdapter baseAdapter) {

                List<Object> followList = AVUser.getCurrentUser().getList("follow_users");
                final AVUser user=(AVUser) baseAdapter.getItem(postion);

                if (null!=followList && followList.contains(user)) {
                    AVUser.getCurrentUser().getList("follow_users").remove(user);

                } else {
                    AVUser.getCurrentUser().addUnique("follow_users", user);
                }

               saveAVObject(AVUser.getCurrentUser(), true, true, new SaveCallback() {
                   @Override
                   public void done(AVException e) {
                       if(null==e){
                           baseAdapter.notifyDataSetChanged();
                           baseAdapter.sendFellowPush(user);
                       }
                   }
               });

            }

        });

        showProgressDialog(true);
        getFacebookFriends();
    }

    @OnClick(R.id.viewBack)
    public void onClickAboutme() {
        finish();
    }

    private void getFacebookFriends() {
        /* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/friends",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {

                        final JSONObject object = response.getJSONObject();
                        if (null == object) {
                            return;
                        }

                        new BaseTask(new NetCallBack() {

                            @Override
                            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {

                                try {
                                    JSONArray array = object.getJSONArray("data");
                                    for(int i=0; i < array.length(); i++) {
                                        JSONObject item = (JSONObject) array.get(i);
                                        String url = "https://graph.facebook.com/" + item.get("id") + "/picture?type=large&return_ssl_resources=1";
                                        AVQuery<AVUser> query = AVUser.getQuery();
                                        query.whereEqualTo("facebookId", item.get("id"));

                                        try {
                                            List<AVUser> users = query.find();
                                            if (users.size() > 0) {
                                                AVUser user = users.get(0);
                                                user.put("name", (String)item.get("name"));
                                                user.put("url", url);
                                                friendsList.add(user);
                                            }

                                        } catch (AVException e) {
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                return null;
                            }

                            @Override
                            public void onFinish(NetResult result, BaseTask baseTask) {
                                hideProgressDialog();
                                if(!isFinishing()){
                                    textViewNumberOfFriends.setText(String.valueOf(friendsList.size()) + " " + getString(R.string.facebook_friends_description));
                                    friendsAdapter.notifyDataSetChanged();
                                }
                            }
                        }).execute(new HashMap<String, String>());

                    }
                }
        ).executeAsync();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

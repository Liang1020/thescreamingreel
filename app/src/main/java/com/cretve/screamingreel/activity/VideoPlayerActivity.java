package com.cretve.screamingreel.activity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

import com.common.util.LogUtil;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.thescreamingreel.androidapp.R;


public class VideoPlayerActivity extends BaseScreamingReelActivity implements OnTouchListener {

    private MediaController mController;
    private VideoView viv;
    private int progress = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_vedio_player);
        viv = (VideoView) findViewById(R.id.videoView);
        mController = new MediaController(this);
        viv.setMediaController(mController);

        showProgressDialog(true);

        String videopath = getIntent().getStringExtra("path");

        LogUtil.i("video","play path:"+videopath);
        if (videopath != null) {
            viv.setVideoPath(videopath);
            viv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    hideProgressDialog();
                }
            });
        }
        viv.requestFocus();
        viv.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onPause() {
        super.onPause();
        progress = viv.getCurrentPosition();
        if(viv!=null&&viv.isPlaying()){
            viv.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        viv.seekTo(progress);
        viv.start();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}

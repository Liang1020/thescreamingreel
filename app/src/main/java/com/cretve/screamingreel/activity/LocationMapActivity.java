package com.cretve.screamingreel.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.thescreamingreel.androidapp.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class LocationMapActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener {

    private GoogleMap googleMap;
    public AVObject aVObject;
    public static final short USE_PIN = 88;
    public static final short HIDE_LOCATION = 99;
    public final short SHORT_ACCESS_LOCATION = 1003;

    @InjectView(R.id.imageButtonMapMode)
    public ImageView buttonMapMode;

    private String location;

    @Override
    protected void onDestroy() {
        aVObject = null;
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_map);
        ButterKnife.inject(this);

        location = (String) App.getApp().getTempObject("location");
        aVObject = (AVObject) App.getApp().getTempObject("area");

        requestAccessLocation();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    public void onBack() {
        finish();
    }


    @Override
    public void onBackPressed() {
        onBack();
    }

    @OnClick(R.id.viewBack)
    public void onBackClick() {
        onBack();
    }


    @OnClick(R.id.imageButtonMapMode)
    public void onClickMapMode() {
        if (null != googleMap) {
            int currentMapMode = googleMap.getMapType();
            if (GoogleMap.MAP_TYPE_NORMAL == currentMapMode) {
                googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                buttonMapMode.setImageResource(R.drawable.icon_map_map_location);
            } else {
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                buttonMapMode.setImageResource(R.drawable.icon_map_satellite_location);
            }

        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        requestAccessLocation();

        if (null != googleMap) {
            int scale = (int) (googleMap.getMaxZoomLevel() * 5.0f / 10.0f);

            if (null != aVObject && null != aVObject.getAVGeoPoint("coordinate")) {
                AVGeoPoint location = aVObject.getAVGeoPoint("coordinate");
                LatLng l = new LatLng(location.getLatitude(), location.getLongitude());
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(l, (scale)));
            } else {
                if (googleMap.isMyLocationEnabled() && null != googleMap.getMyLocation()) {
                    LatLng l = new LatLng(googleMap.getMyLocation().getLatitude(), googleMap.getMyLocation().getLongitude());
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(l, (scale)));
                } else if (null != App.getApp().getAvGeoPoint()) {
                    LatLng sydney = new LatLng(App.getApp().getAvGeoPoint().getLatitude(), App.getApp().getAvGeoPoint().getLongitude());
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, (scale)));
                } else {
                    LatLng sydney = new LatLng(-33.87043364562841, 151.21310759335756);
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, (scale)));
                }
            }
        }

        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title(""));


//        int scale = (int) (googleMap.getMaxZoomLevel() * 8.0f / 10.0f);
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,scale));

//        if (null != aVObject && null != aVObject.getParseGeoPoint("coordinate")) {
//            ParseGeoPoint location = aVObject.getParseGeoPoint("coordinate");
//            LatLng l = new LatLng(location.getLatitude(), location.getLongitude());
//            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(l, (scale)));
//        }

    }

    @OnClick(R.id.buttonPinned)
    public void onButtonPinedClick() {
        if (null != googleMap) {
            LatLng center = Tool.getCenterLocation(googleMap);
            if (null != center) {
                putDataBack(USE_PIN, center);
            } else {
                LogUtil.i(App.tag, "locatoin is null");
            }
        }
    }

    @OnClick(R.id.buttonHideLocation)
    public void onButtonHideLocationclick() {

        if (null != googleMap) {
            LatLng center = Tool.getCenterLocation(googleMap);
            if (null != center) {
                putDataBack(HIDE_LOCATION, center);
            } else {
                LogUtil.i(App.tag, "locatoin is null");
            }
        }
    }


    public void requestAccessLocation() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (null != googleMap) {
                    googleMap.setMyLocationEnabled(true);
                }

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                }, SHORT_ACCESS_LOCATION);
            }
        }
    }


    public void putDataBack(int code, LatLng latlng) {

        if (null == aVObject) {
            aVObject = AVObject.create("Area");
            aVObject.put("title", location);
            aVObject.put("coordinate",new AVGeoPoint(latlng.latitude,latlng.longitude) );
            aVObject.put("pending", true);
        }

        Intent intent = new Intent();
        App.getApp().putTemPObject("latlng", latlng);
        App.getApp().putTemPObject("area", aVObject);
        intent.putExtra("location_name", location);

        setResult(code, intent);
        finish();

    }


    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == SHORT_ACCESS_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (null != googleMap) {
                    googleMap.setMyLocationEnabled(true);
                }
            } else {
//                showNotifyTextIn5Seconds("Location access denied!");
            }
        }

    }


    //    private void setUpMapIfNeeded() {
//        // Do a null check to confirm that we have not already instantiated the map.
//        if (mMap == null) {
//            // Try to obtain the map from the SupportMapFragment.
//            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
//                    .getMap();
//            // Check if we were successful in obtaining the map.
//            if (mMap != null) {
//                setUpMap();
//            }
//        }
//    }
//    private void setUpMap() {
//
//
//        LatLng MELBOURNE = new LatLng(-37.81319, 144.96298);
//
////        Marker melbourne = mMap.addMarker(new MarkerOptions()
////                .position(MELBOURNE).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_fb)).title("fb")
////                .snippet("Population: 4,137,400"));
////        melbourne.showInfoWindow();
//
//
//        final MarkerOptions location = new MarkerOptions().position(new LatLng(0, 0)).title("m1").draggable(true);
//        mMap.addMarker(location);
//
//
//        final MarkerOptions location2 = new MarkerOptions().position(new LatLng(0, 5)).title("m2").draggable(true);
//        mMap.addMarker(location2);
//
//        mMap.setOnMapClickListener(this);
//        mMap.setOnMarkerClickListener(this);
//
//    }
}

package com.cretve.screamingreel.activity;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVObject;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class FishDetailActivity extends BaseScreamingReelActivity {


    AVObject parseFishObject;


    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    @InjectView(R.id.imageView)
    ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fish_detail);
        ButterKnife.inject(this);
        parseFishObject = (AVObject) App.getApp().getTempObject("fish");
        initView();
    }


    @Override
    public void initView() {
        textViewTitle.setText(parseFishObject.getString("name"));
        Point point = Tool.getDisplayMetrics(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(point.x, point.x / 2);
        imageView.setLayoutParams(layoutParams);
        if (parseFishObject.has("image") && null != parseFishObject.getAVFile("image")) {
            Picasso.with(this).load(parseFishObject.getAVFile("image").getUrl()).placeholder(R.drawable.icon_default_fish).resize(200, 100).config(Bitmap.Config.RGB_565).into(imageView);
        } else {
            imageView.setImageResource(R.drawable.icon_default_fish);
        }

    }


    @OnClick(R.id.viewLimits)
    public void onLimitsClick() {

        Tool.startActivity(this, BagSizeLimitsActivity.class);
    }

    @OnClick(R.id.viewBack)
    public void onBackClick() {
        finish();
    }


}

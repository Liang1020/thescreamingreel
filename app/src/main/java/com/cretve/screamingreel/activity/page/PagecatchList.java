package com.cretve.screamingreel.activity.page;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SaveCallback;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.MyAnimationUtils;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamPage;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.activity.CatchDetailActivity;
import com.cretve.screamingreel.activity.MainActivity;
import com.cretve.screamingreel.activity.ProfileActivity;
import com.cretve.screamingreel.adapter.CatchAdapter;
import com.cretve.screamingreel.adapter.MenuAdapter;
import com.thescreamingreel.androidapp.R;
import com.xlist.pull.refresh.XListView;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by wangzy on 15/11/9.
 */
public class PagecatchList extends BaseScreamPage {


    private ListView listViewMenu;
    private XListView xlistViewCatchList;
    private LinearLayout linearLayoutMenuContent;
    private ImageView imageViewIconDown;
    private TextView textViewTitle;
    private int currentIndexInFilter = 0;
    private Context context;
    private View xlistViewStatisRefreshHeader;
    private CatchAdapter catchAdapter;

    private LinearLayout linearLayoutTopmenu;

    private int pageSize = 20;
    private int currentPage = 0;
    private int totallCount = 0;

    private ArrayList<AVObject> arrayListAllCatchs;
    private View linearLayoutTitleMenu;
    private LinearLayout linearLayoutTopMenuContainer;

    public PagecatchList(MainActivity activity) {
        super(activity);
        this.context = activity;
    }

    @Override
    public void initView() {
        this.rootView = View.inflate(activity, R.layout.page_catch_list, null);

        this.listViewMenu = (ListView) findViewById(R.id.listViewMenu);
        this.linearLayoutMenuContent = (LinearLayout) findViewById(R.id.linearLayoutMenuContent);
        this.imageViewIconDown = (ImageView) findViewById(R.id.imageViewIconDown);
        this.textViewTitle = (TextView) findTextViewById(R.id.textViewTitle);
        this.xlistViewCatchList = (XListView) findListViewById(R.id.xlistViewCatchList);
        this.xlistViewStatisRefreshHeader = findViewById(R.id.xlistViewStatisRefreshHeader);
        this.linearLayoutTitleMenu = findViewById(R.id.linearLayoutTitleMenu);

        this.linearLayoutTopmenu = (LinearLayout) findViewById(R.id.linearLayoutTopmenu);
        this.linearLayoutTitleMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                linearLayoutTopmenu.clearAnimation();
                linearLayoutTopMenuContainer.clearAnimation();

                if (linearLayoutTopmenu.getVisibility() == View.VISIBLE) {
                    MyAnimationUtils.animationHideview(linearLayoutTopmenu, AnimationUtils.loadAnimation(activity, R.anim.slide_out_to_top));
                    imageViewIconDown.setVisibility(View.VISIBLE);
                    textViewTitle.setTextColor(activity.getResources().getColor(R.color.text_color_ios_blue));
                } else {
                    MyAnimationUtils.animationShowView(linearLayoutTopmenu, AnimationUtils.loadAnimation(activity, R.anim.slide_in_from_top));
                    imageViewIconDown.setVisibility(View.GONE);
                    textViewTitle.setTextColor(activity.getResources().getColor(R.color.gray_text_color));
                }
            }
        });

        this.linearLayoutTopMenuContainer = findLinearLayout(R.id.linearLayoutTopMenuContainer);

        this.xlistViewCatchList.setPullRefreshEnable(true);
        this.xlistViewCatchList.setPullLoadEnable(true);
        this.xlistViewCatchList.getmFooterView().hide();

        this.arrayListAllCatchs = new ArrayList<AVObject>();
        this.catchAdapter = new CatchAdapter(activity, arrayListAllCatchs, true);
        this.xlistViewCatchList.setAdapter(catchAdapter);
        this.xlistViewCatchList.setXListViewListener(new XListView.IXListViewListener() {
            @Override
            public void onRefresh(XListView v) {

                refresh(false, true, true);
            }

            @Override
            public void onLoadMore(XListView v) {
                refresh(false, true, false);

            }

            @Override
            public void onTouch(boolean downOrUp) {

            }
        });


        final MenuAdapter menuAdapter = new MenuAdapter(activity);
        menuAdapter.select(currentIndexInFilter);
        this.listViewMenu.setAdapter(menuAdapter);
        this.listViewMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                imageViewIconDown.setVisibility(View.VISIBLE);
                textViewTitle.setTextColor(activity.getResources().getColor(R.color.text_color_ios_blue));

                currentIndexInFilter = i;
                menuAdapter.select(i);

                MyAnimationUtils.animationHideview(linearLayoutTopmenu, AnimationUtils.loadAnimation(activity, R.anim.slide_out_to_top));

                if (currentIndexInFilter == 1) {
                    if (null != App.getApp().getAvGeoPoint()) {
                        refresh(true, true, true);
                    } else {
//                        App.getApp().requesetLocation();
                        //please retry
                        ((MainActivity) activity).showToastWithTime("Location fail please retry later!", 3);
                    }
                    return;
                }
                refresh(true, true, true);

            }
        });


        showStaticHeader();
        refresh(false, false, true);
    }


    private void buildAdapter(List<AVObject> catches, boolean refreshOrLoadMore) {

        LogUtil.i(App.tag, "found catch in main:" + ListUtiles.getListSize(catches));

        xlistViewCatchList.setEnabled(false);
        if (refreshOrLoadMore) {//refresh
            arrayListAllCatchs.clear();
            arrayListAllCatchs.addAll(catches);
        } else {
            arrayListAllCatchs.addAll(catches);
        }
        catchAdapter.notifyDataSetChanged();
        xlistViewCatchList.setEnabled(true);
        catchAdapter.setOnLikeFeollowClickListener(new CatchAdapter.OnLikeFeollowClickListener() {

            @Override
            public void onClickLike(final AVObject pb, final BaseAdapter adapter) {

                if (null != AVUser.getCurrentUser().getList("like_catches")) {

                    if (AVUser.getCurrentUser().getList("like_catches").contains(pb)) {

                        LogUtil.i(App.tag, "unlike:" + pb.get("desc") + "删除前:");

                        List<AVObject> mylist = AVUser.getCurrentUser().getList("like_catches");


                        for (AVObject aVObjectTmp : mylist) {
                            if (aVObjectTmp.getObjectId().equals(pb.getObjectId())) {
                                LogUtil.i(App.tag, "是的存在需要删除的object");
                                break;
                            }
                        }


                        ArrayList<AVObject> deleteList = new ArrayList<AVObject>();

                        deleteList.add(pb);

                        AVUser.getCurrentUser().removeAll("like_catches", deleteList);


                        LogUtil.i(App.tag, "已经调用删除方法");

                        boolean isDeleteSucess = true;
                        for (AVObject aVObjectTmp : mylist) {
                            if (aVObjectTmp.getObjectId().equals(pb.getObjectId())) {
                                isDeleteSucess = false;
                                break;
                            }
                        }

                        LogUtil.i(App.tag, "删除是否成功：" + isDeleteSucess);


                        try {
                            if (pb.getInt("likes_count") > 0) {
                                pb.increment("likes_count", -1);
                            }

                        } catch (Exception e) {
                            LogUtil.e(App.tag, "error like1:" + e.getLocalizedMessage());
                            pb.put("likes_count", Integer.parseInt("0"));
                        }

                        unLikeAction(pb, adapter);
                    } else {
                        try {

                            AVUser.getCurrentUser().addUnique("like_catches", pb);
                            pb.increment("likes_count");

                        } catch (Exception e) {
                            LogUtil.e(App.tag, "error like:" + e.getLocalizedMessage());
                            pb.put("likes_count", Integer.parseInt("0"));
                        }

                        likeAction(pb, adapter);
                    }
                } else {
                    AVUser.getCurrentUser().addUnique("like_catches", pb);
                    pb.increment("likes_count");
                    likeAction(pb, adapter);
                }

            }

            @Override
            public void onClickFollow(final AVObject pb, final BaseAdapter adapter) {

                List<AVObject> parseFollowUsers = AVUser.getCurrentUser().getList("follow_users");

                if (null != parseFollowUsers && parseFollowUsers.contains(pb.getAVUser("owner"))) {

                    ArrayList<AVUser> deleteUser = new ArrayList<AVUser>();
                    deleteUser.add(pb.getAVUser("owner"));
                    AVUser.getCurrentUser().removeAll("follow_users", deleteUser);

                    unfollowAction(pb, adapter);
                } else {
                    AVUser.getCurrentUser().addUnique("follow_users", pb.getAVUser("owner"));
                    //send push
                    followAction(pb, adapter);

                }
            }

            @Override
            public void onClickOwner(AVObject aVUser) {

                App.getApp().putTemPObject("user", aVUser);
                Tool.startActivity(context, ProfileActivity.class);

            }
        });

        catchAdapter.setOnItemClickListener(new CatchAdapter.onItemClickListener() {
            @Override
            public void onItemClick(final AVObject aVObject, int postion) {
                App.getApp().putTemPObject("catch", aVObject);
                Tool.startActivityForResult((Activity) context, CatchDetailActivity.class, go_detail_back);
            }
        });

        stopXlist(xlistViewCatchList);
    }


    @Override
    public void onResume() {

        if (null != catchAdapter) {
            catchAdapter.notifyDataSetChanged();
        }
    }

    private int go_detail_back = 0x22;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == go_detail_back) {
//            refresh(false, false, true);
//        }
    }

    public void followAction(final AVObject aVObject, final BaseAdapter adapter) {
        activity.saveAVObject(aVObject, AVUser.getCurrentUser(), true, true, new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (null == e && !activity.isFinishing()) {
                    adapter.notifyDataSetChanged();
                    activity.sendFellowPush(aVObject);
                } else {
                    LogUtil.i(App.tag, "followaction fail:" + e.getLocalizedMessage());
                }
            }
        });

    }

    public void unfollowAction(final AVObject aVObject, final BaseAdapter adapter) {

        activity.saveAVObject(aVObject, AVUser.getCurrentUser(), true, true, new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (null == e && !activity.isFinishing()) {
                    adapter.notifyDataSetChanged();
                } else {
                    LogUtil.e(App.tag, "unflow error:" + e.getLocalizedMessage());
                }
            }
        });
    }


    public void likeAction(final AVObject aVObject, final BaseAdapter adapter) {

        AVUser currentUser = AVUser.getCurrentUser();

        activity.saveAVObject(aVObject, currentUser, true, true, new SaveCallback() {

            @Override
            public void done(AVException e) {
                if (null == e && !activity.isFinishing()) {
                    adapter.notifyDataSetChanged();
                    activity.sendLikePush(aVObject);
                }
            }
        });

    }

    public void unLikeAction(AVObject aVObject, final BaseAdapter adapter) {

        activity.saveAVObject(aVObject, AVUser.getCurrentUser(), true, true, new SaveCallback() {

            @Override
            public void done(AVException e) {
                if (null == e && !activity.isFinishing()) {
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }


    public void refresh(final boolean showDialog, final boolean showNotify, final boolean refreshOrLoadMore) {

//        if (refreshOrLoadMore) {
//            xlistViewCatchList.setEnabled(false);
//            arrayListAllCatchs.clear();
//            catchAdapter.notifyDataSetChanged();
//            xlistViewCatchList.setEnabled(true);
//            xlistViewCatchList.getmFooterView().hide();
//        }

        BaseTask baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    activity.showNewDialogWithNewTask(baseTask);
                }
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask bTask) {
                NetResult netResult = new NetResult();
                try {
                    AVQuery<AVObject> aVQuery = AVQuery.getQuery("Catch");
                    aVQuery.include("owner");
                    aVQuery.include("area");
                    aVQuery.include("fish");
                    aVQuery.include("photos");
//                    aVQuery.include("owner.avatar");

                    aVQuery.whereNotEqualTo("suspend", true);
                    aVQuery.orderByDescending("createdAt");

                    int count = 0;
                    AVQuery<AVObject> countQuery = AVQuery.getQuery("Catch");

                    if (1 == currentIndexInFilter) {
                        aVQuery.whereNear("location", App.getApp().getAvGeoPoint());
                        countQuery.whereNear("location", App.getApp().getAvGeoPoint());
                    }

                    if (2 == currentIndexInFilter) {
                        aVQuery.whereContainedIn("owner", AVUser.getCurrentUser().getList("follow_users"));
                        countQuery.whereContainedIn("owner", AVUser.getCurrentUser().getList("follow_users"));
                    }


                    countQuery.setLimit(Integer.MAX_VALUE);
                    count = countQuery.count();

                    aVQuery.setLimit(pageSize);
                    if (refreshOrLoadMore) {//refresh
                        aVQuery.setSkip(0);
                    } else {//loadmore
                        aVQuery.setSkip(arrayListAllCatchs.size());
//                        countQuery.setSkip(arrayListAllCatchs.size());
                    }
                    List<AVObject> objects = aVQuery.find();
                    Object[] data = {objects, count};
                    netResult.setData(data);
                } catch (Exception e) {
                    netResult.setException(e);
                    LogUtil.e(App.tag, "get catch list error:" + e.getLocalizedMessage());
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {

                if (showDialog) {
                    activity.hideNewDialogWithTask(baseTask);
                }
                if (null == result) {
                    ((BaseScreamingReelActivity) context).showNotifyTextIn5Seconds(R.string.error_net);
                }

                if (null == result.getException()) {
                    List<AVObject> aVObjects = (List<AVObject>) result.getData()[0];
                    totallCount = (Integer) result.getData()[1];
                    buildAdapter(aVObjects, refreshOrLoadMore);
                } else {
                    if (showNotify) {
                        if (result.getException() instanceof AVException) {
                            LogUtil.i(App.tag, "error:" + result.getException());
//                            activity.showLeanCloudError((AVException) result.getException());
                                    if(result.getException().getCause() instanceof UnknownHostException){
                                        activity.showNotifyTextIn5Seconds(R.string.error_net);
                                    }else{
                                        activity.showNotifyTextIn5Seconds("Temporarily no data!");
                                    }

//                            activity.showNotifyTextIn5Seconds("Temporarily no data!");
                        }
                    }
                }

                stopXlist(xlistViewCatchList);
                dealFooter(arrayListAllCatchs, totallCount, xlistViewCatchList);
                hideStaticHeader();
            }
        });

        baseTask.execute(new HashMap<String, String>(0));
        activity.putTask(baseTask);
    }


    public void hideStaticHeader() {
        xlistViewStatisRefreshHeader.setVisibility(View.GONE);
        linearLayoutTitleMenu.setVisibility(View.VISIBLE);
    }

    public void showStaticHeader() {
        xlistViewStatisRefreshHeader.setVisibility(View.VISIBLE);
    }

    protected void stopXlist(XListView xListViewMyTravel) {
        xListViewMyTravel.stopRefresh();
        xListViewMyTravel.stopLoadMore();
    }

    protected void dealFooter(List arrayList, int totalNumber, XListView listViewNannies) {
        try {
            if (arrayList.size() >= totalNumber) {
                listViewNannies.getmFooterView().hide();
            } else {
                listViewNannies.getmFooterView().show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        LogUtil.i(App.tag, "data size:" + arrayList.size() + " total:" + totalNumber);
    }
}

package com.cretve.screamingreel.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.SelectImageAdapter;
import com.thescreamingreel.androidapp.R;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class SelectImageActivity extends BaseScreamingReelActivity {

    @InjectView(R.id.imageGridview)
    GridView imageGridview;

    private SelectImageAdapter imagesAdapter;
    private ArrayList<AVFile> imagesList = new ArrayList<AVFile>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_image);
        ButterKnife.inject(this);

        AVObject catcher = (AVObject) App.getApp().getTempObject("catch");
        imagesList = (ArrayList<AVFile>) catcher.get("photos");

        this.imagesAdapter = new SelectImageAdapter(this, imagesList);
        imageGridview.setAdapter(imagesAdapter);

        imageGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AVFile file = (AVFile) adapterView.getItemAtPosition(i);
                Tool.sendShare(SelectImageActivity.this, "Hey, check out this photo from The Screaming Reel app " + file.getUrl());
            }
        });
    }

    @OnClick(R.id.viewBack)
    public void onClickAboutme() {
        finish();
    }


}

package com.cretve.screamingreel.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.FishAreaAdapter;
import com.thescreamingreel.androidapp.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class FishAreaSelectActivity extends BaseScreamingReelActivity {


    @InjectView(R.id.editTextLocationInput)
    EditText editTextLocationInput;

    @InjectView(R.id.listViewArea)
    ListView listViewArea;

    AVQuery<AVObject> aVQuery;

    private short SHORT_GO_LOCATION = 100;
    private short SHORT_GO_NEW_LOCATION = 101;
    private AVObject tempSelectAVObject;


    @InjectView(R.id.viewEmpty)
    View viewEmpty;

    @InjectView(R.id.textViewLocationNew)
    TextView textViewLocationNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fish_area_select);
        ButterKnife.inject(this);
        loadQuery("");
    }

    @Override
    protected void onPause() {
        super.onPause();


        hideSoftKeyborad(null,getCurrentFocus());
    }

    @OnTextChanged(R.id.editTextLocationInput)
    public void onTextChanged() {
        LogUtil.i(App.tag, "query text:" + getInput(editTextLocationInput));
        loadQuery(getInput(editTextLocationInput));
    }

    @OnClick(R.id.textViewLocationAddNew)
    public void onClickAddNewLocation() {
        if (Tool.isInstallGMS(this)) {
            String location = getInput(editTextLocationInput);
            App.getApp().putTemPObject("location", location);
            Tool.startActivityForResult(this, LocationMapActivity.class, SHORT_GO_NEW_LOCATION);
        } else {
            showNotifyTextIn5Seconds("Please install Google Play Service first!");
        }

    }

    private void loadQuery(String inputText) {
        if (null != aVQuery) {
            aVQuery.cancel();
        }

        aVQuery = AVQuery.getQuery("Area");
        aVQuery.whereNotEqualTo("pending", true);
//        aVQuery.whereEqualTo("pending", false);
        aVQuery.setLimit(100);

        if (!StringUtils.isEmpty(inputText)) {
            String reg = ".*" + inputText + ".*";
            aVQuery.whereMatches("title", reg, "i");
        }

        aVQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> objects, AVException e) {

                LogUtil.i(App.tag, "find end");
                if (null == e) {
                    buildAdapter(objects);
                } else {
                    LogUtil.i(App.tag, "ex:" + e.getLocalizedMessage());
                }
            }
        });

    }


    private void buildAdapter(final List<AVObject> aVObjects) {

        FishAreaAdapter fishAreaAdapter = new FishAreaAdapter(this, aVObjects);
        listViewArea.setAdapter(fishAreaAdapter);
        listViewArea.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                App.getApp().putTemPObject("area", parent.getAdapter().getItem(position));
                if (!Tool.isInstallGMS(App.getApp())) {
                    setResult(Activity.RESULT_OK);
                    finish();
                } else {
                    App.getApp().putTemPObject("area", (AVObject) parent.getAdapter().getItem(position));
                    Tool.startActivityForResult(FishAreaSelectActivity.this, LocationMapActivity.class, SHORT_GO_LOCATION);
                }
            }
        });

        textViewLocationNew.setText(editTextLocationInput.getText().toString());
        if (ListUtiles.isEmpty(aVObjects) && !StringUtils.isEmpty(getInput(editTextLocationInput))) {
            viewEmpty.setVisibility(View.VISIBLE);
        } else {
            viewEmpty.setVisibility(View.GONE);
        }

    }

    public void onBack() {

        if (null != aVQuery) {
            aVQuery.cancel();
        }

        finish();
    }

    @Override
    public void onBackPressed() {
        onBack();
    }


    @OnClick(R.id.viewBack)
    public void onBackClick() {
        onBack();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == SHORT_GO_LOCATION || requestCode == SHORT_GO_NEW_LOCATION) && (resultCode == LocationMapActivity.HIDE_LOCATION || resultCode == LocationMapActivity.USE_PIN)) {
            setResult(resultCode, data);
            finish();
        }
    }
}

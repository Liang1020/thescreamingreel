package com.cretve.screamingreel.activity;

import android.os.Bundle;
import android.widget.LinearLayout;

import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.joanzapata.pdfview.PDFView;
import com.thescreamingreel.androidapp.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class PrivacyPolicyActivity extends BaseScreamingReelActivity {


    @InjectView(R.id.pdfview)
    PDFView pdfview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        ButterKnife.inject(this);

        pdfview.setOrientation(LinearLayout.VERTICAL);
        pdfview.fromAsset("pp.pdf")
                .enableSwipe(true)
                .showMinimap(false)
                .load();

    }

    @OnClick(R.id.viewBack)
    public void onClickAboutme() {
        finish();
    }
}

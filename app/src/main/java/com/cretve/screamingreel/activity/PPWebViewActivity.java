package com.cretve.screamingreel.activity;

import android.os.Bundle;
import android.webkit.WebView;

import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.thescreamingreel.androidapp.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class PPWebViewActivity extends BaseScreamingReelActivity {


    @InjectView(R.id.webView)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ppweb_view);
        ButterKnife.inject(this);

        webView.loadUrl("file:///android_asset/pp_formated.html");
        webView.getSettings().setTextZoom(60);

    }


    @OnClick(R.id.viewBack)
    public void onClickBack() {

        finish();
    }
}

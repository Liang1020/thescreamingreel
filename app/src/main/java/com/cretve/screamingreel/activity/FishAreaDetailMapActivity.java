package com.cretve.screamingreel.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.NumberHelper;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.GroupListAdapter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.thescreamingreel.androidapp.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class FishAreaDetailMapActivity extends BaseScreamingReelActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private AVObject aVObjectArea;

    @InjectView(R.id.textViewDesc)
    TextView textViewDesc;

    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;
    AVGeoPoint parseLocation;
    AVQuery<AVObject> queryCatch;

//    @InjectView(R.id.textViewEmppty)
//    TextView textViewEmppty;
//
//    @InjectView(R.id.listViewCatches)
//    ListView listViewCatches;

    @InjectView(R.id.textViewLat)
    TextView textViewLat;

    @InjectView(R.id.textViewlng)
    TextView textViewlng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.inject(this);

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        aVObjectArea = (AVObject) App.getApp().getTempObject("area");
        if (null != aVObjectArea) {
            textViewDesc.setText("" + aVObjectArea.getString("description"));
            textViewTitle.setText("" + aVObjectArea.getString("title"));
            parseLocation = aVObjectArea.getAVGeoPoint("coordinate");

            textViewLat.setText("Latitude:" + NumberHelper.keepDecimal6(String.valueOf(parseLocation.getLatitude())));
            textViewlng.setText("Longitude:" + NumberHelper.keepDecimal6(String.valueOf(parseLocation.getLongitude())));

            LogUtil.i(App.tag, "DESCRIPTION: "+ aVObjectArea.getString("description"));

            //getCatches();
        }
    }

    @OnClick(R.id.viewBack)
    public void onBackClick() {
        finish();
    }

    @Override
    protected void onDestroy() {
        if (null != queryCatch) {
            queryCatch.cancel();
        }
        super.onDestroy();
    }

//    public void getCatches() {
//        if (null != queryCatch) {
//            queryCatch.cancel();
//        }
//
//        queryCatch = AVQuery.getQuery("Catch");
//        queryCatch.include("owner");
//        queryCatch.include("area");
//        queryCatch.include("fish");
//        queryCatch.include("area");
//        queryCatch.whereEqualTo("area", aVObjectArea);
//        queryCatch.whereEqualTo("hide_location", false);
//        queryCatch.whereNotEqualTo("suspend", true);
//        queryCatch.orderByDescending("createdAt");
//
//        queryCatch.findInBackground(new FindCallback<AVObject>() {
//            @Override
//            public void done(List<AVObject> objects, AVException e) {
//                if (null == e && !isFinishing()) {
//                    tidyResultList(objects);
//                } else {
//                    if (null != e) {
//                        showNotifyTextIn5Seconds(e.getLocalizedMessage());
//                    } else {
//                        showNotifyTextIn5Seconds(R.string.error_net);
//                    }
//
//                }
//            }
//        });
//
//    }

//    public void tidyResultList(List<AVObject> objects) {
//
//        LogUtil.i(App.tag, "found catch size:" + ListUtiles.getListSize(objects));
//
//        if (!ListUtiles.isEmpty(objects)) {
//
//            textViewEmppty.setVisibility(View.GONE);
//            Collections.sort(objects, new Comparator<AVObject>() {
//                @Override
//                public int compare(AVObject lhs, AVObject rhs) {
//                    String sp1 = lhs.getAVObject("fish").getString("name");
//                    String sp2 = rhs.getAVObject("fish").getString("name");
//                    return sp1.compareTo(sp2);
//                }
//            });
//
//            HashMap<String, List<AVObject>> spliteList = splitList(objects);
//            buildSplitAdapter(spliteList);
//
//
//            if (null != parseLocation && null != mMap) {
//
//                LatLng latLng = new LatLng(parseLocation.getLatitude(), parseLocation.getLongitude());
//
//                MarkerOptions location = new MarkerOptions().
//                        icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_fish_location))
//                        .position(latLng)
//                        .draggable(true);
//
//                Marker marker = mMap.addMarker(location);
//
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, mMap.getMaxZoomLevel() / 2));
//
//            }
//
//        } else {
//            textViewEmppty.setVisibility(View.VISIBLE);
//        }
//    }

//    public void buildSplitAdapter(HashMap<String, List<AVObject>> spliteList) {
//
//
//        final GroupListAdapter groupListAdapter = new GroupListAdapter(this, spliteList);
//        listViewCatches.setAdapter(groupListAdapter);
//        listViewCatches.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                int type = groupListAdapter.getItemViewType(position);
//
//                if (type == GroupListAdapter.TYPE_NORMAL) {
//
//                    AVUser pu = ((AVObject) groupListAdapter.getItem(position)).getAVUser("owner");
//                    App.getApp().putTemPObject("user", pu);
//                    Tool.startActivity(FishAreaDetailMapActivity.this, ProfileActivity.class);
//
//                }
//            }
//        });
//    }


//    public HashMap<String, List<AVObject>> splitList(List<AVObject> objects) {
//
//        if (ListUtiles.isEmpty(objects)) {
//            return null;
//        }
//
//        HashMap<String, List<AVObject>> catchSpliteList = new HashMap<String, List<AVObject>>();
//        for (int i = 0, isize = objects.size(); i < isize; i++) {
//            AVObject parseFishObject = objects.get(i);
//
//            try {
//
//                String fishName = parseFishObject.getAVObject("fish").getString("name");
//
//                if (catchSpliteList.containsKey(fishName)) {
//                    List<AVObject> catchList = catchSpliteList.get(fishName);
//                    catchList.add(objects.get(i));
//                } else {
//                    List<AVObject> newCatchList = new ArrayList<AVObject>();
//                    newCatchList.add(objects.get(i));
//                    catchSpliteList.put(fishName, newCatchList);
//                }
//            } catch (Exception e) {
//
//                LogUtil.e(App.tag, "  split fish error! on FishAreaDetailMapActivity");
//            }
//
//
//        }
//        return catchSpliteList;
//
//    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (isFinishing()) {
            return;
        }
        mMap = googleMap;
        if (null != aVObjectArea && null != mMap && null != aVObjectArea.getAVGeoPoint("coordinate")) {


            AVGeoPoint pgoPoint = aVObjectArea.getAVGeoPoint("coordinate");

            LatLng latLng = new LatLng(pgoPoint.getLatitude(), pgoPoint.getLongitude());

//            MarkerOptions location = new MarkerOptions().
//                    icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_fishing_area))
//                    .position(latLng)
//                    .draggable(true);

//            Marker marker = mMap.addMarker(location);

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, googleMap.getMaxZoomLevel() / 2));

        }

//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

    }


}

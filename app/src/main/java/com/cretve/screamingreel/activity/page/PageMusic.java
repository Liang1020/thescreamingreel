package com.cretve.screamingreel.activity.page;

import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.common.util.LogUtil;
import com.common.util.Tool;
import com.cretve.screamingreel.BaseScreamPage;
import com.cretve.screamingreel.activity.MainActivity;
import com.cretve.screamingreel.activity.PurchaseActivity;
import com.cretve.screamingreel.adapter.MusicListAdapter;
import com.thescreamingreel.androidapp.R;

import java.util.ArrayList;


/**
 * Created by wangzy on 15/11/9.
 */
public class PageMusic extends BaseScreamPage {

    ListView musiclistView;

    private MusicListAdapter musicAdapter;
    private ArrayList<String> musicList;

    public PageMusic(MainActivity activity) {

        super(activity);

    }

    @Override
    public void initView() {

        this.rootView = View.inflate(activity, R.layout.page_music, null);
        this.musiclistView = findListViewById(R.id.musiclistView);

    }


    public void stopPlay(){
        musicAdapter.stopPlay();
    }

    public void show(){
        musicList = new ArrayList<String>();
        musicList.add("Fish Slap");
        musicList.add("Freshwater Screaming Reel");
        musicList.add("Saltwater Screaming Reel");
        musicList.add("Screaming Winding Reel");
        musicList.add("Smooth Diesel Boat Engine");
        musicList.add("Steady Screaming Reel");

        musicAdapter = new MusicListAdapter(activity, musicList);
        musiclistView.setAdapter(musicAdapter);

    }


}

package com.cretve.screamingreel.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.GetCallback;
import com.common.adapter.TextWatcherAdapter;
import com.common.util.AssertTool;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.util.ValidateTool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.thescreamingreel.androidapp.R;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class AboutMeActivity extends BaseScreamingReelActivity {


    @InjectView(R.id.textViewFirstNameAbout)
    TextView textViewFirstNameAbout;

    @InjectView(R.id.textViewLastNameAbout)
    TextView textViewLastNameAbout;

    @InjectView(R.id.textViewMobile)
    TextView textViewMobile;

    @InjectView(R.id.textViewEmail)
    TextView textViewEmail;

    @InjectView(R.id.textViewGenderAbout)
    TextView textViewGenderAbout;

    @InjectView(R.id.textViewCuntryCity)
    TextView textViewCuntryCity;

    @InjectView(R.id.editIntroduce)
    EditText editIntroduce;

    private short SHORT_GO_CITY = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            //透明状态栏
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            //透明导航栏
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
//        }

        setContentView(R.layout.activity_about_me);
        ButterKnife.inject(this);
        initView();
    }


    @Override
    public void initView() {
        AVUser aVUser = AVUser.getCurrentUser();
        if (null != aVUser) {
            textViewFirstNameAbout.setText(aVUser.getString("first_name"));
            textViewLastNameAbout.setText(aVUser.getString("last_name"));
            textViewGenderAbout.setText(aVUser.getString("gender"));
            textViewMobile.setText(aVUser.getString("mobile"));
            textViewEmail.setText(aVUser.getString("email"));
            editIntroduce.setText(aVUser.getString("desc"));

            if (!StringUtils.isEmpty(AVUser.getCurrentUser().getString("country"))) {
                ArrayList<String> countrys = AssertTool.readLinesFromAssertsFiles(this, "country");
                String nowCountry = AVUser.getCurrentUser().getString("country");
                for (String line : countrys) {
                    String[] countryCity = line.split(",");
                    if ((countryCity[0].equals(nowCountry))) {
                        textViewCuntryCity.setText(countryCity[0] + "," + countryCity[1]);
                        break;
                    }
                }
            } else {
                AVObject city = AVUser.getCurrentUser().getAVObject("city");

                city.fetchIfNeededInBackground(new GetCallback<AVObject>() {
                    @Override
                    public void done(AVObject object, AVException e) {
                        if (null != object) {
                            textViewCuntryCity.setText(object.getString("name") + "," + object.getString("country"));
                        }
                    }
                });


            }

        }
    }

    @OnClick(R.id.viewGenderGender)
    public void onGenderClick() {
        showGenderSelect(textViewGenderAbout);
    }

    @OnClick(R.id.viewFirstNameAbout)
    public void onFirstNameClick() {
        showEditDialog(getString(R.string.regist_guid_4step_edit_fist_name), textViewFirstNameAbout, true, "first_name");
    }

    @OnClick(R.id.viewLastNameAbout)
    public void onLastNameClick() {

        showEditDialog(getString(R.string.regist_guid_4step_edit_last_name), textViewLastNameAbout, true, "last_name");
    }


    @OnClick(R.id.viewMobileAbout)
    public void onMobileClick() {
        showEditDialog(getString(R.string.regist_guid_4step_edit_mobile), textViewMobile, true, "mobile");
    }

    @OnClick(R.id.viewEmailAbout)
    public void onEditEmail() {

        showEditDialog(getString(R.string.regist_guid_4step_edit_email), textViewEmail, true, "email");
    }


    @OnClick(R.id.viewBack)
    public void onClickAboutme() {

        finish();
    }

    @OnClick(R.id.viewCountryCityCountry)
    public void onCountryClick() {
        Tool.startActivityForResult(this, CitySelectActivity.class, SHORT_GO_CITY);
    }

    @Override
    protected void onPause() {
        super.onPause();
        String introduce = getInput(editIntroduce);
        AVUser.getCurrentUser().put("desc", introduce);
//        AVUser.getCurrentUser().saveEventually();

        saveAVObject(AVUser.getCurrentUser(), false, false, null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SHORT_GO_CITY && resultCode == Activity.RESULT_OK && App.getApp().hasTempKey("city")) {

            AVObject aVObjectCity = (AVObject) App.getApp().getTempObject("city");

            if (StringUtils.isEmpty(aVObjectCity.getString("geoId"))) {
                AVUser.getCurrentUser().put("country", aVObjectCity.getString("country"));
                AVUser.getCurrentUser().put("city", "");
                textViewCuntryCity.setText(aVObjectCity.getString("name"));
            } else {
                AVUser.getCurrentUser().put("city", aVObjectCity);
                AVUser.getCurrentUser().put("country", "");
                textViewCuntryCity.setText(aVObjectCity.getString("name") + "," + aVObjectCity.getString("country"));
            }
//            saveAVObject(AVUser.getCurrentUser(), false, false,null);

            AVUser.getCurrentUser().saveEventually();
        }
    }


    private void showGenderSelect(final TextView textView) {

        final String[] gender = {
                getString(R.string.regist_gender_female),
                getString(R.string.regist_gender_male)
        };

        int oldSelect = 0;
        if (getInput(textView).equalsIgnoreCase(gender[0])) {
            oldSelect = 0;
        } else {
            oldSelect = 1;
        }

        new AlertDialog.Builder(this).setTitle(getString(R.string.regist_guid_gender_slect)).
                setIcon(android.R.drawable.ic_dialog_info).
                setSingleChoiceItems(
                        gender, oldSelect,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                String genderSelect = gender[which];
                                LogUtil.i(App.tag, "gender:" + genderSelect);
                                textView.setText(genderSelect);
                                AVUser.getCurrentUser().put("gender", genderSelect);
                                saveAVObject(AVUser.getCurrentUser(), false, false, null);

                            }
                        }).setCancelable(false).show();
    }


    private String emailInput = "";


    private void showEditDialog(final String title, final TextView textView, final boolean cancellAble, final String tag) {

        View dialogView = View.inflate(this, R.layout.dialog_edit, null);
        final TextView textViewLabel = (TextView) dialogView.findViewById(R.id.textViewLabel);
        final EditText editContent = (EditText) dialogView.findViewById(R.id.editContent);

        if ("email".equals(tag)) {
            editContent.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        }

        editContent.addTextChangedListener(new TextWatcherAdapter() {

            @Override
            public void afterTextChanged(Editable s) {
                String input = getInput(editContent);
                if (!StringUtils.isEmpty(input) && !input.equals(emailInput) && input.endsWith(" ")) {
                    editContent.setText(input.replace(" ", ""));
                    editContent.setSelection(getInput(editContent).length());
                    emailInput = getInput(editContent);
                }
            }
        });

        if ("mobile".equals(tag)) {
            editContent.setInputType(InputType.TYPE_CLASS_PHONE);
        }

        editContent.setText(textView.getText().toString());

        emailInput = getInput(textView);

        editContent.setSelection(getInput(editContent).trim().length());

        textViewLabel.setText(title);


        AlertDialog.Builder builder = new AlertDialog.Builder(AboutMeActivity.this);


        builder.setView(dialogView);

        final AlertDialog dlg = builder.create();


        dlg.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        dlg.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        dlg.setCancelable(cancellAble);

        Button buttonReset = (Button) dialogView.findViewById(R.id.buttonReset);
        if (cancellAble) {
            buttonReset.setText(R.string.cancel);
        }

        Button buttonConfirm = (Button) dialogView.findViewById(R.id.buttonConfirm);

        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button btn = (Button) view;
                if (btn.getText().equals("Reset")) {
                    editContent.setText("");
                } else {
                    dlg.dismiss();
                }
            }
        });

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String content = getInput(editContent).trim();
                if (!StringUtils.isEmpty(content)) {
                    if ("email".equals(tag)) {

                        if (ValidateTool.checkEmail(content)) {
                            textView.setText(content);
                            dlg.dismiss();
                            AVUser.getCurrentUser().put(tag, content);
                            saveAVObject(AVUser.getCurrentUser(), false, false, null);
                        } else {
                            showNotifyTextIn5Seconds(R.string.login_hint_enter_right_email_notify);
                        }
                    } else {
                        textView.setText(content.trim());
                        dlg.dismiss();
                        AVUser.getCurrentUser().put(tag, content.trim());
//
                        AVUser.getCurrentUser().saveEventually();
//                        saveAVObject(AVUser.getCurrentUser(), false, false,null);
                    }
                } else {
                    editContent.startAnimation(AnimationUtils.loadAnimation(AboutMeActivity.this, R.anim.shake));
                }
            }
        });
        dlg.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

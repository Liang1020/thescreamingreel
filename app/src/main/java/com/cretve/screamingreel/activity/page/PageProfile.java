package com.cretve.screamingreel.activity.page;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.CountCallback;
import com.avos.avoscloud.LogInCallback;
import com.avos.avoscloud.SaveCallback;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.FileUtils;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.view.NetImageView;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamPage;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.DataBaseConstants;
import com.cretve.screamingreel.activity.AboutMeActivity;
import com.cretve.screamingreel.activity.CommentsActivity;
import com.cretve.screamingreel.activity.FacebookFriendsActivity;
import com.cretve.screamingreel.activity.FindAnglerActivity;
import com.cretve.screamingreel.activity.FollowingAndFollowersListActivity;
import com.cretve.screamingreel.activity.MainActivity;
import com.cretve.screamingreel.activity.MyCatchesActivity;
import com.cretve.screamingreel.activity.PPWebViewActivity;
import com.cretve.screamingreel.activity.TermsOfUseActivity;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import org.json.JSONArray;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by wangzy on 15/11/9.
 */
public class PageProfile extends BaseScreamPage {


    private TextView textViewVersion;
    private TextView textViewUserName;
    private NetImageView netImageViewHeader;
    private TextView textViewCatches;
    private TextView textViewFlollowing;
    private TextView textViewFollowers;
    private View viewComments;
    private TextView textViewCommentCount;
    private TextView textViewDesc;
    private View inviteFriends;
    private View viewFollowing;
    private View viewTermOfUse;
    private View viewFollowFB;

    private View viewFollowers;

    private TextView textViewFollowersDelta;

    public PageProfile(MainActivity activity) {
        super(activity);
    }

    @Override
    public void initView() {
        this.rootView = View.inflate(activity, R.layout.page_profile, null);

        this.textViewVersion = findTextViewById(R.id.textViewVersion);
        this.textViewUserName = findTextViewById(R.id.textViewUserName);
        this.netImageViewHeader = (NetImageView) findImageViewById(R.id.imageViewHeader);
        this.textViewCatches = findTextViewById(R.id.textViewCatches);
        this.textViewFlollowing = findTextViewById(R.id.textViewFlollowing);
        this.textViewFollowersDelta = findTextViewById(R.id.textViewFollowersDelta);
        this.textViewFollowers = findTextViewById(R.id.textViewFollowers);
        this.viewComments = findViewById(R.id.viewComments);
        this.textViewCommentCount = findTextViewById(R.id.textViewCommentCount);
        this.textViewDesc = findTextViewById(R.id.textViewDesc);
        this.inviteFriends = findViewById(R.id.inviteFriends);
        this.viewFollowing = findViewById(R.id.viewFollowing);
        this.viewTermOfUse = findViewById(R.id.viewTermOfUse);
        this.viewFollowFB = findViewById(R.id.viewFollowFB);

        if (App.getApp().isLoginfb() || AVUser.getCurrentUser().getBoolean("fromFb") /*AVUser.getCurrentUser().isLinked("facebook") ParseFacebookUtils.isLinked(AVUser.getCurrentUser())null != AVUser.getCurrentUser() && null != AVUser.getCurrentUser().get("facebookId")*/) {
            this.viewFollowFB.setVisibility(View.VISIBLE);
            findViewById(R.id.textViewCangePwd).setVisibility(View.GONE);
            findViewById(R.id.viewSpliteBellowFollow).setVisibility(View.VISIBLE);
        } else {
            this.viewFollowFB.setVisibility(View.GONE);
            findViewById(R.id.textViewCangePwd).setVisibility(View.VISIBLE);
            findViewById(R.id.viewSpliteBellowFollow).setVisibility(View.GONE);
        }

        this.viewFollowFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Tool.startActivity(activity, FacebookFriendsActivity.class);
            }
        });

        this.viewTermOfUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tool.startActivity(activity, TermsOfUseActivity.class);
            }
        });

        this.viewFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!StringUtils.isEmpty(activity.getInput(textViewFlollowing)) && !"0".equals(activity.getInput(textViewFlollowing))) {
                    App.getApp().putTemPObject("user", AVUser.getCurrentUser());
                    App.getApp().putTemPObject("type", "Following");
                    Tool.startActivity(activity, FollowingAndFollowersListActivity.class);
                }
            }
        });

        this.viewFollowers = findViewById(R.id.viewFollowers);
        this.viewFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!StringUtils.isEmpty(activity.getInput(textViewFollowers)) && !"0".equals(activity.getInput(textViewFollowers))) {
                    App.getApp().putTemPObject("user", AVUser.getCurrentUser());
                    App.getApp().putTemPObject("type", "Followers");
                    Tool.startActivity(activity, FollowingAndFollowersListActivity.class);
                }

            }
        });


        this.inviteFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) activity).showInviteView();

            }
        });

        findViewById(R.id.viewMyCatches).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!StringUtils.isEmpty(activity.getInput(textViewCatches)) && !"0".equals(activity.getInput(textViewCatches))) {
                    Tool.startActivity(activity, MyCatchesActivity.class);
                }
            }
        });


        findViewById(R.id.viewSearchAngler).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tool.startActivity(activity, FindAnglerActivity.class);
            }
        });


        findViewById(R.id.privacyTextView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Tool.startActivity(activity, PrivacyPolicyActivity.class);

                Tool.startActivity(activity, PPWebViewActivity.class);
            }
        });


        findViewById(R.id.textViewCangePwd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String yes = activity.getResources().getString(R.string.confirm);
                String no = activity.getResources().getString(R.string.cancel);

                View dialogView = View.inflate(activity, R.layout.dialog_pwd_chagne, null);
                final EditText editTextPwd = (EditText) dialogView.findViewById(R.id.editTextNewPwd);
                final EditText editTextConfirm = (EditText) dialogView.findViewById(R.id.editTextConfirmPwd);

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setView(dialogView);
                builder.setMessage(R.string.setting_input_pwd);
                builder.setNegativeButton(no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.setPositiveButton(yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        String pwd = getInput(editTextPwd);
                        if (StringUtils.isEmpty(pwd)) {
                            activity.showNotifyTextIn5Seconds(R.string.login_hint_enter_pwd_notify);
                            return;
                        }

                        String rpwd = getInput(editTextConfirm);
                        if (!pwd.equals(rpwd)) {
                            activity.showNotifyTextIn5Seconds(R.string.register_pwd_repeat_error);
                            return;
                        }

                        changePwd(pwd);
                    }
                });
                builder.create().show();
            }
        });
        initData();
    }


    private BaseTask baseTask;

    private void changePwd(final String pwd) {

        if (null != baseTask && (baseTask.getStatus() == AsyncTask.Status.RUNNING)) {
            baseTask.cancel(true);
        }

        baseTask = new BaseTask(new NetCallBack() {

            @Override
            public void onPreCall() {
                activity.showProgressDialogWithTask(baseTask);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask bTask) {

                NetResult netResult = new NetResult();
                try {
                    AVUser aVUser = AVUser.getCurrentUser();
                    if (null != aVUser) {
                        aVUser.setPassword(pwd);
                    }
                    aVUser.save();
                } catch (AVException e) {
                    netResult.setException(e);
                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result) {
                activity.hideProgressDialogWithTask();
                if (null == result.getException() && !activity.isFinishing()) {

                    String success = activity.getResources().getString(R.string.setting_pwd_success);
                    activity.showNotifyTextIn5Seconds(success);
//                    activity.showToast(success, 3);


//                    AVUser.Login(AVUser.getCurrentUser().getUsername(),pwd)

                    AVUser avUser = new AVUser();
                    avUser.logInInBackground(AVUser.getCurrentUser().getUsername(), pwd, new LogInCallback<AVUser>() {
                        @Override
                        public void done(AVUser avUser, AVException e) {

                            if (null == e) {
                                LogUtil.d(App.tag, "Login success again!");
                            } else {
                                LogUtil.d(App.tag, "Login fail again!");
                            }

                        }
                    });

//                    AVUser.getCurrentUser().refreshInBackground(new RefreshCallback<AVObject>() {
//                        @Override
//                        public void done(AVObject avObject, AVException e) {
//                            if (null == e) {
//                                LogUtil.i(App.tag, "刷新成功！");
//                            } else {
//                                logout();
//                            }
//                        }
//                    });

//                    DialogUtils.showMessageDialog(success, "Please login again!", "Confirm", activity, new DialogCallBackListener() {
//
//                        @Override
//                        public void onDone(boolean yesOrNo) {
//                            logout(true);
//                        }
//                    });


//                    logout();
                } else {
//                    activity.showNotifyTextIn5Seconds(((AVException) result.getException()).getMessage());
                    ((BaseScreamingReelActivity) activity).showLeanCloudError((AVException) result.getException());
                }

            }
        });

        baseTask.execute(new HashMap<String, String>());

    }

    @Override
    public void onResume() {
        initData();
    }

    private void initData() {
        netImageViewHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) activity).showPhotoViewForChangeHeaderPicture();
            }
        });


//        findViewById(R.id.followFriends).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Tool.startActivity(activity, FacebookFriendsActivity.class);
//            }
//        });

        findViewById(R.id.viewGoAboutMe).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tool.startActivity(activity, AboutMeActivity.class);
            }
        });

        findViewById(R.id.textViewLogOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onLogutClick();
            }
        });

        textViewVersion.setText(Tool.getVersionName(activity));


        findViewById(R.id.viewComments).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tool.startActivity(activity, CommentsActivity.class);
            }
        });

        AVUser currentUser = AVUser.getCurrentUser();
        if (null != currentUser) {
            textViewUserName.setText(currentUser.getString("first_name") + " " + currentUser.getString("last_name"));
            JSONArray array = currentUser.getJSONArray("follow_users");
            if (null != array) {
                textViewFlollowing.setText(String.valueOf(array.length()));
            } else {
                textViewFlollowing.setText(String.valueOf(0));
            }
            String desc = currentUser.getString("desc");
            textViewDesc.setText(desc);
            AVFile headerFile = currentUser.getAVFile("avatar");
            if (null != headerFile) {

                Picasso.with(App.getApp()).load(headerFile.getUrl()).resize(150, 150).centerCrop().memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).placeholder(R.drawable.icon_header_default).into(netImageViewHeader);
            }

            loadAllCount();
        }

    }

    private void loadAllCount() {
        loadCatch();
        loadCommentCount();
        loadFolwers();
    }

    private void loadFolwers() {

        AVQuery<AVUser> query2 = AVUser.getQuery();
        query2.whereEqualTo("follow_users", AVUser.getCurrentUser());
        query2.countInBackground(new CountCallback() {
            @Override
            public void done(int count, AVException e) {
                if (null == e && !activity.isFinishing()) {

                    Number countNumber = AVUser.getCurrentUser().getNumber("followers_count");
                    if (null != countNumber) {
                        int oldCount = countNumber.intValue();
                        if (count > oldCount) {
                            textViewFollowersDelta.setText(String.valueOf(count - oldCount));
                            textViewFollowersDelta.setVisibility(View.VISIBLE);
                        } else {
                            textViewFollowersDelta.setVisibility(View.INVISIBLE);
                        }
                    }

                    textViewFollowers.setText(String.valueOf(count));

                }
            }
        });

    }

    private void loadCatch() {
        AVQuery<AVObject> parseCatch = AVQuery.getQuery(DataBaseConstants.tab_catchs);
        parseCatch.whereEqualTo("owner", AVUser.getCurrentUser());
        parseCatch.countInBackground(new CountCallback() {
            @Override
            public void done(int count, AVException e) {
                if (e == null) {
                    textViewCatches.setText(String.valueOf(count));
                }
            }
        });
    }


    private void loadCommentCount() {


        countTask();


//        totalCommentsCount = -1;
//
//        AVQuery<AVObject> query = AVQuery.getQuery("Notifications");
//
//        query.whereEqualTo("receiver", AVUser.getCurrentUser());
//        String[] types = {"Like", "Comment"};
//        query.whereContainedIn("type", Arrays.asList(types));
//        query.whereEqualTo("read", false);
//        query.countInBackground(new CountCallback() {
//            @Override
//            public void done(int count, AVException e) {
//                if (null == e) {
//                    if (-1 == totalCommentsCount) {
//                        totalCommentsCount = count;
//                    } else {
//                        totalCommentsCount += count;
//                        textViewCommentCount.setText(String.valueOf(totalCommentsCount));
//                    }
//                }
//            }
//        });
//
//
//        AVQuery countQueryMe = AVQuery.getQuery("Notifications");
//
//        String[] typesMe = {"Like", "Comment"};
//        countQueryMe.whereContainedIn("type", Arrays.asList(typesMe));
//        countQueryMe.whereEqualTo("read", false);
//        countQueryMe.whereEqualTo("sender", AVUser.getCurrentUser());
//        countQueryMe.whereEqualTo("receiver", AVUser.getCurrentUser());
//
//
//        countQueryMe.countInBackground(new CountCallback() {
//            @Override
//            public void done(int i, AVException e) {
//                if (null == e) {
//                    if (-1 == totalCommentsCount) {
//                        totalCommentsCount = i;
//                    } else {
//                        totalCommentsCount += i;
//                        textViewCommentCount.setText(String.valueOf(totalCommentsCount));
//                    }
//                }
//            }
//        });


    }


    public void onLogutClick() {
        String msg = "Logout?";
        DialogCallBackListener dlg = new DialogCallBackListener() {
            @Override
            public void onDone(boolean yesOrNo) {
                if (yesOrNo) {
                    logout(false);
                }
            }
        };

        DialogUtils.showConfirmDialog(activity, "", msg, activity.getResources().getString(R.string.confirm), activity.getResources().getString(R.string.cancel), dlg);
    }

    private void logout(final boolean isChangepwd) {
        activity.reSetTask();
        activity.baseTask = new BaseTask(new NetCallBack() {
            @Override
            public void onPreCall() {

                activity.showProgressDialogWithTask(activity.baseTask);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask btask) {

                NetResult netResult = new NetResult();
                try {
                    AVUser.logOut();
                } catch (Exception e) {
                    netResult.setException(e);

                }
                return netResult;
            }

            @Override
            public void onFinish(NetResult result) {
                activity.hideProgressDialogWithTask();
                if (null == result.getException()) {

                    if (isChangepwd) {
                        activity.setResult(activity.SHORT_LOG_OUT_PWD);
                    } else {
                        activity.setResult(activity.SHORT_LOG_OUT);
                    }

                    activity.finish();

                    AVInstallation.getCurrentInstallation().remove("user");
                    AVInstallation.getCurrentInstallation().saveInBackground();

                } else {
                    activity.showNotifyTextIn5Seconds(((AVException) result.getException()).getMessage());
                }
            }
        });

        activity.baseTask.execute(new HashMap<String, String>());

    }

    public void onPhotoCropped(Uri uri) {
        final String path = uri.getPath();
        if (!StringUtils.isEmpty(path) && null != AVUser.getCurrentUser()) {
            try {
                final AVUser aVUser = AVUser.getCurrentUser();
                byte[] data = FileUtils.readFile2Byte(path);
                final AVFile headerFile = new AVFile(aVUser.getObjectId(), data);
                headerFile.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(AVException e) {
                        if (null == e) {

                            LogUtil.i(App.tag, "upload header success");
                            AVUser.getCurrentUser().put("avatar", headerFile);
                            AVUser.getCurrentUser().saveEventually();
//                            AVUser.getCurrentUser().saveEventually(new SaveCallback() {
//                                @Override
//                                public void done(AVException e) {
//                                    if (null == e) {
//                                        if (null != AVUser.getCurrentUser().getAVFile("avatar")) {
//                                            String headerUrl=AVUser.getCurrentUser().getAVFile("avatar").getUrl();
//                                            Picasso.with(activity).invalidate(headerUrl);
//                                            Picasso.with(activity).load(headerUrl).resize(150,150).memoryPolicy(MemoryPolicy.NO_CACHE,MemoryPolicy.NO_STORE).placeholder(R.drawable.icon_header_default).into(netImageViewHeader);
//                                            LogUtil.i(App.tag,"refresh header:"+headerUrl);
//                                        }
//
//                                    }
//                                }
//                            });

                            Picasso.with(activity).load(new File(path)).config(Bitmap.Config.RGB_565).resize(150, 150).placeholder(R.drawable.icon_header_default).into(netImageViewHeader);


//                            AVUser.getCurrentUser().saveInBackground(new SaveCallback() {
//                                @Override
//                                public void done(AVException e) {
//                                    if (null == e) {
//                                        if (null != AVUser.getCurrentUser().getAVFile("avatar")) {
//                                            String headerUrl=AVUser.getCurrentUser().getAVFile("avatar").getUrl();
//                                            Picasso.with(activity).invalidate(headerUrl);
//                                            Picasso.with(activity).load(headerUrl).resize(150,150).memoryPolicy(MemoryPolicy.NO_CACHE,MemoryPolicy.NO_STORE).placeholder(R.drawable.icon_header_default).into(netImageViewHeader);
//                                            LogUtil.i(App.tag,"refresh header:"+headerUrl);
//                                        }
//
//                                    }
//                                }
//                            });

//                            activity.saveAVObject(AVUser.getCurrentUser(), true, true, new SaveCallback() {
//                                @Override
//                                public void done(AVException e) {
//                                    if (null == e) {
//                                        netImageViewHeader.setUrlWithHolder(path, R.drawable.icon_header_default);
//                                    }
//                                }
//                            });
                        } else {
                            activity.showNotifyTextIn5Seconds(e.getMessage());
                        }
                    }
                });
            } catch (Exception e) {
                activity.showNotifyTextIn5Seconds(R.string.setting_read_file_fail);
            }
        }

    }


    public TextView getTextViewFollowersDelta() {
        return textViewFollowersDelta;
    }

    public View getViewFollowers() {
        return viewFollowers;
    }


    private BaseTask counTask = null;

    private void countTask() {
        if (null != counTask && counTask.getStatus() == AsyncTask.Status.RUNNING) {
            counTask.cancel(true);
        }

        counTask = new BaseTask(new NetCallBack() {

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask baseTask) {


                NetResult netResult = null;

                try {


                    AVQuery<AVObject> query = AVQuery.getQuery("Notifications");

                    query.whereEqualTo("receiver", AVUser.getCurrentUser());
                    query.whereNotEqualTo("sender",AVUser.getCurrentUser());

                    String[] types = {"Like", "Comment"};
                    query.whereContainedIn("type", Arrays.asList(types));
                    query.whereEqualTo("read", false);

                    int count1 = query.count();


                    AVQuery countQueryMe = AVQuery.getQuery("Notifications");

                    String[] typesMe = {"Like", "Comment"};
                    countQueryMe.whereContainedIn("type", Arrays.asList(typesMe));
                    countQueryMe.whereEqualTo("read", false);
                    countQueryMe.whereEqualTo("sender", AVUser.getCurrentUser());
                    countQueryMe.whereEqualTo("receiver", AVUser.getCurrentUser());


                    int count2 = countQueryMe.count();

                    netResult = new NetResult();
                    netResult.setTag((String.valueOf(count1 + count2)));

                } catch (Exception e) {


                }


                return netResult;
            }


            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                super.onFinish(result, baseTask);
                if (null != result) {

                    textViewCommentCount.setText((String) result.getTag());
                }
            }
        });


        counTask.execute(new HashMap<String, String>());
    }

}

package com.cretve.screamingreel.activity;

import android.os.Bundle;

import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.thescreamingreel.androidapp.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class TermsOfUseActivity extends BaseScreamingReelActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_of_use);
        ButterKnife.inject(this);
    }

    @OnClick(R.id.viewBack)
    public void onClick() {
        finish();
    }
}

package com.cretve.screamingreel.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.common.util.ListUtiles;
import com.common.util.StringUtils;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.FishAdapter;
import com.thescreamingreel.androidapp.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class FishSpeciesActivity extends BaseScreamingReelActivity {

    private AVQuery<AVObject> fishQuery;
    private FishAdapter fishAdapter;

    @InjectView(R.id.listViewFishes)
    ListView listViewFishes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fish_species);
        ButterKnife.inject(this);
        loadFish("");
    }

    @Override
    protected void onPause() {
        super.onPause();

        hideSoftKeyborad(null,getCurrentFocus());
    }

    //    public void loadFish() {
//
//        showProgressDialog();
//
//        fishQuery = AVQuery.getQuery("Fish");
//        fishQuery.orderByAscending("name");
//        fishQuery.setLimit(1000);
//        fishQuery.findInBackground(new FindCallback<AVObject>() {
//            @Override
//            public void done(List<AVObject> objects, AVException e) {
//                hideProgressDialog();
//                if (!ListUtiles.isEmpty(objects)) {
//                    buildAdapter(objects);
//                }
//            }
//        });
//
//
//
//
//    }

    public void loadFish(String inputText) {

        fishQuery = AVQuery.getQuery("Fish");

        if (!StringUtils.isEmpty(inputText)) {
            fishQuery.whereMatches("name", ".*" + inputText + ".*", "i");
        }

        fishQuery.orderByAscending("name");
        fishQuery.setLimit(1000);
        fishQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> objects, AVException e) {
                hideProgressDialog();
                if (!ListUtiles.isEmpty(objects)) {
                    buildAdapter(objects);
                }
            }
        });

    }
    @OnTextChanged(R.id.editTextFishInput)
    public void onTextChange(){

        loadFish(getInputFromId(R.id.editTextFishInput));
    }

    private void buildAdapter(final List<AVObject> fishes) {
        fishAdapter = new FishAdapter(this, fishes);
        listViewFishes.setAdapter(fishAdapter);
        listViewFishes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                App.getApp().putTemPObject("fish", (AVObject) parent.getAdapter().getItem(position));
                setResult(RESULT_OK);
                finish();
            }
        });

    }


    @Override
    public void onBackPressed() {
        onBack();
    }

    @OnClick(R.id.viewBack)
    public void onClickBack() {
        onBack();
    }


    public void onBack() {
        hideProgressDialog();

        if (null != fishQuery) {
            fishQuery.cancel();
        }
        finish();
    }
}

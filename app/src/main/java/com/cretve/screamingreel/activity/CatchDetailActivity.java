package com.cretve.screamingreel.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.avos.avoscloud.AVACL;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVPush;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.SendCallback;
import com.common.adapter.AnimationListenerAdapter;
import com.common.util.DateTool;
import com.common.util.DialogCallBackListener;
import com.common.util.DialogUtils;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.CatchDetailImageAdapter;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.indicator.view.IndicatorView;
import com.squareup.picasso.Picasso;
import com.thescreamingreel.androidapp.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class CatchDetailActivity extends BaseScreamingReelActivity {


    AVObject aVObjectCatch;

    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    Point point = null;

    @InjectView(R.id.viewPager)
    ViewPager viewPager;

    @InjectView(R.id.imageViewHeader)
    ImageView imageViewHeader;

    @InjectView(R.id.textViewOwnerName)
    TextView textViewOwnerName;

    @InjectView(R.id.textViewLike)
    TextView textViewLike;

    @InjectView(R.id.textViewFollow)
    TextView textViewFollow;

    @InjectView(R.id.viewReport)
    View viewReport;

    @InjectView(R.id.iconOfLikeInDetail)
    ImageView iconOfLikeInDetail;

    @InjectView(R.id.textViewLikeCount)
    TextView textViewLikeCount;

    @InjectView(R.id.textViewCommentCount)
    TextView textViewCommentCount;

    @InjectView(R.id.textViewLocationInfo)
    TextView textViewLocationInfo;

    @InjectView(R.id.textViewSpeciesInfo)
    TextView textViewSpeciesInfo;

    @InjectView(R.id.textViewBaitInfo)
    TextView textViewBaitInfo;

    @InjectView(R.id.textViewWeightInfo)
    TextView textViewWeightInfo;

    @InjectView(R.id.textViewLengthInfo)
    TextView textViewLengthInfo;

    @InjectView(R.id.textViewFishMethodInfo)
    TextView textViewFishMethodInfo;

    @InjectView(R.id.textViewTimeInfo)
    TextView textViewTimeInfo;

    @InjectView(R.id.textViewBottomDepthInfo)
    TextView textViewBottomDepthInfo;

    @InjectView(R.id.textViewCatchDepthInfo)
    TextView textViewCatchDepthInfo;

    @InjectView(R.id.textViewWaterVisibilityInfo)
    TextView textViewWaterVisibilityInfo;

    @InjectView(R.id.linearLayoutLikeUsers)
    LinearLayout linearLayoutLikeUsers;

    @InjectView(R.id.linearLayoutComments)
    LinearLayout linearLayoutComments;

    @InjectView(R.id.textViewCommentShowHide)
    TextView textViewCommentShowHide;

    @InjectView(R.id.indicatorView)
    IndicatorView indicatorView;


    @InjectView(R.id.textViewDesc)
    TextView textViewDesc;

    @InjectView(R.id.scrollViewContent)
    ScrollView scrollViewContent;

    @InjectView(R.id.linearLayoutDetail)
    LinearLayout linearLayoutDetail;

    @InjectView(R.id.linearLayoutIndicator)
    LinearLayout linearLayoutIndicator;

    @InjectView(R.id.relativeLayoutPictureContent)
    RelativeLayout relativeLayoutPictureContent;

    @InjectView(R.id.editTextPostContent)
    EditText editTextPostContent;


    @InjectView(R.id.viewGoodLike)
    View viewGoodLike;

    private ArrayList<AVUser> arrayListLikeUsers;
    private ArrayList<AVObject> arrayListComments;

    private boolean areaDisable = false;
    int w;


    AVQuery<AVObject> commentsQuery = null;
    AVQuery<AVUser> queryUserDetail = null;

    @InjectView(R.id.linearLayoutLocationDetail)
    LinearLayout linearLayoutLocationDetail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catch_detail);
        ButterKnife.inject(this);
        w = (int) getResources().getDimension(R.dimen.input_item_height_small);
        this.point = Tool.getDisplayMetrics(this);

        this.arrayListLikeUsers = new ArrayList<AVUser>();
        this.arrayListComments = new ArrayList<AVObject>();

        initImageView();

        aVObjectCatch = (AVObject) App.getApp().getTempObject("catch");
        if (null != aVObjectCatch) {
            initView(aVObjectCatch);
            loadCatchUserDetail(aVObjectCatch);
            loadComments(aVObjectCatch);
        }

//
//        String oidOBjectId = getIntent().getStringExtra("oid");
//        AVQuery<AVObject> query = AVQuery.getQuery("Catch");
//        query.whereEqualTo("objectId", oidOBjectId);
//        query.fromPin("toDetail");
//        query.include("owner");
//        query.findInBackground(new FindCallback<AVObject>() {
//            @Override
//            public void done(List<AVObject> objects, AVException e) {
//                if (null == e && !ListUtiles.isEmpty(objects)) {
//                    aVObjectCatch = objects.get(0);
//                    LogUtil.i(App.tag, " find size:" + objects.size());
//                    initView(objects.get(0));
//                    loadCatchUserDetail(aVObjectCatch);
//                    loadComments(aVObjectCatch);
//                }
//            }
//        });

    }


    @OnClick(R.id.linearLayoutPersonInfoInDetail)
    public void onClickOwner() {


        App.getApp().putTemPObject("user", aVObjectCatch.getAVUser("owner"));

        Tool.startActivity(this, ProfileActivity.class);

    }


    @OnClick(R.id.linearLayoutLocationDetail)
    public void onLocationDetailClick() {
        if (null == aVObjectCatch) {
            return;
        }

        AVObject parseAreaObj = aVObjectCatch.getAVObject("area");
        if (null != parseAreaObj) {

            if (Tool.isInstallGMS(this)) {
                App.getApp().putTemPObject("area", parseAreaObj);
                App.getApp().putTemPObject("catch", aVObjectCatch);
                Tool.startActivity(this, FishAreaMapActivity.class);
            } else {
                App.getApp().putTemPObject("area", parseAreaObj);
                Tool.startActivity(this, FishAreaDetailMapActivity.class);
            }
        }
    }

    @OnClick(R.id.linearLayoutFishSpecies)
    public void onSpeciesClick() {

        if (null != aVObjectCatch.getAVObject("fish")) {

            if (aVObjectCatch.getAVObject("fish").isDataAvailable()) {
                App.getApp().putTemPObject("fish", aVObjectCatch.getAVObject("fish"));
                Tool.startActivity(CatchDetailActivity.this, FishDetailActivity.class);
            } else {

                aVObjectCatch.getAVObject("fish").fetchIfNeededInBackground(new GetCallback<AVObject>() {
                    @Override
                    public void done(AVObject object, AVException e) {
                        if (null != object) {
                            App.getApp().putTemPObject("fish", object);
                            Tool.startActivity(CatchDetailActivity.this, FishDetailActivity.class);
                        }
                    }
                });
            }


        }
//
//        else {
//
//            aVObjectCatch.getAVObject("fish").fetchIfNeededInBackground(new GetCallback<AVObject>() {
//                @Override
//                public void done(AVObject object, AVException e) {
//                    if (null != object) {
//                        App.getApp().putTemPObject("fish", object);
//                        Tool.startActivity(CatchDetailActivity.this, FishDetailActivity.class);
//                    }
//                }
//            });
//        }

    }


    private void initImageView() {
        int w = point.x;
//        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(w, w);
//        viewPager.setLayoutParams(layoutParams);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(w, w);
        relativeLayoutPictureContent.setLayoutParams(layoutParams);
        relativeLayoutPictureContent.requestLayout();
    }


    private void initTextViewLikeText(AVObject aVObjectCatch) {

        if (null != AVUser.getCurrentUser().getList("like_catches")) {
            if (AVUser.getCurrentUser().getList("like_catches").contains(aVObjectCatch)) {
                textViewLike.setText("Liked");
                iconOfLikeInDetail.setImageResource(R.drawable.icon_like_2);
            } else {
                textViewLike.setText("Like");
            }
        } else {
            textViewLike.setText("Like");
        }
    }

    private void initTextViewFollowText(AVObject catchObject) {

        if (null != AVUser.getCurrentUser().getList("follow_users")) {

            if (AVUser.getCurrentUser().getList("follow_users").contains(catchObject.getAVUser("owner"))) {
                textViewFollow.setText("Followed");
                textViewFollow.setTextColor(getResources().getColor(R.color.gray_text_color));
            } else {
                textViewFollow.setText("Follow");
                textViewFollow.setTextColor(getResources().getColor(R.color.text_color_ios_blue));
            }
        } else {
            textViewFollow.setText("Follow");
            textViewFollow.setTextColor(getResources().getColor(R.color.text_color_ios_blue));
        }


    }

    private void initView(AVObject catchObject) {

        AVObject areaObj = catchObject.getAVObject("area");
        if (null != areaObj) {
            try {
                String title = areaObj.getString("title");
                if (!StringUtils.isEmpty(title) && "null".equals(title)) {
                    textViewTitle.setText(title);
                } else {
                    textViewTitle.setText("");
                }

            } catch (Exception e) {
            }
        }

        initTextViewLikeText(catchObject);

        String desc = aVObjectCatch.getString("desc");
        if (StringUtils.isEmpty(desc)) {
            textViewDesc.setVisibility(View.GONE);
        } else {
            textViewDesc.setVisibility(View.VISIBLE);
            textViewDesc.setText(desc);
        }


        List<AVFile> photos = catchObject.getList("photos");

        if (!ListUtiles.isEmpty(photos)) {
            int w = point.x;
            indicatorView.setIndicatorSize(photos.size(), R.drawable.icon_radio_blue_uncheck, R.drawable.icon_radio_blue_check);
            CatchDetailImageAdapter catchDetailImageAdapter = new CatchDetailImageAdapter(this, viewPager, w, photos);
            viewPager.setAdapter(catchDetailImageAdapter);
            catchDetailImageAdapter.setOnItemClickListener(new CatchDetailImageAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int pos, String path) {
                    if (path.endsWith("movie") || path.endsWith("mp4")) {
                        Intent intent = new Intent(CatchDetailActivity.this, VideoPlayerActivity.class);
                        intent.putExtra("path", path);
                        startActivity(intent);
                    }
                }
            });

            if (photos.size() > 1) {
                linearLayoutIndicator.setVisibility(View.VISIBLE);
                int desVisible = textViewDesc.getVisibility();
                if (desVisible == View.VISIBLE) {
                    RelativeLayout.LayoutParams lpp = (RelativeLayout.LayoutParams) linearLayoutIndicator.getLayoutParams();
                    lpp.bottomMargin = (int) getResources().getDimension(R.dimen.input_item_height_small_s);
                    linearLayoutIndicator.setLayoutParams(lpp);
                }
                relativeLayoutPictureContent.requestLayout();
            } else {
                linearLayoutIndicator.setVisibility(View.GONE);
            }

        } else {
            indicatorView.setVisibility(View.GONE);
            linearLayoutIndicator.setVisibility(View.GONE);
        }

        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                indicatorView.checkIndex(position);
            }
        });


        AVUser puser = (AVUser) (catchObject.getAVUser("owner"));
        AVFile headerFile = puser.getAVFile("avatar");
        if (null != headerFile) {
            String url = headerFile.getUrl();
            if (!StringUtils.isEmpty(url)) {
                Picasso.with(this).load(url).resize(150, 150).placeholder(R.drawable.icon_header_default).resize(150, 150).into(imageViewHeader);
            }
        }
        textViewOwnerName.setText(String.valueOf("" + puser.getString("first_name") + " " + puser.getString("last_name")));

        initTextViewFollowText(aVObjectCatch);

        if (null != aVObjectCatch.getAVObject("area")) {

            try {
                textViewLocationInfo.setText(aVObjectCatch.getAVObject("area").getString("title"));
            } catch (Exception e) {
            }


        }

        if (null != aVObjectCatch.getAVObject("fish")) {

            aVObjectCatch.getAVObject("fish").fetchIfNeededInBackground(new GetCallback<AVObject>() {
                @Override
                public void done(AVObject object, AVException e) {
                    if (null != object) {
                        textViewSpeciesInfo.setText("" + object.getString("name"));
                    }
                }
            });

        }
        textViewBaitInfo.setText(aVObjectCatch.getString("bait"));
        textViewWeightInfo.setText(aVObjectCatch.getNumber("weight") == null ? "" : aVObjectCatch.getNumber("weight").toString() + " kg");
        textViewLengthInfo.setText(aVObjectCatch.getNumber("length") == null ? "" : aVObjectCatch.getNumber("length").toString() + " cm");

        //=====show moreDetials=====
        textViewFishMethodInfo.setText(aVObjectCatch.getString("method"));

        Date catchDate = aVObjectCatch.getDate("date");
        if (null != catchDate) {
            textViewTimeInfo.setText(DateTool.getLocalFullTime(catchDate));
        }
        textViewBottomDepthInfo.setText(aVObjectCatch.getNumber("bottom_depth") == null ? "" : aVObjectCatch.getNumber("bottom_depth").floatValue() + " m");
        textViewCatchDepthInfo.setText(aVObjectCatch.getNumber("catch_depth") == null ? "" : aVObjectCatch.getNumber("catch_depth").floatValue() + " m");
        textViewWaterVisibilityInfo.setText(aVObjectCatch.getString("water_visibility"));

        if (AVUser.getCurrentUser().equals(aVObjectCatch.getAVUser("owner"))) {
            textViewFollow.setVisibility(View.INVISIBLE);
        } else {
            textViewFollow.setVisibility(View.VISIBLE);
        }
    }


    @OnClick(R.id.textViewFollow)
    public void onClickFellow() {

        if (null != AVUser.getCurrentUser().getList("follow_users")) {

            if (AVUser.getCurrentUser().getList("follow_users").contains(aVObjectCatch.getAVUser("owner"))) {

                ArrayList<AVUser> deleteUser = new ArrayList<AVUser>();
                deleteUser.add(aVObjectCatch.getAVUser("owner"));
                AVUser.getCurrentUser().removeAll("follow_users", deleteUser);
                unfollowAction(aVObjectCatch);

            } else {
                AVUser.getCurrentUser().addUnique("follow_users", aVObjectCatch.getAVUser("owner"));
                followAction(aVObjectCatch);
            }
        } else {
            AVUser.getCurrentUser().addUnique("follow_users", aVObjectCatch.getAVUser("owner"));
            followAction(aVObjectCatch);
        }

    }


    private void loadCatchUserDetail(AVObject aVObjectCatch) {

        if (null != queryUserDetail) {
            queryUserDetail.cancel();
        }

        queryUserDetail = AVUser.getQuery();
        queryUserDetail.whereEqualTo("like_catches", aVObjectCatch);
        queryUserDetail.findInBackground(new FindCallback<AVUser>() {
            @Override
            public void done(List<AVUser> objects, AVException e) {
                if (null == e && !isFinishing()) {
                    arrayListLikeUsers.clear();
                    arrayListLikeUsers.addAll(objects);
                    showLikeUsers(arrayListLikeUsers);
                }
            }
        });
    }


    private void loadComments(AVObject aVObjectCatch) {
        if (null != commentsQuery) {
            commentsQuery.cancel();
        }

        commentsQuery = AVQuery.getQuery("Comment");
        commentsQuery.include("owner");
        commentsQuery.include("catch");
        commentsQuery.whereEqualTo("catch", aVObjectCatch);

        commentsQuery.findInBackground(new FindCallback<AVObject>() {
            @Override
            public void done(List<AVObject> objects, AVException e) {
                if (null == e && !isFinishing()) {
                    showComments(objects);
                    arrayListComments.addAll(objects);
                }
            }
        });
    }

    private void showComments(List<AVObject> commentsObject) {
        linearLayoutComments.removeAllViews();
        textViewCommentCount.setText(ListUtiles.getListSize(commentsObject) + " comments");
        if (!ListUtiles.isEmpty(commentsObject)) {

            LayoutInflater inflater = LayoutInflater.from(this);
            for (int i = 0, isize = commentsObject.size(); i < isize; i++) {

                AVObject cmt = commentsObject.get(i);

                View itemComment = inflater.inflate(R.layout.item_coment, null);

                ImageView imageViewheader = (ImageView) itemComment.findViewById(R.id.imageViewheader);

                final AVUser puser = cmt.getAVUser("owner");
                if (null != puser) {
                    if (null != puser.getAVFile("avatar")) {
                        Picasso.with(this).load(puser.getAVFile("avatar").getUrl()).resize(150, 150).config(Bitmap.Config.RGB_565).placeholder(R.drawable.icon_header_default).into(imageViewheader);
                    } else {
                        imageViewheader.setImageResource(R.drawable.icon_header_default);
                    }

                    TextView textViewCommentName = (TextView) itemComment.findViewById(R.id.textViewCommentName);
                    textViewCommentName.setText(puser.getString("first_name") + " " + puser.getString("last_name"));

                    RelativeTimeTextView textViewCreatedTime = (RelativeTimeTextView) itemComment.findViewById(R.id.textViewCreatedTime);

                    textViewCreatedTime.setReferenceTime(cmt.getCreatedAt().getTime());

                    TextView textViewCommentContent = (TextView) itemComment.findViewById(R.id.textViewCommentContent);

                    textViewCommentContent.setText(cmt.getString("content"));

                    itemComment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            App.getApp().putTemPObject("user", puser);
                            Tool.startActivity(CatchDetailActivity.this, ProfileActivity.class);

                        }
                    });

                    linearLayoutComments.addView(itemComment);
                } else {
                    LogUtil.i(App.tag, "exception comment no user:" + cmt.getString("content"));
                }
            }
        }
    }


    private void addComment(AVObject cmt) {
        arrayListComments.add(cmt);
        textViewCommentCount.setText(ListUtiles.getListSize(arrayListComments) + " comments");

        View itemComment = View.inflate(this, R.layout.item_coment, null);

        ImageView imageViewheader = (ImageView) itemComment.findViewById(R.id.imageViewheader);

        AVUser puser = cmt.getAVUser("owner");
        if (null != puser.getAVFile("avatar")) {
            Picasso.with(this).load(puser.getAVFile("avatar").getUrl()).resize(150, 150).config(Bitmap.Config.RGB_565).placeholder(R.drawable.icon_header_default).into(imageViewheader);
        } else {
            imageViewheader.setImageResource(R.drawable.icon_header_default);
        }

        TextView textViewCommentName = (TextView) itemComment.findViewById(R.id.textViewCommentName);
        textViewCommentName.setText(puser.getString("first_name") + " " + puser.getString("last_name"));

        RelativeTimeTextView textViewCreatedTime = (RelativeTimeTextView) itemComment.findViewById(R.id.textViewCreatedTime);

        textViewCreatedTime.setReferenceTime(null == cmt.getCreatedAt() ? DateTool.getNow().getTime() : cmt.getCreatedAt().getTime());

        TextView textViewCommentContent = (TextView) itemComment.findViewById(R.id.textViewCommentContent);

        textViewCommentContent.setText(cmt.getString("content"));
        linearLayoutComments.addView(itemComment);
    }

    private void showLikeUsers(ArrayList<AVUser> aVUsers) {
        linearLayoutLikeUsers.removeAllViews();

        String likedText = ListUtiles.getListSize(aVUsers) >= 2 ? ListUtiles.getListSize(aVUsers) + " people like this." :
                ListUtiles.getListSize(aVUsers) + " person likes this.";


        textViewLikeCount.setText(likedText);


        for (int i = 0, isize = aVUsers.size(); i < isize; i++) {

            final AVUser aVUser = aVUsers.get(i);

            ImageView imageView = new ImageView(App.getApp());
            AVFile pfile = aVUser.getAVFile("avatar");

            if (null != pfile) {
                Picasso.with(App.getApp()).load(pfile.getUrl()).resize(150, 150).config(Bitmap.Config.RGB_565).placeholder(R.drawable.icon_header_default).into(imageView);
            } else {
                imageView.setBackgroundResource(R.drawable.icon_header_default);
            }

            imageView.setTag(aVUser.getObjectId());
            LinearLayout.LayoutParams lpp = new LinearLayout.LayoutParams(w, w);

            int margin = (int) getResources().getDimension(R.dimen.margin_micro);
            if (i == 0) {
                lpp.setMargins(0, margin, 0, margin);
            } else {
                lpp.setMargins(margin, margin, 0, margin);
            }

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    App.getApp().putTemPObject("user", aVUser);
                    Tool.startActivity(CatchDetailActivity.this, ProfileActivity.class);
                }
            });

            linearLayoutLikeUsers.addView(imageView, lpp);
        }
    }


    private void addLikedUserToList(AVUser aVUser) {
        ImageView imageView = new ImageView(App.getApp());
        AVFile pfile = aVUser.getAVFile("avatar");
        if (null != pfile) {
            Picasso.with(App.getApp()).load(pfile.getUrl()).resize(150, 150).config(Bitmap.Config.RGB_565).placeholder(R.drawable.icon_header_default).into(imageView);
        } else {
            imageView.setBackgroundResource(R.drawable.icon_header_default);
        }

        imageView.setTag(aVUser.getObjectId());
        LinearLayout.LayoutParams lpp = new LinearLayout.LayoutParams(w, w);

        int margin = (int) getResources().getDimension(R.dimen.margin_micro);
        if (linearLayoutLikeUsers.getChildCount() == 0) {
            lpp.setMargins(0, margin, 0, margin);
        } else {
            lpp.setMargins(margin, margin, 0, margin);
        }
        linearLayoutLikeUsers.addView(imageView, lpp);
    }

    private void removeLikedUser(AVUser aVUser) {

        View view = linearLayoutLikeUsers.findViewWithTag(aVUser.getObjectId());
        if (null != view) {
            linearLayoutLikeUsers.removeView(view);
        }
    }

    @Override
    protected void onDestroy() {

        if (null != commentsQuery) {
            commentsQuery.cancel();
        }
        if (null != queryUserDetail) {
            queryUserDetail.cancel();
        }

        super.onDestroy();
    }

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {


            switch (msg.what) {
                case 0:
                    scrollViewContent.smoothScrollTo(0, linearLayoutComments.getTop());
                    break;
                case 1:
//                    scrollViewContent.smoothScrollTo(0, linearLayoutDetail.getBottom());
                    scrollViewContent.fullScroll(View.FOCUS_DOWN);
                    break;
            }


        }
    };

    @OnClick(R.id.textViewCommentShowHide)
    public void onClickHideShowComments() {
        int visibile = linearLayoutComments.getVisibility();
        linearLayoutComments.setVisibility(visibile == View.VISIBLE ? View.GONE : View.VISIBLE);
        if (linearLayoutComments.getVisibility() == View.VISIBLE) {
            handler.sendEmptyMessageDelayed(0, 100);
        }

        scrollViewContent.requestLayout();
        textViewCommentShowHide.setText(linearLayoutComments.getVisibility() == View.VISIBLE ? "Hide" : "Show");

    }

    @InjectView(R.id.buttonShowMoreOrLess)
    Button buttonShowMoreOrLess;

    @OnClick(R.id.buttonShowMoreOrLess)
    public void onShoeMoreClick() {
        int visibile = linearLayoutDetail.getVisibility();
        linearLayoutDetail.setVisibility(visibile == View.VISIBLE ? View.GONE : View.VISIBLE);
        if (linearLayoutDetail.getVisibility() == View.VISIBLE) {
            buttonShowMoreOrLess.setText("Show less details");
            handler.sendEmptyMessageDelayed(1, 100);
        } else {
            buttonShowMoreOrLess.setText("Show more details");
        }

    }


    @OnClick(R.id.viewShare)
    public void onShareClick() {

        onShareClick(aVObjectCatch);

//        App.getApp().putTemPObject("catch", aVObjectCatch);
//        Tool.startActivity(this, SelectImageActivity.class);
        //Tool.sendShare(this);
    }

    @OnClick(R.id.viewLike)
    public void onLikeClick() {

        if (null != AVUser.getCurrentUser().getList("like_catches")) {

            if (AVUser.getCurrentUser().getList("like_catches").contains(aVObjectCatch)) {


                ArrayList<AVObject> deleteList = new ArrayList<>();

                deleteList.add(aVObjectCatch);

//              AVUser.getCurrentUser().getList("like_catches").remove(aVObjectCatch);

                AVUser.getCurrentUser().removeAll("like_catches", deleteList);


                aVObjectCatch.increment("likes_count", -1);
                unLikeAction(aVObjectCatch);
//                textViewLike.setText("Like");
//                removeLikedUser(AVUser.getCurrentUser());

            } else {
                AVUser.getCurrentUser().addUnique("like_catches", aVObjectCatch);
                aVObjectCatch.increment("likes_count");
                likeAction(aVObjectCatch);

            }
        } else {
            AVUser.getCurrentUser().addUnique("like_catches", aVObjectCatch);
            aVObjectCatch.increment("likes_count");
            likeAction(aVObjectCatch);
//
//            textViewLike.setText("Liked");
//
//            //send push
//            sendLikePush(aVObjectCatch);
        }


//
//        if (null != AVUser.getCurrentUser().getList("like_catches")) {
//
//            if (AVUser.getCurrentUser().getList("like_catches").contains(aVObjectCatch)) {
//                AVUser.getCurrentUser().getList("like_catches").remove(aVObjectCatch);
//                aVObjectCatch.increment("likes_count", -1);
//                textViewLike.setText("Like");
//                removeLikedUser(AVUser.getCurrentUser());
//
//            } else {
//                AVUser.getCurrentUser().addUnique("like_catches", aVObjectCatch);
//                aVObjectCatch.increment("likes_count");
//                textViewLike.setText("Liked");
//                addLikedUserToList(AVUser.getCurrentUser());
//                blinkGood();
//                //send push
//
//                sendLikePush(aVObjectCatch);
//
//            }
//        } else {
//            aVObjectCatch.increment("likes_count");
//            textViewLike.setText("Liked");
//            //send push
//            sendLikePush(aVObjectCatch);
//        }
//        aVObjectCatch.saveEventually();
//        AVUser.getCurrentUser().saveEventually();

//        saveAVObject(AVUser.getCurrentUser(),true,true,null);
//        aVObjectCatch.unpinInBackground(new DeleteCallback() {
//            @Override
//            public void done(AVException e) {
//                aVObjectCatch.saveEventually();
//            }
//        });
    }

    public void followAction(final AVObject aVObjectCatch) {

        saveAVObject(aVObjectCatch, AVUser.getCurrentUser(), true, true, new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (null == e && !isFinishing()) {
                    textViewFollow.setText("Followed");
                    textViewFollow.setTextColor(getResources().getColor(R.color.gray_text_color));
                    //send a push
                    sendFellowPush(aVObjectCatch);
                }
            }
        });


    }

    public void unfollowAction(AVObject aVObjectCatch) {

        saveAVObject(aVObjectCatch, AVUser.getCurrentUser(), true, true, new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (null == e && !isFinishing()) {
                    textViewFollow.setText("Follow");
                    textViewFollow.setTextColor(getResources().getColor(R.color.text_color_ios_blue));
                }
            }
        });

    }

    public void likeAction(AVObject aVObject) {

        saveAVObject(aVObject, AVUser.getCurrentUser(), true, true, new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (null == e && !isFinishing()) {
                    textViewLike.setText("Liked");
                    iconOfLikeInDetail.setImageResource(R.drawable.icon_like_2);
                    addLikedUserToList(AVUser.getCurrentUser());
                    blinkGood();

                    String likedText = linearLayoutLikeUsers.getChildCount() >= 2 ? linearLayoutLikeUsers.getChildCount() + " people like this." :
                            linearLayoutLikeUsers.getChildCount() + " person likes this.";

                    textViewLikeCount.setText(likedText);


                    //send push
                    sendLikePush(aVObjectCatch);
                }
            }
        });


    }

    public void unLikeAction(AVObject aVObject) {

        saveAVObject(aVObject, AVUser.getCurrentUser(), true, true, new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (null == e && !isFinishing()) {
                    textViewLike.setText("Like");
                    iconOfLikeInDetail.setImageResource(R.drawable.icon_like_p);
                    removeLikedUser(AVUser.getCurrentUser());


                    String likedText = linearLayoutLikeUsers.getChildCount() >= 2 ? linearLayoutLikeUsers.getChildCount() + " people like this." :
                            linearLayoutLikeUsers.getChildCount() + " person likes this.";
                    textViewLikeCount.setText(likedText);


                }
            }
        });

    }


    public void blinkGood() {
        //        viewGoodLike

//        Animation fadeOut = AnimationUtils.loadAnimation(this, R.anim.abc_fade_out);
        Animation fadeIin = AnimationUtils.loadAnimation(this, R.anim.abc_fade_in);

        AnimationSet set = new AnimationSet(true);

        set.addAnimation(fadeIin);
//        set.addAnimation(fadeOut);

        set.setDuration(2 * 1000);

        set.setAnimationListener(new AnimationListenerAdapter() {
            @Override
            public void onAnimationStart(Animation animation) {
                viewGoodLike.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                viewGoodLike.setVisibility(View.GONE);
            }
        });

        viewGoodLike.startAnimation(set);
    }

    @OnClick(R.id.viewReport)
    public void onReportClick() {


        DialogUtils.showInputDialog3(this, "Please input report reason here", "Report", "Confirm", "Cancel", "", "Do you want to report this user?", new DialogCallBackListener() {

            @Override
            public void onCallBack(boolean yesNo, String text) {
                if (yesNo) {

                    AVObject repartObject = AVObject.create("Report");
                    repartObject.put("catch", aVObjectCatch);
                    repartObject.put("reporter", AVUser.getCurrentUser());
                    repartObject.put("pending", true);
                    repartObject.put("reason", text);

                    saveAVObject(repartObject, true, false, new SaveCallback() {
                        @Override
                        public void done(AVException e) {
                            if (null == e) {
                                showNotifyTextIn5Seconds("Report has been sent successfully.");
                            } else {
                                showLeanCloudError(e);
                            }
                        }
                    });
                    LogUtil.i(App.tag, "repor user");
                }
            }
        });

    }

    @OnClick(R.id.viewPost)
    public void onPostClick() {
        post();
    }

    private void post() {
        String postContent = getInput(editTextPostContent);
        if (!StringUtils.isEmpty(postContent)) {

            final AVObject comment = AVObject.create("Comment");
            comment.put("content", postContent);
            comment.put("read", Boolean.valueOf(false));
            comment.put("owner", AVUser.getCurrentUser());
            comment.put("catch", aVObjectCatch);





//            aVObjectCatch.increment("comments_count");

            AVObject notification = AVObject.create("Notifications");

            notification.put("sender", AVUser.getCurrentUser());
            notification.put("type", "Comment");
            notification.put("receiver", aVObjectCatch.getAVUser("owner"));
            notification.put("comment", comment);
            notification.put("catch", aVObjectCatch);
            notification.put("read", false);


            AVACL pacl = new AVACL();

            pacl.setPublicReadAccess(true);
            pacl.setPublicWriteAccess(true);

            notification.setACL(pacl);


            saveAVObject(notification, true, false, new SaveCallback() {
                @Override
                public void done(AVException e) {
                    if (null == e) {
                        aVObjectCatch.increment("comments_count");
                        aVObjectCatch.saveEventually();

                        if (!isFinishing()) {
                            editTextPostContent.setText("");
                            addComment(comment);
                        }
                        try {

                            AVQuery query = AVInstallation.getQuery();
                            query.whereEqualTo("user", aVObjectCatch.get("owner"));

                            JSONObject jsobObject = new JSONObject();

                            String alert = AVUser.getCurrentUser().get("first_name") + " " + AVUser.getCurrentUser().get("last_name") + " commented your catch ";
                            if (null != aVObjectCatch.get("area")) {
                                alert += "at " + (aVObjectCatch.getAVObject("area").getString("title"));
                            }
                            if (null != aVObjectCatch.get("fish")) {
                                alert += " with " + aVObjectCatch.getAVObject("fish").getString("name");
                            }
                            jsobObject.put("alert", alert);
                            jsobObject.put("key", App.K_CATCHCOMMENTED_NOTIFICATION_KEY);
                            jsobObject.put("objectId", comment.getObjectId());
                            jsobObject.put("action", "com.avos.UPDATE_STATUS");

                            AVPush.sendDataInBackground(jsobObject, query, new SendCallback() {

                                @Override
                                public void done(AVException e) {

                                }
                            });
                        } catch (Exception e2) {

                        }

                    } else {

                        if (!isFinishing()) {

                            showNotifyTextIn5Seconds(R.string.error_net);
                        }
                    }
                }
            });

//            notification.saveInBackground(new SaveCallback() {
//                @Override
//                public void done(AVException e) {
//                    if (null == e) {
//                        if (!isFinishing()) {
//                            editTextPostContent.setText("");
//                            addComment(comment);
//                            // AVQuery query= ParseInstallation.getQuery();
////                        query.whereEqualTo("user",aVObjectCatch.getAVUser("owner"));
//                            //send push
//                        }
//                    } else {
//                        LogUtil.i(App.tag, "msg:" + e.getLocalizedMessage());
//                    }
//                }
//            });

//            notification.saveEventually();

//            notification.saveEventually(new SaveCallback() {
//                @Override
//                public void done(AVException e) {
//                    if (null == e) {
//                        try {
//
//                            AVQuery query = ParseInstallation.getQuery();
//                            query.whereEqualTo("user", aVObjectCatch.get("owner"));
//
//                            JSONObject jsobObject = new JSONObject();
//
//                            String alert = AVUser.getCurrentUser().get("first_name") + " " + AVUser.getCurrentUser().get("last_name") + " commented your catch ";
//                            if (null != aVObjectCatch.get("area")) {
//                                alert += "at " + (aVObjectCatch.getAVObject("area").getString("title"));
//                            }
//                            if (null != aVObjectCatch.get("fish")) {
//                                alert += " with " + aVObjectCatch.getAVObject("fish").getString("name");
//                            }
//                            jsobObject.put("alert", alert);
//                            jsobObject.put("key", App.K_CATCHCOMMENTED_NOTIFICATION_KEY);
//                            jsobObject.put("objectId", comment.getObjectId());
//
//                            ParsePush.sendDataInBackground(jsobObject, query);
//                        } catch (Exception e2) {
//
//                        }
//                    }
//                }
//            });

//            addComment(comment);
//            editTextPostContent.setText("");
        }
    }


    @OnClick(R.id.viewBack)
    public void onClickBack() {
        onBackClick();
    }

    @Override
    public void onBackPressed() {
        onBackClick();
    }


    private void onBackClick() {

//        if (null != aVObjectCatch) {
//
//            saveAVObject(aVObjectCatch, false, false, null);
//            saveAVObject(AVUser.getCurrentUser(), false, false, null);
//        }

        setResult(RESULT_OK);
        finish();
    }
}

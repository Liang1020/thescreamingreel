package com.cretve.screamingreel.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.LogInCallback;
import com.avos.avoscloud.PushService;
import com.avos.avoscloud.SaveCallback;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.FileUtils;
import com.common.util.LogUtil;
import com.common.util.SharePersistent;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.util.ValidateTool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.WelcomeAdapter;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.indicator.view.IndicatorView;
import com.thescreamingreel.androidapp.R;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class WelcomeActivity extends BaseScreamingReelActivity {


    @InjectView(R.id.viewPager)
    ViewPager viewPager;

    @InjectView(R.id.indicatorView)
    IndicatorView indicatorView;

//    @InjectView(R.id.imageViewSplash)
//    ImageView imageViewSplash;

    private WelcomeAdapter welcomeAdapter;

    private CallbackManager callbackManager;
    private ProfileTracker profileTracker;
    private LoginManager loginManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());//初始化SDK
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        LogUtil.e(App.tag, "=============================================");
        LogUtil.e(App.tag, "device info:" + Tool.getDisplayMetricsText(this));
        LogUtil.e(App.tag, "=============================================");

        View contentView = View.inflate(this, R.layout.activity_welcome, null);

        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        lp.topMargin = -(getStatusBarHeight());
        contentView.setLayoutParams(lp);
        setContentView(contentView);
        ButterKnife.inject(this);
        initView();
        initFbLogin();
        isHadLogin();

    }


    public void isHadLogin() {

        if (null != AVUser.getCurrentUser() && null != AVUser.getCurrentUser().getObjectId()) {
            if (SharePersistent.getBoolean(this, App.KEY_HOST_SERVER_US) == false) {

                //fist time use us server,need to relogin
                SharePersistent.saveBoolean(this, App.KEY_HOST_SERVER_US, true);

            } else {
                //not first login
                if (AVUser.getCurrentUser().getBoolean("fromFb")  /*AVUser.getCurrentUser().get("facebookId") != null*/) {
                    onLoginFbSuccess();
                } else {
                    onLoginSuccess();
                }
            }
        }
    }


//    private Handler handler = new Handler() {
//
//        @Override
//        public void handleMessage(Message msg) {
//            isHadLogin();
//            imageViewSplash.setVisibility(View.GONE);
//        }
//    };
//
//
//    private void showSplash() {
//        imageViewSplash.setVisibility(View.VISIBLE);
//        handler.sendEmptyMessageDelayed(0, 1000 * 2);
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != loginManager) {
            loginManager.logOut();
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    @Override
    public void initView() {
        welcomeAdapter = new WelcomeAdapter(this, viewPager);
        welcomeAdapter.setOnLoginClickInBlock(new WelcomeAdapter.OnLoginClickInBlockListener() {
            @Override
            public void onLoginClickInBlock(EditText editTextLogin, EditText editTextPwd) {

                if (checkValue(editTextLogin, editTextPwd)) {
                    String userName = getInput(editTextLogin);
                    String pwd = getInput(editTextPwd);
                    login(userName, pwd);
                }
            }

            @Override
            public void onRegisterClickInBlock(EditText editEmail, EditText editPwd, EditText editPwdRepeat) {

                if (checkRegistValue(editEmail, editPwd, editPwdRepeat)) {
                    regist(getInput(editEmail), getInput(editPwd));
                }
            }

            @Override
            public void onLoginFb(boolean isNewFbUser) {
//                onLoginSuccess();
                onLoginFbSuccess();
            }

            @Override
            public void onLoginFBClick() {

                Collection<String> permissions = Arrays.asList("public_profile", "email", "user_friends", "user_hometown", "user_location");
//                loginManager.setLoginBehavior(LoginBehavior.WEB_ONLY);
                loginManager.logInWithReadPermissions(WelcomeActivity.this, permissions);


            }

        });
        viewPager.setAdapter(welcomeAdapter);

        indicatorView.setIndicatorSize(welcomeAdapter.getCount(), R.drawable.shape_radio_uncheck, R.drawable.shape_radio_check);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                indicatorView.checkIndex(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }


    private void initFbLogin() {
        callbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();
        loginManager.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        //fb login 100% call this method
                        Log.i(App.tag, "login onSuccess");
                        String userId = loginResult.getAccessToken().getUserId();
                        Log.i(App.tag, "login onSuccess uid:" + userId);
                        LogUtil.e(App.tag, "1.loginfb1");


                        AVUser.AVThirdPartyUserAuth auth = new AVUser.AVThirdPartyUserAuth(
                                loginResult.getAccessToken().toString(),
                                loginResult.getAccessToken().getExpires().toString(),
                                "facebook",
                                userId
                        );

                        AVUser.loginWithAuthData(auth, new LogInCallback<AVUser>() {
                            @Override
                            public void done(AVUser avUser, AVException e) {

                                showProgressDialog(false);


                                getFacebookUserDetails(avUser, new LogInCallback() {
                                    @Override
                                    public void done(AVUser avUser, AVException e) {
                                        hideProgressDialog();
                                        onLoginFbSuccess();
                                    }

                                    @Override
                                    protected void internalDone0(Object o, AVException e) {
                                        hideProgressDialog();
                                        showLeanCloudError(e);
                                    }
                                });

                            }
                        });

//                        onLoginFbSuccess(userId);
//                        onLoginFbSuccess(loginResult, null);
//                       String name = currentProfile.getFirstName() + currentProfile.getMiddleName() + currentProfile.getLastName();
//                       Log.i("wzy", "fuall name:" + name);
//                       profileTracker.startTracking();//获取更加详细的用户数据
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Tool.ToastShow(WelcomeActivity.this, "LoginFail with FB,Please retry!");
                    }
                });


        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                try {
                    String name = currentProfile.getFirstName() + currentProfile.getMiddleName() + currentProfile.getLastName();
                    Log.i(App.tag, "fuall name:" + name);

                    String header = currentProfile.getProfilePictureUri(380, 320).getPath();

                    Log.i(App.tag, "url:" + currentProfile.getProfilePictureUri(380, 320));
                    Log.i(App.tag, "link:" + currentProfile.getLinkUri());
                    Log.i(App.tag, "name:" + currentProfile.getName());
                    Log.i(App.tag, "id:" + currentProfile.getId());
                    Log.i(App.tag, "des:" + currentProfile.describeContents());

//                    Tool.ToastShow(WelcomeActivity.this, "welcome:" + name);

                    LogUtil.e(App.tag, "2.loginfb2");

//                    onLoginFbSuccess(null, currentProfile, name, currentProfile.getId(), header);

                } catch (Exception e) {

                }
            }
        };

    }

    private void login(final String userName, final String pwd) {
        reSetTask();
        baseTask = new BaseTask(new NetCallBack() {
            @Override
            public void onPreCall(BaseTask baseTask) {
                showNewDialogWithNewTask(baseTask);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask bTask) {

                NetResult netResult = new NetResult();
                try {
                    AVUser user = AVUser.logIn(userName, pwd);
                    netResult = new NetResult();
                    netResult.setTag(user);
                } catch (AVException e) {
                    netResult.setException(e);
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                hideNewDialogWithTask(baseTask);
                if (null == result) {
                    showNotifyTextIn5Seconds(result.getMessage());
                    return;
                }

                if (isFinishing()) {
                    return;
                }

                if (null != result && null != result.getTag()) {
                    onLoginSuccess();
                } else {
                    //=====old notifyfication====
                    AVException aVException = ((AVException) result.getException());
//                    int exceptionCode = aVException.getCode();
//                    String msg = aVException.getLocalizedMessage();

                    showLeanCloudError(aVException);
//                    LogUtil.i(App.tag, "login fail code:" + exceptionCode);

                }
            }
        });
        baseTask.execute(new HashMap<String, String>());
        putTask(baseTask);
    }


    private void getFacebookUserDetails(final AVUser user, final LogInCallback callback) {

        Bundle bundle = new Bundle();
        bundle.putString("fields", "first_name, last_name, email, gender, locale, picture, hometown, location");
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me",
                bundle,
                HttpMethod.GET,
                new GraphRequest.Callback() {

                    public void onCompleted(GraphResponse response) {
                        // Application code
                        Log.e("myApp", "get facebook user details");
                        JSONObject object = response.getJSONObject();
                        if (object == null) {
                            callback.done(user, null);
                        }

                        try {

                            AVUser.getCurrentUser().put("fromFb", true);

                            StringBuilder fullName = new StringBuilder();


                            if (object.get("first_name") != null) {
                                fullName.append(object.getString("first_name"));
                                user.put("first_name", object.get("first_name"));
                            }

                            if (object.get("last_name") != null) {
                                fullName.append(" " + object.getString("last_name"));
                                user.put("last_name", object.get("last_name"));
                            }

                            if (!StringUtils.isEmpty(fullName.toString())) {
                                user.put("full_name", fullName.toString());
                            }

                            //=============================
                            if (object.get("locale") != null) {
                                String str = (String) object.get("locale");
                                String newStr = str.substring(str.indexOf("_") + 1);
                                user.put("country", newStr);
                            }

                            user.put("facebookId", object.get("id"));

                            if (object.get("gender") != null) {
                                String sex = (String) object.get("gender");
                                if (sex.equalsIgnoreCase(WelcomeActivity.this.getString(R.string.regist_gender_female))) {
                                    sex = WelcomeActivity.this.getString(R.string.regist_gender_female);
                                } else {
                                    sex = WelcomeActivity.this.getString(R.string.regist_gender_male);
                                }
                                user.put("gender", sex);
                            }

                            if (object.get("email") != null)
                                user.put("contact_email", object.get("email"));

                            if (object.get("id") != null && user.get("avatar") == null) {

                                final String url = "https://graph.facebook.com/" + object.get("id") + "/picture?type=large&return_ssl_resources=1";
                                try {
                                    byte[] data = FileUtils.readFile2ByteUrl(url);
                                    AVUser aVUser = AVUser.getCurrentUser();
                                    final AVFile headerFile = new AVFile(aVUser.getObjectId(), data);
                                    AVUser.getCurrentUser().put("avatar", headerFile);

                                } catch (Exception e) {
                                    LogUtil.e(App.tag, "get fb header file error!");
                                }
                            }

                            saveAVObject(user, false, false, new SaveCallback() {
                                @Override
                                public void done(AVException e) {
                                    if (null == e) {
                                        callback.done(user, e);
                                    } else {
                                        showLeanCloudError(e);
                                    }
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                            callback.done(null, new AVException(-1, "login error"));
                        }

                    }
                }
        ).executeAsync();
    }


    private void gotoCatchListBefore() {
//        Tool.startActivity(this, MainActivity.class);
//        Tool.startActivityForResult(this, MainActivity.class, SHORT_LOG_OUT);
        requestAccessLocation();

    }

    private void gotoCatchList() {
        Tool.startActivityForResult(this, MainActivity.class, SHORT_LOG_OUT);
    }

    public void requestAccessLocation() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                App.getApp().startLocation();
                gotoCatchList();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                }, SHORT_ACCESS_LOCATION);

            }
        } else {
            gotoCatchList();
            App.getApp().stopLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        gotoCatchList();
    }

    private void onLoginFbSuccess() {
        App.getApp().setLoginfb(true);
        gotoCatchListBefore();
        saveInstallation();
    }

    private void onLoginSuccess() {
        // Save the current Installation to Parse.
        App.getApp().setLoginfb(false);
        AVUser aVUser = AVUser.getCurrentUser();
        String firstName = aVUser.getString("first_name");
        if (StringUtils.isEmpty(firstName)) {
            gotoStepActivity();
        } else {
            gotoCatchListBefore();
        }

        saveInstallation();
    }

    private void saveInstallation() {
        // Save the current Installation to Parse.
        AVInstallation.getCurrentInstallation().put("user", AVUser.getCurrentUser());
        AVInstallation.getCurrentInstallation().saveInBackground();

//        PushService.setDefaultPushCallback(this, WelcomeActivity.class);

        PushService.setDefaultPushCallback(this, SplashActivity.class);
        PushService.setNotificationIcon(getNotificationIcon());

    }

    private void onSignUpSuccess() {
        gotoStepActivity();
        saveInstallation();
    }

    private void gotoStepActivity() {
        Intent intent = new Intent(this, Step4Activity.class);
        startActivityForResult(intent,SHORT_LOG_OUT);
    }


    private void regist(final String email, final String pwd) {
        reSetTask();
        baseTask = new BaseTask(new NetCallBack() {
            @Override
            public void onPreCall() {
                showProgressDialogWithTask(baseTask);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask bTask) {

                NetResult netResult = new NetResult();
                try {

                    AVUser aVUser = new AVUser();
                    aVUser.setEmail(email);
                    aVUser.setPassword(pwd);
                    aVUser.setUsername(email);
                    aVUser.signUp();
                    netResult = new NetResult();
                    netResult.setTag(aVUser);
                } catch (AVException e) {
                    netResult.setException(e);
                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result) {
                hideProgressDialogWithTask();
                if (null != result && null != result.getTag()) {
                    onSignUpSuccess();
                } else {
                    AVException aVException = ((AVException) result.getException());
                    showLeanCloudError(aVException);
//                    int exceptionCode = aVException.getCode();
//                    String msg = aVException.getLocalizedMessage();
//                    showNotifyTextIn5Seconds(msg);

//                    LogUtil.i(App.tag, "sigup fail code:" + exceptionCode);
                }
            }
        });
        baseTask.execute(new HashMap<String, String>());
        putTask(baseTask);
    }


    private boolean checkRegistValue(EditText editTextEmail, EditText editTextPwd, EditText editTextPwdRepeat) {


        String email = editTextEmail.getText().toString();
        if (!ValidateTool.checkEmail(email)) {
            showNotifyTextIn5Seconds(R.string.login_hint_enter_right_email_notify);
            return false;
        }

        String pwd = editTextPwd.getText().toString();
        if (StringUtils.isEmpty(pwd)) {
            showNotifyTextIn5Seconds(R.string.login_hint_enter_pwd_notify);
            return false;
        }

        String repwd = getInput(editTextPwdRepeat);
        if (!pwd.equals(repwd)) {
            showNotifyTextIn5Seconds(R.string.register_pwd_repeat_error);
            return false;
        }

        return true;
    }

    private boolean checkValue(EditText editTextEmail, EditText editTextPwd) {

        String email = editTextEmail.getText().toString();
        if (!ValidateTool.checkEmail(email)) {
            showNotifyTextIn5Seconds(R.string.login_hint_enter_right_email_notify);
            return false;
        }

        String pwd = editTextPwd.getText().toString();
        if (StringUtils.isEmpty(pwd)) {
            showNotifyTextIn5Seconds(R.string.login_hint_enter_pwd_notify);
            return false;
        }


        return true;
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if ((Intent.FLAG_ACTIVITY_CLEAR_TOP & intent.getFlags()) != 0) {
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SHORT_LOG_OUT) {

            if (resultCode == SHORT_LOG_OUT) {
                viewPager.setCurrentItem(3);
                welcomeAdapter.clearInputText();
            }

            if (resultCode == SHORT_LOG_OUT_PWD) {
                viewPager.setCurrentItem(3);
                welcomeAdapter.clearInputText();
            }

        }
    }

}

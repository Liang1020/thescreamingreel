package com.cretve.screamingreel.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.SignUpCallback;
import com.bigkoo.pickerview.listener.OnDismissListener;
import com.common.adapter.TextWatcherAdapter;
import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.util.AssertTool;
import com.common.util.ListUtiles;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.common.view.NoScrollViewPager;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.GuildAdapter;
import com.cretve.screamingreel.adapter.LocationAdapter;
import com.cretve.screamingreel.photo.lib.PhotoLibUtils;
import com.cretve.screamingreel.view.AddPhotoDialogInCatchList;
import com.thescreamingreel.androidapp.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by wangzy on 15/11/3.
 */
public class Step4Activity extends BaseScreamingReelActivity {

    @InjectView(R.id.textViewTitle)
    TextView textViewTitle;

    @InjectView(R.id.viewPager)
    NoScrollViewPager viewPager;

    @InjectView(R.id.viewHeader)
    View viewHeader;

    private int current_seting = 1;
    private GuildAdapter guildAdapter;

    private ListView listViewLocation;
    private EditText editTextLocationInput;

    private AVObject tempUserObj;
    private short SHORT_GO_CITY = 100;
    private String fileUrlHeader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tempUserObj = AVObject.create("TempUser");
        tempUserObj.put("email", AVUser.getCurrentUser().getEmail());
//        tempUserObj.pinInBackground();

        initView();

    }


    private String lastSearch = "";

    @Override
    public void initView() {
        setContentView(R.layout.activity_step_four);
        ButterKnife.inject(this);
        guildAdapter = new GuildAdapter(this, viewPager);
        viewPager.setAdapter(guildAdapter);
//        viewPager.setNoScroll(true);
        listViewLocation = guildAdapter.getListViewLocation();
        editTextLocationInput = guildAdapter.getEditTextLocationInput();


        editTextLocationInput.addTextChangedListener(new TextWatcherAdapter() {
            @Override
            public void afterTextChanged(Editable s) {


                String searcTarget = getInput(editTextLocationInput).trim();
//                if (!lastSearch.equals(searcTarget)) {
//                    getLocationList(lastSearch, false);
//                    lastSearch = searcTarget;
//                }
//
//                String input = getInput(editTextLocationInput);
                if (StringUtils.computStrlen(searcTarget) >= 3 && !lastSearch.equals(searcTarget)) {
                    getLocationList(searcTarget, false);
                }

            }
        });

        guildAdapter.getSearchView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input = getInput(editTextLocationInput);
                if (StringUtils.computStrlen(input) >= 3) {
                    getLocationList(input, true);
                }
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (0 == position) {
                    viewHeader.setBackgroundResource(R.drawable.shape_block_white);
                    textViewTitle.setText(R.string.regist_guid_4step_title);
                } else if (1 == position) {
                    viewHeader.setBackgroundColor(Color.TRANSPARENT);
                    textViewTitle.setText(R.string.regist_guid_4step_city_select);
                } else {
                    viewHeader.setBackgroundResource(R.drawable.shape_block_white);
                    textViewTitle.setText(R.string.regist_guid_4step_title);
                }
            }
        });

        //=======add evnet listener for page 3


        guildAdapter.getViewFirstNameP3().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buildSetting(1, guildAdapter.getTextViewFirstNameP3());
            }
        });

        guildAdapter.getViewLastNameP3().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buildSetting(2, guildAdapter.getTextViewLastNameP3());
            }
        });
        guildAdapter.getViewGenderP3().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showGenderSelect(guildAdapter.getTextViewGenderP3(), false);
            }
        });

        guildAdapter.getViewCountryCityP3().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Tool.startActivityForResult(Step4Activity.this, CitySelectActivity.class, SHORT_GO_CITY);
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
            }
        });

        guildAdapter.getViewMobile().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditDialog("Mobile", guildAdapter.getTextViewMobile(), Integer.MAX_VALUE, false, InputType.TYPE_CLASS_PHONE, false);
            }
        });

        guildAdapter.getButtonGo().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String firstName = getInput(guildAdapter.getTextViewFirstNameP3());
                String lastName = getInput(guildAdapter.getTextViewLastNameP3());
                String gender = getInput(guildAdapter.getTextViewGenderP3());
                AVObject cityObj = tempUserObj.getAVObject("city");

                AVUser aVUser = AVUser.getCurrentUser();


                aVUser.put("first_name", firstName);
                aVUser.put("last_name", lastName);
                aVUser.put("gender", gender);
                aVUser.put("full_name", firstName + " " + lastName);

                if (StringUtils.isEmpty(cityObj.getString("geoId"))) {
                    aVUser.put("country", cityObj.get("country"));
                    aVUser.put("city", "");
                } else {
                    aVUser.put("city", cityObj);
                    aVUser.put("country", "");
                }

                aVUser.put("mobile", getInput(guildAdapter.getTextViewMobile()));


                if (!StringUtils.isEmpty(fileUrlHeader)) {
                    File headerFile = new File(fileUrlHeader);
                    if (headerFile.exists()) {
                        try {

                            final AVFile aVFilenew = AVFile.withAbsoluteLocalPath(headerFile.getName(), headerFile.getAbsolutePath());
                            aVFilenew.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(AVException e) {
                                    if (null == e) {
                                        AVUser.getCurrentUser().put("avatar", aVFilenew);
                                    } else {
                                        Tool.showMessageDialog("Avatar set fail,please retry!", Step4Activity.this);
                                    }
                                }
                            });
                        } catch (Exception e) {
                            LogUtil.e(App.tag, "header set error!");
                            Tool.showMessageDialog("Avatar set fail,please retry!", Step4Activity.this);
                        }

                    }
                }
                updateAVUser(aVUser);

            }
        });
        //====================================
//        buildSetting(current_seting);

        if (null == addPhotoDialogInCatchList) {
            showPictureShow();
        }

    }

    AddPhotoDialogInCatchList addPhotoDialogInCatchList = null;

    private void showPictureShow() {

        addPhotoDialogInCatchList = new AddPhotoDialogInCatchList(this);

        addPhotoDialogInCatchList.hideCancelButton();

        addPhotoDialogInCatchList.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss(Object o) {

                LogUtil.i(App.tag, "addPhotoDialogInCatchList dismiss");

            }
        });

        addPhotoDialogInCatchList.setdTitle("Choose a profile photo");
        addPhotoDialogInCatchList.setOnAddCatchListener(new AddPhotoDialogInCatchList.OnAddCatchListener() {
            @Override
            public void onTakePhotoClick() {

                onTakePhtoClickLocal();
            }

            @Override
            public void onChoosePhotoClick() {
                onChosePhtoClickLocal();
            }

            @Override
            public void onSkipClick() {
                buildSetting(current_seting);
            }
        });

        addPhotoDialogInCatchList.show();
    }


    public void takePictureFromCamera(int w, int h) {
        if (null != addPhotoDialogInCatchList && addPhotoDialogInCatchList.isShowing()) {
            addPhotoDialogInCatchList.dismiss();
        }

        startTakePicture(w, h);
    }

    public void selectPictureFromAblum(int w, int h) {
        startSelectPicture(w, h);
    }


    public void onTakePhtoClickLocal() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                takePictureFromCamera(150, 150);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, SHORT_REQUEST_READ_EXTEAL);
            }
        } else {
            takePictureFromCamera(150, 150);
        }
    }

    public void onChosePhtoClickLocal() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                selectPictureFromAblum(150, 150);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, SHORT_REQUEST_READ_EXTEAL);
            }
        } else {
            selectPictureFromAblum(150, 150);
        }
    }

    private void updateAVUser(final AVUser aVUser) {

        aVUser.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(AVException e) {
                if (null == e) {
                    Tool.startActivityForResult(Step4Activity.this, MainActivity.class, SHORT_LOG_OUT);
                } else {
                    showNotifyTextIn5Seconds(e.getLocalizedMessage());
                }
            }
        });

//        aVUser.saveInBackground(new SaveCallback() {
//            @Override
//            public void done(AVException e) {
//                if (null == e) {
//                    Tool.startActivity(Step4Activity.this, MainActivity.class);
//                    finish();
//                } else {
//                    showNotifyTextIn5Seconds(e.getLocalizedMessage());
//                }
//            }
//        });


//
//        reSetTask();
//        baseTask = new BaseTask(new NetCallBack() {
//            @Override
//            public void onPreCall() {
//                showProgressDialogWithTask(baseTask);
//            }
//
//            @Override
//            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask bTask) {
//
//                NetResult netResult = new NetResult();
//
//                try {
//                    aVUser.save();
//                } catch (AVException e) {
//                    e.printStackTrace();
//                    netResult.setException(e);
//                }
//
//                return netResult;
//            }
//
//            @Override
//            public void onFinish(NetResult result) {
//                hideProgressDialogWithTask();
//                if (null == result.getException()) {
//                    Tool.startActivity(Step4Activity.this, MainActivity.class);
//                    finish();
//                } else {
//                    showNotifyTextIn5Seconds(((AVException) result.getException()).getMessage());
//                }
//
//            }
//        });
//        baseTask.execute(new HashMap<String, String>());

    }

    @Override
    public void onBackPressed() {

    }

    private void getLocationList(final String input, final boolean showDialog) {

        reSetTask();
        baseTask = new BaseTask(new NetCallBack() {


            @Override
            public void onPreCall(BaseTask baseTask) {
                if (showDialog) {
                    showNewDialogWithNewTask(baseTask);
                }

//                showNewDialogWithNewTask(baseTask);
            }

            @Override
            public NetResult onDoInBack(HashMap<String, String> paramMap, BaseTask bTask) {

                NetResult netResult = new NetResult();

                try {


                    AVQuery<AVObject> queryCountry = AVQuery.getQuery("City");
//                    queryCountry.whereContains("country", input.toUpperCase());

                    queryCountry.whereMatches("name", ".*" + input + ".*", "i");

                    AVQuery<AVObject> queryCity = AVQuery.getQuery("City");
//                    queryCity.whereContains("name", input);


//                    ".*" + inputText + ".*",
                    queryCity.whereMatches("name", ".*" + input + ".*", "i");


//                    AVQuery<AVObject> queryCountry = AVQuery.getQuery("City");
//                    queryCountry.whereContains("country", input.toUpperCase());
//
//                    AVQuery<AVObject> queryCity = AVQuery.getQuery("City");
//                    queryCity.whereContains("name", input);


                    List<AVQuery<AVObject>> queries = new ArrayList<AVQuery<AVObject>>();
                    queries.add(queryCountry);
                    queries.add(queryCity);

                    AVQuery<AVObject> mainQuery = AVQuery.or(queries);

                    List<AVObject> list = mainQuery.find();
                    netResult.setTag(list);

                } catch (AVException e) {
                    netResult.setException(e);

                }

                return netResult;
            }

            @Override
            public void onFinish(NetResult result, BaseTask baseTask) {
                if (showDialog) {
                    hideNewDialogWithTask(baseTask);
                }
//                hideNewDialogWithTask(baseTask);

                if (null == result.getException()) {
                    List<AVObject> aVObjectCitys = (List<AVObject>) result.getTag();
                    buildCountryAdapter(input, aVObjectCitys);
                } else {
                    showNotifyTextIn5Seconds(((AVException) result.getException()).getMessage());
                }
            }
        });


        baseTask.execute(new HashMap<String, String>());
    }

    private void buildCountryAdapter(String input, List<AVObject> aVObjectCitys) {

        ArrayList<AVObject> totalObjects = new ArrayList<AVObject>();
        if (ListUtiles.isEmpty(aVObjectCitys)) {
//            showNotifyTextIn5Seconds("No result!");
            showToastWithTime("No result!", 2);
        } else {
            AVObject aVObjectCity = AVObject.create("City");
            aVObjectCity.put("name", "City");
            totalObjects.add(aVObjectCity);
        }

        //===========================
        totalObjects.addAll(aVObjectCitys);
        //===========================


        ArrayList<String> countrys = AssertTool.readLinesFromAssertsFiles(this, "country");
        List<AVObject> countryObjs = new ArrayList<AVObject>();

        for (int i = 0, isize = countrys.size(); i < isize; i++) {
            String[] country = countrys.get(i).split(",");
            String code = country[0].trim();
            String name = country[1].trim();
            if (name.contains(input)) {
                AVObject aVObject = AVObject.create("City");
                aVObject.put("country", code);
                aVObject.put("name", name);
                countryObjs.add(aVObject);
            }
        }

        if (!ListUtiles.isEmpty(countryObjs)) {
            AVObject countryObjDivder = AVObject.create("City");
            countryObjDivder.put("name", "Country");
            totalObjects.add(countryObjDivder);
            //===========
            totalObjects.addAll(countryObjs);
        }

//        LogUtil.i(App.tag, "list size:" + aVObjectCitys.size());
        LocationAdapter locationAdapter = new LocationAdapter(Step4Activity.this, totalObjects);
        listViewLocation.setAdapter(locationAdapter);
        listViewLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                AVObject plocation = (AVObject) adapterView.getItemAtPosition(i);
                if (null != plocation && (!"City".equals(plocation.get("name")) && (!"Country".equals(plocation.get("name"))))) {

                    tempUserObj.put("city", plocation);

//                    tempUserObj.pinInBackground();

                    guildAdapter.getTextViewFirstNameP3().setText(tempUserObj.getString("first_name"));
                    guildAdapter.getTextViewLastNameP3().setText(tempUserObj.getString("last_name"));
                    guildAdapter.getTextViewGenderP3().setText(tempUserObj.getString("gender"));
                    AVObject cityObj = tempUserObj.getAVObject("city");
                    guildAdapter.getTextViewCuntryCityP3().setText(cityObj.getString("name") + "," + cityObj.getString("country"));
                    viewPager.setCurrentItem(2);

                    showEditMobileDialog("Mobile", guildAdapter.getTextViewMobile(), Integer.MAX_VALUE, false, InputType.TYPE_CLASS_PHONE);

                }
            }
        });
    }


    private void buildSetting(int current) {

        switch (current) {
            case 1: {
                showEditDialog(getString(R.string.regist_guid_4step_edit_fist_name), guildAdapter.getTextViewFirstName(), 1, true, InputType.TYPE_CLASS_TEXT, true);
            }
            break;
            case 2: {
                showEditDialog(getString(R.string.regist_guid_4step_edit_last_name), guildAdapter.getTextViewLastName(), 2, true, InputType.TYPE_CLASS_TEXT, true);
            }
            break;
        }
    }

    private void buildSetting(int current, TextView textView) {

        switch (current) {
            case 1: {
                showEditDialog(getString(R.string.regist_guid_4step_edit_fist_name), textView, 1, false, InputType.TYPE_CLASS_TEXT, true);
            }
            break;
            case 2: {
                showEditDialog(getString(R.string.regist_guid_4step_edit_last_name), textView, 2, false, InputType.TYPE_CLASS_TEXT, true);
            }
            break;
        }
    }


    private void showEditDialog(final String title, final TextView textView, final int current, final boolean autoNext, int inputtype, final boolean showKeyBorad) {


        View dialogView = View.inflate(this, R.layout.dialog_edit, null);
        final TextView textViewLabel = (TextView) dialogView.findViewById(R.id.textViewLabel);
        final EditText editContent = (EditText) dialogView.findViewById(R.id.editContent);

//        if (showKeyBorad) {
//            showSoftWareKeyBorad();
//        }

        editContent.setInputType(inputtype | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        textViewLabel.setText(title);
        editContent.setText(textView.getText().toString());
        editContent.setSelection(getInput(editContent).trim().length());

        AlertDialog.Builder builder = new AlertDialog.Builder(Step4Activity.this);
        builder.setView(dialogView);
        final AlertDialog dlg = builder.create();

        dlg.setCancelable(false);

        dlg.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        dlg.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        Button buttonReset = (Button) dialogView.findViewById(R.id.buttonReset);
        Button buttonConfirm = (Button) dialogView.findViewById(R.id.buttonConfirm);

        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editContent.setText("");
            }
        });

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (StringUtils.isEmpty(getInput(editContent))) {

                    editContent.setError("content is empty");

                    return;
                }


                dlg.dismiss();

                hideSoftKeyborad(null, editContent);

                String content = getInput(editContent);
                if (!StringUtils.isEmpty(content)) {

                    textView.setText(content);
                    dlg.dismiss();

                    if (current == 1) {
                        tempUserObj.put("first_name", content);
                        guildAdapter.showFirstName();
                        if (autoNext) {
                            buildSetting(2);
                        }

                    }

                    if (current == 2) {
                        tempUserObj.put("last_name", content);
                        guildAdapter.showLastName();

                        if (autoNext) {
//                            closeSoftKeyBorad(null, null);
                            showGenderSelect(guildAdapter.getTextViewGender(), true);
                        }
                    }
//                    tempUserObj.pinInBackground();
                } else {
                    editContent.startAnimation(AnimationUtils.loadAnimation(Step4Activity.this, R.anim.shake));
                }
            }
        });

        dlg.show();
    }


    private void showEditMobileDialog(final String title, final TextView textView, final int current, final boolean autoNext, int inputtype) {


        View dialogView = View.inflate(this, R.layout.dialog_edit, null);
        final TextView textViewLabel = (TextView) dialogView.findViewById(R.id.textViewLabel);
        final EditText editContent = (EditText) dialogView.findViewById(R.id.editContent);

        editContent.setInputType(inputtype | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        textViewLabel.setText(title);
        editContent.setText(textView.getText().toString());

//
//        final Dialog dlg = new Dialog(Step4Activity.this);
//        dlg.setContentView(dialogView);

        AlertDialog.Builder builder = new AlertDialog.Builder(Step4Activity.this);
        builder.setView(dialogView);

        final AlertDialog dlg = builder.create();

        editContent.setSelection(getInput(editContent).trim().length());


//        dlg.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        dlg.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        Button buttonReset = (Button) dialogView.findViewById(R.id.buttonReset);
        buttonReset.setText("Skip");
        Button buttonConfirm = (Button) dialogView.findViewById(R.id.buttonConfirm);

        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlg.dismiss();
                editContent.setText("");

            }
        });

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dlg.dismiss();

                closeSoftWareKeyBoradInBase();

                String content = getInput(editContent);
                if (!StringUtils.isEmpty(content)) {
                    textView.setText(content);
                    dlg.dismiss();

//                    tempUserObj.pinInBackground();
                } else {
                    editContent.startAnimation(AnimationUtils.loadAnimation(Step4Activity.this, R.anim.shake));
                }
            }
        });

        dlg.show();
    }


    private void showGenderSelect(final TextView textView, final boolean autoNext) {

        final String[] gender = {
                getString(R.string.regist_gender_female),
                getString(R.string.regist_gender_male)
        };

        new AlertDialog.Builder(this).setTitle(getString(R.string.regist_guid_gender_slect)).
                setIcon(android.R.drawable.ic_dialog_info).
                setSingleChoiceItems(
                        gender, 0,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                String genderSelect = gender[which];
                                LogUtil.i(App.tag, "gender:" + genderSelect);
                                tempUserObj.put("gender", genderSelect);
//                                tempUserObj.pinInBackground();
                                textView.setText(genderSelect);
                                guildAdapter.showGender();

                                if (autoNext) {
                                    viewPager.setCurrentItem(1);
                                }

                                closeSoftWareKeyBoradInBase();
                            }
                        }).setCancelable(false).show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SHORT_LOG_OUT) {
            setResult(resultCode);
            finish();
            return;
        }

        if (requestCode == SHORT_GO_CITY && resultCode == Activity.RESULT_OK) {

            AVObject aVObjectTemp = (AVObject) App.getApp().getTempObject("city");//putTemPObject("city",aVObject);

            if (null != aVObjectTemp) {
                if (StringUtils.isEmpty(aVObjectTemp.getString("geoId"))) {
                    guildAdapter.getTextViewCuntryCityP3().setText(aVObjectTemp.getString("name"));
                } else {
                    guildAdapter.getTextViewCuntryCityP3().setText(aVObjectTemp.getString("name") + " " + aVObjectTemp.getString("country"));
                }
                tempUserObj.put("city", aVObjectTemp);
            }
        }


        String mFileName;
        if (requestCode == SELECT_PIC && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getDataColumn(getApplicationContext(), data.getData(), null, null);
                PhotoLibUtils.copyFile(mFileName, sharedpreferencesUtil.getImageTempNameString2());
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
                cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);
            }
        }
        if (requestCode == SELECT_PIC_KITKAT && resultCode == RESULT_OK) {
            if (resultCode == RESULT_OK && data != null) {
                mFileName = PhotoLibUtils.getPath(getApplicationContext(), data.getData());
                String newPath = sharedpreferencesUtil.getImageTempNameString2();
                PhotoLibUtils.copyFile(mFileName, newPath);
                Uri uri = sharedpreferencesUtil.getImageTempNameUri();
                Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
                cropImageUri(uri, outoutUri, outputX, outputY, CROP_BIG_PICTURE);

            }
        }
        if (requestCode == TAKE_BIG_PICTURE && resultCode == RESULT_OK) {

            Uri outoutUri = sharedpreferencesUtil.getImageTempNameUriCroped();
            cropImageUri(sharedpreferencesUtil.getImageTempNameUri(), outoutUri, outputX, outputY, CROP_BIG_PICTURE);

        }

        if (requestCode == CROP_BIG_PICTURE && resultCode == Activity.RESULT_OK) {

            if (sharedpreferencesUtil.getImageTempNameUri() != null) {
                Uri result = sharedpreferencesUtil.getImageTempNameUriCroped();
                App.firstRun();
                if (!StringUtils.isEmpty(result.getPath())) {
                    guildAdapter.addImageViewHeaderFile(result.getPath());
                    fileUrlHeader = result.getPath();
                }

                buildSetting(current_seting);
            }
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        if (null != addPhotoDialogInCatchList && addPhotoDialogInCatchList.isShowing()) {
            addPhotoDialogInCatchList.dismiss();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == SHORT_REQUEST_READ_EXTEAL) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //yes allow take picture or
                if (type_request_picture == TYPE_REQEST_PICTURE_SELECT) {
                    startSelectPicture(150, 150);
                } else {
                    startTakePicture(150, 150);
                }
            } else {
                showNotifyTextIn5Seconds("Read external storage access denied!");
            }
        }

    }
}

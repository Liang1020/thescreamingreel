package com.cretve.screamingreel.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.common.util.Tool;
import com.thescreamingreel.androidapp.R;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                Tool.startActivity(SplashActivity.this, WelcomeActivity.class);

                timer.cancel();
                cancel();
                finish();
            }
        }, 1000 *1);
    }
}

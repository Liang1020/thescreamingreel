package com.cretve.screamingreel.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.common.util.LogUtil;
import com.cretve.screamingreel.App;
import com.thescreamingreel.androidapp.R;

public class LocationActivity extends AppCompatActivity implements LocationListener {


    private static final String TAG = "LocationSvc";
    private LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
    }


    @Override
    protected void onStart() {
        super.onStart();

        if (locationManager.getProvider(LocationManager.NETWORK_PROVIDER) != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        }

//            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,this);
        else if (locationManager.getProvider(LocationManager.GPS_PROVIDER) != null) {

        }
//            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,this);
        else Toast.makeText(this, "无法定位", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        LogUtil.i(App.tag,"locatoin chagned:"+location.toString());


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}

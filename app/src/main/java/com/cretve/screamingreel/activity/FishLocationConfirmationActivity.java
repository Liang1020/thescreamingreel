package com.cretve.screamingreel.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.DeleteCallback;
import com.avos.avoscloud.SaveCallback;
import com.common.util.LogUtil;
import com.common.util.NetTool;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.thescreamingreel.androidapp.R;

import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class FishLocationConfirmationActivity extends BaseScreamingReelActivity implements OnMapReadyCallback {

    @InjectView(R.id.linearLayoutConfirm)
    LinearLayout linearLayoutConfirm;

    @InjectView(R.id.linearLayoutPost)
    LinearLayout linearLayoutPost;

    @InjectView(R.id.linearLayoutCorrect)
    LinearLayout linearLayoutCorrect;

    @InjectView(R.id.textViewShare)
    TextView textViewShare;

    @InjectView(R.id.textViewQuestion)
    TextView textViewQuestion;

    @InjectView(R.id.textViewUndetermined)
    TextView textViewUndetermined;

    @InjectView(R.id.switchButtonShared)
    SwitchCompat switchButtonShared;

    private GoogleMap googleMap;
    private double mLatitude = 0.0, mLongitude = 0.0;
    private String title;

    private AVObject aVObjectCatch;
    private boolean isHideLocation = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fish_location_confirmation);
        ButterKnife.inject(this);
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            String location = extras.getString(App.IMAGE_LOCATION);
            mLatitude = Double.valueOf(location.substring(0, location.indexOf(",")));
            mLongitude = Double.valueOf(location.substring(location.indexOf(",") + 1));
            title = extras.getString(App.IMAGE_AREA);

            LogUtil.i(App.tag, "LAT_LON:" + mLatitude + " " + mLongitude);
        }

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        init();

    }

    private void init(){
        if (App.getApp().hasTempKey("NewCatch")) {
            aVObjectCatch = (AVObject) App.getApp().getTempObject("NewCatch");
            isHideLocation = aVObjectCatch.getBoolean("hide_location");
        }

        if (mLatitude == 0.0 && mLongitude == 0.0) {
            isHideLocation = true;
            textViewQuestion.setVisibility(View.GONE);
            linearLayoutCorrect.setVisibility(View.GONE);

            textViewUndetermined.setVisibility(View.VISIBLE);
            linearLayoutPost.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setMapToolbarEnabled(false);
        this.googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        this.googleMap.getUiSettings().setCompassEnabled(true);

        if (mLatitude != 0.0 && mLongitude != 0.0) {
            LatLng latLng1 = new LatLng(mLatitude, mLongitude);
            MarkerOptions location = new MarkerOptions().
                    icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_fish_location))
                    .position(latLng1)
                    .title(title)
                    .draggable(true);

            Marker marker = googleMap.addMarker(location);
            marker.setTitle(title);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng1, googleMap.getMaxZoomLevel() / 2));
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            googleMap.setMyLocationEnabled(true);
        }

    }

    @OnClick(R.id.viewBack)
    public void onViewBack() {
        finish();
    }

    @OnClick(R.id.textViewNo)
    public void onNoClick(){
        isHideLocation = true;
        googleMap.clear();
        textViewQuestion.setVisibility(View.GONE);
        linearLayoutCorrect.setVisibility(View.GONE);

        textViewUndetermined.setVisibility(View.VISIBLE);
        linearLayoutPost.setVisibility(View.VISIBLE);

    }

    @OnClick(R.id.textViewYes)
    public void onYesClick(){

        switchButtonShared.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    isHideLocation = false;
                } else{
                    isHideLocation = true;
                }

            }
        });

        linearLayoutConfirm.setVisibility(View.VISIBLE);
        textViewShare.setVisibility(View.VISIBLE);

        textViewQuestion.setVisibility(View.GONE);
        linearLayoutCorrect.setVisibility(View.GONE);
        textViewUndetermined.setVisibility(View.GONE);

        linearLayoutPost.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.textViewPost)
    public void onPostClick(){

        if (!NetTool.isNetworkAvailable(this)) {
            Tool.showMessageDialog(getResources().getString(R.string.error_net), this);
            return;
        }

        if (null == aVObjectCatch) {
            return;
        }

        aVObjectCatch.put("location", new AVGeoPoint(mLatitude, mLongitude));

//        if (null != title) {
//            aVObjectCatch.put("area", title);
//        }

        aVObjectCatch.put("hide_location", isHideLocation);

        saveAVObject(aVObjectCatch, true, true, new SaveCallback() {
            @Override
            public void done(AVException e) {
                if (null == e) {
                    setResult(RESULT_OK);
                    finish();
                } else {
                    hideProgressDialog();
                    showLeanCloudError(e);
                    return;
                }
            }
        });
    }

    @OnClick(R.id.textViewCancel)
    public void onCancelClick() {

        if (null != aVObjectCatch) {
            deleteAllCatchObject(aVObjectCatch, true, new DeleteCallback() {
                @Override
                public void done(AVException e) {
                    if (null == e) {
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        LogUtil.e(App.tag, e.getLocalizedMessage());
                    }
                }
            });
        }else{
            finish();
        }
    }
}

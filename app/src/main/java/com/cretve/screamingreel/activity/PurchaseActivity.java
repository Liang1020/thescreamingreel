package com.cretve.screamingreel.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.thescreamingreel.androidapp.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class PurchaseActivity extends BaseScreamingReelActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
        this.setFinishOnTouchOutside(false);
        ButterKnife.inject(this);
    }

    @OnClick(R.id.textViewSubscribeMonthly)
    public void onSubscriptionMonthClick(){
        Intent intent = new Intent();
        intent.putExtra(App.SUBSCRIPTION_TYPE, "month");
        Tool.startActivity(this, SubscriptionTermActivity.class, intent);
        finish();
    }

    @OnClick(R.id.textViewSubscribeYearly)
    public void onSubscriptionYearClick(){
        Intent intent = new Intent();
        intent.putExtra(App.SUBSCRIPTION_TYPE, "year");
        Tool.startActivity(this, SubscriptionTermActivity.class, intent);
        finish();
    }

    @OnClick(R.id.textViewCancel)
    public void onCancelClick(){
        finish();
    }
}

package com.cretve.screamingreel.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.FindCallback;
import com.common.util.LogUtil;
import com.common.util.StringUtils;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.adapter.UserAdapter;
import com.thescreamingreel.androidapp.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class FindAnglerActivity extends BaseScreamingReelActivity {


    @InjectView(R.id.editTextUserInput)
    EditText editTextUserInput;

    @InjectView(R.id.listViewAngler)
    ListView listViewAngler;

    AVQuery<AVUser> aVQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_angler);
        ButterKnife.inject(this);
    }


    @Override
    public void onBackPressed() {
        onBack();
    }


    public void onBack() {
        finish();

    }

    @OnClick(R.id.viewBack)
    public void onBackClick() {
        onBack();
    }


    @OnTextChanged(R.id.editTextUserInput)
    public void onTextChanged() {
        String input = getInput(editTextUserInput);
        LogUtil.i(App.tag, "input:" + input);
        if (null != aVQuery) {
            aVQuery.cancel();
        }
        if (!StringUtils.isEmpty(input)) {

//            String reg = ".*" + input + ".*";


            String reg = ".*" + input + ".*";

            AVQuery<AVUser> query1 = AVUser.getQuery();
            query1.whereMatches("full_name", reg, "i");

//            AVQuery<AVUser> query2 = AVUser.getQuery();
//            query2.whereMatches("last_name", reg, "i");

            List<AVQuery<AVUser>> queries = new ArrayList<AVQuery<AVUser>>();

            queries.add(query1);
//            queries.add(query2);

            aVQuery = AVQuery.or(queries);

            aVQuery.orderByAscending("first_name");

            aVQuery.findInBackground(new FindCallback<AVUser>() {
                @Override
                public void done(List<AVUser> objects, AVException e) {
                    if (null == e) {
                        buildAdapter(objects);
                    } else {
                        showLeanCloudError(e);
//                        showNotifyTextIn5Seconds(e.getLocalizedMessage());
                    }
                }
            });
        }


    }


    private void buildAdapter(final List<AVUser> aVUsers) {

        UserAdapter userAdapter = new UserAdapter(this, aVUsers);
        listViewAngler.setAdapter(userAdapter);
        listViewAngler.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                AVUser aVUser = (AVUser) parent.getItemAtPosition(position);
                App.getApp().putTemPObject("user", aVUser);
                Tool.startActivity(FindAnglerActivity.this, ProfileActivity.class);
            }
        });

    }

}

package com.cretve.screamingreel.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.TextView;

import com.avos.avoscloud.AVUser;
import com.common.util.LogUtil;
import com.common.util.Tool;
import com.cretve.screamingreel.App;
import com.cretve.screamingreel.BaseScreamingReelActivity;
import com.cretve.screamingreel.util.IabBroadcastReceiver;
import com.cretve.screamingreel.util.IabHelper;
import com.cretve.screamingreel.util.IabResult;
import com.cretve.screamingreel.util.Inventory;
import com.cretve.screamingreel.util.Purchase;
import com.cretve.screamingreel.util.SkuDetails;
import com.thescreamingreel.androidapp.R;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class SubscriptionTermActivity extends BaseScreamingReelActivity implements IabBroadcastReceiver.IabBroadcastListener {

    @InjectView(R.id.textViewSubTerm)
    TextView textViewSubTerm;

    public static final String action = "iab.broadcast.action";
    static final String TAG = "IAB";

    IabHelper mHelper;
    IabBroadcastReceiver mBroadcastReceiver;

    boolean mAutoRenewEnabled = false;
    boolean mSubscribed = false;

    static final String SKU_PRODUCT_MONTHLY = "com.thescreamingreel.androidapp.inappproduct";
    static final String SKU_PRODUCT_YEARLY = "com.thescreamingreel.androidapp.year";

    static final int RC_REQUEST = 10001;
    static final String ITEM = "ITEM_MAP_RINGTONE";

    String mProductSku = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_term);
        this.setFinishOnTouchOutside(false);
        ButterKnife.inject(this);

        Bundle extras = getIntent().getExtras();
        mProductSku = extras.getString(App.SUBSCRIPTION_TYPE);

        if (mProductSku.equals("month")) {
            textViewSubTerm.setText(getResources().getString(R.string.subscription_first_part_month));
        } else if (mProductSku.equals("year")) {
            textViewSubTerm.setText(getResources().getString(R.string.subscription_first_part_year));
        }

        String base64EncodedPublicKey = getResources().getString(R.string.base64_encoded_public_key);

        // Create the helper, passing it our context and the public key to verify signatures with
        LogUtil.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper(this, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        LogUtil.i(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                LogUtil.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    complain("Problem setting up in-app billing: " + result);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // Important: Dynamically register for broadcast messages about updated purchases.
                // We register the receiver here instead of as a <receiver> in the Manifest
                // because we always call getPurchases() at startup, so therefore we can ignore
                // any broadcasts sent while the app isn't running.
                // Note: registering this listener in an Activity is a bad idea, but is done here
                // because this is a SAMPLE. Regardless, the receiver must be registered after
                // IabHelper is setup, but before first call to getPurchases().
                mBroadcastReceiver = new IabBroadcastReceiver(SubscriptionTermActivity.this);
                IntentFilter broadcastFilter = new IntentFilter(IabBroadcastReceiver.ACTION);
                registerReceiver(mBroadcastReceiver, broadcastFilter);

                ArrayList<String> skuList = new ArrayList<String>();
                skuList.add(SKU_PRODUCT_MONTHLY);
                skuList.add(SKU_PRODUCT_YEARLY);

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                LogUtil.d(TAG, "Setup successful. Querying inventory.");
                try {
                    mHelper.queryInventoryAsync(true, null, skuList,
                            mGotInventoryListener);
                    //mHelper.queryInventoryAsync(mGotInventoryListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    complain("Error querying inventory. Another async operation in progress.");
                }
            }
        });

    }

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            LogUtil.d(TAG, "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                complain("Failed to query inventory: " + result);
                return;
            }

            LogUtil.d(TAG, "Query inventory was successful.");

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */


            Purchase purchaseMonth = inventory.getPurchase(SKU_PRODUCT_MONTHLY);
            SkuDetails skuDetails = inventory.getSkuDetails(SKU_PRODUCT_MONTHLY);

            Purchase purchaseYear = inventory.getPurchase(SKU_PRODUCT_YEARLY);
            SkuDetails skuDetailsYear = inventory.getSkuDetails(SKU_PRODUCT_YEARLY);

            if (purchaseMonth != null && purchaseMonth.isAutoRenewing()) {
                mAutoRenewEnabled = true;
            } else if (purchaseYear != null && purchaseYear.isAutoRenewing()) {
                mAutoRenewEnabled = true;
            } else {
                mAutoRenewEnabled = false;
            }

            LogUtil.d(TAG, "Initial inventory query finished; enabling main UI.");
        }
    };

    /**
     * Verifies the developer payload of a purchase.
     */
    boolean verifyDeveloperPayload(Purchase p) {
        String responsePayload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        String computedPayload = AVUser.getCurrentUser().getUsername() + ITEM;

        return responsePayload != null && responsePayload.equals(computedPayload);

    }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            LogUtil.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            //if we were disposed of in the meantime, quit.
            if (mHelper == null) {
                finish();
                return;
            }

            if (result.isFailure()) {
                complain("Error purchasing: " + result);
                finish();
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                complain("Error purchasing. Authenticity verification failed.");
                finish();
                return;
            }

            LogUtil.d(TAG, "Purchase successful.");//购买成功

            Object expiredObj = AVUser.getCurrentUser().get(App.expiredKey);

            if(expiredObj != null){
                AVUser.getCurrentUser().put(App.expiredKey, false);
                AVUser.getCurrentUser().setFetchWhenSave(true);
                AVUser.getCurrentUser().saveEventually();
            }

            if (purchase.getSku().equals(SKU_PRODUCT_MONTHLY)) {

                LogUtil.d(TAG, "Subscription purchased.");
                Tool.showMessageDialog("Thank you for subscribing to The Screaming Reel!", SubscriptionTermActivity.this);
                mSubscribed = true;
                mAutoRenewEnabled = purchase.isAutoRenewing();

                Intent intent = new Intent(action);
                intent.putExtra("isCompleted", mSubscribed);
                sendBroadcast(intent);
                finish();

            }

            if (purchase.getSku().equals(SKU_PRODUCT_YEARLY)) {

                LogUtil.d(TAG, "Subscription purchased.");
                Tool.showMessageDialog("Thank you for subscribing to The Screaming Reel!", SubscriptionTermActivity.this);
                mSubscribed = true;
                mAutoRenewEnabled = purchase.isAutoRenewing();

                Intent intent = new Intent(action);
                intent.putExtra("isCompleted", mSubscribed);
                sendBroadcast(intent);
                finish();

            }

        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {

            LogUtil.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                LogUtil.d(TAG, "Consumption successful. Provisioning.");
//                mTank = mTank == TANK_MAX ? TANK_MAX : mTank + 1;
//                saveData();
//                alert("You filled 1/4 tank. Your tank is now " + String.valueOf(mTank) + "/4 full!");
            } else {
                complain("Error while consuming: " + result);
            }
            //updateUi();
            LogUtil.d(TAG, "End consumption flow.");
        }
    };


    void complain(String message) {
        LogUtil.e(TAG, "**** IAB Error: " + message);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        LogUtil.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            LogUtil.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // very important:
        if (mBroadcastReceiver != null) {
            unregisterReceiver(mBroadcastReceiver);
        }

        // very important:
        LogUtil.d(TAG, "Destroying helper.");
        if (mHelper != null) {
            mHelper.disposeWhenFinished();
            mHelper = null;
        }
    }

    @OnClick(R.id.textViewCancel)
    public void onCancleClick() {
        finish();
    }

    @OnClick(R.id.textViewAgree)
    public void onAgreeClick() {
        if (!mHelper.subscriptionsSupported()) {
            complain("Subscriptions not supported on your device yet. Sorry!");
            return;
        }

        if (mSubscribed) {
            complain("No need! You're subscribed to The Screaming Reel. Isn't that awesome?");
            return;
        }

        LogUtil.d(TAG, "Launching purchase flow for the screaming reel.");

        /* TODO: for security, generate your payload here for verification. See the comments on
         *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
         *        an empty string, but on a production app you should carefully generate this. */
        String payload = AVUser.getCurrentUser().getUsername() + ITEM;

        LogUtil.d(TAG, "Current User:"+ AVUser.getCurrentUser().getUsername());

        try {
            if (mProductSku.equals("month")) {
                mHelper.launchSubscriptionPurchaseFlow(this, SKU_PRODUCT_MONTHLY, RC_REQUEST,
                        mPurchaseFinishedListener, payload);
            } else if (mProductSku.equals("year")) {
                mHelper.launchSubscriptionPurchaseFlow(this, SKU_PRODUCT_YEARLY, RC_REQUEST,
                        mPurchaseFinishedListener, payload);
            }

        } catch (IabHelper.IabAsyncInProgressException e) {
            complain("Error launching purchase flow. Another async operation in progress.");
        }

    }


    @Override
    public void receivedBroadcast() {
        LogUtil.i(TAG, "Received broadcast notification. Querying inventory.");
        try {
            mHelper.queryInventoryAsync(mGotInventoryListener);
        } catch (IabHelper.IabAsyncInProgressException e) {
            complain("Error querying inventory. Another async operation in progress.");
        }
    }

}



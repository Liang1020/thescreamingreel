package com.cretve.screamingreel;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Location;
import android.os.Environment;
import android.support.multidex.MultiDexApplication;

import com.avos.avoscloud.AVGeoPoint;
import com.avos.avoscloud.AVOSCloud;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.common.cachu.lru.SimpleImgLoaderLru;
import com.common.util.LogUtil;
import com.facebook.FacebookSdk;
import com.squareup.picasso.Picasso;
import com.tencent.bugly.crashreport.CrashReport;
import com.utils.sec.SecTool;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;

/**
 * Created by wangzy on 15/10/27.
 */
public class App extends MultiDexApplication {


    public final static String K_CATCHLIKED_NOTIFICATIONKEY = "CatchLikedNotification";
    public final static String K_CATCHCOMMENTED_NOTIFICATION_KEY = "CatchCommentedNotification";
    public final static String K_USERFOLLOWED_NOTIFICATIONKEY = "UserFollowedNotification";
    public final static String K_CATCHDELETEDNO_TIFICATIONKEY = "CatchDeletedNotification";


    public static App app;
    public static String tag = "screamingreel";

    private Location location;
    private AVGeoPoint avGeoPoint;


    public static String KEY_HOST_SERVER_US = "keyUseUSServer";
    public static final boolean isFree = false;
    public static File CACHE_DIR_IMAGE;
    public static int RESULT_CODE_CAMERA = 1;
    public static String CACHE_DIR;
    public static String CACHE_DIR_PRODUCTS;
    public static String CACHE_DIR_EXCEL;
    public static String CACHE_DIR_PDF;
    public static final String DirFileName = "cretve_cache";
    private HashMap<String, Object> tempParamap;

    private boolean isMainActivityCreate = false;
    private boolean isMainActivityVisible = false;

    public LocationClient mLocationClient;
    public MyLocationListener mMyLocationListener;
    private BDLocation bdLocation;

    private WeakReference<BaseScreamingReelActivity> weakReferenceActivity;

    private boolean isLoginfb;

    public static final String IMAGE_LOCATION = "location";
    public static final String IMAGE_AREA = "area";
    public static final String SUBSCRIPTION_TYPE = "subscription";


    public static final String expiredKey = "expired";

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        this.tempParamap = new HashMap<String, Object>();


        //=========
//        String id="sWeRpvlFjH5hzkmJ62Y0K4iT-MdYXbMMI";
//        String key="wlV99B1serRPIFyLoelHazSw";
//
//        String enId=SecurityUtil.encode(id,2086496266);
//        LogUtil.i(App.tag,enId);
//        String enKey=SecurityUtil.encode(key,2086496266);
//        LogUtil.i(App.tag,enKey);
        //==========


//        refWatcher = LeakCanary.install(this);

        CrashReport.initCrashReport(this, "900012365", false);
        SimpleImgLoaderLru.from(this);//use App instance
        Picasso.with(this);

//        initParse();
        initLeanCloud();

        // Init facebook
        FacebookSdk.sdkInitialize(getApplicationContext());
        //bd location
        mLocationClient = new LocationClient(this.getApplicationContext());
        mMyLocationListener = new MyLocationListener();
        mLocationClient.registerLocationListener(mMyLocationListener);


    }

    Picasso picasso = null;

    public Picasso getPicasso() {
        if (null == picasso) {
            picasso = new Picasso.Builder(this)
                    .addRequestHandler(new PicassoVideoFrameRequestHandler())
                    .build();
        }
        return picasso;
    }


    public WeakReference<BaseScreamingReelActivity> getWeakReferenceActivity() {
        return weakReferenceActivity;
    }

    public void setWeakReferenceActivity(WeakReference<BaseScreamingReelActivity> weakReferenceActivity) {
        this.weakReferenceActivity = weakReferenceActivity;
    }

    public void putTemPObject(String key, Object object) {
        tempParamap.put(key, object);
    }

    public boolean hasTempKey(String key) {

        return tempParamap.containsKey(key);
    }

    public Object getTempObject(String key) {

        return tempParamap.remove(key);
    }

    public static void firstRun() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            CACHE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + DirFileName;
        } else {
            CACHE_DIR = Environment.getRootDirectory().getAbsolutePath() + "/" + DirFileName;
        }
        CACHE_DIR_PRODUCTS = CACHE_DIR + "/products";
        File cacheDir = new File(App.CACHE_DIR);
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        File cacheDirProducts = new File(App.CACHE_DIR_PRODUCTS);
        if (!cacheDirProducts.exists()) {
            cacheDirProducts.mkdirs();
        }
    }

    public static String returnCacheDir() {
        firstRun();
        return CACHE_DIR;
    }


    private void initLeanCloud() {


        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);

            Signature[] signs = packageInfo.signatures;
            Signature sign = signs[0];

            int hashcode = sign.hashCode();

            AVOSCloud.useAVCloudUS();

            LogUtil.e(App.tag, "hash:" + hashcode);

//            AVOSCloud.initialize(this,"sWeRpvlFjH5hzkmJ62Y0K4iT-MdYXbMMI","wlV99B1serRPIFyLoelHazSw");//normal mode


//            String id = SecurityUtil.decode(getResources().getString(R.string.lc_id), 2086496266);
//            String key = SecurityUtil.decode(getResources().getString(R.string.lc_key), 2086496266);

//            LogUtil.e(App.tag, "hash:" + hashcode+" id\n "+id+" key\n"+key);
//            AVOSCloud.initialize(this, id, key);//safe model


            String appid = SecTool.get1(2086496266);
            String key = SecTool.get2(2086496266);

            LogUtil.e(App.tag, "hash:" + hashcode);
            LogUtil.e(App.tag, "id:" + hashcode);
            LogUtil.e(App.tag, "key:" + hashcode);

//          LogUtil.i(App.tag,"hashcode:"+hashcode);
            AVOSCloud.initialize(this, appid, key);
//          AVOSCloud.initialize(this, "yPd0JXO7cb2SgJzQ4vqSKgM0-gzGzoHsz", "HWH5lLxNBVSmeLH2i36SREWK");//china server


//            AVOSCloud.initialize(this,"sWeRpvlFjH5hzkmJ62Y0K4iT-MdYXbMMI","wlV99B1serRPIFyLoelHazSw");

//            SharePersistent.saveBoolean(this,KEY_HOST_SERVER_US,true);


        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            LogUtil.e(App.tag, "init avos cloud error");
        }


//        AVOSCloud.initialize(this, "yPd0JXO7cb2SgJzQ4vqSKgM0-gzGzoHsz", "HWH5lLxNBVSmeLH2i36SREWK");//china server


//        AVOSCloud.useAVCloudUS();
//        AVOSCloud.initialize(this,"KqfaRpTflkfrJt975CKdb4Eo-MdYXbMMI","sKjSEFBJ7N7UHnjmQwYs8DGy");//Amercia
//        [AVOSCloud setApplicationId:@"KqfaRpTflkfrJt975CKdb4Eo-MdYXbMMI" clientKey:@"sKjSEFBJ7N7UHnjmQwYs8DGy"];

    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
        if (null != location) {
            setAvGeoPoint(new AVGeoPoint(location.getLatitude(), location.getLongitude()));
        }
    }


    public static App getApp() {

        return app;
    }


    public boolean isMainActivityCreate() {
        return isMainActivityCreate;
    }

    public void setMainActivityCreate(boolean mainActivityCreate) {
        isMainActivityCreate = mainActivityCreate;
    }


    public LocationClient getLocationClient() {
        return mLocationClient;
    }

    public MyLocationListener getMyLocationListener() {
        return mMyLocationListener;
    }


    private LocationClientOption.LocationMode tempMode = LocationClientOption.LocationMode.Hight_Accuracy;
    private String tempcoor = "gcj02";//google tenxun,等全球通用标准

    public void initLocation() {
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(tempMode);//可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
        option.setCoorType(tempcoor);//可选，默认gcj02，设置返回的定位结果坐标系，
        int span = 1000;
        option.setScanSpan(span);//可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
        option.setIsNeedAddress(true);//可选，设置是否需要地址信息，默认不需要
        option.setOpenGps(true);//可选，默认false,设置是否使用gps
        option.setLocationNotify(true);//可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
        option.setIgnoreKillProcess(true);//可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
        mLocationClient.setLocOption(option);
    }

    public void startLocation() {
        initLocation();
        mLocationClient.start();//定位SDK start之后会默认发起一次定位请求，开发者无须判断isstart并主动调用request
        mLocationClient.requestLocation();

        LogUtil.i(App.tag, "start location with bd");
    }

    public void stopLocation() {
        if (null != mLocationClient) {
            mLocationClient.stop();
        }
    }


    /**
     * location call back
     */
    public class MyLocationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            //Receive Location
            StringBuffer sb = new StringBuffer(256);
            sb.append("time : ");
            sb.append(location.getTime());
            sb.append("\nerror code : ");
            sb.append(location.getLocType());
            sb.append("\nlatitude : ");
            sb.append(location.getLatitude());
            sb.append("\nlontitude : ");
            sb.append(location.getLongitude());
            sb.append("\nradius : ");
            sb.append(location.getRadius());
            if (location.getLocType() == BDLocation.TypeGpsLocation) {// GPS定位结果
                sb.append("\nspeed : ");
                sb.append(location.getSpeed());// 单位：公里每小时
                sb.append("\nsatellite : ");
                sb.append(location.getSatelliteNumber());
                sb.append("\nheight : ");
                sb.append(location.getAltitude());// 单位：米
                sb.append("\ndirection : ");
                sb.append(location.getDirection());
                sb.append("\naddr : ");
                sb.append(location.getAddrStr());
                sb.append("\ndescribe : ");
                sb.append("gps定位成功");

                onLocationReceive(location);

                if (null != weakReferenceActivity && null != weakReferenceActivity.get()) {
                    weakReferenceActivity.get().onLocationReceive(location, null);
                }

            } else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {// 网络定位结果
                sb.append("\naddr : ");
                sb.append(location.getAddrStr());
                //运营商信息
                sb.append("\noperationers : ");
                sb.append(location.getOperators());
                sb.append("\ndescribe : ");
                sb.append("网络定位成功");

                onLocationReceive(location);

                if (null != weakReferenceActivity && null != weakReferenceActivity.get()) {
                    weakReferenceActivity.get().onLocationReceive(location, null);
                }
            } else if (location.getLocType() == BDLocation.TypeOffLineLocation) {// 离线定位结果
                sb.append("\ndescribe : ");
                sb.append("离线定位成功，离线定位结果也是有效的");

                onLocationReceive(location);

                if (null != weakReferenceActivity && null != weakReferenceActivity.get()) {
                    weakReferenceActivity.get().onLocationReceive(location, null);
                }
            } else if (location.getLocType() == BDLocation.TypeServerError) {
                sb.append("\ndescribe : ");
                sb.append("服务端网络定位失败，可以反馈IMEI号和大体定位时间到loc-bugs@baidu.com，会有人追查原因");
            } else if (location.getLocType() == BDLocation.TypeNetWorkException) {
                sb.append("\ndescribe : ");
                sb.append("网络不同导致定位失败，请检查网络是否通畅");
            } else if (location.getLocType() == BDLocation.TypeCriteriaException) {
                sb.append("\ndescribe : ");
                sb.append("无法获取有效定位依据导致定位失败，一般是由于手机的原因，处于飞行模式下一般会造成这种结果，可以试着重启手机");
            }

            LogUtil.i(App.tag, "result:" + sb.toString());
//            LogUtil.i(App.tag, sb.toString());
        }

    }

    public boolean isLoginfb() {
        return isLoginfb;
    }

    public void setLoginfb(boolean loginfb) {
        isLoginfb = loginfb;
    }


    public void onLocationReceive(BDLocation bdLocation) {
        if (null != bdLocation) {
            App.getApp().setBdLocation(bdLocation);
            AVGeoPoint geoPoint = new AVGeoPoint(bdLocation.getLatitude(), bdLocation.getLongitude());
            App.getApp().setAvGeoPoint(geoPoint);
            LogUtil.e(App.tag, "定位成功");
            App.getApp().stopLocation();
        }
    }

    public BDLocation getBdLocation() {
        return bdLocation;
    }

    public void setBdLocation(BDLocation bdLocation) {
        this.bdLocation = bdLocation;
    }

    public AVGeoPoint getAvGeoPoint() {
        return avGeoPoint;
    }

    public void setAvGeoPoint(AVGeoPoint avGeoPoint) {
        this.avGeoPoint = avGeoPoint;
    }

    public boolean isMainActivityVisible() {
        return isMainActivityVisible;
    }

    public void setMainActivityVisible(boolean mainActivityVisible) {
        isMainActivityVisible = mainActivityVisible;
    }
}

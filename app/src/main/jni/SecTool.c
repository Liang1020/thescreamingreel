/*


 * Class:     com_utils_sec_SecTool
 * Method:    get1
 * Signature: (I)Ljava/lang/String;
 */
#include <com_utils_sec_SecTool.h>
#include <string.h>

JNIEXPORT jstring JNICALL Java_com_utils_sec_SecTool_get1(JNIEnv *env,
		jclass clz, jlong h) {

	// "sWeRpvlFjH5hzkmJ62Y0K4iT-MdYXbMMI";
	char r[] = "tXfSqwmGkI6i{lnK73Z1L5jU.NeZYcNNJ";
	int i = 0;
	if (h == 2086496266) {
		for(i=0; i<strlen(r); i++)
		    {
			        r[i] -=1;

		    }
		return (*env)->NewStringUTF(env, r);
	} else {
		return (*env)->NewStringUTF(env, "yfswfJXO7cb2SgJzQ4vqSKgM0-gzGzozj");
	}
}

/*
 * Class:     com_utils_sec_SecTool
 * Method:    get2
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_utils_sec_SecTool_get2(JNIEnv * env,
		jclass clz, jlong h) {

	if (h == 2086496266) {
		return (*env)->NewStringUTF(env, "wlV99B1serRPIFyLoelHazSw");
	} else {
		return (*env)->NewStringUTF(env, "JLB5lLxNBVSmeLH2i36SRHXL");
	}

}


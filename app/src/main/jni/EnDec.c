#include <stdio.h>
#include <string.h>  // strlen()函数所在的头文件
#include <ctype.h>   // isalpha()函数所在的头文件

int main()
{
    // 定义一个明文字符串
    char msg[] = "sWeRpvlFjH5hzkmJ62Y0K4iT-MdYXbMMI";
    int i = 0 ;
    // 逐个遍历字符串中的字符，对其进行处理
    for(i=0; i<strlen(msg); i++)
    {

        char cur = msg[i];

        msg[i] +=1;
    }

    // 输出加密后的字符串
    printf("the encrypted message is: %s\n",msg);

    //解码
    for(i=0; i<strlen(msg); i++)
    {
        char c =msg[i];

        msg[i] -=1;

    }
    printf("the decrypted message is: %s\n",msg);
    return 0;
}